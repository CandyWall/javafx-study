package top.jacktgq.demo2;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.scene.control.TreeTableView;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

public class FileTreeTableView extends TreeTableView<FileItem> {
    private TreeTableColumn<FileItem, FileItem>[] columns;

    public FileTreeTableView(File dir) {
        columns = new TreeTableColumn[3];
        columns[0] = new TreeTableColumn<>("文件名");
        columns[1] = new TreeTableColumn<>("大小");
        columns[2] = new TreeTableColumn<>("任务创建时间");

        getColumns().addAll(columns);
        // 定义每个列的宽度
        columns[0].setPrefWidth(150);
        columns[1].setPrefWidth(140);
        columns[2].setPrefWidth(150);

        // 设置CellValueFactory（此段写法固定）
        for (int i = 0; i < columns.length; i++) {
            columns[i].setCellValueFactory(
                    new Callback<TreeTableColumn.CellDataFeatures<FileItem, FileItem>, ObservableValue<FileItem>>() {
                        @Override
                        public ObservableValue<FileItem> call(CellDataFeatures<FileItem, FileItem> param) {
                            return param.getValue().valueProperty();
                        }
                    });
        }

        // 设置CellFactory
        columns[0]
                .setCellFactory(new Callback<TreeTableColumn<FileItem, FileItem>, TreeTableCell<FileItem, FileItem>>() {
                    @Override
                    public TreeTableCell<FileItem, FileItem> call(TreeTableColumn<FileItem, FileItem> param) {
                        return new FileTreeTableCell("name");
                    }
                });
        columns[1]
                .setCellFactory(new Callback<TreeTableColumn<FileItem, FileItem>, TreeTableCell<FileItem, FileItem>>() {
                    @Override
                    public TreeTableCell<FileItem, FileItem> call(TreeTableColumn<FileItem, FileItem> param) {
                        return new FileTreeTableCell("size");
                    }
                });
        columns[2]
                .setCellFactory(new Callback<TreeTableColumn<FileItem, FileItem>, TreeTableCell<FileItem, FileItem>>() {
                    @Override
                    public TreeTableCell<FileItem, FileItem> call(TreeTableColumn<FileItem, FileItem> param) {
                        return new FileTreeTableCell("lastModified");
                    }
                });

        columns[0].setPrefWidth(300.0);

        setRoot(listFiles(dir));
    }

    private TreeItem<FileItem> listFiles(File file) {
        return listFiles(file, false);
    }

    private TreeItem<FileItem> listFiles(File file, boolean expanded) {
        TreeItem<FileItem> treeItem = new TreeItem<>(new FileItem(file));
        if (file.isDirectory()) {
            treeItem.setExpanded(expanded);
            for (File childFile : file.listFiles()) {
                if (childFile.isDirectory()) {
                    // 如果子file是目录
                    TreeItem<FileItem> childTreeItem = listFiles(childFile, expanded);
                    childTreeItem.setExpanded(expanded);
                    treeItem.getChildren().add(childTreeItem);
                } else {
                    treeItem.getChildren().add(new TreeItem<>(new FileItem(childFile)));
                }
            }
        }

        return treeItem;
    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        Insets insets = getInsets();
        // 动态设置列宽
        double w = this.getWidth() - insets.getLeft() - insets.getRight();
        double w0 = w * 0.4;
        double w1 = w * 0.2;
        double w2 = w - w0 - w1;
        columns[0].setPrefWidth(w0);
        columns[1].setPrefWidth(w1);
        columns[2].setPrefWidth(w2);
    }

    public void setDir(File dir) {
        // 切换目录前先清空上次的根节点下的节点
        getRoot().getChildren().clear();
        setRoot(listFiles(dir, true));
    }

    private class FileTreeTableCell extends TreeTableCell<FileItem, FileItem> {
        String columnName;

        public FileTreeTableCell(String columnName) {
            this.columnName = columnName;
        }

        @Override
        protected void updateItem(FileItem item, boolean empty) {
            super.updateItem(item, empty);

            if (empty || item == null) {
                setGraphic(null);
                setText(null);
            } else {
                if (columnName.equals("name")) {
                    this.setText(item.name);
                    if (item.isDir) {
                        setGraphic(new ImageView("icons/ic_folder.png"));
                    } else {
                        setGraphic(new ImageView("icons/ic_file.png"));
                    }
                } else if (columnName.equals("size")) {
                    if (item.isDir) {
                        this.setText("目录");
                    } else {
                        this.setText(item.size + "");
                    }
                } else if (columnName.equals("lastModified")) {
                    this.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(item.lastModified)) + "");
                }
            }
        }
    }

}
