package top.jacktgq.demo3;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class MyWindow extends Stage {
    StackPane root = new StackPane(); // 根节点
    Scene scene = new Scene(root);

    public MyWindow(Window owner) {
        // 给根节点添加一个背景色以方便辩认
        root.setStyle("-fx-background-color: #333");
        initLayout();
        initDragSupport(); // 拖拽支持

        // 指定父窗口
        initOwner(owner);

        // 指定是否标准窗口（标准窗口有标题栏和边框)
        initStyle(StageStyle.UNDECORATED); // 默认为 DECORATED

        // 指定Scene
        setScene(scene);

        // 调整窗口适应Scene大小
        sizeToScene();
    }

    // 添加一个关闭按钮
    private void initLayout() {
        Button button = new Button("关闭");
        root.getChildren().add(button);
        StackPane.setAlignment(button, Pos.CENTER);

        button.setOnAction((e) -> {
            close();
        });
    }

    // 改变窗口大小
    public void resize(double w, double h) {
        // 第一种方法：指定根容器的大小, 让窗口适应根容器
        root.setPrefWidth(w);
        root.setPrefHeight(h);
        this.sizeToScene();

        // 第二种方法：指定窗口的整体尺寸（包含标题栏）
        // 但是，当设置为 UNDECORATED 时，此方法失效
        // this.setWidth(w);
        // this.setHeight(h);
    }

    // 指定窗口位置
    public void relocate(double x, double y) {
        this.setX(x);
        this.setY(y);
    }

    // 相对于父窗口中心显示
    public void centerInParent() {
        Window owner = this.getOwner();
        if (owner != null) {
            // 父窗口的位置px, py, 大小 pw, ph
            double px = owner.getX(), py = owner.getY();
            double pw = owner.getWidth(), ph = owner.getHeight();

            double dx = (pw - getWidth()) / 2;
            double dy = (ph - getHeight()) / 2;

            this.setX(px + dx);
            this.setY(py + dy);
        }
    }

    ////////////// 拖动支持 ///////////////
    private Stage stage; // 窗口

    private boolean dragging = false;
    private double windowX = 0, windowY = 0; // 初始窗口坐标
    private double windowW = 0, windowH = 0; // 初始窗口大小
    private double startX, startY; // 鼠标按下位置的相对坐标
    private double startScreenX, startScreenY; // 鼠标相对屏幕Screen坐标

    private void initDragSupport() {
        stage = MyWindow.this;

        root.setOnMousePressed((MouseEvent e) -> {

            // 记录鼠标点下时，窗口的坐标、大小
            windowX = stage.getX();
            windowY = stage.getY();
            windowW = stage.getWidth();
            windowH = stage.getHeight();

            // 记录鼠标点下时，鼠标的位置
            startX = e.getX();
            startY = e.getY();
            startScreenX = e.getScreenX();
            startScreenY = e.getScreenY();
        });

        root.setOnDragDetected((MouseEvent e) -> {
            if (e.getButton() == MouseButton.PRIMARY) {
                dragging = true;
            }
        });

        root.setOnMouseDragged((MouseEvent e) -> {
            if (!dragging)
                return;

            // 计算鼠标的位移 , 起始点A点，当前位置 B点
            double dx = e.getScreenX() - startScreenX;
            double dy = e.getScreenY() - startScreenY;
            stage.setX(windowX + dx);
            stage.setY(windowY + dy);
        });

        root.setOnMouseReleased((MouseEvent e) -> {
            dragging = false;
        });
    }
}
