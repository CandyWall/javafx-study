package top.jacktgq.irregularShape.clipImage;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import top.jacktgq.drawImage.CandyCanvasImagePane;

public class MyCanvasPane extends CandyCanvasImagePane {
    public MyCanvasPane(Image image) {
        super(image, FIT_XY);
    }

    @Override
    public void paint(GraphicsContext gc, double w, double h) {
        gc.save(); // 使用clip之前先备份一下初始绘制参数
        gc.setFill(Color.RED);
        // 开始绘制这段路径
        gc.beginPath();

        // 起点(50, 100)
        gc.moveTo(50, 100);

        // 画直接到(100, 100)
        gc.lineTo(100, 100);

        // 画圆弧：中心点(200, 100)，半径(100)，起始角度180度，跨度270度
        gc.arc(200, 100, 100, 100, 180, 270);

        // 结束绘制这段路径
        gc.closePath();

        // 描线
        // gc.stroke();
        // 填充
        // gc.fill();
        // 裁切
        gc.clip();

        super.paint(gc, w, h);

        gc.restore(); // 恢复初始绘制参数
        // 不调用gc.save() 和 gc.restore()方法的话会绘制的导致卡顿
    }
}
