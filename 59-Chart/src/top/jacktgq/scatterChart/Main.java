package top.jacktgq.scatterChart;

import javafx.application.Application;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button btn = new Button("确定");
        FlowPane root = new FlowPane();
        root.getChildren().addAll(btn, getBarChart1());

        primaryStage.setTitle("气泡图");
        primaryStage.setScene(new Scene(root, 1800, 800));
        primaryStage.getScene().getStylesheets().add(getClass().getResource("scatterChart.css").toExternalForm());
        primaryStage.show();

        btn.setOnAction(e -> {
        });
    }

    private ScatterChart getBarChart1() {
        ScatterChart.Data<Number, Number> d1 = new ScatterChart.Data<Number, Number>(10, 10);
        // 修改气泡半径
        d1.setExtraValue(2);
        ScatterChart.Data<Number, Number> d2 = new ScatterChart.Data<Number, Number>(20, 20);
        ScatterChart.Data<Number, Number> d3 = new ScatterChart.Data<Number, Number>(30, 30);
        ScatterChart.Data<Number, Number> d4 = new ScatterChart.Data<Number, Number>(40, 40);
        XYChart.Series series1 = new XYChart.Series();
        series1.getData().addAll(d1, d2, d3, d4);
        series1.setName("2019");
        ScatterChart.Data<Number, Number> d5 = new ScatterChart.Data<Number, Number>(13, 10);
        ScatterChart.Data<Number, Number> d6 = new ScatterChart.Data<Number, Number>(26, 20);
        ScatterChart.Data<Number, Number> d7 = new ScatterChart.Data<Number, Number>(17, 30);
        ScatterChart.Data<Number, Number> d8 = new ScatterChart.Data<Number, Number>(42, 40);
        XYChart.Series series2 = new XYChart.Series();
        series2.getData().addAll(d5, d6, d7, d8);
        series2.setName("2021");
        ScatterChart.Data<Number, Number> d9 = new ScatterChart.Data<Number, Number>(16, 23);
        ScatterChart.Data<Number, Number> d10 = new ScatterChart.Data<Number, Number>(21, 18);
        ScatterChart.Data<Number, Number> d11 = new ScatterChart.Data<Number, Number>(23, 37);
        ScatterChart.Data<Number, Number> d12 = new ScatterChart.Data<Number, Number>(19, 28);
        XYChart.Series series3 = new XYChart.Series();
        series3.getData().addAll(d9, d10, d11, d12);
        series3.setName("2021");

        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("X");
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Y");
        ScatterChart scatterChart = new ScatterChart(xAxis, yAxis);
        scatterChart.getData().addAll(series1, series2, series3);

        scatterChart.setId("candy-bar-chart");

        // 是否显示提示文字和提示线，默认显示：true
        // barChart.setLabelsVisible(true);
        scatterChart.setTitle("各专业毕业设计答辩情况统计1");
        // 设置标题显示的位置
        scatterChart.setTitleSide(Side.TOP);
        // 是否显示图例
        scatterChart.setLegendVisible(true);
        // 设置图例显示的位置
        scatterChart.setLegendSide(Side.LEFT);
        // 当数据发生改变时是否显示动画
        scatterChart.setAnimated(true);
        scatterChart.setPrefSize(600, 600);
        return scatterChart;
    }

    public static void main(String[] args) {
        launch(args);
    }
}