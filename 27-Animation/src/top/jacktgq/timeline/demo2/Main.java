package top.jacktgq.timeline.demo2;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {
    private Timeline timeline;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            HBox controlbar = new HBox();
            ToggleButton tb1 = new ToggleButton("播放");
            ToggleButton tb2 = new ToggleButton("暂停");
            ToggleButton tb3 = new ToggleButton("停止");
            ToggleButton tb4 = new ToggleButton("动画跳跃");
            ToggleButton tb5 = new ToggleButton("从头开始播放");
            ToggleButton tb6 = new ToggleButton("开始反向播放动画");
            ToggleGroup group = new ToggleGroup();
            group.getToggles().addAll(tb1, tb2, tb3, tb4, tb5, tb6);
            controlbar.getChildren().addAll(tb1, tb2, tb3, tb4, tb5, tb6);

            root.setTop(controlbar);

            AnchorPane an = new AnchorPane();
            root.setCenter(an);

            Rectangle rect = new Rectangle(100, 100, Color.PINK);
            an.getChildren().addAll(rect);

            timeline = new Timeline();

            KeyValue kvX1 = new KeyValue(rect.translateXProperty(), 0);
            KeyValue kvY1 = new KeyValue(rect.translateYProperty(), 0);
            KeyValue kvR1 = new KeyValue(rect.rotateProperty(), 0);
            KeyValue kvSX1 = new KeyValue(rect.scaleXProperty(), 1);
            KeyValue kvSY1 = new KeyValue(rect.scaleYProperty(), 1);
            KeyValue kvO1 = new KeyValue(rect.opacityProperty(), 1);
            KeyFrame kf1 = new KeyFrame(Duration.millis(0), kvX1, kvY1, kvR1, kvSX1, kvSY1, kvO1);

            KeyValue kvX2 = new KeyValue(rect.translateXProperty(), 300);
            KeyValue kvY2 = new KeyValue(rect.translateYProperty(), 300);
            KeyValue kvR2 = new KeyValue(rect.rotateProperty(), 360);
            KeyValue kvSX2 = new KeyValue(rect.scaleXProperty(), 1.5);
            KeyValue kvSY2 = new KeyValue(rect.scaleYProperty(), 1.5);
            KeyValue kvO2 = new KeyValue(rect.opacityProperty(), 0.2);
            KeyFrame kf2 = new KeyFrame(Duration.millis(1000), kvX2, kvY2, kvR2, kvSX2, kvSY2, kvO2);

            timeline.getKeyFrames().addAll(kf1, kf2);
            // timeline.setDelay(Duration.seconds(1));
            // 设置循环次数，
            // timeline.setCycleCount(2);
            timeline.setCycleCount(Timeline.INDEFINITE);
            // 是否在逆运动
            timeline.setAutoReverse(true);
            // 动画播放速率为2倍速
            // timeline.setRate(5);
            // 默认为60帧
            System.out.println(timeline.getTargetFramerate());

            Scene scene = new Scene(root, 1000, 800);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试时间线动画");
            primaryStage.show();

            tb1.setOnAction(e -> {
                timeline.play();
            });

            tb2.setOnAction(e -> {
                timeline.pause();
            });

            tb3.setOnAction(e -> {
                timeline.stop();
            });

            tb4.setOnAction(e -> {
                // timeline.jumpTo("kf3");
                timeline.jumpTo(Duration.seconds(3));
            });

            tb5.setOnAction(e -> {
                timeline.playFromStart();
            });

            tb6.setOnAction(e -> {
                timeline.setRate(-timeline.getRate());
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
