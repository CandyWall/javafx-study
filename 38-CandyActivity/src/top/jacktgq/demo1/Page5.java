package top.jacktgq.demo1;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import top.jacktgq.custom_controls.activity.CandyActivity;

public class Page5 extends CandyActivity {
    @Override
    public void onCreate(Object intent) {
        VBox vbox = new VBox();
        this.setContentView(vbox);

        if (true) {
            HBox row = new HBox();
            row.setMaxHeight(40);
            row.setAlignment(Pos.CENTER_LEFT);
            row.setSpacing(20);
            Label label_question = new Label("1.您的姓名：");
            Label label_answer = new Label(context.getParamString("name", ""));
            row.getChildren().addAll(label_question, label_answer);

            vbox.getChildren().add(row);
        }

        if (true) {
            HBox row = new HBox();
            row.setMaxHeight(40);
            row.setAlignment(Pos.CENTER_LEFT);
            row.setSpacing(20);
            Label label_question = new Label("2.您的户籍所在地：");
            Label label_answer = new Label(context.getParamString("from_address", ""));
            row.getChildren().addAll(label_question, label_answer);

            vbox.getChildren().add(row);
        }

        if (true) {
            HBox row = new HBox();
            row.setMaxHeight(40);
            row.setAlignment(Pos.CENTER_LEFT);
            row.setSpacing(20);
            Label label_question = new Label("3.您的毕业院校：");
            Label label_answer = new Label(context.getParamString("graduate_school", ""));
            row.getChildren().addAll(label_question, label_answer);

            vbox.getChildren().add(row);
        }

        Button next = new Button("确认提交");
        next.setOnAction(e -> {
            startActivity(Page6.class, null);
        });
        vbox.getChildren().add(next);
    }
}
