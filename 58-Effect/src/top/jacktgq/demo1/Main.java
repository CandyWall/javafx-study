package top.jacktgq.demo1;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.Bloom;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.ColorInput;
import javafx.scene.effect.DisplacementMap;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.effect.FloatMap;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.effect.Glow;
import javafx.scene.effect.ImageInput;
import javafx.scene.effect.InnerShadow;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.effect.MotionBlur;
import javafx.scene.effect.PerspectiveTransform;
import javafx.scene.effect.Reflection;
import javafx.scene.effect.SepiaTone;
import javafx.scene.effect.Shadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();
            root.setStyle("-fx-background-color: transparent");
            root.setSpacing(30);
            root.setPadding(new Insets(20));
            Button btn = new Button("确认");
            Text text = new Text("日照香炉生紫烟");
            Rectangle rect = new Rectangle(100, 100, Color.ORANGE);
            Circle circle = new Circle(50, Color.AQUA);
            ImageView imageView = new ImageView("images/kenan.png");
            imageView.setFitWidth(100);
            imageView.setFitHeight(100);
            root.getChildren().addAll(btn, text, rect, circle, imageView);

            // root.getChildren().forEach(node -> {
            // node.setEffect(dropShadow);
            // });
            // 可以给父组件设置一个特效，并且父组件的背景颜色设为透明，子组件就可以继承到父组件的设置的特效
            root.setEffect(dropShadowEffect());
            // root.setEffect(innerShadowEffect());
            // root.setEffect(shadowEffect());
            // root.setEffect(boxBlurEffect());
            // root.setEffect(gaussianBlurEffect());
            // root.setEffect(motionBlur());
            // root.setEffect(sepiaToneEffect());
            // root.setEffect(reflectionEffect());
            // root.getChildren().forEach(node -> {
            // node.setEffect(reflectionEffect());
            // });
            // root.getChildren().forEach(node -> {
            // node.setEffect(displacementMapEffect());
            // });
            // root.getChildren().forEach(node -> {
            // node.setEffect(bloomEffect());
            // });
            // root.getChildren().forEach(node -> {
            // node.setEffect(glowEffect());
            // });
            // root.getChildren().forEach(node -> {
            // node.setEffect(colorInputEffect());
            // });
            // root.getChildren().forEach(node -> {
            // node.setEffect(colorAdjustEffect());
            // });
            // root.getChildren().forEach(node -> {
            // node.setEffect(imageInputEffect());
            // });
            // root.getChildren().forEach(node -> {
            // node.setEffect(perspectiveTransformEffect());
            // });
            // root.getChildren().forEach(node -> {
            // node.setEffect(lightingEffect());
            // });

            Scene scene = new Scene(root, 600, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试特效");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Effect shadowEffect() {
        Shadow shadow = new Shadow();
        shadow.setColor(Color.DARKORANGE);
        shadow.setWidth(100);
        shadow.setHeight(100);
        shadow.setBlurType(BlurType.GAUSSIAN);
        shadow.setRadius(2);

        return shadow;
    }

    public Effect dropShadowEffect() {
        DropShadow dropShadow = new DropShadow();
        // 阴影的颜色
        dropShadow.setColor(Color.web("#444444"));
        // dropShadow.setWidth(100);
        // dropShadow.setHeight(100);
        // 阴影的偏移
        dropShadow.setOffsetX(10);
        dropShadow.setOffsetY(10);
        // 阴影的模糊类型
        dropShadow.setBlurType(BlurType.GAUSSIAN);
        // 阴影的羽化半径
        dropShadow.setRadius(20);
        // 阴影的扩散程度
        dropShadow.setSpread(0.5);

        return dropShadow;
    }

    public Effect innerShadowEffect() {
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setWidth(30);
        innerShadow.setHeight(30);
        innerShadow.setBlurType(BlurType.GAUSSIAN);

        // innerShadow.setOffsetX(10);
        // innerShadow.setOffsetY(10);

        innerShadow.setRadius(30);
        innerShadow.setChoke(0.5);

        // 可以把不同的阴影效果进行叠加
        innerShadow.setInput(dropShadowEffect());

        return innerShadow;
    }

    public Effect boxBlurEffect() {
        BoxBlur bb = new BoxBlur();
        bb.setWidth(10);
        bb.setHeight(10);
        bb.setIterations(100);

        return bb;
    }

    public Effect gaussianBlurEffect() {
        GaussianBlur gb = new GaussianBlur();
        gb.setRadius(20);

        return gb;
    }

    public Effect motionBlur() {
        MotionBlur mb = new MotionBlur();
        mb.setAngle(90);

        return mb;
    }

    public Effect sepiaToneEffect() {
        SepiaTone sepiaTone = new SepiaTone();
        sepiaTone.setLevel(0.5);

        return sepiaTone;
    }

    public Effect reflectionEffect() {
        Reflection reflection = new Reflection();
        reflection.setTopOffset(10);
        reflection.setFraction(0.7);
        reflection.setTopOpacity(1);
        reflection.setBottomOpacity(0);

        return reflection;
    }

    public Effect displacementMapEffect() {
        int w = 100;
        int h = 100;
        FloatMap fmap = new FloatMap(w, h);
        float threshold = 0.5f;
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                float temp = j < h / 2 ? threshold : -threshold;

                fmap.setSamples(i, j, temp, 0);
            }
        }

        DisplacementMap dis = new DisplacementMap();
        // 横向只显示一半
        // dis.setOffsetX(0.5);
        // 纵向只显示一半
        // dis.setOffsetY(0.5);

        // 是否要将不可见部分将剩余空间填充
        // dis.setWrap(true);
        dis.setMapData(fmap);
        // 对位移的步长进行缩放
        dis.setScaleX(0.5);

        return dis;
    }

    public Effect bloomEffect() {
        Bloom bloom = new Bloom();
        bloom.setThreshold(0.5);

        return bloom;
    }

    public Effect glowEffect() {
        Glow glow = new Glow();
        glow.setLevel(1);

        return glow;
    }

    // 一般不单独使用，作为其他特效的input参数，组合使用
    public Effect colorInputEffect() {
        ColorInput ci = new ColorInput();
        ci.setWidth(100);
        ci.setHeight(100);
        ci.setX(0);
        ci.setY(0);
        ci.setPaint(Color.YELLOW);

        return ci;
    }

    // 一般不单独使用，作为其他特效的input参数，组合使用
    public Effect imageInputEffect() {
        ImageInput ii = new ImageInput();
        ii.setX(0);
        ii.setY(0);
        ii.setSource(new Image("images/kenan.png"));

        return ii;
    }

    public Effect colorAdjustEffect() {
        ColorAdjust ca = new ColorAdjust();
        ca.setHue(0.5);
        ca.setSaturation(-0.5);
        ca.setBrightness(-0.5);

        return ca;
    }

    public Effect perspectiveTransformEffect() {
        PerspectiveTransform pt = new PerspectiveTransform();
        pt.setUlx(0);
        pt.setUly(0);

        pt.setUrx(100);
        pt.setUry(20);

        pt.setLlx(0);
        pt.setLly(100);

        pt.setLrx(100);
        pt.setLry(80);

        return pt;
    }

    // public Effect blendEffect() {
    // Blend
    //
    // }

    public Effect lightingEffect() {
        Lighting lighting = new Lighting();
        lighting.setSurfaceScale(10);

        lighting.setDiffuseConstant(2);
        lighting.setSpecularConstant(2);
        lighting.setSpecularExponent(40);

        Light.Distant dis = new Light.Distant();
        dis.setColor(Color.ORANGE);
        // 光的方位角。方位角是光源在XY平面上的方向角，单位为度。
        dis.setAzimuth(45);
        // 灯光的高度。高程是光源在YZ平面上的方向角，单位为度。
        dis.setElevation(60);

        lighting.setLight(dis);
        lighting.setBumpInput(gaussianBlurEffect());
        lighting.setContentInput(gaussianBlurEffect());

        return lighting;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
