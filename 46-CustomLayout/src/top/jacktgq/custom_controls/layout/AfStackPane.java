package top.jacktgq.custom_controls.layout;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class AfStackPane extends Pane {

    @Override
    protected void layoutChildren() {
        super.layoutChildren();

        double w = this.getWidth();
        double h = this.getHeight();

        ObservableList<Node> children = getChildren();
        for (int i = 0; i < children.size(); i++) {
            Node child = children.get(i);
            child.resizeRelocate(0, 0, w, h);
        }
    }

}
