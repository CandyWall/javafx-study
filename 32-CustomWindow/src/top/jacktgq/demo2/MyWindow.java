package top.jacktgq.demo2;

import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class MyWindow extends Stage {
    StackPane root = new StackPane(); // 根节点
    Scene scene = new Scene(root);

    public MyWindow(Window owner) {
        // 给根节点添加一个背景色以方便辩认
        root.setStyle("-fx-background-color: #333");

        // 指定父窗口
        initOwner(owner);

        // 指定是否标准窗口（标准窗口有标题栏和边框)
        initStyle(StageStyle.UNDECORATED); // 默认为 DECORATED

        // 指定Scene
        setScene(scene);

        // 调整窗口适应Scene大小
        sizeToScene();
    }

    // 改变窗口大小
    public void resize(double w, double h) {
        // 第一种方法：指定根容器的大小, 让窗口适应根容器
        root.setPrefWidth(w);
        root.setPrefHeight(h);
        this.sizeToScene();

        // 第二种方法：指定窗口的整体尺寸（包含标题栏）
        // 但是，当设置为 UNDECORATED 时，此方法失效
        // this.setWidth(w);
        // this.setHeight(h);
    }

    // 指定窗口位置
    public void relocate(double x, double y) {
        this.setX(x);
        this.setY(y);
    }

    // 相对于父窗口中心显示
    public void centerInParent() {
        Window owner = this.getOwner();
        if (owner != null) {
            // 父窗口的位置px, py, 大小 pw, ph
            double px = owner.getX(), py = owner.getY();
            double pw = owner.getWidth(), ph = owner.getHeight();

            double dx = (pw - getWidth()) / 2;
            double dy = (ph - getHeight()) / 2;

            this.setX(px + dx);
            this.setY(py + dy);
        }
    }
}
