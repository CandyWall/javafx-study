package top.jacktgq.clock;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import top.jacktgq.custom_controls.window.CandyWindowDragSupport;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            CandyClock clock = new CandyClock();
            clock.setStyle("-fx-background: transparent");
            // clock.setPadding(new Insets(10, 10, 10, 10));
            Scene scene = new Scene(clock, 600, 600);
            scene.setFill(null);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.getIcons().add(new Image("images/clock-titleicon.png"));
            primaryStage.initStyle(StageStyle.TRANSPARENT);
            primaryStage.setAlwaysOnTop(true);
            primaryStage.show();
            CandyWindowDragSupport.addDragSupport(primaryStage, clock, true);

            // 窗口关闭的时候关掉定时器
            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    clock.stopRun();
                    Platform.exit();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
