package top.jacktgq;

import java.io.File;
import java.util.zip.ZipEntry;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.progress.CandyProgressBar;
import top.jacktgq.utils.CandyUnzipUtil;
import top.jacktgq.utils.CandyUnzipUtil.ProgressChangeListener;

public class Main extends Application {
    private CandyProgressBar candyProgressBar;
    private File file;

    @Override
    public void start(Stage primaryStage) {
        try {
            VBox root = new VBox();
            root.setSpacing(10);

            HBox row1 = new HBox();
            row1.setAlignment(Pos.CENTER);
            row1.setPadding(new Insets(10));
            row1.setSpacing(10);

            TextField zipFilePathField = new TextField();
            zipFilePathField.setEditable(false);
            zipFilePathField.setPromptText("点打开按钮打开一个zip文件");
            HBox.setHgrow(zipFilePathField, Priority.ALWAYS);

            Button openZipFileBtn = new Button("打开");
            openZipFileBtn.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    FileChooser chooser = new FileChooser();
                    chooser.getExtensionFilters().add(new ExtensionFilter("ZIP压缩文件", "*.zip"));
                    chooser.setInitialDirectory(new File("testData/"));
                    file = chooser.showOpenDialog(primaryStage);
                    if (file != null) {
                        zipFilePathField.setText(file.getAbsolutePath());
                    }
                }

            });

            row1.getChildren().addAll(zipFilePathField, openZipFileBtn);

            HBox row2 = new HBox();
            row2.setAlignment(Pos.CENTER);
            row2.setPadding(new Insets(10));
            row2.setSpacing(10);
            candyProgressBar = new CandyProgressBar();
            candyProgressBar.setMaxHeight(40);
            HBox.setHgrow(candyProgressBar, Priority.ALWAYS);

            Button unzipBtn = new Button("解压");

            unzipBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if (file == null) {
                        return;
                    }

                    new Thread(() -> {
                        CandyUnzipUtil.extract(file, file.getParentFile(), new ProgressChangeListener() {
                            private long lastTime = 0;
                            private String content;

                            @Override
                            public void handle(ZipEntry entry, int done, int error, int total) {
                                // 为了增强演示效果， 故意让解压缩慢一点
                                try {
                                    Thread.sleep(40);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                double percent = done * 1.0 / total;
                                content = done + " / " + total;
                                if (done + error == total) {
                                    content = "解压成功：" + done + " 个" + "，解压错误：" + error + " 个" + "，总共：" + total + " 个";
                                    Platform.runLater(() -> {
                                        candyProgressBar.updateProgress(percent, content);
                                    });
                                    return;
                                }
                                // 不需要太频繁更新进度条，这里选择每隔0.2秒更新一次
                                long now = System.currentTimeMillis();
                                long pass = now - lastTime;
                                if (pass > 100) {
                                    lastTime = now;

                                    Platform.runLater(() -> {
                                        candyProgressBar.updateProgress(percent, content);
                                    });
                                }

                            }
                        });

                    }).start();
                }
            });

            row2.getChildren().addAll(candyProgressBar, unzipBtn);

            root.setAlignment(Pos.CENTER);
            root.getChildren().addAll(row1, row2);
            // root.setStyle("-fx-background-color: yellow");

            Scene scene = new Scene(root, 600, 400);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("Zip文件解压");
            primaryStage.show();
        } catch (

        Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
