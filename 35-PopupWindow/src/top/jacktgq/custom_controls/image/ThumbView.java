package top.jacktgq.custom_controls.image;

import java.io.File;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import top.jacktgq.custom_controls.image.CandyCanvasImagePane;

/**
 * 
 * @Title: RoundIcon.java
 * @Package top.jacktgq
 * @Description: 圆形图片
 * @author CandyWall
 * @date 2021年5月16日 下午2:50:16
 * @version V1.0
 */
public class ThumbView extends CandyCanvasImagePane {
    public ThumbView(File file) {
        this(file, FIT_XY);
    }

    public ThumbView(File file, int scaleType) {
        super(file, scaleType);
    }

    public ThumbView(Image image) {
        this(image, FIT_XY);
    }

    public ThumbView(Image image, int scaleType) {
        super(image, scaleType);
    }

    @Override
    public void paint(GraphicsContext gc, double x, double y, double w, double h) {
        if (image == null)
            return;
        // 确定半径
        double radius = w / 2;
        if (radius > h / 2)
            radius = h / 2;
        radius -= 4;
        gc.save(); // 使用clip之前先备份一下初始绘制参数
        // 开始绘制
        gc.beginPath();
        // 绘制一个圆
        gc.arc(w / 2, h / 2, radius, radius, 0, 360);
        gc.clip();

        gc.setFill(Color.web("#ccca"));
        gc.fill();

        super.paint(gc, x + 30, y + 30, w - 60, h - 60);
        gc.restore(); // 恢复初始绘制参数
    }
}
