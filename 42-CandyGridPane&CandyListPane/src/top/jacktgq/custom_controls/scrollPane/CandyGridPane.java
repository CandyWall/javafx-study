package top.jacktgq.custom_controls.scrollPane;

import javafx.scene.Node;
import javafx.scene.control.ScrollPane;

public class CandyGridPane extends ScrollPane {
    private CandyGridLayout container = new CandyGridLayout();

    /**
     * 
     * @param cellMaxW
     *            单元格最大宽度
     * @param cellMinW
     *            单元格最小宽度
     * @param cellAspectH
     *            单元格横纵比-宽度
     * @param cellAspectV
     *            单元格横纵比-高度
     */
    public CandyGridPane(double cellMaxW, double cellMinW, double cellAspectH, double cellAspectV) {
        container.cellMaxW = cellMaxW;
        container.cellMinW = cellMinW;
        container.cellAspectH = cellAspectH;
        container.cellAspectV = cellAspectV;
        setHbarPolicy(ScrollBarPolicy.NEVER);
        setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

        this.setContent(container);
    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        container.setPrefWidth(this.getViewportBounds().getWidth() - 1);
    }

    public void add(Node node) {
        container.getChildren().add(node);
    }
}
