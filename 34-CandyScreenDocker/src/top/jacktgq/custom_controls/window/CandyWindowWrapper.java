package top.jacktgq.custom_controls.window;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class CandyWindowWrapper {
    protected Stage stage;
    protected Region root;

    protected boolean resizable;

    public CandyWindowWrapper(Stage stage, boolean resizable) {
        this.stage = stage;
        this.resizable = resizable;

        stage.initStyle(StageStyle.UNDECORATED);
    }

    public CandyWindowWrapper(Stage stage) {
        this(stage, true);
    }

    public void setContentView(Region contentView) {
        setContentView(contentView, -1, -1);
    }

    /**
     * 传入scene的宽高后，可以调用centerInParent定位到父窗口的中间
     * 
     * @param root
     * @param sceneWidth
     * @param sceneHeight
     */
    public void setContentView(Region contentView, double width, double height) {
        root = contentView;
        Scene scene = new Scene(root);
        stage.setScene(scene);
        if (width > 0 && height > 0) {
            resize(width, height);
        }
        stage.sizeToScene();
    }

    public Scene getScene() {
        return stage.getScene();
    }

    public void show() {
        stage.show();
    }

    public void hide() {
        stage.hide();
    }

    public void close() {
        stage.close();
    }

    // anchor : 可拖动区域，一般是标题栏位置
    // resizable: 是否要以拖动改变大小
    public void initDragSupport(Node anchor, boolean resizable) {
        if (anchor == null)
            anchor = root;
        CandyWindowDragSupport.addDragSupport(stage, root, anchor, resizable);
    }

    public void setResizable(boolean resizable) {
        this.resizable = resizable;
    }

    // 改变窗口大小
    public void resize(double w, double h) {
        // 第一种方法：指定根容器的大小, 让窗口适应根容器
        root.setPrefSize(w, h);
        stage.sizeToScene();
    }

    // 指定窗口位置
    public void relocate(double x, double y) {
        stage.setX(x);
        stage.setY(y);
    }

    // 相对于父窗口中心显示
    public void centerInParent() {
        Window owner = stage.getOwner();
        if (owner != null) {
            // 父窗口的位置px, py, 大小 pw, ph
            double px = owner.getX(), py = owner.getY();
            double pw = owner.getWidth(), ph = owner.getHeight();

            double dx = (pw - stage.getWidth()) / 2;
            double dy = (ph - stage.getHeight()) / 2;

            stage.setX(px + dx);
            stage.setY(py + dy);
        }
    }
}
