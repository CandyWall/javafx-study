package top.jacktgq.demo1;

import java.io.File;

/**
 * 
 * @Title: UploadItem.java
 * @Package application
 * @Description: 待上传的每项的描述信息
 * @author CandyWall
 * @date 2021年5月14日 下午5:43:23
 * @version V1.0
 */
public class UploadItem {
    public long localId; // 本地上传分配的ID
    public String localGUID; // 本地上传分配的GUID
    public String thumb;
    public File filePath; // 本地文件路径
    public String title; // 标题，默认为文件名，但允许被用户改名
    public long size; // 文件大小
    public long create_time; // 本次上传时间

    public UploadItem() {
    }

    public UploadItem(String title, long size, long create_time) {
        this.title = title;
        this.size = size;
        this.create_time = create_time;
    }
}
