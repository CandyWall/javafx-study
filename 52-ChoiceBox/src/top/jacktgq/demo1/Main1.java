package top.jacktgq.demo1;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class Main1 extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();
            root.setPadding(new Insets(10));
            root.setSpacing(10);

            Student stu1 = new Student("20210501", "空条承太郎", 17);
            Student stu2 = new Student("20210502", "花京院典明", 18);
            Student stu3 = new Student("20210503", "乔瑟夫乔斯达", 76);
            Student stu4 = new Student("20210504", "阿布德尔", 46);

            ObservableList<Student> stuList = FXCollections.observableArrayList();
            stuList.addAll(stu1, stu2, stu3, stu4);

            ChoiceBox<Student> choiceBox = new ChoiceBox<>();
            choiceBox.setItems(stuList);
            // 显示下拉列表的时候只显示学生姓名和年龄
            choiceBox.setConverter(new StringConverter<Student>() {
                @Override
                public String toString(Student stu) {
                    return stu.getName() + ", " + stu.getAge() + "岁";
                }

                @Override
                // 这个方法这里没用上
                public Student fromString(String string) {
                    return null;
                }
            });

            TextField textField = new TextField();
            Button btn = new Button("修改选中项年龄");
            btn.setOnAction(e -> {
                int newAge = Integer.parseInt(textField.getText().trim());
                choiceBox.getSelectionModel().getSelectedItem().setAge(newAge);
            });
            root.getChildren().addAll(choiceBox, textField, btn);

            Scene scene = new Scene(root, 600, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}

class Student {
    private String id;
    private String name;
    private Integer age;

    public Student(String id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student [id=" + id + ", name=" + name + ", age=" + age + "]";
    }
}
