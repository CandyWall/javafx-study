package top.jacktgq.text;

import javafx.application.Application;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            // 创建一个画布，并指定画布的尺寸
            Canvas canvas = new Canvas(1000, 800);
            double w = canvas.getWidth();
            double h = canvas.getHeight();
            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.setFill(Color.BLUE);
            gc.setFont(Font.font("微软雅黑", 30));
            // 设置文字垂直对齐方式
            gc.setTextBaseline(VPos.TOP);
            // 设置文字水平对齐方式
            gc.setTextAlign(TextAlignment.CENTER);

            gc.fillText("人类的伟大就是勇气的伟大，人类的赞歌就是勇气的赞歌！", 0, 0);
            // gc.fillRect(0, 0, w, h);
            LinearGradient lg = new LinearGradient(0, 0, 1, 1, true, CycleMethod.NO_CYCLE,
                    new Stop[] { new Stop(0, Color.BLACK), new Stop(1, Color.RED) });
            gc.setFill(lg);
            gc.setTextAlign(TextAlignment.LEFT);
            gc.fillText("欧拉欧拉欧拉", 0, 50);
            // Effect 特效
            DropShadow ds = new DropShadow();
            ds.setOffsetY(3.0);
            ds.setColor(Color.color(0.4, 0.4, 0.4));
            gc.setEffect(ds);
            gc.fillText("木大木大木大", 0, 100);

            root.setCenter(canvas);
            Scene scene = new Scene(root, 1000, 800);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("绘制文字");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
