package top.jacktgq;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            // 创建ListView指定数据项类型
            RoleListView listView = new RoleListView();
            root.setCenter(listView);
            // root.setCenter(new RoundIcon(new Image("images/波鲁那雷夫.jpg")));

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("/top/jacktgq/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("JOJO的奇妙冒险3 星辰远征军");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
