package top.jacktgq.contextMenu;

import com.sun.javafx.tk.Toolkit;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import top.jacktgq.custom_controls.image.CandyCanvasImageScaler;
import top.jacktgq.custom_controls.image.CandyCanvasPane;

public class CandyMenuItemView extends CandyCanvasPane {
    // 默认check图标, 外部可设
    static Image defaultIconCheck = new Image(CandyMenuItemView.class.getResource("ic_check.png").toExternalForm());

    String text; // 菜单项文字
    Image icon; // 菜单项图标
    boolean checkable = false; // 该项是否可选
    boolean checked; // 选中/未选中
    Image iconCheck = defaultIconCheck; // check图标

    /////////////////////
    // 绘制参数
    private double paddingLeft = 2; // 左右的padding
    private double paddingRight = 20;
    private double iconW = 24; // 图标的占位
    private double itemHeightPadding = 4;// 每行上下的padding

    private double textWidth;

    public CandyMenuItemView() {
        canvas.getStyleClass().add("candy-menu-item-view");
    }

    @Override
    protected double computePrefWidth(double height) {
        textWidth = 4;
        // String text = getText();// CustomeMenuItem的自带方法
        if (text != null) {
            GraphicsContext gc = this.canvas.getGraphicsContext2D();
            textWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(text, gc.getFont());
        }

        return paddingLeft + iconW + textWidth + paddingRight;
    }

    @Override
    protected double computePrefHeight(double width) {
        GraphicsContext gc = this.canvas.getGraphicsContext2D();
        double h = gc.getFont().getSize();
        h += itemHeightPadding * 2; // 竖直方向的padding

        if (h < 20)
            h = 20;
        return h;
    }

    @Override
    public void paint(GraphicsContext gc, double x, double y, double w, double h) {
        x += paddingLeft;
        // 先绘制文字
        // String text = getText();// CustomeMenuItem的自带方法
        if (text != null) {
            gc.setTextBaseline(VPos.CENTER);
            gc.fillText(text, x, h / 2);
        }
        x += textWidth + 4;
        // 再绘制图标
        // icon
        Image image = icon;
        ;
        if (checkable && checked) {
            image = iconCheck;
        }
        if (image != null) {
            Rectangle rect = CandyCanvasImageScaler.fitCenterInside(image.getWidth(), image.getHeight(),
                    image.getWidth(), h);
            gc.drawImage(image, x + rect.getX(), y + rect.getY(), rect.getWidth(), rect.getHeight());
        }

    }
}
