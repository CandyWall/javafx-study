package top.jacktgq.demo1;

public class Student {
    public int id;
    public String name;
    public boolean sex;
    public String phone;

    public Student() {

    }

    public Student(int id, String name, boolean sex, String phone) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Student [id=" + id + ", name=" + name + ", sex=" + sex + ", phone=" + phone + "]";
    }
}
