package top.jacktgq.demo1;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.window.CandyTransparentImageFloatWindowWrapper;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            CandyTransparentImageFloatWindowWrapper floatWindow = new CandyTransparentImageFloatWindowWrapper(
                    primaryStage, new Image("images/kenan-2.png"), 200, 200);
            floatWindow.setIcon(new Image("images/kenan-icon.png"));
            floatWindow.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
