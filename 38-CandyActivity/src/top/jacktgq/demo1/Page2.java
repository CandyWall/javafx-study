package top.jacktgq.demo1;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import top.jacktgq.custom_controls.activity.CandyActivity;

public class Page2 extends CandyActivity {
    @Override
    public void onCreate(Object intent) {
        StackPane stackPane = new StackPane();
        this.setContentView(stackPane);

        HBox hbox = new HBox();
        hbox.setMaxHeight(40);
        hbox.setAlignment(Pos.CENTER);
        hbox.setSpacing(20);
        Label label = new Label("1.您的姓名：");
        TextField textField = new TextField();
        hbox.getChildren().addAll(label, textField);

        Button next = new Button("下一题");
        next.setOnAction(e -> {
            context.putParam("name", textField.getText().trim());
            startActivity(Page3.class, null);
        });
        stackPane.getChildren().addAll(hbox, next);
        StackPane.setAlignment(hbox, Pos.CENTER);
        StackPane.setAlignment(next, Pos.BOTTOM_CENTER);
    }
}
