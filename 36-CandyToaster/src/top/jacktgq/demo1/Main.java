package top.jacktgq.demo1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {
    MyToasterWindow toaster = new MyToasterWindow();

    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();
            Button button = new Button("打开提示框");

            button.setOnAction(e -> {
                toaster.setText("日照香炉生紫烟，遥看瀑布挂前川，飞流直下三千尺，疑是银河落九天。");
                toaster.show(primaryStage);
            });

            root.getChildren().add(button);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
