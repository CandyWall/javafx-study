package top.jacktqg;


/*
 * 描述每一个拓扑节点的数据
 */

public class TopoData {
    public String name;
    public String ip;
    public int status; // 0, 正常, -1, 出错

    public TopoData() {

    }

    public TopoData(String name, String ip, int status) {
        this.name = name;
        this.ip = ip;
        this.status = status;
    }

}
