package top.jacktgq.login;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import top.jacktgq.custom_controls.image.CandyRoundIcon;

public class LoginPane extends StackPane {
    Image image = new Image(getClass().getResource("ic_user.png").toExternalForm());
    ThumbView thumb = new ThumbView(image, CandyRoundIcon.FIT_CENTER_INSIDE);

    Label label = new Label("请登录");

    TextField username = new TextField();

    PasswordField password = new PasswordField();
    Image arrow = new Image(getClass().getResource("ic_right.png").toExternalForm());
    Button loginButton = new Button("", new ImageView(arrow));

    public LoginPane() {
        getStylesheets().add(getClass().getResource("login.css").toExternalForm());
        getStyleClass().add("login-pane");
        label.getStyleClass().add("prompt");

        thumb.setPrefHeight(150);

        initLayout();

        username.setPromptText("用户名");
        password.setPromptText("密码");

    }

    // 界面布局
    private void initLayout() {
        HBox line1 = new HBox();
        line1.getChildren().add(username);
        line1.getStyleClass().add("input-box");

        HBox line2 = new HBox();
        line2.getChildren().addAll(password, loginButton);
        line2.getStyleClass().add("input-box");
        HBox.setHgrow(password, Priority.ALWAYS);

        VBox vbox = new VBox();
        vbox.getChildren().addAll(thumb, label, line1, line2);
        vbox.setMaxWidth(240);
        vbox.setMaxHeight(400);

        vbox.getStyleClass().add("vbox");

        this.getChildren().add(vbox);
        StackPane.setAlignment(vbox, Pos.CENTER);
    }

}
