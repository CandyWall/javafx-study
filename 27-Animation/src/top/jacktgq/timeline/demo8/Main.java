package top.jacktgq.timeline.demo8;

import javafx.animation.Animation;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            HBox controlbar = new HBox();
            Button btn = new Button("播放");
            controlbar.getChildren().addAll(btn);

            root.setTop(controlbar);

            AnchorPane an = new AnchorPane();
            root.setCenter(an);

            Rectangle rect = new Rectangle(100, 100, Color.PINK);
            // btn.translateXProperty().bind(rect.translateXProperty());
            //
            // TranslateTransition tt = new TranslateTransition();
            // tt.setDuration(Duration.millis(1500));
            // tt.setNode(rect);
            // tt.setFromX(0);
            // tt.setToX(500);
            // tt.setFromY(0);
            // tt.setToY(300);
            // tt.setAutoReverse(true);
            // tt.setCycleCount(Animation.INDEFINITE);

            // 如果要指定沿着左上角进行旋转，需要先进行位移
            // rect.getTransforms().add(new Translate(50, 50));
            // RotateTransition rt = new RotateTransition();
            // rt.setDuration(Duration.seconds(3));
            // rt.setNode(rect);
            // rt.setFromAngle(0);
            // rt.setToAngle(720);
            // rt.setInterpolator(Interpolator.LINEAR);

            // 如果要指定左上角为缩放点，需要先进行位移
            // rect.getTransforms().add(new Translate(50, 50));
            // ScaleTransition st = new ScaleTransition();
            // st.setDuration(Duration.seconds(3));
            // st.setFromX(1);
            // st.setToX(2);
            // st.setFromY(1);
            // st.setToY(0.5);
            // st.setNode(rect);

            // FadeTransition ft = new FadeTransition();
            // ft.setDuration(Duration.seconds(1));
            // ft.setFromValue(1);
            // ft.setToValue(0);
            // ft.setAutoReverse(true);
            // ft.setCycleCount(Animation.INDEFINITE);
            // ft.setNode(rect);

            // FillTransition fillt = new FillTransition();
            // fillt.setShape(rect);
            // fillt.setDuration(Duration.seconds(1));
            // fillt.setFromValue(Color.GREEN);
            // fillt.setToValue(Color.BLUE);
            // fillt.setAutoReverse(true);
            // fillt.setCycleCount(Animation.INDEFINITE);

            // rect.setStrokeWidth(5);
            // StrokeTransition stroket = new StrokeTransition();
            // stroket.setShape(rect);
            // stroket.setDuration(Duration.seconds(1));
            // stroket.setFromValue(Color.YELLOW);
            // stroket.setToValue(Color.GREEN);
            // stroket.setAutoReverse(true);
            // stroket.setCycleCount(Animation.INDEFINITE);

            // rect.setX(-50);
            // rect.setY(-50);
            // StackPane stackPane = new StackPane();
            //
            // Path path = new Path();
            // stackPane.getChildren().add(path);
            //
            // MoveTo mt = new MoveTo(100, 100);
            //
            // LineTo lt = new LineTo(200, 100);
            //
            // QuadCurveTo qct = new QuadCurveTo(50, 0, 100, 100);
            // qct.setAbsolute(false);
            //
            // HLineTo hlt = new HLineTo(100);
            // hlt.setAbsolute(false);
            //
            // CubicCurveTo cct = new CubicCurveTo(50, -150, 150, 150, 200, 0);
            // cct.setAbsolute(false);
            //
            // VLineTo vlt = new VLineTo(100);
            // vlt.setAbsolute(false);
            //
            // ClosePath cp = new ClosePath();
            //
            // ArcTo at = new ArcTo(100, 100, 0, 100, 100, true, true);
            // at.setAbsolute(false);
            //
            // path.getElements().addAll(mt, lt, qct, hlt, cct, vlt, at, cp);
            // path.setFill(Color.web("#FFCC00"));
            // path.setStrokeWidth(10);
            // path.setStroke(Color.RED);
            // path.getStrokeDashArray().addAll(5.0, 5.0);

            // 路径动画
            // PathTransition pt = new PathTransition();
            // pt.setNode(rect);
            // pt.setPath(path);
            // pt.setDuration(Duration.seconds(3));
            // pt.setAutoReverse(true);
            // pt.setCycleCount(Animation.INDEFINITE);

            // an.getChildren().addAll(rect, stackPane);
            // AnchorPane.setTopAnchor(stackPane, 100.0);
            // AnchorPane.setLeftAnchor(stackPane, 100.0);

            // 并行动画和串行动画
            TranslateTransition tt = new TranslateTransition();
            tt.setDuration(Duration.seconds(1));
            tt.setFromX(0);
            tt.setToX(600);
            // tt.setAutoReverse(true);
            // tt.setCycleCount(Animation.INDEFINITE);
            RotateTransition rt = new RotateTransition();
            rt.setDuration(Duration.seconds(1));
            rt.setFromAngle(0);
            rt.setToAngle(360);
            // tt.setAutoReverse(true);
            // rt.setCycleCount(Animation.INDEFINITE);
            ParallelTransition parallelT = new ParallelTransition();
            // parallelT.setNode(rect);
            // parallelT.getChildren().addAll(tt, rt);
            // parallelT.setAutoReverse(true);
            // parallelT.setCycleCount(Animation.INDEFINITE);

            SequentialTransition seqT = new SequentialTransition();
            seqT.setNode(rect);
            seqT.setAutoReverse(true);
            seqT.setCycleCount(Animation.INDEFINITE);
            seqT.getChildren().addAll(tt, rt);

            an.getChildren().addAll(rect);
            AnchorPane.setTopAnchor(rect, 100.0);
            AnchorPane.setLeftAnchor(rect, 100.0);

            Scene scene = new Scene(root, 1000, 800);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试时间线动画");
            primaryStage.show();

            btn.setOnAction(e -> {
                // tt.play();
                // rt.play();
                // st.play();
                // ft.play();
                // fillt.play();
                // stroket.play();
                // parallelT.play();
                seqT.play();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
