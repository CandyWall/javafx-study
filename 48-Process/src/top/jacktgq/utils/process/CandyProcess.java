package top.jacktgq.utils.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class CandyProcess {
    public interface ConsoleHasOutputListener {
        void handle(String output);
    }

    // 全路径, 参数以空格分开, 使用反斜线
    // 如 "c:\\windows\\notepad.exe e:\\test\\abc.txt"
    public static Map<String, Object> execute(String cmdline, File workDirectory, String charset,
            ConsoleHasOutputListener listener) throws Exception {
        String[] args = cmdline.split(" ");
        ProcessBuilder pb = new ProcessBuilder(args);

        // 设置 Work Directory
        if (workDirectory == null) {
            File exe = new File(args[0]);
            if (exe.isFile())
                workDirectory = exe.getParentFile();
        }
        if (workDirectory != null)
            pb.directory(workDirectory);

        Process process = pb.start();
        StringBuffer sb = new StringBuffer();
        // 解析输出的XML
        try (BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream(), charset))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
                if (listener != null) {
                    listener.handle(line);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 取得返回码
        int returnCode = process.waitFor();
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("code", returnCode);
        resultMap.put("consoleInfo", sb.toString());
        return resultMap;
    }

    public static Map<String, Object> execute(String cmdline, ConsoleHasOutputListener listener) throws Exception {
        return execute(cmdline, null, "UTF-8", listener);
    }

    public static Map<String, Object> execute(String cmdline, String charset) throws Exception {
        return execute(cmdline, null, charset, null);
    }

    public static Map<String, Object> execute(String cmdline) throws Exception {
        return execute(cmdline, null, "UTF-8", null);
    }

    public static void runDetached(String shellCommand) throws Exception {
        try {
            Process p = Runtime.getRuntime().exec(shellCommand);// 调用控制台执行shell

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
