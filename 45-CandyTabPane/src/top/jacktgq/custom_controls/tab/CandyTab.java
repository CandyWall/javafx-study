package top.jacktgq.custom_controls.tab;

import javafx.scene.Parent;

public abstract class CandyTab {
    protected Parent contentView;
    public String name;
    public String label;
    protected boolean created = false;
    protected CandyTabContext context;

    public CandyTab() {

    }

    public CandyTab(String name) {
        this.name = name;
    }

    ////////////////////////////////

    public Parent getContentView() {
        return contentView;
    }

    public void setContentView(Parent contentView) {
        this.contentView = contentView;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    //////////////////
    public void setCurrentTab(String tabName) {
        context.setCurrentTab(this, tabName);
    }

    //////////////////
    // 生命期回调
    public abstract void onTabCreate();

    public void onTabDestroy() {

    }

    public void onTabStart() {

    }

    public void onTabStop() {

    }

}
