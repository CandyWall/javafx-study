package top.jacktgq.demo3;

import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import top.jacktgq.custom_controls.layout.CandyFreeLayoutPane;
import top.jacktgq.custom_controls.layout.CandyLinearLayoutPane;

public class MyLayoutPane extends CandyFreeLayoutPane {
    private Button b1 = new Button("按钮A");
    private Button b2 = new Button("按钮B");
    private Button b3 = new Button("按钮C");
    private Button b4 = new Button("按钮D");

    public MyLayoutPane() {
        CandyLinearLayoutPane hbox = new CandyLinearLayoutPane();
        hbox.orientation(Orientation.HORIZONTAL).padding(4).spacing(4);

        hbox.add(b1, "60px"); // 固定值
        hbox.add(b2, "25%"); // 百分比
        hbox.add(b3, "1"); // 权重值
        hbox.add(b4, "1"); // 权重值

        this.addChild(hbox, "100%", "40px", TOP_CENTER);
    }
}
