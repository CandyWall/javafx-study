package top.jacktgq.login;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.window.CandyPrettyWindowWrapper;

public class Main2 extends Application {

    CandyPrettyWindowWrapper cpww;

    @Override
    public void start(Stage primaryStage) {

        try {
            LoginPane root = new LoginPane();

            cpww = new CandyPrettyWindowWrapper(primaryStage, false, false);
            cpww.setContentView(root, 800, 600);
            cpww.setBackground(new Image("top/jacktgq/login/bg.jpg"));
            cpww.show();

            initLayout();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 登录界面布局
    private void initLayout() {

    }

    public static void main(String[] args) {
        launch(args);
    }
}
