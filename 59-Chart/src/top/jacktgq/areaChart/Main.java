package top.jacktgq.areaChart;

import javafx.application.Application;
import javafx.geometry.NodeOrientation;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button btn = new Button("确定");
        FlowPane root = new FlowPane();
        root.getChildren().addAll(btn, getLineChart1());

        primaryStage.setTitle("面积图");
        primaryStage.setScene(new Scene(root, 1800, 800));
        primaryStage.getScene().getStylesheets().add(getClass().getResource("stackedAreaChart.css").toExternalForm());
        primaryStage.show();

        btn.setOnAction(e -> {
        });
    }

    private AreaChart getLineChart1() {
        AreaChart.Data<String, Number> d1 = new AreaChart.Data<String, Number>("2016", 50);
        AreaChart.Data<String, Number> d2 = new AreaChart.Data<String, Number>("2017", 40);
        AreaChart.Data<String, Number> d3 = new AreaChart.Data<String, Number>("2018", 45);
        AreaChart.Data<String, Number> d4 = new AreaChart.Data<String, Number>("2019", 42);
        // ObservableList<BarChart.Data<String, Number>> data1 = FXCollections.observableArrayList(d1, d2, d3, d4);
        XYChart.Series<String, Number> series1 = new XYChart.Series<>();
        series1.setName("计划招生人数");
        series1.getData().addAll(d1, d2, d3, d4);
        AreaChart.Data<String, Number> d5 = new AreaChart.Data<String, Number>("2016", 37);
        AreaChart.Data<String, Number> d6 = new AreaChart.Data<String, Number>("2017", 77);
        AreaChart.Data<String, Number> d7 = new AreaChart.Data<String, Number>("2018", 42);
        AreaChart.Data<String, Number> d8 = new AreaChart.Data<String, Number>("2019", 36);
        // ObservableList<BarChart.Data<String, Number>> data2 = FXCollections.observableArrayList(d5, d6, d7, d8);
        XYChart.Series<String, Number> series2 = new XYChart.Series<>();
        series2.setName("实际报道人数");
        series2.getData().addAll(d5, d6, d7, d8);
        // ObservableList<XYChart.Series<String, Number>> dataList = FXCollections.observableArrayList(series1, series2);

        CategoryAxis xAxis = new CategoryAxis();
        // x轴的相关参数
        xAxis.setLabel("年份");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("人数");
        AreaChart areaChart = new AreaChart(xAxis, yAxis);
        areaChart.getData().addAll(series1, series2);
        areaChart.setId("candy-area-chart");

        // 从右往左开始绘制 RIGHT_TO_LEFT
        areaChart.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
        areaChart.setTitle("2016-2020空间专业招生情况1");
        // 是否显示拐点
        areaChart.setCreateSymbols(true);
        return areaChart;
    }

    public static void main(String[] args) {
        launch(args);
    }
}