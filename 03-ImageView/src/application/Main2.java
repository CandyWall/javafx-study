package application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * @Title: Main2.java
 * @Package application
 * @Description: 显示一张本地图片
 * @author CandyWall
 * @date 2021年5月12日 下午5:52:24
 * @version V1.0
 */
public class Main2 extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            // 写法1：直接使用资源文件路径
            // ImageView imageView = new ImageView("/images/1.jpg");
            // 写法2：使用image类，使用资源文件路径，/images/1.jpg和images/1.jpg都能被正确解析
            // Image image = new Image("/images/1.jpg");
            Image image = new Image("images/1.jpg");
            ImageView imageView = new ImageView();
            // 写法3：使用Image类，使用绝对地址，需要加“file:”前缀
            // Image image = new
            // Image("file:D:\\我的编程文件-2018-7-3\\OneDrive\\Java编程文件\\Eclipse\\框架学习\\03-ImageView\\src\\images\\1.jpg");
            // ImageView imageView = new ImageView(image);
            // 写法4：使用File类，使用绝对地址
            // File file = new
            // File("D:\\我的编程文件-2018-7-3\\OneDrive\\Java编程文件\\Eclipse\\框架学习\\03-ImageView\\src\\images\\1.jpg");
            // String url = file.toURI().toString();
            // System.out.println(url);
            // Image image = new Image(url);
            // ImageView imageView = new ImageView(image);

            imageView.setImage(image);
            imageView.setPreserveRatio(true);
            root.setCenter(imageView);
            Scene scene = new Scene(root, 1000, 800);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            scene.getAccelerators().put(KeyCombination.valueOf("Ctrl+m"), new Runnable() {
                @Override
                public void run() {
                    // 按下ctrl+m 全屏显示
                    primaryStage.setFullScreen(true);
                }
            });
            primaryStage.setScene(scene);
            primaryStage.setTitle("显示本地图片");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
