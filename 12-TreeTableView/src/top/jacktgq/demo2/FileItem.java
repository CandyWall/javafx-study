package top.jacktgq.demo2;

import java.io.File;

/**
 * 
 * @Title: UploadItem.java
 * @Package application
 * @Description: 待上传的每项的描述信息
 * @author CandyWall
 * @date 2021年5月14日 下午5:43:23
 * @version V1.0
 */
public class FileItem {
    public File file;
    public String name; // 文件名
    public long lastModified; // 最后修改时间
    public long size;
    public boolean isDir = false;

    public FileItem(File file) {
        this.file = file;
        name = file.getName();
        size = file.length();
        lastModified = file.lastModified();
        isDir = file.isDirectory();
    }
}
