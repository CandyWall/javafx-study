package top.jacktgq.demo1;

import java.io.File;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class FileListView extends ListView<File> {
    private ObservableList<File> listData = FXCollections.observableArrayList();

    public FileListView() {
        setItems(listData);

        setCellFactory(new Callback<ListView<File>, ListCell<File>>() {
            @Override
            public ListCell<File> call(ListView<File> param) {
                return new MyListCell();
            }
        });
    }

    public void add(File file) {
        listData.add(file);
    }

    public ObservableList<File> getListData() {
        return listData;
    }

    private class MyListCell extends ListCell<File> {

        @Override
        protected void updateItem(File item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {
                this.setText(item.getName());
            } else {
                this.setText("");
            }
        }
    }
}
