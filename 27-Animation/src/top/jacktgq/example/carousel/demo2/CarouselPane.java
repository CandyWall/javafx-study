package top.jacktgq.example.carousel.demo2;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class CarouselPane extends StackPane {
    public CarouselPane() {
        setStyle("-fx-background-color: pink");
        setPrefWidth(600);
        setPrefHeight(300);
        // setBorder(new Border(new BorderStroke(Color.RED,
        // BorderStrokeStyle.SOLID, null, new BorderWidths(2))));

        StackPane controlBar = new StackPane();

        Button prevBtn = new Button("", new ImageView("icons/prev-btn.png"));
        Button nextBtn = new Button("", new ImageView("icons/next-btn.png"));
        controlBar.getChildren().addAll(nextBtn, prevBtn);
        StackPane.setAlignment(prevBtn, Pos.CENTER_LEFT);
        StackPane.setAlignment(nextBtn, Pos.CENTER_RIGHT);

        getChildren().addAll(controlBar);
        // 加载轮播图样式
        this.getStylesheets().add(getClass().getResource("candy-carousel.css").toExternalForm());
        // 设置样式
        this.setId("carousel-pane");
        controlBar.setId("control-bar");
    }
}
