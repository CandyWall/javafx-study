package application;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.image.CandyCanvasImagePane;

public class Main extends Application {
    private CandyCanvasImagePane imagePane;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            VBox buttonPane = new VBox();
            buttonPane.setId("buttons");
            ScrollPane leftPane = new ScrollPane(buttonPane);
            leftPane.setPrefWidth(136);
            /*
             * buttonPane.setPadding(new Insets(20)); buttonPane.setSpacing(20);
             */
            buttonPane.setAlignment(Pos.CENTER);

            CandyDoubleFaceButton button1 = new CandyDoubleFaceButton(new Image("images/空条承太郎.jpg"), "空条承太郎");
            CandyDoubleFaceButton button2 = new CandyDoubleFaceButton(new Image("images/花京院典明.jpg"), "花京院典明");
            CandyDoubleFaceButton button3 = new CandyDoubleFaceButton(new Image("images/阿布德尔.jpg"), "阿布德尔");
            CandyDoubleFaceButton button4 = new CandyDoubleFaceButton(new Image("images/乔瑟夫乔斯达.jpg"), "乔瑟夫乔斯达");
            CandyDoubleFaceButton button5 = new CandyDoubleFaceButton(new Image("images/伊奇.jpg"), "伊奇");
            buttonPane.getChildren().addAll(button1, button2, button3, button4, button5);

            root.setLeft(leftPane);

            EventHandler<MouseEvent> handler = new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    CandyDoubleFaceButton btn = (CandyDoubleFaceButton) event.getSource();
                    imagePane.setImage(new Image("images/" + btn.getText() + ".jpg"));
                }
            };
            button1.setOnAction(handler);
            button2.setOnAction(handler);
            button3.setOnAction(handler);
            button4.setOnAction(handler);
            button5.setOnAction(handler);

            imagePane = new CandyCanvasImagePane();
            root.setCenter(imagePane);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("双面按钮");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
