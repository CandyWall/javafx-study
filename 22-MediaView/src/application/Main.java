package application;

import java.io.File;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import javafx.scene.media.MediaView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {
    private MediaView mediaView;
    private MediaPlayer mediaPlayer;
    private HBox controlBar; // 控制栏
    private Slider progressSlider; // 进度条
    private RefreshThread refreshThread; // 定时更新界面显示的线程
    long lastUserActive = 0; // 上一次用户操作的时间

    @Override
    public void start(Stage primaryStage) {
        try {
            StackPane root = new StackPane() {
                @Override
                protected void layoutChildren() {
                    super.layoutChildren();

                    // 调整 MediaView
                    double w = getWidth();
                    double h = getHeight();
                    mediaView.setFitWidth(w);
                    mediaView.setFitHeight(h);

                    // 调整control bar
                    controlBar.resizeRelocate(0, h - 60, w, 60);
                }
            };
            mediaView = new MediaView();

            // 视频url
            // File mediaFile = new File("testData/宣传片.mp4");
            // playMedia(mediaFile);

            controlBar = new HBox();
            controlBar.setAlignment(Pos.CENTER_LEFT);
            controlBar.setPadding(new Insets(10));
            controlBar.setSpacing(10);
            Button openBtn = new Button("", new ImageView("icons/open.png"));
            openBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    FileChooser chooser = new FileChooser();
                    chooser.getExtensionFilters().add(new ExtensionFilter("MP4 文件", "*.mp4"));
                    File file = chooser.showOpenDialog(primaryStage);
                    System.out.println(file);

                    if (file != null) {
                        // 播放视频
                        playMedia(file);
                    }
                }
            });
            Button playPauseBtn = new Button("", new ImageView("icons/play.png"));
            playPauseBtn.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    ImageView imageView = (ImageView) playPauseBtn.getGraphic();
                    if (mediaPlayer == null) {
                        return;
                    }
                    if (mediaPlayer.getStatus() == Status.PLAYING) {
                        imageView.setImage(new Image("icons/play.png"));
                        mediaPlayer.pause();
                        refreshThread.stopRun();
                    } else {
                        imageView.setImage(new Image("icons/pause.png"));
                        mediaPlayer.play();
                        if (refreshThread == null || !refreshThread.isAlive()) {
                            refreshThread = new RefreshThread();
                            refreshThread.start();
                        }
                    }
                }
            });
            progressSlider = new Slider(0, 1.0, 0);
            progressSlider.setOrientation(Orientation.HORIZONTAL);
            controlBar.getChildren().addAll(openBtn, playPauseBtn, progressSlider);
            HBox.setHgrow(progressSlider, Priority.ALWAYS);

            // 添加样式
            mediaView.setId("mediaView");
            controlBar.setId("control-bar");
            root.setId("root");

            root.getChildren().addAll(mediaView, controlBar);

            Scene scene = new Scene(root, 1000, 800);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("播放MP4格式的视频");
            primaryStage.show();

            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    if (refreshThread != null && refreshThread.isAlive()) {
                        refreshThread.stopRun();
                    }
                    Platform.exit();
                }
            });

            collectLastUserActive();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 鼠标移动时，记录最后一次活动的时间
    private void collectLastUserActive() {
        mediaView.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                // 用户鼠标移动了，就记录鼠标最后一次移动后的时间
                lastUserActive = System.currentTimeMillis();
                // 控制栏如果隐藏了，就让它显示
                if (!controlBar.isVisible()) {
                    controlBar.setVisible(true);
                }
            }
        });
    }

    // 用户长时间（这里仅仅3秒）不移动鼠标，就隐藏控制栏
    private void autoHideControlBar() {
        long elapsed = System.currentTimeMillis() - lastUserActive;

        if (elapsed > 1000 * 3) {
            if (controlBar.isVisible()) {
                controlBar.setVisible(false);
            }
        }
    }

    private void playMedia(File mediaFile) {
        Media media = new Media(mediaFile.toURI().toString());
        mediaPlayer = new MediaPlayer(media);
        // 设置自动播放
        mediaView.setMediaPlayer(mediaPlayer);
        mediaPlayer.setAutoPlay(true);

        if (refreshThread == null || !refreshThread.isAlive()) {
            refreshThread = new RefreshThread();
            refreshThread.start();
        }
    }

    // 刷新滑动条的播放进度
    private void refreshProgressSlider() {
        double duration = mediaPlayer.getTotalDuration().toMillis();
        double position = mediaPlayer.getCurrentTime().toMillis();
        progressSlider.setValue(position / duration);
    }

    private class RefreshThread extends Thread {
        private volatile boolean running = true;

        public void stopRun() {
            running = false;
            interrupt();
        }

        @Override
        public void run() {
            while (running) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    // e.printStackTrace();
                }
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        refreshProgressSlider();
                    }
                });
                autoHideControlBar();
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
