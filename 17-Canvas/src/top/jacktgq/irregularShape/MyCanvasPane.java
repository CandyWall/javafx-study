package top.jacktgq.irregularShape;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import top.jacktgq.layout.CandyCanvasPane;

public class MyCanvasPane extends CandyCanvasPane {

    @Override
    public void paint(GraphicsContext gc, double w, double h) {
        gc.save(); // 使用clip之前先备份一下初始绘制参数
        gc.setFill(Color.RED);
        // 开始绘制这段路径
        gc.beginPath();

        // 起点(50, 100)
        gc.moveTo(50, 100);

        // 画直接到(100, 100)
        gc.lineTo(100, 100);

        // 画圆弧：中心点(200, 100)，半径(100)，起始角度180度，跨度270度
        gc.arc(200, 100, 100, 100, 180, 270);

        // 结束绘制这段路径
        gc.closePath();
        gc.stroke();
    }
}
