package application;

import java.io.File;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.HTMLEditor;

public class Main extends Application
{
	HTMLEditor editor = new HTMLEditor();
	BorderPane root = new BorderPane();
	
	@Override
	public void start(Stage primaryStage)
	{
		try
		{
			Button btnOpen = new Button("打开");
			Button btnSave = new Button("保存");
			HBox toolbar = new HBox();
			toolbar.getChildren().addAll(btnOpen, btnSave);
			toolbar.setPadding(new Insets(4));
			toolbar.setSpacing(4);;
			
			
			root.setTop(toolbar);
			root.setCenter(editor);
			
			Scene scene = new Scene(root, 640, 400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			
			// 按钮事件
			btnOpen.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event)
				{					
					onOpen();
				}				
			});
			
			// 简写为 lambda 表达式
			btnSave.setOnAction((ActionEvent e)->{
				onSave();
			});

			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	// 打开一个 *.xyz 文件
	private void onOpen() 
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("打开文件");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Text Files", "*.txt"));

		// 可以从任意控件 Node 获取到 Window 对象，传给showOpenDialog()
		// Node.getScene().getWindow()
		File selectedFile = fileChooser.showOpenDialog(root.getScene().getWindow());
		if(selectedFile == null) return; // 用户没有选中文件, 已经取消操作
		
		try {
			String text = TextFileUtils.read(selectedFile, "GBK");
			editor.setHtmlText(text);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// 保存到文件
	private void onSave() 
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("保存文件");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Text Files", "*.txt"));

		File selectedFile = fileChooser.showSaveDialog(root.getScene().getWindow());
		if(selectedFile == null) return; // 用户没有选中文件, 已经取消操作
		
		try {
			String text = editor.getHtmlText();
			TextFileUtils.write(selectedFile, text, "GBK");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		launch(args);
	}
}
