package top.jacktgq.demo3;

import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.stage.Window;

/**
 * 
 * @Title: CandyToaster.java
 * @Package top.jacktgq.custom_controls.toaster
 * @Description: 短消息提示框
 * @author CandyWall
 * @date 2021年5月21日 下午10:11:59
 * @version V1.0
 */
public class MyToasterWindow extends Popup {

    private LayoutPane root;

    public MyToasterWindow() {
        root = new LayoutPane();
        root.setStyle("-fx-background-color: #ADFF2F;-fx-padding:10;");

        getScene().setRoot(root);

        // setAutoHide(true); // 离焦不会自动消失
        setHideOnEscape(true); // 按ESC键可以关闭弹出框
        // 给Popup的左上角作为锚点，即设置Popup的x和y坐标的时候以左上角为基准
        setAnchorLocation(AnchorLocation.CONTENT_TOP_LEFT);
        sizeToScene();
    }

    // 创建一个专门用于布局的类

    private class LayoutPane extends BorderPane {
        private Text textNode = new Text();

        public LayoutPane() {
            setCenter(textNode);
            setWrappingWidth(240);
            // textNode.setStyle("-fx-font-size: 30;");
        }

        @Override
        protected double computePrefWidth(double height) {
            // 减去padding
            Insets insets = getInsets();
            return textNode.getBoundsInLocal().getWidth() + insets.getLeft() + insets.getRight();
        }

        @Override
        protected double computePrefHeight(double width) {
            // 减去padding
            Insets insets = getInsets();
            return textNode.getBoundsInLocal().getHeight() + insets.getTop() + insets.getBottom();
        }

        public void setText(String text) {
            textNode.setText(text);
        }

        public void setWrappingWidth(double width) {
            textNode.setWrappingWidth(width);
        }
    }

    public void setText(String text) {
        root.setText(text);
    }

    /**
     * 文字过长时需要换行，默认文字长度超过240像素就会换行，可以修改这个换行长度
     * 
     * @param width
     */
    public void setWrappingWidth(double width) {
        root.setWrappingWidth(width);
    }

    public void show(Window owner) {
        double x = owner.getX();
        double y = owner.getY();
        double w = owner.getWidth();
        double h = owner.getHeight();
        Bounds bounds = root.getBoundsInLocal();
        double width = bounds.getWidth();
        double height = bounds.getHeight();

        x += (w - width) / 2;
        y += (h - height) / 2;

        super.show(owner, x, y);
    }
}
