package top.jacktgq.demo1;

import java.io.File;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

/**
 * 
 * @Title: Main.java
 * @Package application
 * @Description: 简单图片浏览器：可以浏览某个文件夹下所有图片 第二种是实现：使用封装的CandyImagePane
 * @author CandyWall
 * @date 2021年5月12日 下午6:14:12
 * @version V1.0
 */
public class ImageBrowser1 extends Application {
    private ImageView[] imageViews;
    // private ImageView imageShow;
    private Pane imageShow;
    private Background backgroud;
    private File imageDir;
    private BorderPane root;
    private BorderPane[] panes;
    private HBox thumbPane;

    @Override
    public void start(Stage primaryStage) {
        try {
            root = new BorderPane();

            // 设置下拉菜单
            MenuBar menuBar = new MenuBar();
            Menu menuFile = new Menu("文件");
            MenuItem menuItem = new MenuItem("打开文件夹", new ImageView("icons/open.png"));

            imageDir = new File("images/");
            menuItem.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    DirectoryChooser dirChooser = new DirectoryChooser();
                    dirChooser.setTitle("打开文件夹");
                    // fileChooser.getExtensionFilters().add(new
                    // ExtensionFilter("Image Files", "*.png", "*.jpg",
                    // "*.gif"));
                    File file = dirChooser.showDialog(primaryStage);
                    if (file != null) {
                        imageDir = file;
                        thumbPane.getChildren().clear();
                        loadImage();
                    }
                }
            });

            menuFile.getItems().add(menuItem);

            menuBar.getMenus().add(menuFile);

            // 保存图片缩略图
            thumbPane = new HBox();
            ScrollPane scrollPane = new ScrollPane(thumbPane);
            scrollPane.setPrefHeight(185);

            VBox topPane = new VBox();
            topPane.getChildren().addAll(menuBar, scrollPane);
            root.setTop(topPane);

            // 加载图片
            loadImage();

            Scene scene = new Scene(root, 1000, 800);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("简易图片浏览器");
            primaryStage.show();
        } catch (

        Exception e) {
            e.printStackTrace();
        }
    }

    private void loadImage() {
        File[] imageFiles = imageDir.listFiles();
        imageViews = new ImageView[imageFiles.length];
        panes = new BorderPane[imageFiles.length];
        for (int i = 0; i < imageFiles.length; i++) {
            panes[i] = new BorderPane();
            imageViews[i] = new ImageView(imageFiles[i].toURI().toString());
            imageViews[i].setFitWidth(200);
            imageViews[i].setFitHeight(150);
            panes[i].setCenter(imageViews[i]);
            Paint color = Paint.valueOf("white");
            if (i == 0) {
                color = Paint.valueOf("#99CCFF");
                imageShow = new Pane();
                backgroud = new Background(new BackgroundImage(new Image(imageFiles[i].toURI().toString()),
                        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                        new BackgroundSize(-1, -1, true, true, true, false)));
                imageShow.setBackground(backgroud);
                root.setCenter(imageShow);
            }
            BorderStroke borderStroke = new BorderStroke(color, BorderStrokeStyle.SOLID, new CornerRadii(20),
                    new BorderWidths(10));
            panes[i].setBorder(new Border(borderStroke));

            panes[i].setOnMouseClicked(new EventHandler<Event>() {
                @Override
                public void handle(Event event) {
                    BorderPane pane = (BorderPane) event.getSource();
                    for (int i = 0; i < panes.length; i++) {
                        Paint color = Paint.valueOf("white");
                        if (panes[i] == pane) {
                            color = Paint.valueOf("#99CCFF");
                            backgroud = new Background(new BackgroundImage(new Image(imageFiles[i].toURI().toString()),
                                    BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                                    new BackgroundSize(-1, -1, true, true, true, false)));
                        }
                        BorderStroke borderStroke = new BorderStroke(color, BorderStrokeStyle.SOLID,
                                new CornerRadii(20), new BorderWidths(10));
                        panes[i].setBorder(new Border(borderStroke));
                    }

                    imageShow.setBackground(backgroud);
                }
            });

            // 添加白色边框
            // Border border = new Border(new BorderStroke());
            // 将每一个ImageView保存到缩略图面板中
            thumbPane.getChildren().add(panes[i]);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
