package top.jacktgq.custom_controls.layout;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class CandyLinearLayoutPane extends Pane {
    private List<String> sizes = new ArrayList<>();
    private double spacing = 0;
    private Orientation orient = Orientation.HORIZONTAL;

    //////////// padding ///////////
    public CandyLinearLayoutPane padding(double p) {
        this.setPadding(new Insets(p));
        return this;
    }

    public CandyLinearLayoutPane padding(double px, double py) {
        this.setPadding(new Insets(py, px, py, px));
        return this;
    }

    public CandyLinearLayoutPane padding(double t, double r, double b, double l) {
        this.setPadding(new Insets(t, r, b, l));
        return this;
    }

    //////// spacing ///////////////
    public CandyLinearLayoutPane spacing(double s) {
        this.spacing = s;
        return this;
    }

    /////// 方向 ////////////////////
    public CandyLinearLayoutPane orientation(Orientation orient) {
        this.orient = orient;
        return this;
    }

    //////// 添加子控件 ///////////////
    // 尺寸支持:
    // 绝对值, 如 120px
    // 百分比, 如 30%
    // 权重值, 如 1 3
    public CandyLinearLayoutPane add(Node node, String size) {
        this.getChildren().add(node);
        this.sizes.add(size);
        return this;
    }

    public CandyLinearLayoutPane add(Node node) {
        this.getChildren().add(node);
        this.sizes.add("0px");
        return this;
    }

    public void remove(Node node) {
        int index = this.getChildren().indexOf(node);
        if (index >= 0) {
            this.getChildren().remove(index);
            this.sizes.remove(index);
        }
    }

    ////////////////////////////////////

    // 解析字符串, 以逗号或空格分开
    public void layout(String sizeDefines) {
        String[] sss = sizeDefines.split("[,\\s]");

        sizes.clear();

        for (int i = 0; i < sss.length; i++) {
            String s = sss[i].trim();
            if (s.length() == 0)
                continue;

            if (sizes.size() < this.getChildren().size()) {
                sizes.add(s); // 不得超过子控件的个数
            }
        }
    }

    @Override
    protected void layoutChildren() {
        double w = getWidth(), h = getHeight();
        if (w <= 0 || h <= 0)
            return;

        Insets insets = this.getInsets();
        // System.out.println("窗口: " + w + "," + h);

        int numOfSpace = sizes.size() - 1; // 间隔的数目
        double sizeOfSpace = numOfSpace * spacing;

        // 实用宽度和高度
        double rw = w - insets.getLeft() - insets.getRight();
        double rh = h - insets.getTop() - insets.getBottom();
        double x = insets.getLeft();
        double y = insets.getTop();

        ObservableList<Node> children = this.getChildren();

        // System.out.println("afhbox: insets=" + insets);
        if (orient == Orientation.HORIZONTAL) {
            rw -= sizeOfSpace;
            double[] rsizes = calculateSize(rw, sizes);

            for (int i = 0; i < children.size(); i++) {
                Node node = children.get(i);
                node.resizeRelocate(x, y, rsizes[i], rh);
                // System.out.println("node: " + i + ", x=" + x + ", y=" + y +
                // ", w=" + rsizes[i] + ",h=" + rh);

                x += (rsizes[i] + spacing);
            }
        } else {
            rh -= sizeOfSpace;
            double[] rsizes = calculateSize(rh, sizes);

            for (int i = 0; i < children.size(); i++) {
                Node node = children.get(i);

                node.resizeRelocate(x, y, rw, rsizes[i]);
                // System.out.println("node: " + i + ", x=" + x + ", y=" + y +
                // ", w=" + rw + ",h=" + rsizes[i]);

                y += (rsizes[i] + spacing);
            }
        }

    }

    public double[] calculateSize(double size, List<String> sss) {
        // 去除空项
        Iterator<String> iter = sss.iterator();
        while (iter.hasNext()) {
            String s = iter.next();
            if (s.trim().length() == 0)
                iter.remove();
        }

        int count = sss.size();
        double used = 0;
        double weight = 0;

        double[] _aaa = new double[count];
        int[] _ppp = new int[count];

        for (int i = 0; i < count; i++) {
            String s = sss.get(i);
            if (used >= size)
                continue;

            double aa = 0;
            int pp = 0;

            if (s.endsWith("px")) {
                aa = Integer.valueOf(s.substring(0, s.length() - 2));
            } else if (s.endsWith("%")) {
                int percent = Integer.valueOf(s.substring(0, s.length() - 1));
                aa = size * percent / 100;
            } else {
                pp = Integer.valueOf(s);
            }

            // 不能超出
            if (used + aa > size)
                aa = size - used;

            _aaa[i] = aa;
            _ppp[i] = pp;
            used += aa;
            weight += pp;
        }

        // 按权重分配剩余空间
        double remaining = size - used;
        if (remaining > 0 & weight > 0) {
            double unit = remaining / weight;
            for (int i = 0; i < count; i++) {
                double pp = _ppp[i];
                if (pp > 0) {
                    _aaa[i] = unit * pp;
                }
            }
        }

        return _aaa;
    }

    // public static void main(String[] args)
    // {
    // String sizeDefines = "1 29% 30px, 123";
    // String[] sss = sizeDefines.split("[,\\s]");
    //
    // System.out.println("exit");
    // }
}
