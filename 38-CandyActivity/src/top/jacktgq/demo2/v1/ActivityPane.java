package top.jacktgq.demo2.v1;

import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.activity.CandyActivity;
import top.jacktgq.custom_controls.activity.CandyActivityContext;
import top.jacktgq.custom_controls.activity.CandyActivityObserver;

public class ActivityPane extends BorderPane implements CandyActivityObserver {
    private CandyActivityContext context = new CandyActivityContext();

    public ActivityPane(Stage primaryStage) {
        context.putParam("owner", primaryStage);
        context.setObserver(this);
        context.startActivity(null, LoginActivity.class, null);
    }

    @Override
    public void onActivityShow(CandyActivity a) {
        this.setCenter(a.getContentView());
    }

    @Override
    public void onActivityDismiss(CandyActivity a) {

    }
}
