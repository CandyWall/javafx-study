package top.jacktgq.v1;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeCell;
import javafx.util.Callback;

public class StudentTableView extends TableView<Student> {
    ObservableList<Student> tableViewData = FXCollections.observableArrayList();
    TableColumn<Student, String>[] tableColumns;

    public StudentTableView() {
        tableViewData.add(new Student(111, "空条承太郎", "12314324233"));
        tableViewData.add(new Student(222, "花京院典明", "15821342178"));
        tableViewData.add(new Student(333, "波鲁那雷夫", "14345644354"));
        setItems(tableViewData);

        tableColumns = new TableColumn[3];
        tableColumns[0] = new TableColumn<>("学号");
        tableColumns[1] = new TableColumn<>("姓名");
        tableColumns[2] = new TableColumn<>("手机号");
        getColumns().addAll(tableColumns);

        // TableColumn<Student, String> firstNameCol = new TableColumn<>("First
        // Name");
        // firstNameCol.setCellValueFactory(new PropertyValueFactory<>(new
        // SimpleStringProperty().getName()));

        tableColumns[0].setCellValueFactory(new MyCellValueFatory("id"));
        tableColumns[1].setCellValueFactory(new MyCellValueFatory("name"));
        tableColumns[2].setCellValueFactory(new MyCellValueFatory("phone"));

        /*
         * setRowFactory(new Callback<TableView<Student>, TableRow<Student>>() {
         * 
         * @Override public TableRow<Student> call(TableView<Student> param) {
         * return new MyTreeCell(""); } });
         */
    }

    private class MyCellValueFatory
            implements Callback<TableColumn.CellDataFeatures<Student, String>, ObservableValue<String>> {
        private StringProperty valueProperty;
        private String columnName;

        public MyCellValueFatory(String columnName) {
            this.columnName = columnName;
            valueProperty = new SimpleStringProperty();
        }

        @Override
        public ObservableValue<String> call(CellDataFeatures<Student, String> param) {
            if (columnName.equals("id")) {
                valueProperty.set(param.getValue().id + "");
            } else if (columnName.equals("name")) {
                valueProperty.set(param.getValue().name);
            } else if (columnName.equals("phone")) {
                valueProperty.set(param.getValue().phone);
            }
            return valueProperty;
        }
    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        Insets insets = getInsets();
        double w = getWidth() - insets.getLeft() - insets.getRight();
        double w0 = w * 0.3;
        double w1 = w * 0.3;
        double w2 = w - w0 - w1;
        tableColumns[0].setPrefWidth(w0);
        tableColumns[1].setPrefWidth(w1);
        tableColumns[2].setPrefWidth(w2);
    }

    private class MyTreeCell extends TreeCell<Student> {
        private String columnName;

        public MyTreeCell(String columnName) {
            this.columnName = columnName;

        }

        @Override
        protected void updateItem(Student item, boolean empty) {
            super.updateItem(item, empty);

            if (item == null) {
                this.setText(null);
            } else {
                if (columnName.equals("id")) {
                    this.setText(item.id + "");
                } else if (columnName.equals("name")) {
                    this.setText(item.name);
                } else if (columnName.equals("phone")) {
                    this.setText(item.phone);
                }
            }
        }

    }
}
