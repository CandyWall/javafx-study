package top.jacktgq.drawImage;

import java.io.File;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import top.jacktgq.layout.CandyCanvasPane;

public class CandyCanvasImagePane extends CandyCanvasPane {
    // 缩放类型（也可以用枚举语法Enum来定义）
    // 图片宽度和高度都拉伸显示在矩形内
    public static final int FIT_XY = 0;
    // 图片宽度和高度等比例缩放显示在矩形区域内
    public static final int FIT_CENTER = 1;
    // 如果图片的宽度和高度都小于矩形区域，图片居中显示在矩形区域内，
    // 否则图片宽度和高度等比例缩放显示在矩形区域内
    public static final int FIT_CENTER_INSIDE = 2;

    protected int scaleType = FIT_CENTER;

    protected Image image;

    public CandyCanvasImagePane() {
        this(FIT_CENTER);
    }

    public CandyCanvasImagePane(int scaleType) {
        this.scaleType = scaleType;
    }

    public CandyCanvasImagePane(File file) {
        this(file, FIT_CENTER);
    }

    public CandyCanvasImagePane(Image image) {
        this(image, FIT_CENTER);
    }

    public CandyCanvasImagePane(File file, int scaleType) {
        this(new Image(file.toURI().toString()), scaleType);
    }

    public CandyCanvasImagePane(Image image, int scaleType) {
        this.image = image;
        this.scaleType = scaleType;
    }

    public void setImage(Image image) {
        this.image = image;
        // 重绘
        repaint();
    }

    @Override
    public void paint(GraphicsContext gc, double w, double h) {
        if (image == null) {
            return;
        }
        double imgW = image.getWidth();
        double imgH = image.getHeight();
        Rectangle rect = CandyCanvasImageScaler.fitCenter(imgW, imgH, w, h);
        if (scaleType == FIT_XY) {
            rect = CandyCanvasImageScaler.fitXY(w, h);
        } else if (scaleType == FIT_CENTER_INSIDE) {
            rect = CandyCanvasImageScaler.fitCenterInside(imgW, imgH, w, h);
        }
        gc.drawImage(image, 0, 0, imgW, imgH, rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
    }
}
