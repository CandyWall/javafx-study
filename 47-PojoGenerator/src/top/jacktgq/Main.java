package top.jacktgq;

import javafx.application.Application;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;
import top.jacktgq.activity.ActivityStarter;
import top.jacktgq.custom_controls.window.CandyPrettyWindowWrapper;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            CandyPrettyWindowWrapper cpww = new CandyPrettyWindowWrapper(primaryStage, false, false);

            Stop[] stops = new Stop[] { new Stop(0, Color.LIGHTBLUE), new Stop(1, Color.WHITE) };
            LinearGradient lg1 = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
            cpww.setBackground(lg1);

            new ActivityStarter(cpww);

            cpww.getScene().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            cpww.setTitle("糖果POJO生成器");
            cpww.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
