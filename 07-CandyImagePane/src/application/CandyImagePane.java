package application;

import java.io.File;
import java.math.BigDecimal;

import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 * @Title: CandyImagePane.java
 * @Package top.jacktgq.custom_controls
 * @Description: 图片面板，可以设置图片的显示的缩放格式
 * @author CandyWall
 * @date 2021年5月13日 下午8:18:02
 * @version V1.0
 */
public class CandyImagePane extends Pane {
    private ImageView imageView;

    private int scaleType = FIT_CENTER;
    // 图片宽度和高度都拉伸显示在矩形内
    public static final int FIT_XY = 0;
    // 图片宽度和高度等比例缩放显示在矩形区域内
    public static final int FIT_CENTER = 1;
    // 如果图片的宽度和高度都小于矩形区域，图片居中显示在矩形区域内，
    // 否则图片宽度和高度等比例缩放显示在矩形区域内
    public static final int FIT_CENTER_INSIDE = 2;

    public CandyImagePane(File file) {
        this(new Image(file.toURI().toString()));
    }

    public CandyImagePane(Image image) {
        this(image, FIT_CENTER);
    }

    public CandyImagePane(File file, int scaleType) {
        this(new Image(file.toURI().toString()), scaleType);
    }

    public CandyImagePane(Image image, int scaleType) {
        this.imageView = new ImageView(image);
        getChildren().add(imageView);

        this.scaleType = scaleType;
    }

    public void setImage(File file) {
        setImage(new Image(file.toURI().toString()));
    }

    public void setImage(Image image) {
        imageView.setImage(image);
    }

    public Image getImage() {
        return imageView.getImage();
    }

    @Override
    protected void layoutChildren() {
        Insets insets = getInsets();
        // 获取面板的宽高
        double width = getWidth() - insets.getLeft() - insets.getRight();
        double height = getHeight() - insets.getTop() - insets.getBottom();

        // 获取图片的宽高
        Image image = imageView.getImage();
        double imageWidth = image.getWidth();
        double imageHeight = image.getHeight();

        if (scaleType == FIT_CENTER_INSIDE && imageWidth <= width && imageHeight <= height) {
            double x = (width - imageWidth) / 2 + insets.getLeft();
            double y = (height - imageHeight) / 2 + insets.getTop();
            imageView.resizeRelocate(x, y, imageWidth, imageHeight);
        } else {
            imageResizeRelocate(width, height, imageWidth, imageHeight, insets);
        }
    }

    private void imageResizeRelocate(double width, double height, double imageWidth, double imageHeight,
            Insets insets) {
        imageView.setFitWidth(width);
        imageView.setFitHeight(height);
        if (scaleType == FIT_XY) {
            imageView.resizeRelocate(insets.getLeft(), insets.getTop(), width, height);
            return;
        }
        imageView.setPreserveRatio(true);
        // 假设用图片的宽度去适配父容器的宽度，
        // 保持长宽比不变的情况下，如果缩放后的宽度没有超过父容器的宽度
        double x;
        double y;
        // 比较两个浮点数的大小，转成BigDecimal后再进行比较
        if (new BigDecimal(imageHeight * width / imageWidth).compareTo(new BigDecimal(height)) <= 0) {
            double h = imageHeight * width / imageWidth;
            x = insets.getLeft();
            y = (height - h) / 2 + insets.getTop();
        } else {
            double w = imageWidth * height / imageHeight;
            x = (width - w) / 2 + insets.getLeft();
            y = insets.getTop();
        }

        imageView.resizeRelocate(x, y, width, height);
    }
}
