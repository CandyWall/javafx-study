package top.jacktgq.activity;

import java.io.File;
import java.util.HashMap;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import top.jacktgq.custom_controls.activity.CandyActivity;
import top.jacktgq.utils.process.CandyProcess;

public class ResultActivity extends CandyActivity {
    @Override
    public void onCreate(Object arg0) {
        HBox box = new HBox();
        setContentView(box);

        HashMap<String, Object> paramsMap = (HashMap<String, Object>) intent;
        boolean hasError = (boolean) paramsMap.get("hasError");
        box.getChildren().addAll(new ImageView("icons/ic_" + (hasError ? "cry" : "smile") + ".png"),
                new Label(hasError ? "生成失败" : "生成成功！"));
        if (!hasError) {
            Button btn = new Button("查看生成的POJO代码文件");
            btn.setOnAction(e -> {
                File generateDir = (File) paramsMap.get("generateDir");
                String cmd = "explorer \"" + generateDir.getAbsolutePath() + "\"";
                try {
                    CandyProcess.execute(cmd);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            });
            box.getChildren().add(btn);
        }
        box.setAlignment(Pos.CENTER);
        box.setSpacing(10);
    }
}
