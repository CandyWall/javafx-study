package top.jacktgq.demo2;

import java.io.File;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class Main extends Application {
    private FileTreeTableView treeTableView;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            MenuBar menuBar = new MenuBar();
            Menu fileMenu = new Menu("文件");
            MenuItem fileMenuItem = new MenuItem("打开文件", new ImageView("icons/open.png"));
            fileMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    DirectoryChooser dirChooser = new DirectoryChooser();
                    File dir = dirChooser.showDialog(primaryStage);
                    if (dir != null) {
                        treeTableView.setDir(dir);
                    }
                }
            });
            fileMenu.getItems().addAll(fileMenuItem);
            menuBar.getMenus().add(fileMenu);
            root.setTop(menuBar);

            treeTableView = new FileTreeTableView(new File("."));

            // treeTableView.setShowRoot(false);

            root.setCenter(treeTableView);

            Scene scene = new Scene(root, 595, 600);
            scene.getStylesheets().add(getClass().getResource("/top/jacktgq/demo1/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("文件浏览器");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
