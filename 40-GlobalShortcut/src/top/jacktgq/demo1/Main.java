package top.jacktgq.demo1;

import com.melloware.jintellitype.HotkeyListener;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {
    TextArea content = new TextArea();

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            root.setCenter(content);

            // 1. 初始化热键
            try {
                GlobalHotkey.i.init();
            } catch (Exception e) {
                System.out.println("热键冲突! 注册热键失败!");
            }

            // 2. 热键处理
            GlobalHotkey.i.addHotKeyListener(new HotkeyListener() {
                @Override
                public void onHotKey(int hotkeyID) {
                    System.out.println("监听到" + hotkeyID + "号热键!");

                    Platform.runLater(() -> {
                        // 处理热键
                        if (hotkeyID == 1) {

                        } else if (hotkeyID == 2) {

                        } else if (hotkeyID == 3) {

                        }
                    });
                }
            });

            primaryStage.setOnCloseRequest((e) -> {

                // 3. 退出时应记得注销热键
                GlobalHotkey.i.cleanUp();
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
