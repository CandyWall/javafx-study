package top.jacktgq.custom_controls.window;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Paint;
import javafx.stage.Window;

public class CandyPrettyWindow extends CandyWindow {
    private LayoutPane customPane;
    private boolean hasTitleBar;

    public CandyPrettyWindow(Window owner, boolean resizable, boolean hasTitleBar) {
        super(owner, resizable);
        this.hasTitleBar = hasTitleBar;
        customPane = new LayoutPane();
        customPane.closeBtn.setOnAction((e) -> {
            close();
        });
    }

    public CandyPrettyWindow(Window owner) {
        this(owner, true, true);
    }

    /**
     * 给当前自定义的对话框设置标题需要调用此方法，而不是setTitle()
     * setTitle()方法是stage中的final方法，当前类作为Stage类的子类不能覆盖 所以只能另取一个名字
     * 
     * @param title
     */
    public void setWindowTitle(String title) {
        customPane.titleLabel.setText(title);
    }

    @Override
    public void setContentView(Region contentView) {
        setContentView(contentView, -1, -1);
    }

    /**
     * 传入宽高后，可以调用centerInParent定位到父窗口的中间
     * 
     * @param root
     * @param sceneWidth
     * @param sceneHeight
     */
    @Override
    public void setContentView(Region contentView, double width, double height) {
        customPane.setCenter(contentView);
        super.setContentView(customPane, width, height + 40);
        initDragSupport(customPane.titleLabel, resizable);
    }

    private class LayoutPane extends BorderPane {
        Label titleLabel = new Label();
        Button closeBtn = new Button("", new ImageView("top/jacktgq/custom_controls/window/ic_close.png"));

        public LayoutPane() {
            HBox titlePane = new HBox();
            titlePane.getChildren().addAll(titleLabel, closeBtn);
            titlePane.setAlignment(Pos.TOP_LEFT);
            titleLabel.setMaxWidth(9999);

            HBox.setHgrow(titleLabel, Priority.ALWAYS);
            // titlePane.setPadding(new Insets(10));
            titlePane.setMaxHeight(40);
            setTop(titlePane);

            if (!hasTitleBar) {
                titlePane.setStyle("-fx-background-color: transparent;");
                titleLabel.setStyle("-fx-background-color: transparent;");
            } else {
                titlePane.setStyle("-fx-background-color: #ccc;");
                titleLabel.setStyle("-fx-background-color: #ccc;");
            }

            // 设置样式
            this.getStylesheets()
                    .add(CandyPrettyWindowWrapper.class.getResource("candy-pretty-window.css").toExternalForm());
            this.getStyleClass().add("candy-pretty-window");
            closeBtn.getStyleClass().add("candy-pretty-window-close");
            titleLabel.getStyleClass().add("candy-pretty-window-title");
            // getCenter().getStyleClass().add("candy-pretty-dialog-content");
        }
    }

    /**
     * 设置背景颜色
     * 
     * @param color
     */
    public void setBackground(Paint color) {
        customPane.setBackground(new Background(new BackgroundFill(color, null, null)));
    }

    /**
     * 设置背景图片
     * 
     * @param image
     */
    public void setBackground(Image image) {
        customPane.setBackground(
                new Background(new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                        BackgroundPosition.CENTER, new BackgroundSize(-1, -1, true, true, true, true))));
    }
}
