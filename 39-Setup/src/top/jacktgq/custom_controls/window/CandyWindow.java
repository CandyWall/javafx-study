package top.jacktgq.custom_controls.window;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class CandyWindow extends Stage {
    protected Region root;
    protected boolean resizable;
    private Scene scene;

    public CandyWindow(Window owner, boolean resizable) {
        if (owner != null)
            initOwner(owner);
        this.resizable = resizable;
        initStyle(StageStyle.UNDECORATED);
    }

    public CandyWindow(Window owner) {
        this(owner, true);
    }

    /**
     * 传入scene的宽高后，可以调用centerInParent定位到父窗口的中间
     * 
     * @param root
     * @param sceneWidth
     * @param sceneHeight
     */
    public void setContentView(Region contentView, double width, double height) {
        root = contentView;
        if (scene == null) {
            scene = new Scene(root);
            setScene(scene);
        } else {
            getScene().setRoot(root);
        }
        if (width > 0 && height > 0) {
            resize(width, height);
        } else {
            sizeToScene();
        }
    }

    public void setContentView(Region contentView) {
        setContentView(contentView, -1, -1);
    }

    public Region getContentView() {
        return root;
    }

    // anchor : 可拖动区域，一般是标题栏位置
    // resizable: 是否要以拖动改变大小
    public void initDragSupport(Node anchor, boolean resizable) {
        if (anchor == null)
            anchor = root;
        CandyWindowDragSupport.addDragSupport(this, root, anchor, resizable);
    }

    public void setWindowResizable(boolean resizable) {
        this.resizable = resizable;
    }

    ////////////////////////////////////

    // 改变窗口大小
    public final void resize(double w, double h) {
        root.setPrefSize(w, h);
        this.sizeToScene();
    }

    // 指定窗口位置
    public final void relocate(double x, double y) {
        this.setX(x);
        this.setY(y);
    }

    // 相对于父窗口中心显示
    public final void centerInParent() {
        Window owner = this.getOwner();
        if (owner != null) {
            // 父窗口的位置px, py, 大小 pw, ph
            double px = owner.getX(), py = owner.getY();
            double pw = owner.getWidth(), ph = owner.getHeight();

            double dx = (pw - getWidth()) / 2;
            double dy = (ph - getHeight()) / 2;

            this.setX(px + dx);
            this.setY(py + dy);
        }
    }
}
