package top.jacktgq.contextMenu;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class Main extends Application {
    // 创建ListView，指定数据项类型
    private ListView<Student> listView = new ListView<>();

    // 数据源
    ObservableList<Student> listData = FXCollections.observableArrayList();

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            // 准备数据
            listData.add(new Student(111, "空条承太郎", true));
            listData.add(new Student(222, "花京院典明", true));
            listData.add(new Student(333, "空条徐伦", false));
            listData.add(new Student(444, "波鲁那雷夫", true));

            // 设置列表项
            listView.setItems(listData);

            // 设置单元格生成器（工厂）
            listView.setCellFactory(new Callback<ListView<Student>, ListCell<Student>>() {
                @Override
                public ListCell<Student> call(ListView<Student> param) {
                    return new MyListCell();
                }
            });

            MenuItem menuAdd = new MenuItem("添加", new ImageView("icons/add.png"));
            menuAdd.setOnAction(e -> {
                new AddWindow(primaryStage, listView, listData);
            });
            MenuItem menuEdit = new MenuItem("修改", new ImageView("icons/edit.png"));
            menuEdit.setOnAction(e -> {
                int index = listView.getSelectionModel().getSelectedIndex();
                new ModifyWindow(primaryStage, listView, listData, index);
            });
            MenuItem menuRemove = new MenuItem("删除", new ImageView("icons/delete.png"));
            menuRemove.setOnAction(e -> {
                int index = listView.getSelectionModel().getSelectedIndex();
                listData.remove(index);
            });
            ContextMenu contextMenu = new ContextMenu(menuAdd, menuEdit, menuRemove);
            listView.setContextMenu(contextMenu);

            root.setCenter(listView);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets()
                    .add(getClass().getResource("/top/jacktgq/contextMenu/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    // 负责listView的ListCell
    class MyListCell extends ListCell<Student> {
        @Override
        protected void updateItem(Student item, boolean empty) {
            // fx框架奥球必须先调用super.updateItem()
            super.updateItem(item, empty);

            // 自己的代码
            if (item != null) {
                this.setText(item.id + "\t" + item.name + "\t" + (item.sex ? "男" : "女"));
            } else {
                this.setText("");
            }
        }
    }

}
