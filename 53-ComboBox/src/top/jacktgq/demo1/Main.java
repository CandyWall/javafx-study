package top.jacktgq.demo1;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();
            Button btn = new Button("确认");

            ComboBox<String> cbb = new ComboBox<>();
            cbb.getItems().addAll("空条承太郎", "花京院典明", "乔瑟夫乔斯达");
            cbb.getSelectionModel().select("空条承太郎");
            cbb.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    System.out.println("选中了" + newValue);
                }
            });
            cbb.setEditable(true);
            cbb.setPromptText("请输入人物姓名");
            // 没有列表项的时候会显示
            cbb.setPlaceholder(new Label("暂无选项"));
            cbb.setVisibleRowCount(2);

            root.getChildren().addAll(btn, cbb);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
