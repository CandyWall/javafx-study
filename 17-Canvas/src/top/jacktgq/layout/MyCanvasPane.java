package top.jacktgq.layout;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class MyCanvasPane extends CandyCanvasPane {
    @Override
    public void paint(GraphicsContext gc, double w, double h) {
        gc.setFill(Color.color(Math.random(), Math.random(), Math.random()));
        gc.fillOval(0, 0, w, h);
    }
}
