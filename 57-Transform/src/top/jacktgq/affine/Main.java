package top.jacktgq.affine;

import javafx.application.Application;
import javafx.geometry.Bounds;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();

            AnchorPane an = new AnchorPane();
            an.setPrefSize(200, 200);
            an.setStyle("-fx-background-color: pink");

            Button btn1 = new Button("提交");
            btn1.setPrefSize(200, 100);
            btn1.setLayoutX(50);
            btn1.setLayoutY(50);
            an.getChildren().add(btn1);

            Button btn2 = new Button("重置");
            btn2.setPrefSize(200, 100);
            btn2.setLayoutX(50);
            btn2.setLayoutY(50);
            btn2.setOpacity(0.5);

            Scale scale = new Scale(1, -1, 200, 100);
            btn2.getTransforms().add(scale);

            an.getChildren().add(btn2);
            root.getChildren().add(an);
            AnchorPane.setLeftAnchor(an, 100.0);
            AnchorPane.setTopAnchor(an, 100.0);

            Scene scene = new Scene(root, 600, 600);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            // 获取当前结点在父节点中的坐标位置
            System.out.println("(" + btn2.getLayoutX() + ", " + btn2.getLayoutY() + ")");
            // 获取当前结点在父节点中的经过变换后的真实位置
            System.out.println("(" + btn2.getLocalToParentTransform().getTx() + ", "
                    + btn2.getLocalToParentTransform().getTy() + ")");
            // 获取当前结点在父节点中的经过变换后的真实位置
            System.out.println("(" + btn2.getLocalToSceneTransform().getTx() + ", "
                    + btn2.getLocalToSceneTransform().getTy() + ")");

            Bounds bd = btn2.getLayoutBounds();
            System.out.println(bd);
            System.out.println(btn2.localToParent(bd));
            System.out.println(btn2.localToScreen(bd));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
