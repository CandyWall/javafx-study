package top.jacktgq.custom_controls.toaster;

import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Stop;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.stage.Window;
import top.jacktgq.animation.CandyAnimationTimer;

/**
 *
 * @Title: CandyToaster.java
 * @Package top.jacktgq.custom_controls.toaster
 * @Description: 短消息提示框
 * @author CandyWall
 * @date 2021年5月21日 下午10:11:59
 * @version V1.0
 */
public class CandyToaster extends Popup {
    private LayoutPane root;

    // 消息提示级别
    public final static int SUCCESS = 0;// 操作成功
    public final static int INFO = 1; // 普通
    public final static int WARN = 2; // 警告
    public final static int ERROR = 3; // 故障、错误

    private Window owner;
    private Node node; // 可以从Node获取Window

    private CandyAnimationTimer timer;

    /////////////

    public CandyToaster(Window owner, String text, int level) {
        // 如果不指定timeout,默认为1500毫秒
        this(owner, text, 1500, level, Pos.CENTER);
    }

    public CandyToaster(Window owner, String text) {
        // 如果不指定timeout,默认为1500毫秒
        // 如果不指定消息提示级别，默认为普通级别
        this(owner, text, 1500, SUCCESS, Pos.CENTER);
    }

    /**
     * 显示消息提示窗口
     *
     * @param ownerWindow
     *            ：调用弹出提示窗口的主窗口
     * @param delay
     *            ：多少毫秒以后窗口自动关闭
     * @param level
     *            ：消息提示级别
     */
    public CandyToaster(Window owner, String text, int timeout, int level, Pos pos) {
        this.owner = owner;
        root = new LayoutPane();

        getScene().setRoot(root);

        // setAutoHide(true); // 离焦不会自动消失
        setHideOnEscape(true); // 按ESC键可以关闭弹出框
        // 给Popup的左上角作为锚点，即设置Popup的x和y坐标的时候以左上角为基准
        setAnchorLocation(AnchorLocation.CONTENT_TOP_LEFT);
        sizeToScene();

        showToast(level, timeout, text, pos);
    }

    // 创建一个专门用于布局的类
    private class LayoutPane extends Pane {
        private Paint bgColor = Color.web("#ffcc00", 0.8);
        private Canvas bgCanvas = new Canvas();
        private Text textNode = new Text();

        public LayoutPane() {
            setPadding(new Insets(10));
            getChildren().addAll(bgCanvas, textNode);
            setEffect(new DropShadow());
        }

        @Override
        protected double computePrefWidth(double height) {
            // 减去padding
            Insets insets = getInsets();
            return textNode.getBoundsInLocal().getWidth() + insets.getLeft() + insets.getRight();
        }

        @Override
        protected double computePrefHeight(double width) {
            // 减去padding
            Insets insets = getInsets();
            return textNode.getBoundsInLocal().getHeight() + insets.getTop() + insets.getBottom();
        }

        @Override
        protected void layoutChildren() {
            super.layoutChildren();

            double w = computePrefWidth(0);
            double h = computePrefHeight(0);
            bgCanvas.setWidth(w);
            bgCanvas.setHeight(h);
            // 绘制一下背景
            paintBg(w, h);

            Insets insets = getInsets();
            textNode.relocate(insets.getLeft(), insets.getTop());
        }

        private void paintBg(double w, double h) {
            GraphicsContext gc = bgCanvas.getGraphicsContext2D();
            gc.clearRect(0, 0, w, h);
            gc.setFill(bgColor);
            gc.fillRoundRect(0, 0, w, h, 12, 12);
        }

        public void setText(String text, double wrappingWidth) {
            textNode.setText(text);
            Bounds bounds = textNode.getBoundsInLocal();
            if (bounds.getWidth() > wrappingWidth) {
                textNode.setWrappingWidth(wrappingWidth);
            }
        }

        public void setTextColor(Paint color) {
            textNode.setFill(color);
        }

        public void setBgColor(Paint bgColor) {
            this.bgColor = bgColor;
            double w = computePrefWidth(0);
            double h = computePrefHeight(0);
            paintBg(w, h);
        }
    }

    public void setText(String text) {
        setText(text, 240);
    }

    public void setText(String text, double wrappingWidth) {
        root.setText(text, wrappingWidth);
    }

    public void setTextColor(Paint color) {
        root.setTextColor(color);
    }

    public void setBgColor(Paint bgColor) {
        root.setBgColor(bgColor);
    }

    public void show(Window owner) {
        double x = owner.getX();
        double y = owner.getY();
        double w = owner.getWidth();
        double h = owner.getHeight();
        Bounds bounds = root.getBoundsInLocal();
        double width = bounds.getWidth();
        double height = bounds.getHeight();

        x += (w - width) / 2;
        y += (h - height) / 2;

        super.show(owner, x, y);
    }

    /**
     * 根据消息提示级别，改变提示窗口的风格
     *
     * @param level
     */
    private void changeStyle(int level) {
        // Paint bgColor = Color.rgb(87, 183, 87, 0.9);
        Paint bgColor = new LinearGradient(0, 0, 0, 0.5, true, CycleMethod.NO_CYCLE,
                new Stop[] { new Stop(0, Color.rgb(86, 182, 86, 0.8)), new Stop(1, Color.rgb(87, 183, 87, 0.9)),
                        new Stop(0, Color.rgb(86, 182, 86, 0.8)) });

        Paint fgColor = Color.rgb(250, 250, 210);
        switch (level) {
        case INFO:
            // bgColor = Color.rgb(60, 60, 60, 0.8);
            // bgColor = new LinearGradient(0, 0, 0, 0.5, true,
            // CycleMethod.NO_CYCLE,
            // new Stop[] { new Stop(0, Color.rgb(160, 160, 160, 0.8)), new
            // Stop(1, Color.rgb(60, 60, 60, 0.8)),
            // new Stop(0, Color.rgb(160, 160, 160, 0.8)) });
            bgColor = new LinearGradient(0, 0, 0, 0.5, true, CycleMethod.REFLECT,
                    new Stop[] { new Stop(0, Color.rgb(102, 102, 102, 0.8)), new Stop(1, Color.rgb(0, 0, 0, 0.8)) });
            fgColor = Color.rgb(230, 230, 230);
            break;
        case WARN:
            // bgColor = Color.rgb(250, 164, 27);
            bgColor = new LinearGradient(0, 0, 0, 0.5, true, CycleMethod.REFLECT,
                    new Stop[] { new Stop(0, Color.rgb(250, 164, 27)), new Stop(1, Color.rgb(255, 194, 14, 0.8)),
                            new Stop(0, Color.rgb(250, 164, 27)) });
            fgColor = Color.rgb(255, 255, 255);
            break;
        case ERROR:
            // bgColor = Color.rgb(255, 0, 0);
            bgColor = new LinearGradient(0, 0, 0, 0.5, true, CycleMethod.REFLECT,
                    new Stop[] { new Stop(0, Color.rgb(240, 96, 44)), new Stop(1, Color.rgb(255, 0, 0, 0.8)),
                            new Stop(0, Color.rgb(240, 96, 44)) });
            fgColor = Color.rgb(255, 255, 255);
            break;
        }
        setBgColor(bgColor);
        setTextColor(fgColor);
    }

    public void showToast(int level, int timeout, String text, Pos pos) {
        // 消息级别与背景色
        changeStyle(level);

        // 设置文本
        setText(text);

        // 如果指定的是Node，则只有Node被显示的时候才能得到Window
        if (owner == null)
            owner = node.getScene().getWindow();
        double x = owner.getX(), y = owner.getY();
        double w = owner.getWidth(), h = owner.getHeight();

        // 计算窗口显示的位置
        Bounds bounds = root.getBoundsInLocal();
        if (pos == Pos.CENTER) {
            x += (w - bounds.getWidth()) / 2;
            y += (h - bounds.getHeight()) / 2;
        } else if (pos == Pos.TOP_CENTER) {
            x += (w - bounds.getWidth()) / 2;
            y += 50;
        } else if (pos == Pos.BOTTOM_CENTER) {
            x += (w - bounds.getWidth()) / 2;
            y += (h - bounds.getHeight() - 50);
        }

        // System.out.println(bounds.toString());

        show(owner, x, y);

        timer = new CandyAnimationTimer(100) {
            private long startTime = System.currentTimeMillis();

            @Override
            public void updateUI(long now) {
                // 已经过去了多少时间
                long pass = System.currentTimeMillis() - startTime;
                // 还剩下多少时间
                long remain = timeout - pass;
                // 已经结束，关闭定时器，关闭提示框
                if (remain <= 0) {
                    timer.stop();
                    timer = null;
                    hide();
                    return;
                }

                if (remain <= 500) {
                    float percent = remain / 500f;
                    CandyToaster.this.setOpacity(percent);
                }
            }
        };
        timer.start();
    }

    // --------------------------工具方法-----------------------------
    public static void show(Window ownerWindow, String text) {
        show(ownerWindow, text, 1500, SUCCESS, Pos.CENTER);
    }

    public static void show(Window ownerWindow, String text, int level) {
        show(ownerWindow, text, 1500, level, Pos.CENTER);
    }

    /**
     * 显示消息提示窗口
     *
     * @param ownerWindow
     *            ：调用弹出提示窗口的主窗口
     * @param text
     *            ：提示的内容
     * @param timeout
     *            ：多少毫秒以后窗口自动关闭
     * @param level
     *            ：消息提示级别
     */
    public static void show(Window ownerWindow, String text, int timeout, int level, Pos pos) {
        new CandyToaster(ownerWindow, text, timeout, level, pos);
    }

    // ------------------------------------------------------------
    public static void show(Node node, String text) {
        show(node, text, 1500, SUCCESS);
    }

    public static void show(Node node, String text, int level) {
        show(node, text, 1500, level);
    }

    /**
     * 显示消息提示窗口
     *
     * @param node
     *            ：调用弹出提示窗口的控件
     * @param text
     *            ：提示的内容
     * @param timeout
     *            ：多少毫秒以后窗口自动关闭
     * @param level
     *            ：消息提示级别
     */
    public static void show(Node node, String text, int timeout, int level) {
        Window ownerWindow = node.getScene().getWindow();
        show(ownerWindow, text, timeout, level, Pos.CENTER);
    }
}
