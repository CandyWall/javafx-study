package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            VBox root = new VBox();
            root.setSpacing(10);
            root.setId("root"); // application.css 样式定义
            root.setPadding(new Insets(30));

            TextField usernameField = new TextField();
            usernameField.setPromptText("用户名");
            PasswordField passwordField = new PasswordField();
            passwordField.setPromptText("密码");
            Button loginButton = new Button("登录");
            loginButton.setMaxWidth(Double.MAX_VALUE);
            loginButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    // 获取用户名和密码
                    String username = usernameField.getText();
                    String password = passwordField.getText();
                    
                    if (username.length() == 0 || password.length() == 0) {
                        Alert errorAlert = new Alert(AlertType.WARNING);
                        errorAlert.setHeaderText("出错");
                        errorAlert.setContentText("用户名或密码不能为空！");
                        errorAlert.showAndWait();
                    } else if (username.equals("admin") && password.equals("admin")) {
                        Alert successAlert = new Alert(AlertType.INFORMATION);
                        successAlert.setHeaderText("成功");
                        successAlert.setContentText("欢迎进入系统！");
                        successAlert.showAndWait();
                    } else {
                        Alert errorAlert = new Alert(AlertType.WARNING);
                        errorAlert.setHeaderText("出错");
                        errorAlert.setContentText("用户名或密码错误！");
                        errorAlert.showAndWait();
                    }
                }
            });

            root.getChildren().addAll(usernameField, passwordField, loginButton);

            Scene scene = new Scene(root, 400, 230);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("用户登录");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
