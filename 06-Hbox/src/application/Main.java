package application;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();

            TextField textField = new TextField();
            Button button1 = new Button("选择文件");
            Button button2 = new Button("开始上传");
            root.getChildren().addAll(textField, button1, button2);
            HBox.setHgrow(textField, Priority.ALWAYS);
            root.setPadding(new Insets(10));

            Scene scene = new Scene(root, 400, -1);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
