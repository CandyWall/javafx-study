package top.jacktgq;

import java.io.File;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import top.jacktgq.custom_control.image.CandyCanvasImagePane;

/**
 * 
 * @Title: RoundIcon.java
 * @Package top.jacktgq
 * @Description: 圆形图片
 * @author CandyWall
 * @date 2021年5月16日 下午2:50:16
 * @version V1.0
 */
public class CandyRoundIcon extends CandyCanvasImagePane {
    public CandyRoundIcon(File file) {
        super(file, FIT_XY);
    }

    public CandyRoundIcon(Image image) {
        super(image, FIT_XY);
    }

    @Override
    public void paint(GraphicsContext gc, double w, double h) {
        gc.save(); // 使用clip之前先备份一下初始绘制参数
        // 开始绘制
        gc.beginPath();
        // 绘制一个圆
        gc.arc(w / 2, h / 2, w / 2 - 4, h / 2 - 4, 0, 360);
        gc.clip();
        super.paint(gc, w, h);
        gc.restore(); // 恢复初始绘制参数
    }
}
