package top.jacktgq.drawSinCurve;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import top.jacktgq.layout.CandyCanvasPane;

public class SinCurvePane extends CandyCanvasPane {
    private double amplitude = 100; // 振幅
    private double period = 120; // 周期
    private static final double BORDERWIDTH = 2; // 边框宽度
    private static final int HORIZONTAL_SPLIT_COUNT = 40; // 水平辅助线分割区域数
    private static final int VERTICAL_SPLIT_COUNT = 20; // 竖直辅助线条数分割区域数
    private static final int GAP = 2;

    public SinCurvePane(double amplitude, double period) {
        this.amplitude = amplitude;
        this.period = period;
    }

    public double getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(double amplitude) {
        this.amplitude = amplitude;
        repaint();
    }

    public double getPeriod() {
        return period;
    }

    public void setPeriod(double period) {
        this.period = period;
        repaint();
    }

    @Override
    public void paint(GraphicsContext gc, double w, double h) {
        // 绘制边框
        gc.setStroke(Color.rgb(17, 125, 187));
        gc.setLineWidth(2);

        gc.strokeRect(BORDERWIDTH, BORDERWIDTH, w - BORDERWIDTH * 2, h - BORDERWIDTH * 2);

        // 绘制辅助线
        gc.setStroke(Color.rgb(217, 234, 244));
        // 水平方向绘制辅助线间隔
        double hGap = w / HORIZONTAL_SPLIT_COUNT;
        // 竖直方向绘制辅助线间隔
        double vGap = h / VERTICAL_SPLIT_COUNT;
        // 绘制水平辅助线
        for (int i = 1; i < HORIZONTAL_SPLIT_COUNT; i++) {
            gc.strokeLine(BORDERWIDTH + hGap * i, BORDERWIDTH, BORDERWIDTH + hGap * i, h - BORDERWIDTH);
        }
        // 绘制竖直辅助线
        for (int i = 1; i < VERTICAL_SPLIT_COUNT; i++) {
            gc.strokeLine(BORDERWIDTH, BORDERWIDTH + vGap * i, w - BORDERWIDTH, BORDERWIDTH + vGap * i);
        }

        // 将画布中心点当作直角坐标系的原点
        gc.setStroke(Color.SLATEGRAY);
        // 绘制x轴
        gc.strokeLine(BORDERWIDTH, h / 2, w - BORDERWIDTH, h / 2);
        // 绘制Y轴
        gc.strokeLine(w / 2, BORDERWIDTH, w / 2, h - BORDERWIDTH);

        // 先求出正弦曲线的值，坐标值需要进行变换
        // 绘制正弦曲线
        gc.setStroke(Color.rgb(17, 125, 187));
        double y;
        int nextX = 0;
        double nextY = transferY(amplitude * Math.sin(2 * Math.PI / period * transferX(nextX)));
        for (int x = 0; x < w - GAP; x += GAP) {
            y = nextY;
            // 绘制正弦函数的圆拱
            gc.strokeLine(x, y, x, transferY(0));
            nextX = x + GAP;
            nextY = transferY(amplitude * Math.sin(2 * Math.PI / period * transferX(nextX)));
            // 用线连接相邻点
            gc.strokeLine(x, y, nextX, nextY);
        }

    }

    // 将屏幕坐标转成平面直角坐标
    private double transferX(double x) {
        return x - w / 2;
    }

    private double transferY(double y) {
        return -y + h / 2;
    }
}
