package top.jacktgq.custom_controls.scrollPane;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class CandyGridLayout extends Pane {
    // 外部直接可设的参数
    public double cellMaxW = 150; // 可适应最大宽度
    public double cellMinW = 100; // 可适应最小宽度
    public double cellAspectH = 1; // 宽高比
    public double cellAspectV = 1;

    // 内部参数
    private int cols = 0; // 适配计算后的实际列数
    private double x_unit = 0; // 适配计算后的水平单元格大小
    private double y_unit = 0;

    public CandyGridLayout() {
    }

    public void caculate(double w, double h) {
        // 尽量大, 尽量少的空余
        double bestRemaining = w; // 多余空间
        int cc = 0;
        for (double cw = cellMaxW; cw >= cellMinW; cw -= 4) {
            cc = (int) (w / cw);
            double remaining = w - cc * cw;
            if (remaining < bestRemaining) {
                x_unit = cw;
                cols = cc;
                bestRemaining = remaining;
            }
        }

        // 根据长宽比, 决定单元格的竖起高度
        y_unit = x_unit * cellAspectV / cellAspectH;
        // System.out.println("最佳尺寸: " + x_unit +"," + bestRemaining);
    }

    @Override
    public void resize(double width, double height) {
        super.resize(width, height);
        caculate(width, height);
    }

    @Override
    protected void layoutChildren() {
        ObservableList<Node> nodes = this.getChildren();
        for (int i = 0; i < nodes.size(); i++) {
            Node node = nodes.get(i);
            int col = i % cols;
            int row = i / cols;
            node.resizeRelocate(x_unit * col, y_unit * row, x_unit, y_unit);
        }
    }

    /*
     * @Override protected double computePrefWidth(double height) { return
     * super.computePrefWidth(height); }
     */

    @Override
    protected double computePrefHeight(double width) {
        if (cols <= 0)
            return 100;

        int num = this.getChildren().size();
        int rows = num / cols;
        if (num % cols != 0)
            rows += 1;

        return rows * y_unit;
    }

}
