package top.jacktgq.path.demo2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.FillRule;
import javafx.scene.shape.HLineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.VLineTo;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();

            Path path = new Path();

            MoveTo mt1 = new MoveTo(100, 100);

            HLineTo hlt1 = new HLineTo(100);
            hlt1.setAbsolute(false);

            VLineTo vlt1 = new VLineTo(100);
            vlt1.setAbsolute(false);

            HLineTo hlt2 = new HLineTo(-100);
            hlt2.setAbsolute(false);

            VLineTo vlt2 = new VLineTo(-100);
            vlt2.setAbsolute(false);

            MoveTo mlt2 = new MoveTo(50, 50);

            path.getElements().addAll(mt1, hlt1, vlt1, hlt2, vlt2, mlt2, hlt1, vlt1, hlt2, vlt2);

            path.setFill(Color.GREEN);

            path.setFillRule(FillRule.EVEN_ODD);

            root.getChildren().add(path);

            Scene scene = new Scene(root, 1000, 800);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
