package top.jacktgq.demo1;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import top.jacktgq.custom_controls.activity.CandyActivity;

public class Page1 extends CandyActivity {
    @Override
    public void onCreate(Object intent) {
        StackPane stackPane = new StackPane();
        this.setContentView(stackPane);
        Label label = new Label("请完成该问卷");
        Button next = new Button("开始填写");
        next.setOnAction(e -> {
            startActivity(Page2.class, null);
        });
        stackPane.getChildren().addAll(label, next);
        StackPane.setAlignment(label, Pos.CENTER);
        StackPane.setAlignment(next, Pos.BOTTOM_CENTER);
    }
}
