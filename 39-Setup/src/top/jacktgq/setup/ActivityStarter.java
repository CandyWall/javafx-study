package top.jacktgq.setup;

import top.jacktgq.custom_controls.activity.CandyActivity;
import top.jacktgq.custom_controls.activity.CandyActivityContext;
import top.jacktgq.custom_controls.activity.CandyActivityObserver;
import top.jacktgq.custom_controls.window.CandyPrettyWindowWrapper;

public class ActivityStarter implements CandyActivityObserver {
    private CandyActivityContext context = new CandyActivityContext();
    private CandyPrettyWindowWrapper cpww;

    public ActivityStarter(CandyPrettyWindowWrapper cpww) {
        this.cpww = cpww;
        context.setObserver(this);
        context.setOwner(cpww.getStage());
        context.startActivity(null, ChooseSetupDirActivity.class, null);
    }

    @Override
    public void onActivityShow(CandyActivity a) {
        cpww.setContentView(a.getContentView(), 600, 400);
    }

    @Override
    public void onActivityDismiss(CandyActivity a) {

    }
}
