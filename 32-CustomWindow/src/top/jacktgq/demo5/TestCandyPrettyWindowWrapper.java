package top.jacktgq.demo5;

import javafx.application.Application;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.window.CandyPrettyWindowWrapper;

public class TestCandyPrettyWindowWrapper extends Application {

    CandyPrettyWindowWrapper cpww;

    @Override
    public void start(Stage primaryStage) {

        try {
            BorderPane root = new BorderPane();

            cpww = new CandyPrettyWindowWrapper(primaryStage);
            cpww.setContentView(root, 500, 600);
            cpww.setTitle("自定义窗口美化器");
            // cpww.initDragSupport(null, true);
            cpww.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
