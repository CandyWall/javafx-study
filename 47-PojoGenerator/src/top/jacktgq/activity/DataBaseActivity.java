package top.jacktgq.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import top.jacktgq.custom_controls.activity.CandyActivity;
import top.jacktgq.custom_controls.layout.CandyHBox;
import top.jacktgq.custom_controls.scrollPane.CandyListPane;
import top.jacktgq.custom_controls.toaster.CandyToaster;
import top.jacktgq.thread.CandyShortTask;

public class DataBaseActivity extends CandyActivity {
    private static final Image CHECK_ICON = new Image("icons/ic_check.png");
    private static final Image UNCHECK_ICON = new Image("icons/ic_uncheck.png");
    private CandyListPane listPane;

    @Override
    public void onCreate(Object intent) {
        BorderPane root = new BorderPane();
        Label titleLabel = new Label("请选择数据库");
        root.setTop(titleLabel);
        listPane = new CandyListPane();
        root.setCenter(listPane);
        this.setContentView(root);
        listPane.setId("database-name-list");

        Button next = new Button("下一步");
        next.setOnAction(e -> {
            List<String> databaseNameList = getSelectedItems();
            if (databaseNameList.size() > 0) {
                startActivity(TableActivity.class, databaseNameList);
            } else {
                CandyToaster.show(context.getOwner(), "您必须先选择一个数据库进行操作", CandyToaster.ERROR);
            }
        });
        HBox hbox = new HBox();
        hbox.getChildren().add(next);
        hbox.setAlignment(Pos.CENTER_RIGHT);
        hbox.setPadding(new Insets(10));
        root.setBottom(hbox);

        QueryRunner runner = (QueryRunner) context.getParam("queryRunner", null);
        new CandyShortTask() {
            private List<Map<String, Object>> mapList;

            @Override
            protected void doInBackground() throws Exception {
                mapList = runner.query("SELECT SCHEMATA.SCHEMA_NAME,SCHEMATA.DEFAULT_CHARACTER_SET_NAME FROM SCHEMATA",
                        new MapListHandler());
            }

            @Override
            protected void done() {
                if (mapList != null) {
                    for (Map<String, Object> map : mapList) {
                        listPane.add(new ItemView((String) map.get("SCHEMA_NAME"),
                                (String) map.get("DEFAULT_CHARACTER_SET_NAME")));
                    }
                }
            }
        }.execute();
    }

    // 获取所有选中项
    private List<String> getSelectedItems() {
        ArrayList<String> result = new ArrayList<>();
        for (Node item : listPane.getItems()) {
            ItemView itemView = (ItemView) item;
            if (itemView.isSelected()) {
                result.add(itemView.getDatabaseName());
            }
        }
        return result;
    }

    private EventHandler<MouseEvent> onMouseClickHandler = e -> {
        ItemView itemView = (ItemView) e.getSource();
        itemView.setSelected(!itemView.isSelected());
    };

    // 自定义每一行的显示
    private class ItemView extends CandyHBox {
        private boolean selected = false;
        private ImageView imageView;
        private Label databaseNameLabel;

        public ItemView(String databaseName, String characterName) {
            imageView = new ImageView(UNCHECK_ICON);
            databaseNameLabel = new Label(databaseName);
            Label characterNameLabel = new Label(characterName);
            this.getChildren().addAll(imageView, databaseNameLabel, characterNameLabel);
            this.setLayout("24px 1 1");
            this.getStyleClass().add("item");

            this.setOnMouseClicked(onMouseClickHandler);
        }

        // 当前行是否被选中
        public boolean isSelected() {
            return selected;
        }

        // 设置选中状态
        public void setSelected(boolean selected) {
            this.selected = selected;
            imageView.setImage(selected ? CHECK_ICON : UNCHECK_ICON);
        }

        public String getDatabaseName() {
            return databaseNameLabel.getText();
        }
    }
}
