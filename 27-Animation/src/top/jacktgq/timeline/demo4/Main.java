package top.jacktgq.timeline.demo4;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            HBox controlbar = new HBox();
            ToggleButton tb1 = new ToggleButton("播放");
            ToggleButton tb2 = new ToggleButton("暂停");
            ToggleButton tb3 = new ToggleButton("停止");
            ToggleButton tb4 = new ToggleButton("动画跳跃");
            ToggleButton tb5 = new ToggleButton("从头开始播放");
            ToggleButton tb6 = new ToggleButton("开始反向播放动画");
            ToggleGroup group = new ToggleGroup();
            group.getToggles().addAll(tb1, tb2, tb3, tb4, tb5, tb6);
            controlbar.getChildren().addAll(tb1, tb2, tb3, tb4, tb5, tb6);

            root.setTop(controlbar);

            AnchorPane an = new AnchorPane();
            root.setCenter(an);

            Rectangle rect = new Rectangle(100, 100, Color.PINK);
            an.getChildren().addAll(rect);

            Timeline timeline1 = new Timeline();
            KeyValue kv1 = new KeyValue(rect.rotateProperty(), 0);
            KeyFrame kf1 = new KeyFrame(Duration.millis(0), kv1);
            KeyValue kv2 = new KeyValue(rect.rotateProperty(), 360);
            KeyFrame kf2 = new KeyFrame(Duration.millis(1000), kv2);
            timeline1.getKeyFrames().addAll(kf1, kf2);

            Timeline timeline2 = new Timeline();
            KeyValue kv3 = new KeyValue(rect.translateXProperty(), 0);
            KeyFrame kf3 = new KeyFrame(Duration.millis(0), kv3);
            KeyValue kv4 = new KeyValue(rect.translateXProperty(), 300);
            KeyFrame kf4 = new KeyFrame(Duration.millis(1000), kv4);
            timeline2.getKeyFrames().addAll(kf3, kf4);

            timeline1.setOnFinished(e -> {
                timeline2.play();
            });

            // timeline.setDelay(Duration.seconds(1));
            // 设置循环次数，
            // timeline.setCycleCount(2);
            // timeline1.setCycleCount(Timeline.INDEFINITE);
            // 是否在逆运动
            // timeline1.setAutoReverse(true);
            // 动画播放速率为2倍速
            // timeline.setRate(5);
            // 默认为60帧
            System.out.println(timeline2.getTargetFramerate());

            Scene scene = new Scene(root, 1000, 800);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试时间线动画");
            primaryStage.show();

            tb1.setOnAction(e -> {
                timeline1.play();
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
