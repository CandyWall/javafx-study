package top.jacktgq.custom_controls.activity;

public interface CandyActivityObserver {
    // 显示
    public void onActivityShow(CandyActivity a);

    // 退出显示
    public void onActivityDismiss(CandyActivity a);

}
