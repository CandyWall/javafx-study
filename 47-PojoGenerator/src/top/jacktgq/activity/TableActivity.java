package top.jacktgq.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ColumnListHandler;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import top.jacktgq.custom_controls.activity.CandyActivity;
import top.jacktgq.custom_controls.layout.CandyHBox;
import top.jacktgq.custom_controls.scrollPane.CandyListPane;
import top.jacktgq.custom_controls.toaster.CandyToaster;
import top.jacktgq.thread.CandyShortTask;

public class TableActivity extends CandyActivity {
    private static final Image CHECK_ICON = new Image("icons/ic_check.png");
    private static final Image UNCHECK_ICON = new Image("icons/ic_uncheck.png");
    private CandyListPane listPane;

    @Override
    public void onCreate(Object intent) {
        BorderPane root = new BorderPane();
        HBox topPane = new HBox();
        Label titleLabel = new Label("请选择表");
        Label packageLabel = new Label("生成的包路径");
        TextField packageField = new TextField();
        topPane.setAlignment(Pos.CENTER_LEFT);
        topPane.setPadding(new Insets(10));
        topPane.setSpacing(10);
        topPane.getChildren().addAll(titleLabel, packageLabel, packageField);
        HBox.setHgrow(packageField, Priority.ALWAYS);
        root.setTop(topPane);
        listPane = new CandyListPane();
        root.setCenter(listPane);
        this.setContentView(root);
        listPane.setId("database-name-list");

        HBox bottomPane = new HBox();
        CheckBox checkBox = new CheckBox("全选/取消");
        checkBox.setOnAction(e -> {
            boolean selected = checkBox.isSelected();
            for (Node node : listPane.getItems()) {
                ItemView item = (ItemView) node;
                item.setSelected(selected);
            }
        });
        Label label = new Label();
        label.setMaxWidth(9999);
        Button next = new Button("生成POJO");
        next.setOnAction(e -> {
            List<String> tableNameList = getSelectedItems();
            String packageName = packageField.getText().trim();
            if (packageName.length() == 0 || !packageName.matches("^[a-z][a-z1-9]+([\\.][a-z][a-z1-9]*)*$")) {
                CandyToaster.show(context.getOwner(), "包路径有误！", CandyToaster.ERROR);
                return;
            }
            if (tableNameList.size() > 0) {
                HashMap<String, Object> paramsMap = new HashMap<>();
                paramsMap.put("tableNameList", tableNameList);
                paramsMap.put("packagePath", packageField.getText());
                startActivity(GenerateActivity.class, paramsMap);
            } else {
                CandyToaster.show(context.getOwner(), "您必须先选择一张表进行操作", CandyToaster.ERROR);
            }
        });
        bottomPane.getChildren().addAll(checkBox, label, next);
        bottomPane.setAlignment(Pos.CENTER);
        HBox.setHgrow(label, Priority.ALWAYS);
        bottomPane.setPadding(new Insets(10));
        root.setBottom(bottomPane);

        ArrayList<String> databaseNameList = (ArrayList<String>) intent;
        QueryRunner runner = (QueryRunner) context.getParam("queryRunner", null);
        new CandyShortTask() {
            private List<String> tablesMap;

            @Override
            protected void doInBackground() throws Exception {
                String sql = "SELECT TABLE_NAME FROM `TABLES` where TABLE_SCHEMA in (";
                int i = 0;
                for (; i < databaseNameList.size() - 1; i++) {
                    sql += "'" + databaseNameList.get(i) + "', ";
                }
                sql += "'" + databaseNameList.get(i) + "')";
                System.out.println(sql);

                tablesMap = runner.query(sql, new ColumnListHandler<>());
            }

            @Override
            protected void done() {
                if (tablesMap != null) {
                    for (String tableName : tablesMap) {
                        listPane.add(new ItemView(tableName));
                    }
                }
            }
        }.execute();
    }

    // 获取所有选中项
    private List<String> getSelectedItems() {
        ArrayList<String> result = new ArrayList<>();
        for (Node item : listPane.getItems()) {
            ItemView itemView = (ItemView) item;
            if (itemView.isSelected()) {
                result.add(itemView.getDatabaseName());
            }
        }
        return result;
    }

    private EventHandler<MouseEvent> onMouseClickHandler = e -> {
        ItemView itemView = (ItemView) e.getSource();
        itemView.setSelected(!itemView.isSelected());
    };

    // 自定义每一行的显示
    private class ItemView extends CandyHBox {
        private boolean selected = false;
        private ImageView imageView;
        private Label tableNameLabel;

        public ItemView(String tableName) {
            imageView = new ImageView(UNCHECK_ICON);
            tableNameLabel = new Label(tableName);
            this.getChildren().addAll(imageView, tableNameLabel);
            this.setLayout("24px 1");
            this.getStyleClass().add("item");

            this.setOnMouseClicked(onMouseClickHandler);
        }

        // 当前行是否被选中
        public boolean isSelected() {
            return selected;
        }

        // 设置选中状态
        public void setSelected(boolean selected) {
            this.selected = selected;
            imageView.setImage(selected ? CHECK_ICON : UNCHECK_ICON);
        }

        public String getDatabaseName() {
            return tableNameLabel.getText();
        }
    }
}
