package startDragAndDrop;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.image.CandyImagePane;

public class Main extends Application {
    private BorderPane root;
    private CandyImagePane imagePane;
    private Label label;

    @Override
    public void start(Stage primaryStage) {
        try {
            root = new BorderPane();

            label = new Label("发起拖放");
            root.setCenter(label);

            Scene scene = new Scene(root, 400, 400);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            initDragDrop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 发起拖放
    private void initDragDrop() {
        // 有东西被拖进来
        label.setOnDragDetected((e) -> {
            Dragboard db = root.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.putUrl("https://blog.csdn.net/qq_38505969");
            content.putHtml("<a href='https://blog.csdn.net/qq_38505969'> 阿发你好 </a>");
            content.putImage(snapshot(label));
            db.setContent(content);
            db.setDragView(snapshot(label));
            e.consume();
        });
    }

    // 获取节点图片
    private Image snapshot(Node node) {
        SnapshotParameters snapshotParameters = new SnapshotParameters();
        snapshotParameters.setFill(Color.AQUA);

        return node.snapshot(snapshotParameters, null);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
