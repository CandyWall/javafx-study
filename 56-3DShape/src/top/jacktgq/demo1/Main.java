package top.jacktgq.demo1;

import javafx.application.Application;
import javafx.application.ConditionalFeature;
import javafx.application.Platform;
import javafx.scene.DepthTest;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();

            System.out.println("当前系统是否支持3D：" + Platform.isSupported(ConditionalFeature.SCENE3D));
            Button btn1 = new Button("按钮1");
            btn1.setTranslateZ(0);
            // btn1.setDepthTest(DepthTest.ENABLE);
            Button btn2 = new Button("按钮2");
            btn2.setTranslateZ(100);
            // btn2.setDepthTest(DepthTest.ENABLE);
            Button btn3 = new Button("按钮3");
            btn3.setTranslateZ(200);
            // btn3.setDepthTest(DepthTest.ENABLE);
            Button btn4 = new Button("按钮4");
            btn4.setTranslateZ(50);
            btn4.setDepthTest(DepthTest.DISABLE);

            root.getChildren().addAll(btn1, btn2, btn3, btn4);
            root.setStyle("-fx-background: transparent");

            Scene scene = new Scene(root, 400, 400, true);
            scene.setCamera(new PerspectiveCamera());

            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("3D图形绘制");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
