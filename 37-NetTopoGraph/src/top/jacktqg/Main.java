package top.jacktqg;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    TopoPane root = new TopoPane();

    @Override
    public void start(Stage primaryStage) {
        try {
            Scene scene = new Scene(root, 800, 600);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            root.add(new TopoNode(new TopoData("王", "192.168.1.101", 0)));

            root.add(new TopoNode(new TopoData("李", "192.168.1.102", -1)));

            root.add(new TopoNode(new TopoData("邵", "192.168.1.103", -1)));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
