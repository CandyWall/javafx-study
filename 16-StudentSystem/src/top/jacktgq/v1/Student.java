package top.jacktgq.v1;

public class Student {
    public Integer id;
    public String name;
    public String phone;

    public Student(Integer id, String name, String phone) {
        this.id = id;
        this.name = name;
        this.phone = phone;
    }
}
