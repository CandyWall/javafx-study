package top.jacktgq.demo1;

import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

public class MyLayoutPane extends Pane {
    private Button b1 = new Button("左上");
    private Button b2 = new Button("居中");
    private Button b3 = new Button("右下");

    public MyLayoutPane() {
        getChildren().addAll(b1, b2, b3);
    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        double w = this.getWidth();
        double h = this.getHeight();
        double btnW = 60;
        double btnH = 30;
        b1.resizeRelocate(0, 0, btnW, btnH);
        b2.resizeRelocate((w - btnW) / 2, (h - btnH) / 2, btnW, btnH);
        b3.resizeRelocate(w - btnW, h - btnH, btnW, btnH);
    }
}
