package top.jacktgq.pieChart;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        PieChart.Data d1 = new PieChart.Data("空间16", 40);
        PieChart.Data d2 = new PieChart.Data("计科16", 30);
        PieChart.Data d3 = new PieChart.Data("网工16", 20);
        PieChart.Data d4 = new PieChart.Data("物联网16", 10);
        ObservableList<PieChart.Data> dataList = FXCollections.observableArrayList(d1, d2, d3, d4);
        PieChart pieChart = new PieChart(dataList);
        pieChart.setId("candy-pie-chart");

        // 是否显示提示文字和提示线，默认显示：true
        // pieChart.setLabelsVisible(true);
        // 提示线长度
        pieChart.setLabelLineLength(40);
        pieChart.setTitle("各专业毕业设计答辩通过人数");
        // 设置标题显示的位置
        pieChart.setTitleSide(Side.TOP);
        // 为true时顺时针绘制，否则逆时针绘制，默认为true
        pieChart.setClockwise(true);
        // 从45度角开始绘制
        pieChart.setStartAngle(45);
        // 是否显示图例
        pieChart.setLegendVisible(true);
        // 设置图例显示的位置
        pieChart.setLegendSide(Side.LEFT);
        // 当数据发生改变时是否显示动画
        pieChart.setAnimated(true);

        dataList.forEach(data -> {
            Tooltip tp = new Tooltip(data.getName() + " 通过人数：" + data.getPieValue() + "");
            tp.setFont(Font.font(20));
            Tooltip.install(data.getNode(), tp);
            data.pieValueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    tp.setText(data.getName() + " 通过人数：" + data.getPieValue() + "");
                }
            });
        });

        Button btn = new Button("确定");
        AnchorPane root = new AnchorPane();
        root.getChildren().addAll(btn, pieChart);
        AnchorPane.setLeftAnchor(pieChart, 100.0);

        primaryStage.setTitle("饼状图");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.getScene().getStylesheets().add(getClass().getResource("pieChart.css").toExternalForm());
        primaryStage.show();

        btn.setOnAction(e -> {
            d2.setPieValue((int) (Math.random() * 100));
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
