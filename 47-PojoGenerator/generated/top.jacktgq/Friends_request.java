package top.jacktgq; 

import java.util.Date; 

public class Friends_request { 

	private String accept_user_id ; 
	private String id ; 
	private Date request_date_time ; 
	private Byte request_status ; 
	private String send_user_id ; 


	public void setAccept_user_id(String accept_user_id) {
		this.accept_user_id=accept_user_id;
	}
	public String getAccept_user_id() {
		return this.accept_user_id;
	}
	public void setId(String id) {
		this.id=id;
	}
	public String getId() {
		return this.id;
	}
	public void setRequest_date_time(Date request_date_time) {
		this.request_date_time=request_date_time;
	}
	public Date getRequest_date_time() {
		return this.request_date_time;
	}
	public void setRequest_status(Byte request_status) {
		this.request_status=request_status;
	}
	public Byte getRequest_status() {
		return this.request_status;
	}
	public void setSend_user_id(String send_user_id) {
		this.send_user_id=send_user_id;
	}
	public String getSend_user_id() {
		return this.send_user_id;
	}

}
 