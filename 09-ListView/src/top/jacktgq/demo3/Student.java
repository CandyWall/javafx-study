package top.jacktgq.demo3;

public class Student {
    public int id;
    public String name;
    public boolean sex;
    public boolean checked;

    public Student() {
    }

    public Student(int id, String name, boolean sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Student [id=" + id + ", name=" + name + ", sex=" + sex + ", checked=" + checked + "]";
    }
}