package top.jacktgq.plot;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            // 创建一个画布，并指定画布的尺寸
            Canvas canvas = new Canvas(800, 800);
            double w = canvas.getWidth();
            double h = canvas.getHeight();
            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.setFill(Color.BLUE);
            // 在中央画一个矩形
            int rectW = 100;
            int rectH = 80;
            gc.fillRect((w - rectW) / 2, (h - rectH) / 2, rectW, rectH);
            gc.strokeRect(0, 0, rectW, rectH);
            gc.setStroke(Color.RED);
            gc.setLineWidth(5);
            gc.strokeRect(20, 20, rectW, rectH);
            // gc.fillRect(0, 0, w, h);

            root.setCenter(canvas);
            Scene scene = new Scene(root, 1000, 800);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
