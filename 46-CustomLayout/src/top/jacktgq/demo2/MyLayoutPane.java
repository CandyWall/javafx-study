package top.jacktgq.demo2;

import javafx.scene.control.Button;
import top.jacktgq.custom_controls.layout.CandyFreeLayoutPane;

public class MyLayoutPane extends CandyFreeLayoutPane {
    private Button b1 = new Button("左上");
    private Button b2 = new Button("居中");
    private Button b3 = new Button("右下");

    public MyLayoutPane() {
        this.addChild(b1, "100px", "32px", TOP_LEFT);
        this.addChild(b2, "20%", "32px", CENTER);
        this.addChild(b3, "100px", "60px", BOTTOM_RIGHT);
    }
}
