# javafx-study

## 介绍
javafx学习测试代码，内含各种javafx学习案例，以及本人封装和搜集的各种工具类，欢迎交流学习。

## 实用Javafx案例

以下精选了我做的比较好看实用的JavaFX案例，方便各位学习参考。

### 我封装JavaFX的工具类

参考`candy-javafx-extensions`

### 短消息提示框

参考`36-CandyToaster`

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210531204459922.gif#pic_center)

### 安装导航程序

参考`39-Setup`

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210531204459963.gif#pic_center)

### 简易图片浏览器1

参考`05-Album1`

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210531204501746.gif#pic_center)

### 简易图片浏览器2

参考`43-Album2`

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210531204500113.gif#pic_center)

### 糖果桌面透明可拖拽时钟

参考`26-CandyClock`

![糖果桌面时钟](https://img-blog.csdnimg.cn/20210521012452379.gif#pic_center)

### 自制pojo生成器

参考`47-PojoGenerator`

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210531204500264.gif#pic_center)

### 自制等待特效和等待对话框

参考`28-CandyWaitingAnimation`

![在这里插入图片描述](https://img-blog.csdnimg.cn/2021053120450036.gif#pic_center)

### 自制桌面透明落花特效

参考`27-1-CandyFlowerFallingBG`

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210531204837894.gif#pic_center)

