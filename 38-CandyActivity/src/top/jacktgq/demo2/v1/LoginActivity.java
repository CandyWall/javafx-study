package top.jacktgq.demo2.v1;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.activity.CandyActivity;

public class LoginActivity extends CandyActivity {

    @Override
    public void onCreate(Object intent) {
        Stage owner = (Stage) context.getParam("owner", null);
        if (owner != null) {
            owner.setTitle("用户登录");
            owner.setMaximized(false);
        }

        VBox root = new VBox();
        setContentView(root);

        TextField usernameFiled = new TextField();
        Button loginBtn = new Button("登录");
        loginBtn.setMaxWidth(9999);
        loginBtn.setOnAction(e -> {
            startActivity(MainActivity.class, usernameFiled.getText().trim());
        });
        root.getChildren().addAll(usernameFiled, loginBtn);
        root.setAlignment(Pos.CENTER);
        root.setSpacing(20);
        root.setPadding(new Insets(30));
    }

}
