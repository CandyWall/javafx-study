package top.jacktgq.flowerFalling;

import java.util.Random;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.PerspectiveCamera;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 *
 * @Title: FlowerFallingPane.java
 * @Package top.jacktgq.example.flowerFalling
 * @Description: 带有雪花飘落特效的面板
 * @author CandyWall
 * @date 2021年5月31日 下午5:22:55
 * @version V1.0
 */
public class FlowerFallingPane extends StackPane {
    private Random random = new Random();
    private double x;
    private double y;
    private double z;

    public FlowerFallingPane(double w, double h, int flowerCount) {
        setStyle("-fx-background: transparent");
        ImageView bgiv = new ImageView("images/寿沙都.png");
        DropShadow shadow = new DropShadow(5, 10, 10, Color.web("#aaa"));
        bgiv.setEffect(shadow);
        bgiv.setPreserveRatio(true);
        bgiv.setFitHeight(h - 20);
        shadow.setOffsetX(5);
        shadow.setOffsetY(5);
        ImageView[] ivs = new ImageView[flowerCount];
        // 加载一些落花
        for (int i = 0; i < flowerCount; i++) {
            ivs[i] = new ImageView("images/flower.png");
            ivs[i].setPreserveRatio(true);
            ivs[i].setFitWidth(100);
            ivs[i].setEffect(shadow);
            if (random.nextBoolean()) {
                x = random.nextDouble() * w + random.nextDouble() * 100;
            } else {
                x = random.nextDouble() * w - random.nextDouble() * 100;
            }
            y = random.nextDouble() * 50;
            z = random.nextDouble() * 2500;
            ivs[i].setTranslateX(x);
            ivs[i].setTranslateY(y);
            ivs[i].setTranslateZ(z);
        }

        AnchorPane ap = new AnchorPane();
        ap.setBackground(null);
        ap.getChildren().addAll(ivs);

        SubScene subScene = new SubScene(ap, w, h, true, SceneAntialiasing.BALANCED);
        PerspectiveCamera camera = new PerspectiveCamera();
        subScene.setCamera(camera);

        getChildren().addAll(bgiv, subScene);

        // 花瓣飘落动画
        for (int i = 0; i < flowerCount; i++) {
            double duration = random.nextDouble() * 10 + 5;
            TranslateTransition tt = new TranslateTransition(Duration.seconds(duration));
            tt.setFromX(ivs[i].getTranslateX());
            tt.setByX(400);

            tt.setFromY(ivs[i].getTranslateY());
            tt.setByY(1500);

            RotateTransition rt = new RotateTransition(Duration.seconds(duration));
            rt.setFromAngle(0);
            rt.setByAngle(random.nextDouble() * 361);

            FadeTransition ft1 = new FadeTransition(Duration.seconds(duration * 1 / 4));
            ft1.setFromValue(0);
            ft1.setToValue(1);
            FadeTransition ft2 = new FadeTransition(Duration.seconds(duration * 3 / 4));
            ft2.setFromValue(1);
            ft2.setToValue(0);
            SequentialTransition st = new SequentialTransition();
            st.getChildren().addAll(ft1, ft2);

            ParallelTransition pt = new ParallelTransition();
            pt.setInterpolator(Interpolator.LINEAR);
            pt.setNode(ivs[i]);
            pt.getChildren().addAll(tt, rt, st);
            pt.setCycleCount(Animation.INDEFINITE);
            pt.play();
        }
    }
}
