package top.jacktgq.custom_controls.layout;

import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public abstract class CandyLayoutPane extends Pane {

    @Override
    protected void layoutChildren() {
        double w = getWidth(), h = getHeight();
        if (w <= 0 || h <= 0)
            return;

        layoutChildren(w, h);
    }

    protected abstract void layoutChildren(double w, double h);

    //
    protected final void layout(Node node, Rectangle2D rect) {
        node.resizeRelocate(rect.getMinX(), rect.getMinY(), rect.getWidth(), rect.getHeight());
    }

}
