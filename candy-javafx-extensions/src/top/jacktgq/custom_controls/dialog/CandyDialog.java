package top.jacktgq.custom_controls.dialog;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

public class CandyDialog<T> extends Stage {
    private Scene scene;
    private boolean status = false;
    protected T result; // 该对话框需要传出去的值

    /**
     * 
     * @param owner
     *            设置对话框的父窗口
     */
    public CandyDialog(Window owner) {
        initOwner(owner);
        initModality(Modality.WINDOW_MODAL);
        // 可以在子类中再去调用，或者根据需求在子类中重写centerInParent()方法的逻辑
        // centerInParent();
    }

    /**
     * 
     * @param anchor
     *            也可以传一个控件，可以调用api获取控件的父窗口
     */
    public CandyDialog(Node anchor) {
        this(anchor.getScene().getWindow());
    }

    public void setDialogPane(Parent root) {
        setDialogPane(root, -1, -1);
    }

    /**
     * 传入scene的宽高后，可以调用centerInParent定位到父窗口的中间
     * 
     * @param root
     * @param sceneWidth
     * @param sceneHeight
     */
    public void setDialogPane(Parent root, double sceneWidth, double sceneHeight) {
        if (sceneWidth < 0 && sceneHeight < 0) {
            scene = new Scene(root);
        } else {
            scene = new Scene(root, sceneWidth, sceneHeight);
        }
        setScene(scene);
        sizeToScene();
    }

    // final: 子类不可以override 此方法
    public final void accept() {
        status = true;
        close(); // 调用close方法会关闭对话框，并解除showAndWait()造成的阻塞问题
    }

    // final: 子类不可以override 此方法
    public final void cancel() {
        status = false;
        close(); // 调用close方法会关闭对话框，并解除showAndWait()造成的阻塞问题
    }

    // public final void

    public boolean exec() {
        // 会阻塞住，直到用户点确认按钮accept或者关闭窗口才会返回
        showAndWait();
        return status;
    }

    public T getResult() {
        return result;
    }

    public void resize(double w, double h) {
        setWidth(w);
        setHeight(h);
    }

    public void relocate(double x, double y) {
        setX(x);
        setY(y);
    }

    // 相对于父窗口中心显示
    // 如果当前对话框没有设置宽高，会导致scene.getWidth()和scene.getHeight()的值为NAN，居中会失败
    // 所以最好显示给scene设置宽高
    public void centerInParent() {
        Window owner = this.getOwner();
        if (owner != null) {
            // 父窗口的位置px, py, 大小 pw, ph
            double px = owner.getX(), py = owner.getY();
            double pw = owner.getWidth(), ph = owner.getHeight();

            double dx = (pw - scene.getWidth()) / 2;
            double dy = (ph - scene.getHeight()) / 2;

            relocate(px + dx, py + dy);
        }
    }
}
