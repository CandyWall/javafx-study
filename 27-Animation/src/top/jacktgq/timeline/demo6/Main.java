package top.jacktgq.timeline.demo6;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            HBox controlbar = new HBox();
            ToggleButton tb1 = new ToggleButton("播放");
            ToggleButton tb2 = new ToggleButton("暂停");
            ToggleButton tb3 = new ToggleButton("停止");
            ToggleButton tb4 = new ToggleButton("动画跳跃");
            ToggleButton tb5 = new ToggleButton("从头开始播放");
            ToggleButton tb6 = new ToggleButton("开始反向播放动画");
            ToggleGroup group = new ToggleGroup();
            group.getToggles().addAll(tb1, tb2, tb3, tb4, tb5, tb6);
            controlbar.getChildren().addAll(tb1, tb2, tb3, tb4, tb5, tb6);

            root.setTop(controlbar);

            AnchorPane an = new AnchorPane();
            root.setCenter(an);

            Rectangle rect = new Rectangle(100, 100, Color.PINK);
            an.getChildren().addAll(rect);

            Timeline timeline = new Timeline();
            rect.setLayoutX(100);
            rect.setLayoutY(100);
            // 自转
            Rotate rotate1 = new Rotate(0, 50, 50);
            // 公转
            Rotate rotate2 = new Rotate(0, 200, 200);

            KeyValue kv1 = new KeyValue(rotate1.angleProperty(), 0);
            KeyValue kv3 = new KeyValue(rotate2.angleProperty(), 0);
            KeyFrame kf1 = new KeyFrame(Duration.millis(0), kv1, kv3);

            KeyValue kv2 = new KeyValue(rotate1.angleProperty(), 360);
            KeyValue kv4 = new KeyValue(rotate2.angleProperty(), 360);
            KeyFrame kf2 = new KeyFrame(Duration.millis(1000), kv2, kv4);

            // 先自转再公转会出问题
            // rect.getTransforms().addAll(rotate1, rotate2);
            rect.getTransforms().addAll(rotate2, rotate1);
            // 先公转再自转才能实现边自转边公转的效果
            timeline.getKeyFrames().addAll(kf1, kf2);

            // timeline.setDelay(Duration.seconds(1));
            // 设置循环次数，
            // timeline.setCycleCount(2);
            timeline.setCycleCount(Timeline.INDEFINITE);
            // 是否在逆运动
            // timeline.setAutoReverse(true);
            // 动画播放速率为2倍速
            // timeline.setRate(5);
            // 默认为60帧

            Scene scene = new Scene(root, 1000, 800);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试时间线动画");
            primaryStage.show();

            tb1.setOnAction(e -> {
                timeline.play();
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
