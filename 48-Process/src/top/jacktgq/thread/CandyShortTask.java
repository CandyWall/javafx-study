package top.jacktgq.thread;

import javafx.application.Platform;

/**
 * 
 * @Title: CandyShortTask.java
 * @Package top.jacktgq.utils.thread
 * @Description: 短任务工具类
 * @author CandyWall
 * @date 2020年1月22日 下午4:27:27
 * @version V1.0
 */
public abstract class CandyShortTask extends Thread {
    protected Object[] args; // 参数列表
    protected Exception err; // 任务出错信息
    // 传入参数，并启动线程

    public CandyShortTask(Object... args) {
        this.args = args;
    }

    public void execute() {
        this.start(); // 启动线程
    }

    @Override
    public void run() {
        // 在后台执行短任务
        try {
            doInBackground();
        } catch (Exception e) {
            err = e;
        }

        // 完成以后更新页面
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                done();
            }
        });
    }

    // 在后台处理任务
    protected abstract void doInBackground() throws Exception;

    // 完成以后更新页面
    // 如果this.err != null则说明doInbBackground()里面出错了
    protected abstract void done();
}
