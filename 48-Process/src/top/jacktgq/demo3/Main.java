package top.jacktgq.demo3;

import java.io.File;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.image.CandyImagePane;
import top.jacktgq.custom_controls.waiting.canvas.CandyCanvasWaitingBarDialog;
import top.jacktgq.thread.CandyShortTask;
import top.jacktgq.utils.media.Transcode;

public class Main extends Application {
    private CandyImagePane imagePane;
    private CandyCanvasWaitingBarDialog dialog;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            HBox toolbar = new HBox();
            Button openFilebtn = new Button("打开文件");
            toolbar.getChildren().add(openFilebtn);
            toolbar.setAlignment(Pos.CENTER);
            openFilebtn.setOnAction(e -> {
                FileChooser chooser = new FileChooser();
                chooser.setTitle("选择视频");
                chooser.getExtensionFilters()
                        .addAll(new FileChooser.ExtensionFilter("All Videos", "*.mp4;*.wmv;*.avi;*.mov;*.flv;*.f4v"));
                chooser.setInitialDirectory(new File("testData/"));
                File file = chooser.showOpenDialog(primaryStage);
                if (file != null) {
                    showMediaSnapshot(primaryStage, file);
                }
            });
            root.setTop(toolbar);

            imagePane = new CandyImagePane();
            root.setCenter(imagePane);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("视频抓帧截图");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showMediaSnapshot(Stage primaryStage, File file) {
        new CandyShortTask() {
            private File thumbFile;

            @Override
            protected void doInBackground() throws Exception {
                // 图片加个时间戳
                String timestamp = LocalTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
                thumbFile = new File("video-thumb-" + timestamp + ".jpg");
                File ffmpgFile = new File("tools/ffmpeg/ffmpeg.exe");
                new Transcode(ffmpgFile.getAbsolutePath(), file, thumbFile).thumbOfVideo(200);
            }

            @Override
            protected void done() {
                // 显示缩略图
                if (thumbFile.exists()) {
                    imagePane.setImage(thumbFile);
                } else {
                    System.out.println("视频抓帧失败！");
                }

                if (dialog != null) {
                    dialog.close();
                }

            }
        }.execute();

        dialog = new CandyCanvasWaitingBarDialog(primaryStage);
        dialog.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
