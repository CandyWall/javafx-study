package top.jacktgq.stackedBarChart;

import javafx.application.Application;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button btn = new Button("确定");
        FlowPane root = new FlowPane();
        root.getChildren().addAll(btn, getBarChart1());

        primaryStage.setTitle("堆叠柱状图");
        primaryStage.setScene(new Scene(root, 1800, 800));
        primaryStage.getScene().getStylesheets().add(getClass().getResource("stackedBarChart.css").toExternalForm());
        primaryStage.show();

        btn.setOnAction(e -> {
        });
    }

    private StackedBarChart getBarChart1() {
        StackedBarChart.Data<String, Number> d1 = new StackedBarChart.Data<String, Number>("空间16", 40);
        StackedBarChart.Data<String, Number> d2 = new StackedBarChart.Data<String, Number>("计科16", 28);
        StackedBarChart.Data<String, Number> d3 = new StackedBarChart.Data<String, Number>("网工16", 36);
        StackedBarChart.Data<String, Number> d4 = new StackedBarChart.Data<String, Number>("物联网16", 32);
        // ObservableList<BarChart.Data<String, Number>> data1 = FXCollections.observableArrayList(d1, d2, d3, d4);
        XYChart.Series<String, Number> series1 = new XYChart.Series<>();
        series1.setName("答辩通过人数");
        series1.getData().addAll(d1, d2, d3, d4);
        StackedBarChart.Data<String, Number> d5 = new StackedBarChart.Data<String, Number>("空间16", 46);
        StackedBarChart.Data<String, Number> d6 = new StackedBarChart.Data<String, Number>("计科16", 40);
        StackedBarChart.Data<String, Number> d7 = new StackedBarChart.Data<String, Number>("网工16", 42);
        StackedBarChart.Data<String, Number> d8 = new StackedBarChart.Data<String, Number>("物联网16", 44);
        // ObservableList<BarChart.Data<String, Number>> data2 = FXCollections.observableArrayList(d5, d6, d7, d8);
        XYChart.Series<String, Number> series2 = new XYChart.Series<>();
        series2.setName("参与答辩人数");
        series2.getData().addAll(d5, d6, d7, d8);

        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("专业");
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("人数");
        StackedBarChart barChart = new StackedBarChart(xAxis, yAxis);
        barChart.getData().addAll(series1, series2);
        barChart.setId("candy-bar-chart");

        // 是否显示提示文字和提示线，默认显示：true
        // barChart.setLabelsVisible(true);
        barChart.setTitle("各专业毕业设计答辩情况统计1");
        // 设置标题显示的位置
        barChart.setTitleSide(Side.TOP);
        // 是否显示图例
        barChart.setLegendVisible(true);
        // 设置图例显示的位置
        barChart.setLegendSide(Side.LEFT);
        // 当数据发生改变时是否显示动画
        barChart.setAnimated(true);
        return barChart;
    }

    public static void main(String[] args) {
        launch(args);
    }
}