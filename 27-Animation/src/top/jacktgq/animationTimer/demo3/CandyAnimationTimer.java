package top.jacktgq.animationTimer.demo3;

import javafx.animation.AnimationTimer;

/**
 * 
 * @Title: CandyAnimationTimer.java
 * @Package animationTimer.demo3
 * @Description: 基于AnimationTimer进行了封装，降低AnimationTimer刷新的帧率
 * @author CandyWall
 * @date 2021年5月18日 下午8:45:19
 * @version V1.0
 */
public abstract class CandyAnimationTimer extends AnimationTimer {
    private long interval;
    private long lastTime;

    // interval 定时器间隔, 毫秒值
    public CandyAnimationTimer(long interval) {
        this.interval = interval * 1000000;
    }

    @Override
    public void handle(long now) {
        if (now - lastTime >= interval) {
            lastTime = now;
            /*
             * if (lastTime == 0) { lastTime = now; } else { lastTime +=
             * interval; }
             */

            // 更新动画显示
            updateUI(now);
        }
    }

    // 调用者需实现这个方法
    public abstract void updateUI(long now);
}
