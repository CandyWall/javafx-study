package top.jacktgq.custom_controls.activity;

import javafx.scene.Parent;

public abstract class CandyActivity {
    protected CandyActivityContext context;
    protected Parent contentView;

    protected boolean created = false;
    protected boolean started = false;

    protected Object intent; // 外部参数数据

    // 额外属性, 可以方便操作
    public String name;
    public Object userData;

    public CandyActivity() {

    }

    ////////////////////////////////

    public Parent getContentView() {
        return contentView;
    }

    public void setContentView(Parent contentView) {
        this.contentView = contentView;
    }

    public void setContext(CandyActivityContext context) {
        this.context = context;
    }

    /////////////////////////////

    // 结束本Activity
    public final void finish() {
        if (context != null) {
            context.finish(this);
        }
    }

    // 结束本Activity, 并返回到上一个Activity
    public final void back() {
        if (context != null) {
            context.back(this);
        }
    }

    // 启动新的Activity
    public final void startActivity(Class<? extends CandyActivity> clazz, Object data) {
        if (context != null) {
            context.startActivity(this, clazz, data);
        }
    }

    ///////////////////////////////////////////

    // 生命期回调
    public abstract void onCreate(Object intent);

    public void onDestroy() {

    }

    public void onStart() {

    }

    public void onStop() {

    }

}
