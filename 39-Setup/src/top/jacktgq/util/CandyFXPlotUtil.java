package top.jacktgq.util;

import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;

/**
 * 
 * @Title: FXPlotUtil.java
 * @Package application
 * @Description: JavaFX绘制时用到的工具类
 * @author CandyWall
 * @date 2021年5月16日 下午10:33:54
 * @version V1.0
 */
public class CandyFXPlotUtil {
    public static void pathRoundRect(GraphicsContext gc, Rectangle2D rect, double radius) {
        gc.beginPath();
        double w = rect.getWidth();
        double h = rect.getHeight();
        if (radius > w / 2)
            radius = w / 2;
        if (radius > h / 2)
            radius = h / 2;
        // 左上角开始
        double cx, cy;
        double x1, y1, x2, y2;
        cx = rect.getMinX() + radius;
        cy = rect.getMinY() + radius;
        gc.arc(cx, cy, radius, radius, 90, 90);

        x2 = rect.getMinX();
        y2 = rect.getMaxY() - radius;
        gc.lineTo(x2, y2);

        cx = rect.getMinX() + radius;
        cy = rect.getMaxY() - radius;
        gc.arc(cx, cy, radius, radius, 180, 90);

        x2 = rect.getMaxX() - radius;
        y2 = rect.getMaxY();
        gc.lineTo(x2, y2);

        cx = rect.getMaxX() - radius;
        cy = rect.getMaxY() - radius;
        gc.arc(cx, cy, radius, radius, 270, 90);

        x2 = rect.getMaxX();
        y2 = rect.getMinY() + radius;
        gc.lineTo(x2, y2);

        cx = rect.getMaxX() - radius;
        cy = rect.getMinY() + radius;
        gc.arc(cx, cy, radius, radius, 0, 90);

        gc.closePath();
    }

}
