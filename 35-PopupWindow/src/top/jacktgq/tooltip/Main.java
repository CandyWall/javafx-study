package top.jacktgq.tooltip;

import javafx.application.Application;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.stage.Popup;
import javafx.stage.PopupWindow.AnchorLocation;
import javafx.stage.Stage;

public class Main extends Application {
    private Popup popup;

    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();
            Button btn1 = new Button("鼠标移上去显示默认的tooltip");
            Button btn2 = new Button("移上去显示定制后的tooltip");

            // 简便写法
            btn1.setTooltip(new Tooltip("自然选择，前进四！"));

            // 复杂写法
            Tooltip tooltip = new Tooltip("混沌未分天地乱，茫茫渺渺无人见，自从盘古破鸿蒙，开辟从兹清浊辨。覆载群生仰至仁,发明万物皆成善。欲知造化会元功,须看西游释厄传。");
            tooltip.setPrefWidth(200);
            tooltip.setWrapText(true);
            tooltip.setStyle("-fx-background-color: rgb(255,255,219); -fx-text-fill: #666;");
            Tooltip.install(btn2, tooltip);

            root.getChildren().addAll(btn1, btn2);
            Scene scene = new Scene(root, 400, 400);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            createPopupWindow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 创建弹出式窗口
    private void createPopupWindow() {
        Label label = new Label("自然选择，前进四");
        label.setWrapText(true);
        label.setPrefSize(300, 200);
        label.setStyle("-fx-background-color: rgb(255,255,217);" + "-fx-alignment:center;" + "-fx-font-size: 20;"
                + "-fx-border-width:1;" + "-fx-border-color: #aaa;" + "-fx-text-fill: #666;");

        popup = new Popup();
        popup.getScene().setRoot(label); // 指定根节点
        popup.sizeToScene(); // 调整窗口大小
        popup.setAutoHide(true); // 失去焦点自动隐藏
        popup.setHideOnEscape(true); // 按下ESC键时隐藏
        popup.setAnchorLocation(AnchorLocation.CONTENT_TOP_LEFT); // 左上角为锚点
    }

    private void showPopupWindow(Node node) {
        // 相对坐标 -> 屏幕坐标
        Bounds bounds = node.localToScreen(node.getBoundsInLocal());

        // node指定父结点，anchorX， anchorY 指定位置
        popup.show(node, bounds.getMinX(), bounds.getMaxY());
    }

    public static void main(String[] args) {
        launch(args);
    }
}
