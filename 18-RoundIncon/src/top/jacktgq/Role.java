package top.jacktgq;

public class Role {
    public int id;
    public String name;
    public boolean sex;

    public Role(int id, String name, boolean sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
    }
}
