package top.jacktgq.barChart;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button btn = new Button("确定");
        FlowPane root = new FlowPane();
        root.getChildren().addAll(btn, getBarChart1(), getBarChart2(), getBarChart3(), getBarChart4());

        primaryStage.setTitle("柱状图");
        primaryStage.setScene(new Scene(root, 1800, 800));
        primaryStage.getScene().getStylesheets().add(getClass().getResource("scatterChart.css").toExternalForm());
        primaryStage.show();

        btn.setOnAction(e -> {
        });
    }

    private BarChart getBarChart1() {
        BarChart.Data<String, Number> d1 = new BarChart.Data<String, Number>("空间16", 40);
        BarChart.Data<String, Number> d2 = new BarChart.Data<String, Number>("计科16", 28);
        BarChart.Data<String, Number> d3 = new BarChart.Data<String, Number>("网工16", 36);
        BarChart.Data<String, Number> d4 = new BarChart.Data<String, Number>("物联网16", 32);
        ObservableList<BarChart.Data<String, Number>> data1 = FXCollections.observableArrayList(d1, d2, d3, d4);
        XYChart.Series<String, Number> series1 = new XYChart.Series<>("答辩通过人数", data1);
        ObservableList<XYChart.Series<String, Number>> dataList = FXCollections.observableArrayList(series1);

        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("专业");
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("人数");
        BarChart barChart = new BarChart(xAxis, yAxis, dataList);
        barChart.setId("candy-bar-chart");

        // 是否显示提示文字和提示线，默认显示：true
        // barChart.setLabelsVisible(true);
        barChart.setTitle("各专业毕业设计答辩情况统计1");
        // 设置标题显示的位置
        barChart.setTitleSide(Side.TOP);
        // 是否显示图例
        barChart.setLegendVisible(true);
        // 设置图例显示的位置
        barChart.setLegendSide(Side.LEFT);
        // 当数据发生改变时是否显示动画
        barChart.setAnimated(true);
        return barChart;
    }

    private BarChart getBarChart2() {
        BarChart.Data<String, Number> d1 = new BarChart.Data<String, Number>("空间16", 40);
        BarChart.Data<String, Number> d2 = new BarChart.Data<String, Number>("计科16", 28);
        BarChart.Data<String, Number> d3 = new BarChart.Data<String, Number>("网工16", 36);
        BarChart.Data<String, Number> d4 = new BarChart.Data<String, Number>("物联网16", 32);
        // ObservableList<BarChart.Data<String, Number>> data1 = FXCollections.observableArrayList(d1, d2, d3, d4);
        XYChart.Series<String, Number> series1 = new XYChart.Series<>();
        series1.setName("答辩通过人数");
        series1.getData().addAll(d1, d2, d3, d4);
        BarChart.Data<String, Number> d5 = new BarChart.Data<String, Number>("空间16", 46);
        BarChart.Data<String, Number> d6 = new BarChart.Data<String, Number>("计科16", 40);
        BarChart.Data<String, Number> d7 = new BarChart.Data<String, Number>("网工16", 42);
        BarChart.Data<String, Number> d8 = new BarChart.Data<String, Number>("物联网16", 44);
        // ObservableList<BarChart.Data<String, Number>> data2 = FXCollections.observableArrayList(d5, d6, d7, d8);
        XYChart.Series<String, Number> series2 = new XYChart.Series<>();
        series2.setName("答辩总人数");
        series2.getData().addAll(d5, d6, d7, d8);
        // ObservableList<XYChart.Series<String, Number>> dataList = FXCollections.observableArrayList(series1, series2);

        series1.getData().forEach(data -> {
            data.setNode(new Label(data.getYValue() + ""));
        });

        CategoryAxis xAxis = new CategoryAxis();
        // x轴的相关参数
        xAxis.setLabel("专业");
        // 距离左（上）边的距离
        // xAxis.setStartMargin(50);
        // 距离右（下）边的距离
        // xAxis.setEndMargin(50);
        // 前后是否留空间，默认为true
        // xAxis.setGapStartAndEnd(true);
        // 轴位置
        // xAxis.setSide(Side.TOP);
        // 刻度标签颜色
        xAxis.setTickLabelFill(Color.web("#ffcc00"));
        // 刻度标签字体
        xAxis.setTickLabelFont(Font.font(16));
        // 类型名称与轴刻度的距离
        xAxis.setTickLabelGap(0);
        // 类型名称旋转
        xAxis.setTickLabelRotation(45);
        // 是否显示刻度标签
        xAxis.setTickLabelsVisible(true);
        // 刻度长度
        xAxis.setTickLength(20);
        // 是否显示刻度
        xAxis.setTickMarkVisible(true);

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("人数");
        BarChart barChart = new BarChart(xAxis, yAxis);
        barChart.getData().addAll(series1, series2);
        barChart.setId("candy-bar-chart");

        // 相邻柱子之间的间隔
        barChart.setBarGap(10);
        // 相邻类目之间的间隔
        barChart.setCategoryGap(15);
        // 从右往左开始绘制 RIGHT_TO_LEFT
        barChart.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
        barChart.setTitle("各专业毕业设计答辩情况统计2");
        // 设置标题显示的位置
        barChart.setTitleSide(Side.TOP);
        // 是否显示图例
        barChart.setLegendVisible(true);
        // 设置图例显示的位置
        barChart.setLegendSide(Side.LEFT);
        // 当数据发生改变时是否显示动画
        barChart.setAnimated(true);
        return barChart;
    }

    private BarChart getBarChart3() {
        BarChart.Data<String, Number> d1 = new BarChart.Data<String, Number>("答辩通过人数", 40);
        BarChart.Data<String, Number> d2 = new BarChart.Data<String, Number>("答辩通过人数", 28);
        BarChart.Data<String, Number> d3 = new BarChart.Data<String, Number>("答辩通过人数", 36);
        BarChart.Data<String, Number> d4 = new BarChart.Data<String, Number>("答辩通过人数", 32);
        
        BarChart.Data<String, Number> d5 = new BarChart.Data<String, Number>("答辩总人数", 46);
        BarChart.Data<String, Number> d6 = new BarChart.Data<String, Number>("答辩总人数", 40);
        BarChart.Data<String, Number> d7 = new BarChart.Data<String, Number>("答辩总人数", 42);
        BarChart.Data<String, Number> d8 = new BarChart.Data<String, Number>("答辩总人数", 44);
        XYChart.Series<String, Number> series1 = new XYChart.Series<>();
        series1.setName("空间16");
        series1.getData().addAll(d1, d5);

        XYChart.Series<String, Number> series2 = new XYChart.Series<>();
        series2.setName("计科16");
        series2.getData().addAll(d2, d6);

        XYChart.Series<String, Number> series3 = new XYChart.Series<>();
        series3.setName("网工16");
        series3.getData().addAll(d3, d7);

        XYChart.Series<String, Number> series4 = new XYChart.Series<>();
        series4.setName("物联网16");
        series4.getData().addAll(d4, d8);

        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("类别");
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("人数");
        BarChart barChart = new BarChart(xAxis, yAxis);
        barChart.getData().addAll(series1, series2, series3, series4);
        barChart.setId("candy-bar-chart");

        // 是否显示提示文字和提示线，默认显示：true
        // barChart.setLabelsVisible(true);
        barChart.setTitle("各专业毕业设计答辩情况统计4");
        // 设置标题显示的位置
        barChart.setTitleSide(Side.TOP);
        // 是否显示图例
        barChart.setLegendVisible(true);
        // 设置图例显示的位置
        barChart.setLegendSide(Side.LEFT);
        // 当数据发生改变时是否显示动画
        barChart.setAnimated(true);
        return barChart;
    }

    private BarChart getBarChart4() {
        BarChart.Data<Number, String> d1 = new BarChart.Data<Number, String>( 40, "答辩通过人数");
        BarChart.Data<Number, String> d2 = new BarChart.Data<Number, String>( 28, "答辩通过人数");
        BarChart.Data<Number, String> d3 = new BarChart.Data<Number, String>( 36, "答辩通过人数");
        BarChart.Data<Number, String> d4 = new BarChart.Data<Number, String>( 32, "答辩通过人数");

        BarChart.Data<Number, String> d5 = new BarChart.Data<Number, String>( 46, "答辩总人数");
        BarChart.Data<Number, String> d6 = new BarChart.Data<Number, String>( 40, "答辩总人数");
        BarChart.Data<Number, String> d7 = new BarChart.Data<Number, String>( 42, "答辩总人数");
        BarChart.Data<Number, String> d8 = new BarChart.Data<Number, String>( 44, "答辩总人数");
        XYChart.Series<Number, String> series1 = new XYChart.Series<>();
        series1.setName("空间16");
        series1.getData().addAll(d1, d5);

        XYChart.Series<Number, String> series2 = new XYChart.Series<>();
        series2.setName("计科16");
        series2.getData().addAll(d2, d6);

        XYChart.Series<Number, String> series3 = new XYChart.Series<>();
        series3.setName("网工16");
        series3.getData().addAll(d3, d7);

        XYChart.Series<Number, String> series4 = new XYChart.Series<>();
        series4.setName("物联网16");
        series4.getData().addAll(d4, d8);

        CategoryAxis yAxis = new CategoryAxis();
        yAxis.setLabel("类别");
        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("人数");
        BarChart barChart = new BarChart(xAxis, yAxis);
        barChart.getData().addAll(series1, series2, series3, series4);
        barChart.setId("candy-bar-chart");

        // 是否显示提示文字和提示线，默认显示：true
        // barChart.setLabelsVisible(true);
        barChart.setTitle("各专业毕业设计答辩情况统计3");
        // 设置标题显示的位置
        barChart.setTitleSide(Side.TOP);
        // 是否显示图例
        barChart.setLegendVisible(true);
        // 设置图例显示的位置
        barChart.setLegendSide(Side.LEFT);
        // 当数据发生改变时是否显示动画
        barChart.setAnimated(true);
        return barChart;
    }

    public static void main(String[] args) {
        launch(args);
    }
}