package top.jacktgq.contextMenu;

import javafx.scene.control.Menu;
import javafx.scene.image.Image;

public class CandyMenu extends Menu {
    private CandyMenuItemView node = new CandyMenuItemView();

    public CandyMenu(String text) {
        node.text = text;

        this.setGraphic(node);

        // 可选中的菜单
        node.setOnMouseClicked((e) -> {
            if (node.checkable) {
                node.checked = !node.checked;
                update();
            }
        });
    }

    public void setText2(String text) {
        node.text = text;
        update();
    }

    public void setIcon(Image icon) {
        node.icon = icon;
    }

    public void setIconCheck(Image iconCheck) {
        node.iconCheck = iconCheck;
    }

    // 更新显示
    public void update() {
        node.requestLayout();
    }

    ////////////// MENU子菜单应该不能勾选 /////////

    // 复选框
    public void setCheckable(boolean checkable) {
        node.checkable = checkable;
    }

    public void setChecked(boolean checked) {
        node.checked = checked;
    }

    public boolean isChecked() {
        return node.checked;
    }

}
