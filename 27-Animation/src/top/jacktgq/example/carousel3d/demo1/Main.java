package top.jacktgq.example.carousel3d.demo1;

import java.io.File;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();
            Button btn1 = new Button("开始");
            Button btn2 = new Button("逆向");

            File imageDir = new File("images");
            Image[] images = null;
            if (imageDir.exists()) {
                File[] imageFiles = imageDir.listFiles();
                images = new Image[imageFiles.length];
                for (int i = 0; i < imageFiles.length; i++) {
                    images[i] = new Image(imageFiles[i].toURI().toString());
                }
            }
            // 轮播图面板
            CarouselPane carouselPane = new CarouselPane(800, 300, images);

            root.getChildren().addAll(btn1, btn2, carouselPane);
            AnchorPane.setTopAnchor(carouselPane, 200.0);
            AnchorPane.setLeftAnchor(carouselPane, 500.0);
            AnchorPane.setLeftAnchor(btn2, 100.0);

            Scene scene = new Scene(root, 1400, 800);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("3D轮播图");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
