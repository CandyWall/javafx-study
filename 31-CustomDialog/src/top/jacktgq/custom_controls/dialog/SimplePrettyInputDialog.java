package top.jacktgq.custom_controls.dialog;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Window;

public class SimplePrettyInputDialog extends CandyPrettyDialog<String> {

    public SimplePrettyInputDialog(Window owner) {
        super(owner);
        initLayout();
    }

    public SimplePrettyInputDialog(Node anchor) {
        super(anchor);
        initLayout();
    }

    private void initLayout() {
        HBox root = new HBox();
        TextField textField = new TextField();
        Button okBtn = new Button("确定");
        okBtn.setOnAction(e -> {
            // 获取对话框中的值
            result = textField.getText();
            if (result.trim().length() > 0) {
                accept();
            } else {
                Alert errorAlert = new Alert(AlertType.ERROR);
                errorAlert.setTitle("错误！");
                errorAlert.setHeaderText("出错");
                errorAlert.setContentText("输入的信息不能为空！");
                errorAlert.showAndWait();
            }
        });
        Button cancelBtn = new Button("取消");
        cancelBtn.setOnAction(e -> {
            cancel();
        });
        root.getChildren().addAll(textField, okBtn, cancelBtn);
        root.setSpacing(10);
        root.setPadding(new Insets(10));

        setDialogPane(root, 300, 40);
        centerInParent();
        setDialogTitle("请输入昵称");
    }

}
