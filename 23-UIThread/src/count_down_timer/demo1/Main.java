package count_down_timer.demo1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
    private Label label;

    @Override
    public void start(Stage primaryStage) {
        try {
            StackPane root = new StackPane();

            Button button = new Button("开始倒计时");
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    for (int i = 5; i >= 0; i--) {
                        label.setText(i + "");
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
            });

            label = new Label("5");

            root.getChildren().addAll(button, label);

            StackPane.setAlignment(button, Pos.TOP_CENTER);
            StackPane.setAlignment(label, Pos.CENTER);

            label.getStyleClass().add("display");
            root.getStyleClass().add("main");

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("倒计时效果");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
