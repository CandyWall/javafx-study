package top.jacktgq.demo2;

import javafx.scene.image.Image;
import top.jacktgq.custom_controls.image.CandyImagePane;
import top.jacktgq.custom_controls.tab.CandyTab;

public class TabPage2 extends CandyTab {

    @Override
    public void onTabCreate() {
        CandyImagePane imagePane = new CandyImagePane(new Image("top/jacktgq/demo1/1.jpg"));
        this.setContentView(imagePane);
    }

}
