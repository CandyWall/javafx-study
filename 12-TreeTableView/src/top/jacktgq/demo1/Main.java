package top.jacktgq.demo1;

import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.scene.control.TreeTableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            TreeTableView<UploadItem> treeTableView = new TreeTableView<>();

            // 添加多个列
            TreeTableColumn<UploadItem, UploadItem>[] columns = new TreeTableColumn[3];
            columns[0] = new TreeTableColumn<>("文件名");
            columns[1] = new TreeTableColumn<>("大小");
            columns[2] = new TreeTableColumn<>("任务创建时间");

            treeTableView.getColumns().addAll(columns);
            // 定义每个列的宽度
            columns[0].setPrefWidth(150);
            columns[1].setPrefWidth(80);
            columns[2].setPrefWidth(120);

            // 设置CellValueFactory（此段写法固定）
            for (int i = 0; i < columns.length; i++) {
                columns[i].setCellValueFactory(
                        new Callback<TreeTableColumn.CellDataFeatures<UploadItem, UploadItem>, ObservableValue<UploadItem>>() {
                            @Override
                            public ObservableValue<UploadItem> call(CellDataFeatures<UploadItem, UploadItem> param) {
                                return param.getValue().valueProperty();
                            }
                        });
            }

            // 设置CellFactory
            columns[0].setCellFactory(
                    new Callback<TreeTableColumn<UploadItem, UploadItem>, TreeTableCell<UploadItem, UploadItem>>() {
                        @Override
                        public TreeTableCell<UploadItem, UploadItem> call(
                                TreeTableColumn<UploadItem, UploadItem> param) {
                            return new MyTreeTableCell("title");
                        }
                    });
            columns[1].setCellFactory(
                    new Callback<TreeTableColumn<UploadItem, UploadItem>, TreeTableCell<UploadItem, UploadItem>>() {
                        @Override
                        public TreeTableCell<UploadItem, UploadItem> call(
                                TreeTableColumn<UploadItem, UploadItem> param) {
                            return new MyTreeTableCell("size");
                        }
                    });
            columns[2].setCellFactory(
                    new Callback<TreeTableColumn<UploadItem, UploadItem>, TreeTableCell<UploadItem, UploadItem>>() {
                        @Override
                        public TreeTableCell<UploadItem, UploadItem> call(
                                TreeTableColumn<UploadItem, UploadItem> param) {
                            return new MyTreeTableCell("create_time");
                        }
                    });

            // 添加数据项
            TreeItem<UploadItem> rootItem = new TreeItem<>(new UploadItem());
            treeTableView.setRoot(rootItem);
            treeTableView.setShowRoot(false);
            TreeItem<UploadItem> item1 = new TreeItem<>(new UploadItem("java学习指南", 19200, System.currentTimeMillis()));
            TreeItem<UploadItem> item1_1 = new TreeItem<>(
                    new UploadItem("java学习指南-1", 6400, System.currentTimeMillis()));
            TreeItem<UploadItem> item1_2 = new TreeItem<>(
                    new UploadItem("java学习指南-2", 6400, System.currentTimeMillis()));
            TreeItem<UploadItem> item1_3 = new TreeItem<>(
                    new UploadItem("java学习指南-3", 6400, System.currentTimeMillis()));
            item1.getChildren().addAll(item1_1, item1_2, item1_3);

            TreeItem<UploadItem> item2 = new TreeItem<>(new UploadItem("玩转数据结构", 23900, System.currentTimeMillis()));
            TreeItem<UploadItem> item2_1 = new TreeItem<>(new UploadItem("玩转数据结构", 12700, System.currentTimeMillis()));
            TreeItem<UploadItem> item2_2 = new TreeItem<>(new UploadItem("玩转数据结构", 11200, System.currentTimeMillis()));
            item2.getChildren().addAll(item2_1, item2_2);

            TreeItem<UploadItem> item3 = new TreeItem<>(new UploadItem("谷粒商城", 478000, System.currentTimeMillis()));
            rootItem.getChildren().addAll(item1, item2, item3);

            root.setCenter(treeTableView);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("/top/jacktgq/demo1/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class MyTreeTableCell extends TreeTableCell<UploadItem, UploadItem> {
        String columnName;

        public MyTreeTableCell(String columnID) {
            this.columnName = columnID;
        }

        @Override
        protected void updateItem(UploadItem item, boolean empty) {
            super.updateItem(item, empty);

            if (empty || item == null) {
                setText(null);
                setGraphic(null);
            } else {
                setGraphic(null);
                if (columnName.equals("title")) {
                    this.setText(item.title);
                } else if (columnName.equals("size")) {
                    this.setText(item.size + "");
                } else if (columnName.equals("create_time")) {
                    this.setText(item.create_time + "");
                }
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
