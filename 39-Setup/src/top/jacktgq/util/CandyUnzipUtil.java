package top.jacktgq.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * 
 * @Title: CandyUnzipUtil.java
 * @Package top.jacktgq.utils
 * @Description: Zip解压缩工具类
 * @author CandyWall
 * @date 2021年5月19日 下午2:42:30
 * @version V1.0
 */
public class CandyUnzipUtil {
    @FunctionalInterface
    // 解压进度监听器
    public interface ProgressChangeListener {
        /**
         * 
         * @param entry
         *            现在解压到的文件
         * @param done
         *            已经解压完毕的文件数
         * @param error
         *            解压中出错的文件数
         * @param total
         *            所有待解压的文件数
         */
        public void handle(ZipEntry entry, int done, int error, int total);
    }

    /**
     * 解压缩
     * 
     * @param srcFile
     *            待解压zip文件路径
     * @param targetDir
     *            解压到的目录
     */
    public static void extract(File srcFile, File targetDir) {
        extract(srcFile, targetDir, null);
    }

    /**
     * 解压缩
     * 
     * @param srcFile
     *            待解压zip文件路径
     * @param targetDir
     *            解压到的目录
     * @param listener
     *            解压进度监听器
     */
    public static void extract(File srcFile, File targetDir, ProgressChangeListener listener) {
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(srcFile, Charset.forName("gbk")); // 解决中文乱码问题
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 完成解压的文件个数
        int done = 0;

        // 解压中出错的文件数
        int error = 0;

        // 统计压缩包内的文件总数
        int total = 0;
        Enumeration<?> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            entries.nextElement();
            total++;
        }

        if (!targetDir.exists()) {
            targetDir.mkdirs();
        }

        // 需要重新获取ZipEntries
        entries = zipFile.entries();
        // 依次解压缩
        while (entries.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) entries.nextElement();

            if (entry.isDirectory()) {
                File subDir = new File(targetDir, entry.getName());
                if (!subDir.exists()) {
                    subDir.mkdirs();
                }
            } else {
                File dstFile = new File(targetDir, entry.getName());
                dstFile.getParentFile().mkdirs();

                try (InputStream in = zipFile.getInputStream(entry);
                        OutputStream out = new FileOutputStream(dstFile);) {
                    byte[] buf = new byte[4096];
                    int len = -1;
                    while ((len = in.read(buf)) != -1) {
                        out.write(buf, 0, len);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    error++;
                }
            }

            done++;

            if (listener != null) {
                // 通知调用者，刚处理完的条目信息
                listener.handle(entry, done, error, total);
            }
        }

        try {
            if (zipFile != null) {
                zipFile.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
