package top.jacktqg;


import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;
import javafx.stage.Window;

public class TopoInfoWindow extends Popup {
    VBox root = new VBox();

    Label m_name = new Label();
    Label m_status = new Label();
    TextField m_ip = new TextField();

    TopoNode node;

    public TopoInfoWindow(TopoNode node) {
        this.node = node;

        this.getScene().setRoot(root);
        this.setAutoHide(true);
        this.setHideOnEscape(true);
        this.setAnchorLocation(AnchorLocation.WINDOW_BOTTOM_LEFT);

        initLayout();
        // root.setPrefSize(200, 150);
    }

    private void initLayout() {
        root.getChildren().addAll(m_name, m_status, m_ip);

        Button save = new Button("保存设置");
        HBox hbox = new HBox();
        hbox.getChildren().add(save);
        root.getChildren().add(hbox);

        root.getStylesheets().add(getClass().getResource("info-window.css").toExternalForm());
        root.getStyleClass().add("root");

        save.setOnAction((e) -> {
            saveConfig();
            hide();
        });
    }

    private void saveConfig() {
        String str = this.m_ip.getText().trim();
        node.data.ip = str;
        node.update();
    }

    @Override
    public void show(Window ownerWindow, double anchorX, double anchorY) {
        super.show(ownerWindow, anchorX, anchorY);

        if (this.node != null) {
            m_name.setText("主机: " + node.data.name);
            m_ip.setText(node.data.ip);

            String ip = node.data.status == 0 ? "正常" : "故障";
            m_status.setText("主机: " + ip);
        }
    }

}
