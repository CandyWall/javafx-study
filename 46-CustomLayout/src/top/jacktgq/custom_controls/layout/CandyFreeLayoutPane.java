package top.jacktgq.custom_controls.layout;

import java.util.HashMap;
import java.util.Map;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class CandyFreeLayoutPane extends Pane {
    Map<Node, Policy> settings = new HashMap<>();

    public static final Margin TOP_LEFT = new Margin(0, null, null, 0);
    public static final Margin TOP_CENTER = new Margin(0, null, null, null);
    public static final Margin TOP_RIGHT = new Margin(0, 0, null, null);
    public static final Margin CENTER_LEFT = new Margin(null, null, null, 0);
    public static final Margin CENTER = new Margin(null, null, null, null);
    public static final Margin CENTER_RIGHT = new Margin(null, 0, null, null);
    public static final Margin BOTTOM_LEFT = new Margin(null, null, 0, 0);
    public static final Margin BOTTOM_CENTER = new Margin(null, null, 0, null);
    public static final Margin BOTTOM_RIGHT = new Margin(null, 0, 0, null);

    public CandyFreeLayoutPane() {
    }

    @Override
    protected void layoutChildren() {
        double w = getWidth(), h = getHeight();
        if (w <= 0 || h <= 0)
            return;

        // 支持padding
        Insets insets = this.getInsets();
        double xoff = insets.getLeft();
        double yoff = insets.getTop();
        w -= (insets.getLeft() + insets.getRight());
        h -= (insets.getTop() + insets.getBottom());

        // 布局
        ObservableList<Node> nodes = this.getChildren();
        for (int i = 0; i < nodes.size(); i++) {
            Node node = nodes.get(i);
            Policy policy = settings.get(node);
            if (policy == null)
                continue;

            //
            Rectangle2D r = layout(node, w, h, policy);
            node.resizeRelocate(xoff + r.getMinX(), yoff + r.getMinY(), r.getWidth(), r.getHeight());
        }
    }

    ///////////////////////////
    public static class Margin {
        public Integer top, right, bottom, left;

        public Margin() {
        }

        public Margin(Integer top, Integer right, Integer bottom, Integer left) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }
    }

    public static class Policy {
        public Integer top, right, bottom, left;
        public String sizeW, sizeH;
    }

    /////////////////////////////
    public Margin margin(Integer top, Integer right, Integer bottom, Integer left) {
        return new Margin(top, right, bottom, left);
    }

    public void addChild(Node node, String width, String height, Margin pos) {
        Policy p = new Policy();
        p.left = pos.left;
        p.right = pos.right;
        p.top = pos.top;
        p.bottom = pos.bottom;
        p.sizeW = width;
        p.sizeH = height;
        settings.put(node, p);
        this.getChildren().add(node);
    }

    public void removeChild(Node node) {
        this.getChildren().remove(node);
        this.settings.remove(node);

    }

    ///////////////////////////////////////////

    // 计算一个控件的位置
    private Rectangle2D layout(Node node, double w, double h, Policy p) {
        double cw = calculateWidth(node, w, p.sizeW);
        double ch = calculateHeight(node, h, p.sizeH);
        // System.out.println("cw: " + cw + ", ch: " + ch);;

        // 水平方向
        // xx[0] : 左限 xx[1] : 右限
        double xx[] = calculatePosistion(w, cw, p.left, p.right);
        // yy[0] : 上限 yy[1] : 下限
        double yy[] = calculatePosistion(h, ch, p.top, p.bottom);

        // System.out.println("minX: " + xx[0] + ",maxX:" + xx[1]);
        // System.out.println("minY: " + yy[0] + ",maxY:" + yy[1]);

        return new Rectangle2D(xx[0], yy[0], xx[1] - xx[0], yy[1] - yy[0]);
    }

    private double calculateWidth(Node node, double size, String define) {
        if (define.equals("auto")) {
            node.autosize();
            return node.getBoundsInLocal().getWidth();
        } else {
            return calculateSize(node, size, define);
        }
    }

    private double calculateHeight(Node node, double size, String define) {
        if (define.equals("auto")) {
            node.autosize();
            return node.getBoundsInLocal().getHeight();
        } else {
            return calculateSize(node, size, define);
        }
    }

    private double calculateSize(Node node, double size, String define) {
        if (define.endsWith("%")) {
            String v = define.substring(0, define.length() - 1);
            double percent = Double.valueOf(v.trim()) / 100.0;
            if (percent < 0)
                percent = 0.0;
            if (percent > 100)
                percent = 1.0;
            return size * percent; // 百分比
        } else if (define.endsWith("px")) {
            String v = define.substring(0, define.length() - 2);
            double value = Double.valueOf(v.trim());
            if (value < 0)
                value = 0.0;
            if (value > size)
                value = size;
            return value;
        }

        return 0.0;
    }

    // wsize: 外部窗口尺寸, csize 内部控件尺寸, a1 下限 a2 上限
    private double[] calculatePosistion(double wsize, double csize, Integer a1, Integer a2) {
        double min = 0, max = 0;
        if (a1 == null && a2 == null) {
            // 居中
            min = (wsize - csize) / 2;
            max = min + csize;
        } else if (a1 != null && a2 != null) {
            min = a1;
            max = wsize - a2;
        } else if (a1 != null && a2 == null) {
            min = a1;
            max = a1 + csize;
        } else if (a1 == null && a2 != null) {
            max = wsize - a2;
            min = max - csize;
        }
        if (min < 0)
            min = 0;
        if (max > wsize)
            max = wsize;
        return new double[] { min, max };
    }

    //////////// padding ///////////
    public CandyFreeLayoutPane padding(double p) {
        this.setPadding(new Insets(p));
        return this;
    }

    public CandyFreeLayoutPane padding(double px, double py) {
        this.setPadding(new Insets(py, px, py, px));
        return this;
    }

    public CandyFreeLayoutPane padding(double t, double r, double b, double l) {
        this.setPadding(new Insets(t, r, b, l));
        return this;
    }

}
