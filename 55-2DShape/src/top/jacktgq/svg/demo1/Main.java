package top.jacktgq.svg.demo1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();

            HBox hbox = new HBox(10);
            SVGPath path1 = new SVGPath();
            path1.setContent("m50,50 l100,100 v100 h-100 z");
            path1.setStrokeWidth(2);
            path1.setStroke(Color.RED);
            path1.setFill(Color.ORANGE);

            SVGPath path2 = new SVGPath();
            path2.setContent("m0,0 c50,-50,150,50,200,0 z");
            path2.setStrokeWidth(2);
            path2.setStroke(Color.RED);
            path2.setFill(Color.ORANGE);

            SVGPath path3 = new SVGPath();
            path3.setContent("m0,0 s100,-50,200,0 t200,0 z");
            path3.setStrokeWidth(2);
            path3.setStroke(Color.RED);
            path3.setFill(Color.ORANGE);

            SVGPath path4 = new SVGPath();
            path4.setContent("m0,0 q100,-50,200,0 t50,0 z");
            path4.setStrokeWidth(2);
            path4.setStroke(Color.RED);
            path4.setFill(Color.ORANGE);

            SVGPath path5 = new SVGPath();
            path5.setContent("m0,0 a100,100,0,1,1,100,100 z");
            path5.setStrokeWidth(2);
            path5.setStroke(Color.RED);
            path5.setFill(Color.ORANGE);

            hbox.getChildren().addAll(path1, path2, path3, path4, path5);
            root.getChildren().addAll(hbox);
            AnchorPane.setTopAnchor(hbox, 20.0);
            AnchorPane.setLeftAnchor(hbox, 20.0);

            Scene scene = new Scene(root, 1600, 800);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("svg绘图");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
