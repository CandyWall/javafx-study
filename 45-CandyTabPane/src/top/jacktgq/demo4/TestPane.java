package top.jacktgq.demo4;

import javafx.scene.layout.BorderPane;
import top.jacktgq.custom_controls.tab.CandyTabPane;
import top.jacktgq.custom_controls.tab.label.CandyVLabelsBar;

public class TestPane extends BorderPane {
    private CandyTabPane tabPane = new CandyTabPane();
    private TabPage1 tab1 = new TabPage1();
    private TabPage2 tab2 = new TabPage2();

    public TestPane() {
        tabPane.setStyle("-fx-background-color: white");
        tab1.setName("tab1");
        tab2.setName("tab2");
        tabPane.addTab(tab1);
        tabPane.addTab(tab2);
        tabPane.setCurrentTab(tab1);
        this.setCenter(tabPane);

        initTabLabels();
    }

    private void initTabLabels() {
        CandyVLabelsBar labelBar = new CandyVLabelsBar();
        labelBar.addLabel("人物信息", "tab1");
        labelBar.addLabel("人物照片", "tab2");
        labelBar.select("tab1");
        labelBar.setOnLabelClickedListener(data -> {
            tabPane.setCurrentTab((String) data);
        });

        this.setLeft(labelBar);
    }
}
