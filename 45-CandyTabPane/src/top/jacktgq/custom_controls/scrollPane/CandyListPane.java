package top.jacktgq.custom_controls.scrollPane;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;

public class CandyListPane extends ScrollPane {
    private VBox container = new VBox();

    public CandyListPane() {
        setHbarPolicy(ScrollBarPolicy.NEVER);
        setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

        this.getStylesheets().add(getClass().getResource("candy-list-pane.css").toExternalForm());
        this.setContent(container);
        this.getStyleClass().add("candy-list-pane");
        container.getStyleClass().add("candy-list-pane-container");
    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        container.setPrefWidth(this.getViewportBounds().getWidth() - 1);
    }

    public void add(Node node) {
        container.getChildren().add(node);
    }

    public ObservableList<Node> getItems() {
        return container.getChildren();
    }
}
