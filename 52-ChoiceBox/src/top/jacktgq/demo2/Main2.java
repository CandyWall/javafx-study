package top.jacktgq.demo2;

import javafx.application.Application;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class Main2 extends Application {
    private ObservableList<Student> stuList;
    private ChoiceBox<Student> choiceBox;

    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();
            root.setPadding(new Insets(10));
            root.setSpacing(10);

            Student stu1 = new Student("20210501", "空条承太郎", 17);
            Student stu2 = new Student("20210502", "花京院典明", 18);
            Student stu3 = new Student("20210503", "乔瑟夫乔斯达", 76);
            Student stu4 = new Student("20210504", "阿布德尔", 46);

            stuList = FXCollections.observableArrayList();
            stuList.addAll(stu1, stu2, stu3, stu4);

            choiceBox = new ChoiceBox<>();
            choiceBox.setItems(stuList);
            // 显示下拉列表的时候只显示学生姓名和年龄
            choiceBox.setConverter(new StringConverter<Student>() {
                @Override
                public String toString(Student stu) {
                    return stu.getName() + ", " + stu.getAge() + "岁";
                }

                @Override
                // 这个方法这里没用上
                public Student fromString(String string) {
                    return null;
                }
            });

            TextField textField = new TextField();
            Button btn = new Button("修改选中项年龄");
            btn.setOnAction(e -> {
                int newAge = Integer.parseInt(textField.getText().trim());
                choiceBox.getSelectionModel().getSelectedItem().setAge(newAge);
            });
            root.getChildren().addAll(choiceBox, textField, btn);

            Scene scene = new Scene(root, 600, 400);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    class Student {
        private SimpleStringProperty idProperty = new SimpleStringProperty();
        private SimpleStringProperty nameProperty = new SimpleStringProperty();
        private SimpleIntegerProperty ageProperty = new SimpleIntegerProperty();

        public Student(String id, String name, Integer age) {
            this.idProperty.setValue(id);
            this.nameProperty.setValue(name);
            this.ageProperty.setValue(age);

            this.ageProperty.addListener(new ChangeListener<Number>() {

                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    System.out.println("old:" + oldValue + ", new:" + newValue);
                    // 获取选中的学生对象
                    Student stu = choiceBox.getSelectionModel().getSelectedItem();
                    int index = stuList.indexOf(stu);
                    stuList.set(index, stu);
                    choiceBox.getSelectionModel().select(index);
                }
            });
        }

        public String getId() {
            return idProperty.getValue();
        }

        public void setId(String id) {
            this.idProperty.setValue(id);
        }

        public String getName() {
            return nameProperty.getValue();
        }

        public void setName(String name) {
            this.nameProperty.setValue(name);
        }

        public Integer getAge() {
            return ageProperty.getValue();
        }

        public void setAge(Integer age) {
            this.ageProperty.setValue(age);
        }

        public SimpleStringProperty getNameProperty() {
            return nameProperty;
        }
    }
}
