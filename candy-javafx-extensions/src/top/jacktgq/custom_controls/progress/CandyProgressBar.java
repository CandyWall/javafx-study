package top.jacktgq.custom_controls.progress;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import top.jacktgq.custom_controls.image.CandyCanvasPane;

/**
 * 
 * @Title: CandyProgressBar.java
 * @Package top.jacktgq
 * @Description: 自定义进度条
 * @author CandyWall
 * @date 2021年5月19日 下午2:45:56
 * @version V1.0
 */
public class CandyProgressBar extends CandyCanvasPane {
    private double percent = 0;
    private String progressText;
    private Color hasDoneProgressColor = Color.web("#418cc3");
    private Color hasnotDoneProgressColor = Color.web("#f5f5f5");
    private Color progressBorderColor = Color.web("#ececec");
    private Font progressTextFont = Font.getDefault();
    private Color progressTextColor = Color.BLACK;

    public void updateProgress(double percent) {
        updateProgress(percent, null);
    }

    public void updateProgress(double percent, String progressText) {
        this.percent = percent;
        this.progressText = progressText;
        repaint();
    }

    @Override
    public void paint(GraphicsContext gc, double x, double y, double w, double h) {
        // 绘制已完成进度
        gc.setFill(hasDoneProgressColor);
        double hasDoneProgressLength = w * percent;
        gc.fillRect(x, y, hasDoneProgressLength, h);

        // 绘制未完成进度
        gc.setFill(hasnotDoneProgressColor);
        gc.fillRect(x + hasDoneProgressLength, y, w - hasDoneProgressLength, h);

        // 绘制边框
        gc.setStroke(progressBorderColor);
        gc.setLineWidth(4);
        gc.strokeRect(x, y, w, h);

        // 绘制进度百分比文字
        // BlendMode blendMode = gc.getGlobalBlendMode();
        gc.setFont(progressTextFont);
        gc.setFill(progressTextColor);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setTextBaseline(VPos.CENTER);
        // gc.setGlobalBlendMode(BlendMode.MULTIPLY);
        gc.fillText((progressText == null ? String.format("%.2f", percent * 100) + "%" : progressText), x + w / 2,
                y + h / 2);
        // gc.setGlobalBlendMode(blendMode);
    }

    public String getProgressText() {
        return progressText;
    }

    public void setProgressText(String progressText) {
        this.progressText = progressText;
    }

    public Color getHasDoneProgressColor() {
        return hasDoneProgressColor;
    }

    public void setHasDoneProgressColor(Color hasDoneProgressColor) {
        this.hasDoneProgressColor = hasDoneProgressColor;
    }

    public Color getHasnotDoneProgressColor() {
        return hasnotDoneProgressColor;
    }

    public void setHasnotDoneProgressColor(Color hasnotDoneProgressColor) {
        this.hasnotDoneProgressColor = hasnotDoneProgressColor;
    }

    public Color getProgressBorderColor() {
        return progressBorderColor;
    }

    public void setProgressBorderColor(Color progressBorderColor) {
        this.progressBorderColor = progressBorderColor;
    }

    public Font getProgressTextFont() {
        return progressTextFont;
    }

    public Color getProgressTextColor() {
        return progressTextColor;
    }

    public void setProgressTextColor(Color progressTextColor) {
        this.progressTextColor = progressTextColor;
    }
}
