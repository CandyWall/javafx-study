package top.jacktgq.example.carousel3d.demo2;

import javafx.animation.TranslateTransition;
import javafx.geometry.Pos;
import javafx.scene.PerspectiveCamera;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 * 
 * @Title: CarouselPane.java
 * @Package top.jacktgq.example.carousel3d
 * @Description: 3D场景轮播图
 * @author CandyWall
 * @date 2021年5月31日 下午12:41:30
 * @version V1.0
 */
public class CarouselPane extends StackPane {
    private double w;
    private double h;
    private Image[] images;
    private double iv0x;
    private double iv1x;
    private double iv2x;
    private double ivY;
    private double imgZ;
    private ImageView iv0;
    private ImageView iv1;
    private ImageView iv2;
    private int index0;
    private int index1;
    private int index2;

    public CarouselPane(double w, double h, Image[] images) {
        this.w = w;
        this.h = h;
        this.images = images;
        // setStyle("-fx-background-color: pink");
        // setBorder(new Border(new BorderStroke(Color.RED,
        // BorderStrokeStyle.SOLID, null, new BorderWidths(2))));

        // 图片切换按钮
        StackPane controlBar = new StackPane();
        Button prevBtn = new Button("", new ImageView("icons/prev-btn.png"));
        Button nextBtn = new Button("", new ImageView("icons/next-btn.png"));
        controlBar.getChildren().addAll(nextBtn, prevBtn);
        StackPane.setAlignment(prevBtn, Pos.CENTER_LEFT);
        StackPane.setAlignment(nextBtn, Pos.CENTER_RIGHT);

        // 图片显示面板
        AnchorPane showImagePane = new AnchorPane();
        // showImagePane.setStyle("-fx-background: transparent");
        showImagePane.setBackground(null);
        iv0 = new ImageView();
        iv0.setPreserveRatio(true);
        iv0.setFitWidth(w / 1.6);
        iv1 = new ImageView();
        iv1.setPreserveRatio(true);
        iv1.setFitWidth(w / 1.6);
        iv2 = new ImageView();
        iv2.setPreserveRatio(true);
        iv2.setFitWidth(w / 1.6);
        showImagePane.getChildren().addAll(iv0, iv1, iv2);

        imgZ = 50;
        relocate();
        if (images != null) {
            index1 = 0;
            if (index1 == -1) {
                index1 = images.length - 1;
            }
            index0 = index1 - 1;
            if (index0 == -1) {
                index0 = images.length - 1;
            }
            index2 = index1 + 1;
            if (index2 == images.length) {
                index2 = 0;
            }
            toggleImage();
            relocate();
        }
        SubScene subScene = new SubScene(showImagePane, w, h, true, SceneAntialiasing.BALANCED);
        PerspectiveCamera camera = new PerspectiveCamera();
        subScene.setCamera(camera);

        getChildren().addAll(subScene, controlBar);

        prevBtn.setOnAction(e -> {
            if (images != null) {
                // 切换动画
                // 右边的图片移动到中间
                rightToCenterAnimation(iv2);
                // 中间的图片移动到左边
                centerToLeftAnimation(iv1);
                // 左边的图片移动到右边
                leftToRightAnimation(iv0);
                ImageView temp = iv0;
                iv0 = iv1;
                iv1 = iv2;
                iv2 = temp;
            }
        });

        nextBtn.setOnAction(e -> {
            if (images != null) {
                // 切换动画
                // 左边的图片移动到中间
                leftToCenterAnimation(iv0);
                // 中间的图片移动到右边
                centerToRightAnimation(iv1);
                // 右边的图片移动到左边
                rightToLeftAnimation(iv2);
                ImageView temp = iv2;
                iv2 = iv1;
                iv1 = iv0;
                iv0 = temp;
            }
        });

        // StackPane.setAlignment(subScene, Pos.CENTER);
        // 加载轮播图样式
        this.getStylesheets().add(getClass().getResource("candy-carousel.css").toExternalForm());
        // 设置样式
        this.setId("carousel-pane");
        controlBar.setId("control-bar");
    }

    // 轮播图切换图片
    public void toggleImage() {
        iv0.setImage(images[index0]);
        iv1.setImage(images[index1]);
        iv2.setImage(images[index2]);
    }

    public void relocate() {
        iv0x = -imgZ;
        iv1x = (w - iv1.getFitWidth()) / 2;
        iv2x = w - iv2.getFitWidth() + imgZ;
        ivY = (h - iv1.prefHeight(-1)) / 2;
        iv0.setTranslateX(iv0x);
        iv1.setTranslateX(iv1x);
        iv2.setTranslateX(iv2x);

        iv0.setTranslateY(ivY);
        iv1.setTranslateY(ivY);
        iv2.setTranslateY(ivY);

        iv0.setTranslateZ(imgZ);
        iv2.setTranslateZ(imgZ);
    }

    // 图片切换动画-左边的图片移动到中间
    public void leftToCenterAnimation(ImageView iv) {
        TranslateTransition tt = new TranslateTransition();
        tt.setNode(iv);
        tt.setDuration(Duration.millis(600));
        tt.setFromX(iv0x);
        tt.setToX(iv1x);
        tt.setFromZ(imgZ);
        tt.setToZ(0);
        tt.play();
    }

    // 图片切换动画-中间的图片移动到右边
    public void centerToRightAnimation(ImageView iv) {
        TranslateTransition tt = new TranslateTransition();
        tt.setNode(iv);
        tt.setDuration(Duration.millis(600));
        tt.setFromX(iv1x);
        tt.setToX(iv2x);
        tt.setFromZ(0);
        tt.setToZ(imgZ);
        tt.play();
    }

    // 图片切换动画-右边的图片移动到左边
    public void rightToLeftAnimation(ImageView iv) {
        // 需要先切换到上一张图片
        index1++;
        if (index1 == images.length) {
            index1 = 0;
        }
        index0 = index1 - 1;
        if (index0 == -1) {
            index0 = images.length - 1;
        }
        iv2.setImage(images[index0]);

        TranslateTransition tt = new TranslateTransition();
        tt.setNode(iv);
        tt.setDuration(Duration.millis(600));
        tt.setFromX(iv2x);
        tt.setToX(iv0x);
        tt.play();
    }

    // --------------------------------------
    // 图片切换动画-右边的图片移动到中间
    public void rightToCenterAnimation(ImageView iv) {
        TranslateTransition tt = new TranslateTransition();
        tt.setNode(iv);
        tt.setDuration(Duration.millis(600));
        tt.setFromX(iv2x);
        tt.setToX(iv1x);
        tt.setFromZ(imgZ);
        tt.setToZ(0);
        tt.play();
    }

    // 图片切换动画-中间的图片移动到左边
    public void centerToLeftAnimation(ImageView iv) {
        TranslateTransition tt = new TranslateTransition();
        tt.setNode(iv);
        tt.setDuration(Duration.millis(600));
        tt.setFromX(iv1x);
        tt.setToX(iv0x);
        tt.setFromZ(0);
        tt.setToZ(imgZ);
        tt.play();
    }

    // 图片切换动画-左边的图片移动到右边
    public void leftToRightAnimation(ImageView iv) {
        // 需要先切换到下一张图片
        index1--;
        if (index1 == -1) {
            index1 = images.length - 1;
        }
        index2 = index1 + 1;
        if (index2 == images.length) {
            index2 = 0;
        }
        iv0.setImage(images[index2]);

        TranslateTransition tt = new TranslateTransition();
        tt.setNode(iv);
        tt.setDuration(Duration.millis(600));
        tt.setFromX(iv0x);
        tt.setToX(iv2x);
        tt.play();
    }
}
