package top.jacktgq.demo1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.CandyImagePane;

public class Main extends Application {
    private FileListView fileListView;
    private TabPane tabPane;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            MenuBar menuBar = new MenuBar();
            Menu fileMenu = new Menu("文件");
            MenuItem fileMenuItem = new MenuItem("打开文件夹", new ImageView("icons/open.png"));
            fileMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    DirectoryChooser dirChooser = new DirectoryChooser();
                    File dir = dirChooser.showDialog(primaryStage);
                    if (dir != null) {
                        loadFileList(dir);
                    }
                }
            });
            fileMenu.getItems().add(fileMenuItem);
            menuBar.getMenus().add(fileMenu);
            root.setTop(menuBar);

            fileListView = new FileListView();

            fileListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    // 如果鼠标左键双击
                    if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                        Node node = null;
                        int selectedIndex = fileListView.getSelectionModel().getSelectedIndex();
                        // 将这个文件打开
                        File file = fileListView.getListData().get(selectedIndex);

                        Tab tab = findTab(file);
                        // 如果已经打开过了，就选中这个选项卡
                        if (tab != null) {
                            int tabIndex = tabPane.getTabs().indexOf(tab);
                            tabPane.getSelectionModel().select(tabIndex);
                            return;
                        }

                        // 获取文件后缀名
                        String fileExtension = file.getName().substring(file.getName().lastIndexOf("."));
                        System.out.println(fileExtension);
                        // 如果是图片
                        if (fileExtension.equals(".jpg") || fileExtension.equals(".png")
                                || fileExtension.equals(".gif")) {
                            node = new CandyImagePane(file);
                        }
                        // 如果是.txt文本
                        else if (fileExtension.equals(".txt") || fileExtension.equals(".java")
                                || fileExtension.equals(".properties")) {
                            TextArea textArea = new TextArea();

                            try (BufferedReader reader = new BufferedReader(
                                    new InputStreamReader(new FileInputStream(file), Charset.forName("gbk")))) {
                                String line = null;
                                while ((line = reader.readLine()) != null) {
                                    textArea.appendText(line + "\n");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            node = textArea;
                        } else {
                            node = new Label("暂不支持改类型文件的打开！");
                        }

                        // 将Node放进TabPane中
                        tab = new Tab(file.getName(), node);
                        tabPane.getTabs().add(tab);
                        int tabIndex = tabPane.getTabs().indexOf(tab);
                        tabPane.getSelectionModel().select(tabIndex);
                    }
                }
            });

            loadFileList(new File("examples"));
            root.setLeft(fileListView);

            tabPane = new TabPane();
            root.setCenter(tabPane);

            Scene scene = new Scene(root, 1000, 800);
            scene.getStylesheets().add(getClass().getResource("/top/jacktgq/demo1/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("文件浏览器");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 查看选项卡是否已经打开
    private Tab findTab(File file) {
        ObservableList<Tab> tabs = tabPane.getTabs();
        for (Tab tab : tabs) {
            if (tab.getText().equals(file.getName())) {
                return tab;
            }
        }
        return null;
    }

    private void loadFileList(File dir) {
        // 先清空列表数据
        fileListView.getListData().clear();
        // 遍历文件
        File[] files = dir.listFiles();
        for (File file : files) {
            fileListView.add(file);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
