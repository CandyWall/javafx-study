package top.jacktgq.setup;

import java.io.File;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.stage.DirectoryChooser;
import top.jacktgq.custom_controls.activity.CandyActivity;

/**
 * 
 * @Title: ChooseSetupDirActivity.java
 * @Package top.jacktgq.setup
 * @Description: 安装目录选择
 * @author CandyWall
 * @date 2021年5月23日 下午2:35:45
 * @version V1.0
 */
public class ChooseSetupDirActivity extends CandyActivity {
    private File dir;
    private TextField setupDirField;

    @Override
    public void onCreate(Object intent) {
        StackPane root = new StackPane();

        setContentView(root);

        Button setupBtn = new Button("", new ImageView("top/jacktgq/setup/ic_setup.png"));
        setupBtn.setOnAction(e -> {
            startActivity(SetupProgressWaitingActivity.class, setupDirField.getText());
            finish();
        });
        HBox hbox = new HBox();
        dir = new File("testData/targetDir/");
        setupDirField = new TextField(dir.getAbsolutePath());
        setupDirField.setEditable(false);
        Button chooseSetupDirBtn = new Button("...");
        chooseSetupDirBtn.setOnAction(e -> {
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("选择安装目录");
            chooser.setInitialDirectory(new File("testData/"));
            dir = chooser.showDialog(context.getOwner());
            if (dir != null) {
                setupDirField.setText(dir.getAbsolutePath());
            }
        });
        hbox.getChildren().addAll(setupDirField, chooseSetupDirBtn);
        hbox.setAlignment(Pos.BOTTOM_CENTER);
        hbox.setSpacing(10);
        hbox.setMaxHeight(40);
        HBox.setHgrow(setupDirField, Priority.ALWAYS);
        root.getChildren().addAll(setupBtn, hbox);
        StackPane.setAlignment(setupBtn, Pos.CENTER);
        StackPane.setAlignment(hbox, Pos.BOTTOM_CENTER);
        root.setPadding(new Insets(0, 40, 40, 40));

        // 设置样式
        root.setId("page1");
        setupBtn.setId("setup-btn");
        chooseSetupDirBtn.setId("choose-setup-btn");
    }

}
