package top.jacktgq.Scale;

import javafx.application.Application;
import javafx.geometry.Bounds;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();

            AnchorPane an = new AnchorPane();
            an.setPrefSize(200, 200);
            an.setStyle("-fx-background-color: pink");

            Button btn1 = new Button("提交");
            btn1.setPrefSize(80, 40);
            btn1.setLayoutX(50);
            btn1.setLayoutY(50);
            an.getChildren().add(btn1);

            Button btn2 = new Button("重置");
            btn2.setPrefSize(80, 40);
            btn2.setLayoutX(50);
            btn2.setLayoutY(50);
            // 方式1：
            // Scale scale = new Scale(0.5, 0.5, 40, 20);
            // btn2.getTransforms().add(scale);

            an.getChildren().add(btn2);
            // 方式2：
            btn2.setScaleX(0.5);
            btn2.setScaleY(0.5);

            Scene scene = new Scene(root, 600, 600);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            System.out.println(btn2.getPrefWidth() + "---" + btn2.getPrefHeight());
            System.out.println(btn2.getWidth() + "---" + btn2.getHeight());
            // 获取缩放后左上角的坐标
            System.out.println("x=" + btn2.getLocalToParentTransform().getTx() + ", y="
                    + btn2.getLocalToParentTransform().getTy());
            Bounds bd = btn2.getLayoutBounds();
            Bounds bd2 = btn2.localToParent(bd);
            System.out.println(bd2);

            root.getChildren().add(an);
            AnchorPane.setLeftAnchor(an, 100.0);
            AnchorPane.setTopAnchor(an, 100.0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
