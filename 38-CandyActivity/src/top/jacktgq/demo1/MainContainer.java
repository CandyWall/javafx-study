package top.jacktgq.demo1;

import javafx.scene.layout.BorderPane;
import top.jacktgq.custom_controls.activity.CandyActivity;
import top.jacktgq.custom_controls.activity.CandyActivityContext;
import top.jacktgq.custom_controls.activity.CandyActivityObserver;

public class MainContainer extends BorderPane implements CandyActivityObserver {
    CandyActivityContext activityContext = new CandyActivityContext();

    public MainContainer() {
        activityContext.setObserver(this);
        activityContext.startActivity(null, Page1.class, null);
    }

    @Override
    public void onActivityShow(CandyActivity a) {
        this.setCenter(a.getContentView());
    }

    @Override
    public void onActivityDismiss(CandyActivity a) {

    }
}
