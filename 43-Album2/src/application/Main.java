package application;

import java.io.File;
import java.io.FileFilter;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.image.CandyImagePane;
import top.jacktgq.custom_controls.scrollPane.CandyGridPane;

public class Main extends Application {
    private CandyGridPane showImagePane;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            HBox toolPane = new HBox();
            TextField imageDirField = new TextField();
            imageDirField.setPromptText("选择图片目录...");
            imageDirField.setEditable(false);
            Button openBtn = new Button("打开");
            toolPane.getChildren().addAll(imageDirField, openBtn);
            root.setTop(toolPane);
            HBox.setHgrow(imageDirField, Priority.ALWAYS);

            showImagePane = new CandyGridPane(150, 250, 3, 2, 10, 10);
            root.setCenter(showImagePane);

            Scene scene = new Scene(root, 800, 600);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("图片浏览器");
            primaryStage.show();

            openBtn.setOnAction(e -> {
                DirectoryChooser chooser = new DirectoryChooser();
                File imageDir = chooser.showDialog(primaryStage);
                if (imageDir != null) {
                    imageDirField.setText(imageDir.getAbsolutePath());
                    File[] imageFiles = imageDir.listFiles(new FileFilter() {
                        @Override
                        public boolean accept(File pathname) {
                            String filename = pathname.getName();
                            if (filename.endsWith(".jpg") || filename.endsWith(".png") || filename.endsWith(".jpeg")
                                    || filename.endsWith(".gif")) {
                                return true;
                            }
                            return false;
                        }
                    });
                    for (File file : imageFiles) {
                        CandyImagePane candyImagePane = new CandyImagePane(file);
                        candyImagePane.getStyleClass().add("candy-grid-image");
                        showImagePane.add(candyImagePane);
                    }
                }
            });

            // 设置样式
            toolPane.getStyleClass().add("tool-pane");
            showImagePane.getStyleClass().add("show-image-pane");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
