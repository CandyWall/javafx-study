package top.jacktgq.demo1;

public class Student {
    public int id;
    public String name;
    public boolean sex;

    public Student() {
    }

    public Student(int id, String name, boolean sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
    }
}