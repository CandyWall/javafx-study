package application;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class Main extends Application {
    private ImageView imageView;

    @Override
    public void start(Stage primaryStage) {
        try {
            VBox root = new VBox();
            root.setStyle("-fx-background-color: #ffcc00");
            root.setPadding(new Insets(10));
            root.setSpacing(10);
            TextField textField = new TextField();
            /*
             * textField.setTextFormatter(new TextFormatter<Change>(change -> {
             * String value = change.getText(); System.out.print(value); //
             * value.matches("[0-9]*") //
             * value.matches("^[a-z][a-z1-9]+([\\.][a-z][a-z1-9]*)*$") //
             * 匹配java下面的包路径：如 top.jacktgq.demo if
             * (!value.matches("^[a-z][a-z1-9]*([\\.][a-z][a-z1-9]*)*$")) {
             * imageView.setImage(new Image("ic_cry.png"));
             * System.out.println("--不匹配"); } else { imageView.setImage(new
             * Image("ic_smile.png")); System.out.println("--匹配"); } return
             * change; })); textField.commitValue();
             */

            TextArea textArea = new TextArea();

            imageView = new ImageView();

            root.getChildren().addAll(textField, textArea, imageView);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            textArea.textProperty().addListener(new ChangeListener<String>() {

                @Override
                public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                    textArea.setTextFormatter(new TextFormatter<String>(new StringConverter<String>() {

                        @Override
                        public String toString(String object) {
                            System.out.println("obj=" + object);
                            return object;
                        }

                        @Override
                        public String fromString(String string) {
                            System.out.println("str=" + string);
                            if (string.contains("QQ")) {
                                String value = string.replace("QQ", "2557304924");
                                return value;
                            }
                            return string;
                        }
                    }));
                    textArea.commitValue();
                }
            });

            textField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                    textField.setTextFormatter(new TextFormatter<String>(new StringConverter<String>() {
                        @Override
                        public String toString(String object) {
                            return object;
                        }

                        @Override
                        public String fromString(String string) {
                            if (!string.matches("^[a-z][a-z1-9]*([\\.][a-z][a-z1-9]*)*$")) {
                                imageView.setImage(new Image("ic_cry.png"));
                                System.out.println("--不匹配");
                            } else {
                                imageView.setImage(new Image("ic_smile.png"));
                                System.out.println("--匹配");
                            }
                            return string;
                        }
                    }));
                    textField.commitValue();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
