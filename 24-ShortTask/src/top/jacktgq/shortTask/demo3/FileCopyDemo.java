package top.jacktgq.shortTask.demo3;

import java.io.File;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import top.jacktgq.utils.CandyFileUtil;

public class FileCopyDemo extends Application {
    private Label statusLabel;

    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();
            Button button = new Button("文件拷贝");
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    File srcFile = new File("testData/宣传片.mp4");
                    File targetFile = new File("testData/备份.mp4");

                    statusLabel.setText("开始拷贝！");

                    // 创建工作线程
                    new CandyShortTask(srcFile, targetFile) {
                        @Override
                        protected void doInBackground() {
                            CandyFileUtil.transferTo((File) args[0], (File) args[1]);
                        }

                        @Override
                        protected void done() {
                            if (err != null) {
                                statusLabel.setText("拷贝完毕！");
                            } else {
                                statusLabel.setText("拷贝出错！");
                            }
                        }
                    }.execute();
                }
            });
            button.setStyle("-fx-font-size: 20");
            statusLabel = new Label("状态提示...");
            statusLabel.setStyle("-fx-font-size: 20");

            root.getChildren().addAll(button, statusLabel);
            root.setAlignment(Pos.CENTER);
            root.setSpacing(10);

            Scene scene = new Scene(root, 300, 100);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("小任务");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
