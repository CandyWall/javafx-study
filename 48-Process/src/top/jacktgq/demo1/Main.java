package top.jacktgq.demo1;

import java.io.IOException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import top.jacktgq.utils.process.CandyProcess;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();

            Button btn1 = new Button("方式1");
            btn1.setOnAction(e -> {
                openDir1();
            });
            Button btn2 = new Button("方式2");
            btn2.setOnAction(e -> {
                openDir2();
            });
            Button btn3 = new Button("打开记事本");
            btn3.setOnAction(e -> {
                openNotepad();
            });
            root.getChildren().addAll(btn1, btn2, btn3);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("打开系统文件夹");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openDir1() {
        String path = "D:\\";
        String cmd = "explorer \"" + path + "\"";
        ProcessBuilder pb = new ProcessBuilder(cmd.split(" "));
        try {
            Process process = pb.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openDir2() {
        String path = "D:\\";
        String cmd = "explorer \"" + path + "\"";
        try {
            Process process = Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openNotepad() {
        String path = "c:\\windows\\notepad.exe";
        try {
            CandyProcess.execute(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
