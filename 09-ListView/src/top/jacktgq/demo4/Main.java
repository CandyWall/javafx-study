package top.jacktgq.demo4;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ListView.EditEvent;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            HBox topPane = new HBox();
            Button btn = new Button("获取选中项");
            btn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                }
            });
            topPane.getChildren().add(btn);
            root.setTop(topPane);

            // 准备数据
            ObservableList<String> listData = FXCollections.observableArrayList();
            listData.add("空条承太郎");
            listData.add("花京院典明");
            listData.add("空条徐伦");
            listData.add("波鲁那雷夫");
            ListView<String> listView = new ListView<>(listData);
            listView.setCellFactory(TextFieldListCell.forListView(new StringConverter<String>() {

                @Override
                public String toString(String object) {
                    return object;
                }

                @Override
                public String fromString(String string) {
                    return string;
                }
            }));
            listView.setEditable(true);

            listView.setOnEditStart(new EventHandler<ListView.EditEvent<String>>() {

                @Override
                public void handle(EditEvent<String> event) {
                    // TODO Auto-generated method stub
                    System.out.println("start edit");
                    System.out.println(event.getIndex());
                    System.out.println(event.getNewValue());
                }
            });

            listView.setOnEditCancel(new EventHandler<ListView.EditEvent<String>>() {

                @Override
                public void handle(EditEvent<String> event) {
                    // TODO Auto-generated method stub
                    System.out.println("cancel edit");
                }
            });

            listView.setOnEditCommit(new EventHandler<ListView.EditEvent<String>>() {
                @Override
                public void handle(EditEvent<String> event) {

                    System.out.println("commit edit");
                    System.out.println(event.getIndex());
                    System.out.println(event.getNewValue());

                    listData.set(event.getIndex(), event.getNewValue());
                }
            });

            root.setCenter(listView);

            Scene scene = new Scene(root, 400, 400);
            // scene.getStylesheets().add(getClass().getResource("/top/jacktgq/demo1/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    // 负责listView的ListCell
    /*
     * class MyListCell extends ListCell<Student> {
     * 
     * @Override protected void updateItem(Student item, boolean empty) { //
     * fx框架奥球必须先调用super.updateItem() super.updateItem(item, empty);
     * 
     * // 自己的代码 if (item != null) { this.setText(item.id + "\t" + item.name +
     * "\t" + (item.sex ? "男" : "女")); } else { this.setText(""); } } }
     */

}
