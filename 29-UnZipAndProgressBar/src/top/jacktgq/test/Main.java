package top.jacktgq.test;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import top.jacktgq.animation.CandyAnimationTimer;
import top.jacktgq.custom_controls.progress.CandyProgressBar;
import top.jacktgq.custom_controls.progress.CandyProgressBarDialog;

public class Main extends Application {
    private int i = 0;
    private CandyAnimationTimer timer;

    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();
            root.setSpacing(10);
            CandyProgressBar candyProgressBar = new CandyProgressBar();
            candyProgressBar.setMaxSize(320, 40);
            Button btn1 = new Button("开始执行任务1");

            btn1.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    i = 0;

                    timer = new CandyAnimationTimer(10) {
                        @Override
                        public void updateUI(long now) {
                            if (i < 100) {
                                candyProgressBar.updateProgress(i / 100.0);
                            } else if (i == 100) {
                                candyProgressBar.updateProgress(1, "加载完毕！");
                            } else {
                                this.stop();
                            }
                            i++;
                        }
                    };
                    timer.start();
                }
            });

            Button btn2 = new Button("开始执行任务2");

            btn2.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    i = 0;

                    timer = new CandyAnimationTimer(10) {
                        @Override
                        public void updateUI(long now) {
                            if (i < 180) {
                                candyProgressBar.updateProgress(i / 180.0, i + " / " + 180);
                            } else if (i == 180) {
                                candyProgressBar.updateProgress(1, "加载完毕！");
                            } else {
                                this.stop();
                            }
                            i++;
                        }
                    };
                    timer.start();
                }
            });

            Button btn3 = new Button("开始执行任务2");

            btn3.setOnAction(new EventHandler<ActionEvent>() {
                private CandyProgressBarDialog dialog;

                @Override
                public void handle(ActionEvent event) {
                    i = 0;

                    timer = new CandyAnimationTimer(10) {
                        @Override
                        public void updateUI(long now) {
                            if (dialog == null) {
                                return;
                            }
                            if (i < 180) {
                                dialog.updateProgress(i / 180.0, i + " / " + 180);
                            } else if (i == 180) {
                                dialog.updateProgress(1, "加载完毕！");
                            } else {
                                dialog.close();
                                this.stop();
                            }
                            i++;
                        }
                    };
                    timer.start();
                    dialog = new CandyProgressBarDialog(primaryStage);
                    dialog.showAndWait();
                }
            });

            root.setAlignment(Pos.CENTER);
            root.getChildren().addAll(candyProgressBar, btn1, btn2, btn3);
            // root.setStyle("-fx-background-color: yellow");

            Scene scene = new Scene(root, 800, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试进度条");
            primaryStage.show();

        } catch (

        Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
