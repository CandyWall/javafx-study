package application;

import javafx.application.Application;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.window.CandyPrettyWindowWrapper;
import top.jacktgq.custom_controls.window.CandyScreenDocker;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            CandyPrettyWindowWrapper windowWrapper = new CandyPrettyWindowWrapper(primaryStage, false, false);
            windowWrapper.setBackground(Color.web("#bf7fa9"));
            windowWrapper.setContentView(root, 400, 400);
            windowWrapper.show();

            Label label = new Label("我不是针对某个人，我只想说在座的各位都是垃圾！");
            label.setStyle("-fx-text-fill: white");
            root.setCenter(label);

            CandyScreenDocker.initSupport(primaryStage, root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
