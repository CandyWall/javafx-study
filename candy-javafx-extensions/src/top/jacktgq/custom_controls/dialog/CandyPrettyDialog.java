package top.jacktgq.custom_controls.dialog;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class CandyPrettyDialog<T> extends CandyDialog<T> {
    protected LayoutPane root = new LayoutPane();

    public CandyPrettyDialog(Window owner) {
        super(owner);
        initStyledDialog();
    }

    public CandyPrettyDialog(Node anchor) {
        super(anchor);
        initStyledDialog();
    }

    private void initStyledDialog() {
        // 去掉标题栏和边框
        initStyle(StageStyle.UNDECORATED);

        // 对话框拖动支持
        initDragSupport();

        root.closeBtn.setOnAction((e) -> {
            cancel();
        });
    }

    /**
     * 给当前自定义的对话框设置标题需要调用此方法，而不是setTitle()
     * setTitle()方法是stage中的final方法，当前类作为Stage类的子类不能覆盖 所以只能另取一个名字
     * 
     * @param title
     */
    public void setDialogTitle(String title) {
        root.titleLabel.setText(title);
    }

    @Override
    public void setDialogPane(Parent content) {
        setDialogPane(content, -1, -1);
    }

    @Override
    public void setDialogPane(Parent content, double sceneWidth, double sceneHeight) {
        root.setCenter(content);
        super.setDialogPane(this.root, sceneWidth, sceneHeight + 40);
    }

    private class LayoutPane extends BorderPane {
        Label titleLabel = new Label();
        Button closeBtn = new Button("", new ImageView("top/jacktgq/custom_controls/dialog/ic_close.png"));

        public LayoutPane() {
            HBox titlePane = new HBox();
            titlePane.getChildren().addAll(titleLabel, closeBtn);
            titlePane.setAlignment(Pos.TOP_LEFT);
            titleLabel.setMaxWidth(9999);
            HBox.setHgrow(titleLabel, Priority.ALWAYS);
            // titlePane.setPadding(new Insets(10));
            titlePane.setMaxHeight(40);
            setTop(titlePane);

            // 设置样式
            this.getStylesheets().add(CandyPrettyDialog.class.getResource("candy-style-dialog.css").toExternalForm());
            this.getStyleClass().add("candy-pretty-dialog");
            closeBtn.getStyleClass().add("candy-pretty-dialog-close");
            titleLabel.getStyleClass().add("candy-pretty-dialog-title");
            // getCenter().getStyleClass().add("candy-pretty-dialog-content");
        }
    }

    ////////////// 拖动支持 ///////////////
    private boolean dragging = false;
    private double windowX = 0, windowY = 0;
    private double startX, startY;

    private void initDragSupport() {
        root.setOnMousePressed((MouseEvent e) -> {
            windowX = CandyPrettyDialog.this.getX();
            windowY = CandyPrettyDialog.this.getY();
            startX = e.getScreenX();
            startY = e.getScreenY();
        });

        root.setOnDragDetected((MouseEvent e) -> {
            if (e.getButton() == MouseButton.PRIMARY) {
                dragging = true;
            }
        });

        root.setOnMouseDragged((MouseEvent e) -> {
            if (!dragging)
                return;
            double dx = e.getScreenX() - startX;
            double dy = e.getScreenY() - startY;
            setX(windowX + dx);
            setY(windowY + dy);
        });

        root.setOnMouseReleased((MouseEvent e) -> {
            dragging = false;
        });
    }

}
