package top.jacktgq.demo5;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.toaster.CandyToaster;

/**
 *
 * @Title: TestCandyToaster.java
 * @Package top.jacktgq.demo5
 * @Description: 测试短消息提示
 * @author CandyWall
 * @date 2021年5月22日 下午1:07:18
 * @version V1.0
 */
public class TestCandyToaster extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();
            Button btn1 = new Button("成功");

            btn1.setOnAction(e -> {
                CandyToaster.show(primaryStage, "上传成功");
            });
            Button btn2 = new Button("警告");

            btn2.setOnAction(e -> {
                CandyToaster.show(primaryStage, "您的余额已经趋近于0，请及时缴费！", CandyToaster.WARN);
            });
            Button btn3 = new Button("错误");

            btn3.setOnAction(e -> {
                CandyToaster.show(primaryStage, "上传失败", CandyToaster.ERROR);
            });
            Button btn4 = new Button("提示");

            btn4.setOnAction(e -> {
                CandyToaster.show(primaryStage, "今天是星期六，出去玩了吗？", CandyToaster.INFO);
            });

            root.getChildren().addAll(btn1, btn2, btn3, btn4);

            Scene scene = new Scene(root, 400, 400);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("短消息提示框测试");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
