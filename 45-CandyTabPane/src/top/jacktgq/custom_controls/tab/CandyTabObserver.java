package top.jacktgq.custom_controls.tab;

public interface CandyTabObserver {
    public void onTabShow(CandyTab node);

    public void onTabDismiss(CandyTab node);
}
