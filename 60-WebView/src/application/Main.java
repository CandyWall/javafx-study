package application;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebHistory;
import javafx.scene.web.WebHistory.Entry;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            HBox hbox = new HBox(20);
            Button btn1 = new Button("后退");
            Button btn2 = new Button("前进");
            Button btn3 = new Button("信息");
            hbox.getChildren().addAll(btn1, btn2, btn3);
            root.setTop(hbox);

            WebView webView = new WebView();
            WebEngine engine = webView.getEngine();
            // engine.load("http://localhost:8080/Sample_Data_Storage");
            // engine.load("https://map.baidu.com");
            // engine.load("https://echarts.apache.org/zh/index.html");
            engine.load("https://www.baidu.com");
            // webView.setFontScale(0.5);
            // webView.setZoom(0.5);
            // engine.loadContent(content);

            root.setCenter(webView);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("WebView测试");
            primaryStage.show();

            WebHistory history = engine.getHistory();
            ObservableList<Entry> webList = history.getEntries();

            btn1.setOnAction(e -> {
                if (history.getCurrentIndex() > 0) {
                    history.go(-1);
                }
            });

            btn2.setOnAction(e -> {
                if (history.getCurrentIndex() < webList.size() - 1) {
                    history.go(1);
                }
            });

            btn3.setOnAction(e -> {
                System.out.println("当前页面的索引：" + history.getCurrentIndex());
                System.out.println("最大存储数量：" + history.getMaxSize());
                System.out.println("当前列表数量：" + webList.size());
            });

            primaryStage.setOnCloseRequest(e -> {
                engine.load(null);
                Platform.exit();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
