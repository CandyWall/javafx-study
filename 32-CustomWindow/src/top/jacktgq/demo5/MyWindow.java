package top.jacktgq.demo5;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Window;
import top.jacktgq.custom_controls.window.CandyWindow;

public class MyWindow extends CandyWindow {
    StackPane root = new StackPane(); // 根节点
    Label title = new Label();

    public MyWindow(Window owner) {
        // 指定父窗口
        super(owner);

        initLayout();

        // 指定主节点
        this.setContentView(root);

        // 拖拽支持: anchor,指定可拖拽的位置;
        // resizable,指定是否可以拖拽改变大小
        this.initDragSupport(title, true);

        // 指定大小
        this.resize(400, 300);

    }

    // 添加一个关闭按钮
    private void initLayout() {
        // 给根节点添加一个背景色以方便辩认
        root.setStyle("-fx-background-color: #333");

        //
        title.setText("窗口标题");
        title.setMaxWidth(9999);
        title.setPrefHeight(30);
        title.setStyle("-fx-background-color: #888;-fx-text-fill:#fff;");
        root.getChildren().add(title);
        StackPane.setAlignment(title, Pos.TOP_CENTER);

        Button button = new Button("关闭");
        root.getChildren().add(button);
        StackPane.setAlignment(button, Pos.CENTER);

        button.setOnAction((e) -> {
            close();
        });

    }

}
