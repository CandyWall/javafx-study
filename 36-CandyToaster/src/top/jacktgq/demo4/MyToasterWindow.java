package top.jacktgq.demo4;

import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.stage.Window;

/**
 * 
 * @Title: CandyToaster.java
 * @Package top.jacktgq.custom_controls.toaster
 * @Description: 短消息提示框
 * @author CandyWall
 * @date 2021年5月21日 下午10:11:59
 * @version V1.0
 */
public class MyToasterWindow extends Popup {

    private LayoutPane root;

    public MyToasterWindow() {
        root = new LayoutPane();

        getScene().setRoot(root);

        // setAutoHide(true); // 离焦不会自动消失
        setHideOnEscape(true); // 按ESC键可以关闭弹出框
        // 给Popup的左上角作为锚点，即设置Popup的x和y坐标的时候以左上角为基准
        setAnchorLocation(AnchorLocation.CONTENT_TOP_LEFT);
        sizeToScene();
    }

    // 创建一个专门用于布局的类
    private class LayoutPane extends Pane {
        private Color bgColor = Color.web("#ffcc00", 0.8);
        private Canvas bgCanvas = new Canvas();
        private Text textNode = new Text();

        public LayoutPane() {
            setPadding(new Insets(10));
            getChildren().addAll(bgCanvas, textNode);
        }

        @Override
        protected double computePrefWidth(double height) {
            // 减去padding
            Insets insets = getInsets();
            return textNode.getBoundsInLocal().getWidth() + insets.getLeft() + insets.getRight();
        }

        @Override
        protected double computePrefHeight(double width) {
            // 减去padding
            Insets insets = getInsets();
            return textNode.getBoundsInLocal().getHeight() + insets.getTop() + insets.getBottom();
        }

        @Override
        protected void layoutChildren() {
            super.layoutChildren();

            double w = computePrefWidth(0);
            double h = computePrefHeight(0);
            bgCanvas.setWidth(w);
            bgCanvas.setHeight(h);
            // 绘制一下背景
            paintBg(w, h);

            Insets insets = getInsets();
            textNode.relocate(insets.getLeft(), insets.getTop());
        }

        private void paintBg(double w, double h) {
            GraphicsContext gc = bgCanvas.getGraphicsContext2D();
            gc.clearRect(0, 0, w, h);
            gc.setFill(bgColor);
            gc.fillRoundRect(0, 0, w, h, 12, 12);
        }

        public void setText(String text, double wrappingWidth) {
            textNode.setText(text);
            Bounds bounds = textNode.getBoundsInLocal();
            if (bounds.getWidth() > wrappingWidth) {
                textNode.setWrappingWidth(wrappingWidth);
            }
        }

        public void setTextColor(Color color) {
            textNode.setFill(color);
        }

        public void setBgColor(Color bgColor) {
            this.bgColor = bgColor;
            double w = computePrefWidth(0);
            double h = computePrefHeight(0);
            paintBg(w, h);
        }
    }

    public void setText(String text) {
        setText(text, 240);
    }

    public void setText(String text, double wrappingWidth) {
        root.setText(text, wrappingWidth);
    }

    public void setTextColor(Color color) {
        root.setTextColor(color);
    }

    public void setBgColor(Color bgColor) {
        root.setBgColor(bgColor);
    }

    public void show(Window owner) {
        double x = owner.getX();
        double y = owner.getY();
        double w = owner.getWidth();
        double h = owner.getHeight();
        Bounds bounds = root.getBoundsInLocal();
        double width = bounds.getWidth();
        double height = bounds.getHeight();

        x += (w - width) / 2;
        y += (h - height) / 2;

        super.show(owner, x, y);
    }
}
