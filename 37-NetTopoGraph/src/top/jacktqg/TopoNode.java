package top.jacktqg;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import top.jacktgq.utils.AfCanvasPane;
import top.jacktgq.utils.AfCanvasUtils;
import top.jacktgq.utils.AfImageUtils;
import top.jacktgq.utils.AfRectUtils;

public class TopoNode extends AfCanvasPane {
    Image image = new Image(getClass().getResource("ic_topo_node.png").toExternalForm());

    private boolean hover = false;
    private boolean dragging = false;
    private double startLayoutX, startLayoutY;
    private double startScreenX, startScreenY; // 相对于父容器

    TopoData data = null;
    TopoInfoWindow infoWindow = new TopoInfoWindow(this);

    public TopoNode(TopoData data) {
        this.data = data;
        initDragSupport();
        this.setPrefSize(100, 130);
    }

    public void setData(TopoData data) {
        this.data = data;
        update();
    }

    @Override
    protected void update(GraphicsContext gc, double w, double h) {
        // gc.setFill(Color.RED);
        // gc.fillRect(0, 0, w, h);
        gc.clearRect(0, 0, w, h);

        Rectangle2D r = new Rectangle2D(0, 0, w, h);
        if (hover) {
            gc.setFill(Color.web("#fff1"));
            gc.fillRect(0, 0, w, h);
        }

        // 画图标
        double h1 = 0;
        double h2 = 30;
        double h3 = 30;
        h1 = h - h2 - h3;

        if (image != null) {
            Rectangle2D r1 = new Rectangle2D(0, 0, image.getWidth(), image.getHeight());
            Rectangle2D r2 = new Rectangle2D(0, 0, w, h1);

            Rectangle2D r3 = AfImageUtils.centerInside(r1, r2);
            AfCanvasUtils.drawImage(gc, image, r3);
        }

        // System.out.println("draw: " + w + "," + h);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setTextBaseline(VPos.CENTER);
        gc.setFill(Color.WHITE);
        if (data != null) {
            // 故障时以橙色显示
            if (data.status != 0)
                gc.setFill(Color.ORANGE);
        }

        if (data != null && data.name != null) {
            Rectangle2D r1 = new Rectangle2D(0, h1, w, h2);
            double cx = AfRectUtils.centerX(r1);
            double cy = AfRectUtils.centerY(r1);
            gc.fillText(data.name, cx, cy);
        }
        if (data != null && data.ip != null) {
            Rectangle2D r1 = new Rectangle2D(0, h1 + h2, w, h3);
            double cx = AfRectUtils.centerX(r1);
            double cy = AfRectUtils.centerY(r1);
            gc.fillText(data.ip, cx, cy);
        }
    }

    private void initDragSupport() {
        this.setOnMouseEntered((e) -> {
            hover = true;
            update();
        });
        this.setOnMouseExited((e) -> {
            hover = false;
            update();
        });

        this.setOnMousePressed((MouseEvent e) -> {
            if (e.getButton() == MouseButton.SECONDARY) {
                showInfoWindow();
                return;
            }

            startLayoutX = getLayoutX();
            startLayoutY = getLayoutY();

            startScreenX = e.getScreenX();
            startScreenY = e.getScreenY();

            // System.out.println("setOnMousePressed ...: " + e.getX() + "," +
            // e.getY());
        });

        this.setOnDragDetected((MouseEvent e) -> {
            dragging = true;
            hideInfoWindow();
            // System.out.println("setOnDragDetected ...: " + e.getX() + "," +
            // e.getY());
        });

        this.setOnMouseDragged((MouseEvent e) -> {
            // System.out.println(id + " mouse dragged: " + e.getX() + "," +
            // e.getY());

            double dx = e.getScreenX() - startScreenX;
            double dy = e.getScreenY() - startScreenY;

            this.setLayoutX(startLayoutX + dx);
            this.setLayoutY(startLayoutY + dy);
        });

        this.setOnMouseReleased((MouseEvent e) -> {

            dragging = false;
            // System.out.println("setOnMouseReleased ...: " + e.getX() + "," +
            // e.getY());
        });
    }

    private void showInfoWindow() {
        Point2D pos = this.localToScreen(getWidth(), 0);
        infoWindow.show(this.getScene().getWindow(), pos.getX(), pos.getY());
    }

    private void hideInfoWindow() {
        infoWindow.hide();
    }
}
