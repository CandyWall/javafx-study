package top.jacktgq.example.carousel.demo1;

import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DisplacementMap;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();
            Button btn1 = new Button("开始");
            Button btn2 = new Button("逆向");

            StackPane sp = new StackPane();
            sp.setPrefWidth(400);
            sp.setPrefHeight(400);
            sp.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, null, new BorderWidths(2))));

            HBox hbox1 = new HBox();
            hbox1.setAlignment(Pos.CENTER);
            hbox1.getChildren().add(new Button("button1"));
            // hbox1.setPrefWidth(400);
            // hbox1.setPrefHeight(400);
            hbox1.setBackground(new Background(new BackgroundFill(Color.BLANCHEDALMOND, null, null)));

            HBox hbox2 = new HBox();
            hbox2.setAlignment(Pos.CENTER);
            hbox2.getChildren().add(new Button("button2"));
            // hbox2.setPrefWidth(400);
            // hbox2.setPrefHeight(400);
            hbox2.setBackground(new Background(new BackgroundFill(Color.AQUAMARINE, null, null)));
            // hbox2.setTranslateX(200);
            Pane pane = new Pane();
            TranslateTransition tt = new TranslateTransition();
            // tt.setDelay(Duration.seconds(2));
            tt.setDuration(Duration.seconds(1));

            tt.setNode(pane);
            // tt.setCycleCount(Animation.INDEFINITE);
            tt.setInterpolator(Interpolator.LINEAR);

            DisplacementMap dismap1 = new DisplacementMap();
            // dismap.setWrap(true);
            dismap1.setOffsetX(0);
            hbox1.setEffect(dismap1);

            DisplacementMap dismap2 = new DisplacementMap();
            // dismap.setWrap(true);
            dismap2.setOffsetX(0);
            hbox2.setEffect(dismap2);
            pane.translateXProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    dismap2.setOffsetX(-newValue.doubleValue() / 400);
                    dismap1.setOffsetX(1 - newValue.doubleValue() / 400);
                }
            });

            tt.setOnFinished(e -> {
                // hbox2.setVisible(false);
                System.out.println(dismap1.getOffsetX());
                System.out.println(dismap2.getOffsetX());
            });

            sp.getChildren().addAll(hbox1, hbox2);

            root.getChildren().addAll(btn1, btn2, sp);
            AnchorPane.setTopAnchor(sp, 200.0);
            AnchorPane.setLeftAnchor(sp, 500.0);
            AnchorPane.setLeftAnchor(btn2, 100.0);

            Scene scene = new Scene(root, 1400, 800);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("跑马灯特效");
            primaryStage.show();

            btn1.setOnAction(e -> {
                tt.setFromX(0);
                tt.setToX(400);
                tt.play();
            });

            btn2.setOnAction(e -> {
                tt.setFromX(400);
                tt.setToX(0);
                tt.play();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
