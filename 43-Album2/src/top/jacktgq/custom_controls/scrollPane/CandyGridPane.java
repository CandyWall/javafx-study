package top.jacktgq.custom_controls.scrollPane;

import javafx.scene.Node;
import javafx.scene.control.ScrollPane;

public class CandyGridPane extends ScrollPane {
    private CandyGridLayout container = new CandyGridLayout();

    /**
     * 
     * @param cellMaxW
     *            单元格最大宽度
     * @param cellMinW
     *            单元格最小宽度
     * @param cellAspectH
     *            单元格横纵比-宽度
     * @param cellAspectV
     *            单元格横纵比-高度
     * @param hGap
     *            水平方向单元格间隔
     * @param vGap
     *            竖直方向单元格间隔
     */
    public CandyGridPane(double cellMinW, double cellMaxW, double cellAspectH, double cellAspectV, double hGap,
            double vGap) {
        container.cellMaxW = cellMaxW;
        container.cellMinW = cellMinW;
        container.cellAspectH = cellAspectH;
        container.cellAspectV = cellAspectV;
        container.hGap = hGap;
        container.vGap = vGap;
        setHbarPolicy(ScrollBarPolicy.NEVER);
        setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

        this.setContent(container);
    }

    public CandyGridPane(double cellMinW, double cellMaxW, double cellAspectH, double cellAspectV) {
        this(cellMinW, cellMaxW, cellAspectH, cellAspectV, 5, 5);
    }

    public CandyGridPane(double cellMinW, double cellMaxW) {
        this(cellMinW, cellMaxW, 1, 1, 5, 5);
    }

    public CandyGridPane() {
        this(100, 150, 1, 1, 5, 5);
    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        container.setPrefWidth(this.getViewportBounds().getWidth() - 1);
    }

    public void add(Node node) {
        container.getChildren().add(node);
    }
}
