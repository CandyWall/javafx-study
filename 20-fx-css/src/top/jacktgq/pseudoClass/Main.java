package top.jacktgq.pseudoClass;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            HBox topPane = new HBox();
            TextField textField = new TextField();

            Button submit = new Button("提交");
            Button reset = new Button("重置");
            CheckBox checkbox = new CheckBox("下载完毕自动关机");

            topPane.getChildren().addAll(textField, submit, reset, checkbox);
            root.setTop(topPane);

            TextArea textArea = new TextArea();
            root.setCenter(textArea);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets()
                    .add(getClass().getResource("/top/jacktgq/pseudoClass/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
