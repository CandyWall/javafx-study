package top.jacktgq.demo5;

import javafx.application.Application;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.window.CandyWindowWrapper;

public class Main2 extends Application {

    CandyWindowWrapper asww;

    @Override
    public void start(Stage primaryStage) {

        try {
            BorderPane root = new BorderPane();

            asww = new CandyWindowWrapper(primaryStage);
            asww.setContentView(root, 800, 600);
            asww.initDragSupport(null, true);
            asww.show();

            Button button = new Button("新窗口");
            root.setTop(button);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
