package top.jacktgq.custom_controls.tab;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class CandyTabPane extends Pane implements CandyTabObserver {
    Node currentTabView = null;
    public CandyTabContext context = new CandyTabContext();

    public CandyTabPane() {
        context.setObserver(this);
    }

    ///////////////////////////

    public void addTab(CandyTab tab) {
        // tab.onCreate();
        // this.getChildren().add( tab.getContentView());
        context.addTab(tab);
    }

    public void removeTab(CandyTab tab) {
        context.removeTab(tab);
    }

    public void removeTab(int index) {
        context.removeTab(index);
    }

    public void removeTab(String tabName) {
        context.removeTab(tabName);
    }

    public void setCurrentTab(String tabName) {
        this.context.setCurrentTab(null, tabName);
    }

    public void setCurrentTab(CandyTab tab) {
        this.context.setCurrentTab(null, tab);
    }

    public CandyTab getCurrentTab() {
        return this.context.getCurrentTab();
    }

    //////////////////////////////

    @Override
    protected void layoutChildren() {
        double w = getWidth(), h = getHeight();
        if (w <= 0 || h <= 0)
            return;

        // padding支持
        Insets insets = this.getInsets();

        ObservableList<Node> nodes = this.getChildren();
        for (Node node : nodes) {
            if (node == currentTabView) {
                // node.resizeRelocate(0, 0, w, h);
                node.resizeRelocate(insets.getLeft(), insets.getTop(), w - insets.getLeft() - insets.getRight(),
                        h - insets.getTop() - insets.getBottom());

                node.setVisible(true);
            } else {
                node.setVisible(false);
            }
        }
    }

    //////////////////////////////

    @Override
    public void onTabShow(CandyTab node) {
        if (this.getChildren().indexOf(node.getContentView()) < 0) {
            this.getChildren().add(node.getContentView());
        }

        currentTabView = node.getContentView();
        requestLayout();
    }

    @Override
    public void onTabDismiss(CandyTab node) {

    }

}
