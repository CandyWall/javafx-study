package top.jacktgq.lineChart;

import javafx.application.Application;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button btn = new Button("确定");
        FlowPane root = new FlowPane();
        root.getChildren().addAll(btn, getLineChart1(), getLineChart2(), getLineChart3());

        primaryStage.setTitle("折线图");
        primaryStage.setScene(new Scene(root, 1800, 800));
        primaryStage.getScene().getStylesheets().add(getClass().getResource("chart.css").toExternalForm());
        primaryStage.show();

        btn.setOnAction(e -> {
        });
    }

    private LineChart getLineChart1() {
        LineChart.Data<String, Number> d1 = new LineChart.Data<String, Number>("2016", 50);
        LineChart.Data<String, Number> d2 = new LineChart.Data<String, Number>("2017", 40);
        LineChart.Data<String, Number> d3 = new LineChart.Data<String, Number>("2018", 45);
        LineChart.Data<String, Number> d4 = new LineChart.Data<String, Number>("2019", 42);
        // ObservableList<BarChart.Data<String, Number>> data1 = FXCollections.observableArrayList(d1, d2, d3, d4);
        XYChart.Series<String, Number> series1 = new XYChart.Series<>();
        series1.setName("计划招生人数");
        series1.getData().addAll(d1, d2, d3, d4);
        LineChart.Data<String, Number> d5 = new LineChart.Data<String, Number>("2016", 37);
        LineChart.Data<String, Number> d6 = new LineChart.Data<String, Number>("2017", 77);
        LineChart.Data<String, Number> d7 = new LineChart.Data<String, Number>("2018", 42);
        LineChart.Data<String, Number> d8 = new LineChart.Data<String, Number>("2019", 40);
        // ObservableList<BarChart.Data<String, Number>> data2 = FXCollections.observableArrayList(d5, d6, d7, d8);
        XYChart.Series<String, Number> series2 = new XYChart.Series<>();
        series2.setName("实际报道人数");
        series2.getData().addAll(d5, d6, d7, d8);
        // ObservableList<XYChart.Series<String, Number>> dataList = FXCollections.observableArrayList(series1, series2);

        CategoryAxis xAxis = new CategoryAxis();
        // x轴的相关参数
        xAxis.setLabel("年份");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("人数");
        LineChart lineChart = new LineChart(xAxis, yAxis);
        lineChart.getData().addAll(series1, series2);
        lineChart.setId("candy-line-chart");

        // 从右往左开始绘制 RIGHT_TO_LEFT
        lineChart.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
        lineChart.setTitle("2016-2020空间专业招生情况1");
        return lineChart;
    }

    private LineChart getLineChart2() {
        LineChart.Data<Number, String> d1 = new LineChart.Data<Number, String>(50, "2016");
        LineChart.Data<Number, String> d2 = new LineChart.Data<Number, String>(40, "2017");
        LineChart.Data<Number, String> d3 = new LineChart.Data<Number, String>(45, "2018");
        LineChart.Data<Number, String> d4 = new LineChart.Data<Number, String>(42, "2019");
        // ObservableList<BarChart.Data<String, Number>> data1 = FXCollections.observableArrayList(d1, d2, d3, d4);
        XYChart.Series<Number, String> series1 = new XYChart.Series<>();
        series1.setName("计划招生人数");
        series1.getData().addAll(d1, d2, d3, d4);
        LineChart.Data<Number, String> d5 = new LineChart.Data<Number, String>(37, "2016");
        LineChart.Data<Number, String> d6 = new LineChart.Data<Number, String>(77, "2017");
        LineChart.Data<Number, String> d7 = new LineChart.Data<Number, String>(42, "2018");
        LineChart.Data<Number, String> d8 = new LineChart.Data<Number, String>(40, "2019");
        // ObservableList<BarChart.Data<String, Number>> data2 = FXCollections.observableArrayList(d5, d6, d7, d8);
        XYChart.Series<Number, String> series2 = new XYChart.Series<>();
        series2.setName("实际报道人数");
        series2.getData().addAll(d5, d6, d7, d8);
        // ObservableList<XYChart.Series<String, Number>> dataList = FXCollections.observableArrayList(series1, series2);

        CategoryAxis xAxis = new CategoryAxis();
        // x轴的相关参数
        xAxis.setLabel("年份");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("人数");
        LineChart lineChart = new LineChart(yAxis, xAxis);
        lineChart.getData().addAll(series1, series2);
        lineChart.setId("candy-line-chart");

        // 从右往左开始绘制 RIGHT_TO_LEFT
        lineChart.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
        lineChart.setTitle("2016-2020空间专业招生情况1");
        return lineChart;
    }

    private LineChart getLineChart3() {
        LineChart.Data<String, Number> d1 = new LineChart.Data<String, Number>("2016", 50);
        LineChart.Data<String, Number> d2 = new LineChart.Data<String, Number>("2017", 40);
        LineChart.Data<String, Number> d3 = new LineChart.Data<String, Number>("2018", 45);
        LineChart.Data<String, Number> d4 = new LineChart.Data<String, Number>("2019", 42);
        // ObservableList<BarChart.Data<String, Number>> data1 = FXCollections.observableArrayList(d1, d2, d3, d4);
        XYChart.Series<String, Number> series1 = new XYChart.Series<>();
        series1.setName("计划招生人数");
        series1.getData().addAll(d1, d2, d3, d4);
        LineChart.Data<String, Number> d5 = new LineChart.Data<String, Number>("2016", 37);
        LineChart.Data<String, Number> d6 = new LineChart.Data<String, Number>("2017", 77);
        LineChart.Data<String, Number> d7 = new LineChart.Data<String, Number>("2018", 42);
        LineChart.Data<String, Number> d8 = new LineChart.Data<String, Number>("2019", 40);
        // ObservableList<BarChart.Data<String, Number>> data2 = FXCollections.observableArrayList(d5, d6, d7, d8);
        XYChart.Series<String, Number> series2 = new XYChart.Series<>();
        series2.setName("实际报道人数");
        series2.getData().addAll(d5, d6, d7, d8);
        // ObservableList<XYChart.Series<String, Number>> dataList = FXCollections.observableArrayList(series1, series2);

        series1.getData().forEach(data -> {
            data.setNode(new Label(data.getYValue() + ""));
            Tooltip tp = new Tooltip(data.getXValue() + "年计划招生人数：" + data.getYValue());
            Tooltip.install(data.getNode(), tp);
        });

        CategoryAxis xAxis = new CategoryAxis();
        // x轴的相关参数
        xAxis.setLabel("专业");
        // 距离左（上）边的距离
        // xAxis.setStartMargin(50);
        // 距离右（下）边的距离
        // xAxis.setEndMargin(50);
        // 前后是否留空间，默认为true
        // xAxis.setGapStartAndEnd(true);
        // 轴位置
        // xAxis.setSide(Side.TOP);
        // 刻度标签颜色
        xAxis.setTickLabelFill(Color.web("#ffcc00"));
        // 刻度标签字体
        xAxis.setTickLabelFont(Font.font(16));
        // 类型名称与轴刻度的距离
        xAxis.setTickLabelGap(0);
        // 类型名称旋转
        xAxis.setTickLabelRotation(45);
        // 是否显示刻度标签
        xAxis.setTickLabelsVisible(true);
        // 刻度长度
        xAxis.setTickLength(20);
        // 是否显示刻度
        xAxis.setTickMarkVisible(true);

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("人数");
        LineChart lineChart = new LineChart(xAxis, yAxis);
        lineChart.getData().addAll(series1, series2);
        lineChart.setId("candy-line-chart");

        // 从右往左开始绘制 RIGHT_TO_LEFT
        lineChart.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
        lineChart.setTitle("2016-2020空间专业招生情况2");
        // 设置标题显示的位置
        lineChart.setTitleSide(Side.TOP);
        // 是否显示图例
        lineChart.setLegendVisible(true);
        // 设置图例显示的位置
        lineChart.setLegendSide(Side.LEFT);
        // 当数据发生改变时是否显示动画
        lineChart.setAnimated(true);
        // 是否显示拐点
        lineChart.setCreateSymbols(true);
        return lineChart;
    }

    public static void main(String[] args) {
        launch(args);
    }
}