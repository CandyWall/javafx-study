package top.jacktgq.utils;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;

public abstract class AfCanvasPane extends Pane {
    Canvas canvas = new Canvas();

    public AfCanvasPane() {
        this.getChildren().add(canvas);
    }

    @Override
    public void resize(double width, double height) {
        super.resize(width, height);
        canvas.setWidth(width);
        canvas.setHeight(height);
        update();
    }

    @Override
    protected void layoutChildren() {
        canvas.resizeRelocate(0, 0, this.getWidth(), this.getHeight());
    }

    public void update() {
        double w = this.getWidth();
        double h = this.getHeight();
        if (w <= 0 || h <= 0)
            return;

        GraphicsContext gc = canvas.getGraphicsContext2D();
        update(gc, w, h);
    }

    protected abstract void update(GraphicsContext gc, double w, double h);
}
