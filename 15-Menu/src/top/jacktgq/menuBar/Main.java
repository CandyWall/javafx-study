package top.jacktgq.menuBar;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 * 
 * @Title: Main.java
 * @Package application
 * @Description: 通过文件选择对话框，将文本保存到文件，从文件中读取文本
 * @author CandyWall
 * @date 2021年5月15日 上午1:45:27
 * @version V1.0
 */
public class Main extends Application {
    private HTMLEditor editor;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            MenuItem openMenuItem = new MenuItem("打开", new ImageView("icons/open.png"));
            openMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    FileChooser chooser = new FileChooser();
                    chooser.getExtensionFilters().add(new ExtensionFilter("HTML Files", "*.html"));
                    File file = chooser.showOpenDialog(primaryStage);
                    if (file == null)
                        return;
                    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                        StringBuffer sb = new StringBuffer();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                        editor.setHtmlText(sb.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            MenuItem saveMenuItem = new MenuItem("保存", new ImageView("icons/save.png"));
            saveMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    FileChooser chooser = new FileChooser();
                    chooser.getExtensionFilters().add(new ExtensionFilter("HTML Files", "*.html"));
                    File file = chooser.showSaveDialog(primaryStage);
                    if (file == null)
                        return;
                    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                        writer.write(editor.getHtmlText());
                        writer.flush();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            MenuItem exitMenuItem = new MenuItem("退出", new ImageView("icons/exit.png"));
            exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    Platform.exit();
                }
            });

            Menu menu = new Menu("文件", null, openMenuItem, saveMenuItem, exitMenuItem);
            MenuBar menuBar = new MenuBar(menu);

            root.setTop(menuBar);

            editor = new HTMLEditor();
            root.setCenter(editor);

            Scene scene = new Scene(root, 800, 600);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("通过文件选择对话框，将文本保存到文件，从文件中读取文本");
            primaryStage.setMaximized(true);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
