package top.jacktgq.utils;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class HikariUtils {
    // 获得Connection，从连接池中获取
    private static HikariDataSource datasource;
    // 创建ThreadLocal对象
    private static ThreadLocal<Connection> tl = new ThreadLocal<Connection>();

    public static HikariDataSource init(String host, String port, String username, String password) {
        FileInputStream in = null;
        try {
            in = new FileInputStream(new File("hikari.properties"));
            Properties props = new Properties();
            props.load(in);
            HikariConfig config = new HikariConfig(props);
            config.setJdbcUrl("jdbc:mysql://" + host + ":" + port
                    + "/information_schema?useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai&useSSL=false&allowPublicKeyRetrieval=true");
            config.setUsername(username);
            config.setPassword(password);

            datasource = new HikariDataSource(config);
            return datasource;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("连接数据库失败！", e);
        }
    }

    // 开启事务
    public static void startTransaction() throws SQLException {
        getConnection().setAutoCommit(false);
    }

    // 回滚事务
    public static void rollbackTransaction() throws SQLException {
        getConnection().rollback();
    }

    // 提交事务
    public static void commitTransaction() throws SQLException {
        getConnection().commit();
        tl.remove();
    }

    // 控制事务的时候用该方法
    public static Connection getConnection() throws SQLException {
        // 从ThreadLocal对象中寻找当前线程上是否有对应的Connection对象
        Connection conn = tl.get();
        // 如果ThreadLocal中没有Connection对象
        if (conn == null) {
            // 获得新的Connection
            conn = getCurrentConnection();
            // 将Connection资源绑定到ThreadLocal(map)上
            tl.set(conn);
        }
        return conn;
    }

    // 不需要控制事务的时候获取连接池即可
    public static DataSource getDataSource() {
        return datasource;
    }

    // 内部调用该方法
    public static Connection getCurrentConnection() throws SQLException {
        return datasource.getConnection();
    }

    public static void main(String[] args) throws SQLException {
        QueryRunner runner = new QueryRunner(HikariUtils.getDataSource());
        List<Map<String, Object>> result = runner.query("select * from userinfo", new MapListHandler());
        result.forEach(System.out::println);
    }

}
