package top.jacktgq.demo2;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import top.jacktgq.custom_controls.tab.CandyTabPane;

public class TestPane extends BorderPane {
    private CandyTabPane tabPane = new CandyTabPane();
    private TabPage1 tab1 = new TabPage1();
    private TabPage2 tab2 = new TabPage2();

    public TestPane() {
        tabPane.setStyle("-fx-background-color: white");
        tab1.setName("tab1");
        tab2.setName("tab2");
        tabPane.addTab(tab1);
        tabPane.addTab(tab2);
        tabPane.setCurrentTab(tab1);
        this.setCenter(tabPane);

        initTabLabels();
    }

    private void initTabLabels() {
        TabLabelBar labelBar = new TabLabelBar();
        labelBar.addLabel("tab1", "人物信息");
        labelBar.addLabel("tab2", "人物照片");
        labelBar.setActive("tab1", true);

        this.setTop(labelBar);
    }

    private class TabLabelBar extends HBox {

        public TabLabelBar() {
            this.getStyleClass().add("tab-label-bar");
        }

        public void addLabel(String id, String tabTitle) {
            Label b = new Label(tabTitle);
            b.getStyleClass().add("item");
            b.setUserData(id);
            b.addEventHandler(MouseEvent.MOUSE_CLICKED, clickHandler);
            this.getChildren().add(b);
        }

        private EventHandler<MouseEvent> clickHandler = e -> {
            for (Node node : this.getChildren()) {
                if (e.getSource() == node) {
                    setActive(node, true);
                    String id = (String) node.getUserData();
                    tabPane.setCurrentTab(id);
                } else {
                    setActive(node, false);
                }
            }
        };

        public void setActive(String id, boolean active) {
            for (Node node : this.getChildren()) {
                if (id.equals(node.getUserData())) {
                    setActive(node, active);
                    break;
                }
            }
        }

        public void setActive(Node node, boolean active) {
            if (active) {
                node.getStyleClass().add("active");
            } else {
                node.getStyleClass().removeIf(t -> t.equals("active"));
            }
        }

    }
}
