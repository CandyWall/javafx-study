package application;

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();

            Button openBaidu = new Button("www.baidu.com");
            root.getChildren().add(openBaidu);
            Button openWangyiyun = new Button(
                    "https://study.163.com/course/introduction.htm?courseId=1005241020#/courseDetail?tab=1");
            root.getChildren().add(openWangyiyun);

            AnchorPane.setTopAnchor(openWangyiyun, 50.0);

            EventHandler<ActionEvent> handler = new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    HostServices services = Main.this.getHostServices();
                    services.showDocument(((Button) event.getSource()).getText());
                }
            };
            openBaidu.setOnAction(handler);
            openWangyiyun.setOnAction(handler);

            Scene scene = new Scene(root, 600, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("点击按钮用浏览器打开相应的网址");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
