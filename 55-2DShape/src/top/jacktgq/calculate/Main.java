package top.jacktgq.calculate;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();

            Rectangle rect = new Rectangle(200, 100);
            rect.setFill(Color.GREEN);

            Circle circle = new Circle(100, 100, 50);
            circle.setFill(Color.RED);

            Shape intersectShape = Shape.intersect(rect, circle);
            intersectShape.setFill(Color.BLUE);
            intersectShape.setLayoutX(200);

            Shape substractShape1 = Shape.subtract(rect, circle);
            substractShape1.setFill(Color.BLUE);
            substractShape1.setLayoutX(400);

            Shape substractShape2 = Shape.subtract(circle, rect);
            substractShape2.setFill(Color.BLUE);
            substractShape2.setLayoutX(600);

            Shape unionShape = Shape.union(circle, rect);
            unionShape.setFill(Color.BLUE);
            unionShape.setLayoutX(800);

            root.getChildren().addAll(rect, circle, intersectShape, substractShape1, substractShape2, unionShape);

            Scene scene = new Scene(root, 1600, 800);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
