package top.jacktgq.demo2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.Window;
import top.jacktgq.custom_controls.window.CandyTransparentImageFloatWindow;

public class Main extends Application {
    private CandyTransparentImageFloatWindow imageFloatWindow;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            HBox hbox = new HBox();
            Button btn_show = new Button("弹出透明的浮动窗口");
            btn_show.setOnAction((e) -> {
                openImageFloatWindow(primaryStage);
            });
            Button btn_hide = new Button("隐藏透明的浮动窗口");
            btn_hide.setOnAction((e) -> {
                hideImageFloatWindow();
            });
            hbox.getChildren().addAll(btn_show, btn_hide);
            root.setTop(hbox);
            Scene scene = new Scene(root, 400, 400);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openImageFloatWindow(Window owner) {
        if (imageFloatWindow == null) {
            imageFloatWindow = new CandyTransparentImageFloatWindow(owner, new Image("images/kenan-2.png"), 200, 200);
        }
        imageFloatWindow.show();
    }

    private void hideImageFloatWindow() {
        if (imageFloatWindow != null) {
            imageFloatWindow.hide();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
