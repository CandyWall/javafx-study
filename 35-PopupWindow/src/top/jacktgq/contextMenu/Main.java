package top.jacktgq.contextMenu;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();

            // 上下文菜单
            MenuItem aboutItem = new MenuItem("关于");
            CheckMenuItem putWindowOnTopItem = new CheckMenuItem("窗口置顶");
            MenuItem helpItem = new MenuItem("帮助");

            aboutItem.setOnAction(e -> {

            });

            putWindowOnTopItem.setOnAction(e -> {
                primaryStage.setAlwaysOnTop(putWindowOnTopItem.isSelected());
            });

            helpItem.setOnAction(e -> {

            });

            // 自定义菜单项
            CandyMenuItem setAutoStart = new CandyMenuItem("开机启动");
            setAutoStart.setCheckable(true);
            setAutoStart.setChecked(false);
            setAutoStart.setOnAction((e) -> {
                primaryStage.setAlwaysOnTop(setAutoStart.isChecked());
            });

            ContextMenu contextMenu = new ContextMenu(aboutItem, putWindowOnTopItem, new SeparatorMenuItem(), helpItem,
                    setAutoStart);
            contextMenu.setId("candy-context-menu");
            root.setOnContextMenuRequested(e -> {
                // 如果上一次右键打开菜单没有点击，再点击其他位置打开菜单会有显示问题，所有不管有没有点击先调用hide()方法关闭菜单
                contextMenu.hide();
                contextMenu.show(root, e.getScreenX(), e.getScreenY());
            });
            /*
             * root.setOnMouseClicked(e -> { contextMenu.hide(); if
             * (e.getButton() == MouseButton.SECONDARY) { contextMenu.show(root,
             * e.getScreenX(), e.getScreenY()); } });
             */

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
