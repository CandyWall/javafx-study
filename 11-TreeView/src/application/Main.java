package application;

import java.io.File;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * 
 * @Title: Main.java
 * @Package application
 * @Description: 用TreeView加载目录和文件
 * @author CandyWall
 * @date 2021年5月14日 下午5:37:57
 * @version V1.0
 */
public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            TreeView<FileItem> treeView = new TreeView<>();

            treeView.setCellFactory(new Callback<TreeView<FileItem>, TreeCell<FileItem>>() {
                @Override
                public TreeCell<FileItem> call(TreeView<FileItem> param) {
                    return new MyTreeCell();
                }
            });

            File file = new File("./");
            treeView.setRoot(listFile(file, true));

            root.setCenter(treeView);

            Scene scene = new Scene(root, 800, 600);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 如果不指定，默认是不展开的
    private TreeItem<FileItem> listFile(File file) {
        return listFile(file, false);
    }

    // 将File下的文件夹和文件都装到TreeItem中，expanded是否要展开这个节点
    private TreeItem<FileItem> listFile(File file, boolean expanded) {
        TreeItem<FileItem> treeItem = new TreeItem<FileItem>(new FileItem(file, file.isDirectory()));
        if (file.isDirectory()) {
            treeItem.setExpanded(expanded);
            for (File childFile : file.listFiles()) {
                if (childFile.isDirectory()) {
                    TreeItem<FileItem> childTreeItem = listFile(childFile, expanded);
                    treeItem.getChildren().add(childTreeItem);
                    childTreeItem.setExpanded(expanded);
                } else {
                    treeItem.getChildren().add(new TreeItem<FileItem>(new FileItem(childFile, false)));
                }
            }
        }

        return treeItem;
    }

    /*
     * private void listFile(File file, TreeItem<FileItem> treeItem) {
     * TreeItem<FileItem> childTreeItem = new TreeItem<FileItem>(new
     * FileItem(file, file.isDirectory()));
     * treeItem.getChildren().add(childTreeItem); if (file.isDirectory()) {
     * for(File childFile : file.listFiles()) { if(childFile.isDirectory()) {
     * listFile(childFile, childTreeItem); } else {
     * childTreeItem.getChildren().add(new TreeItem<FileItem>(new
     * FileItem(childFile, childFile.isDirectory()))); } } } }
     */

    private class MyTreeCell extends TreeCell<FileItem> {
        @Override
        protected void updateItem(FileItem item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {
                // HBox hbox = new HBox();
                ImageView iconView = new ImageView(item.isDir ? "icons/ic_folder.png" : "icons/ic_file.png");
                // Label namelabel = new Label(item.file.getName());
                // hbox.getChildren().addAll(iconView, namelabel);
                // this.setGraphic(hbox);
                this.setText(item.file.getName());
                this.setGraphic(iconView);
            } else {
                this.setGraphic(null);
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
