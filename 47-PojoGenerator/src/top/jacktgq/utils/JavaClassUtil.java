package top.jacktgq.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

public class JavaClassUtil {
    public static void generateJavaClass(List<Map<String, Object>> mapList, String tableName, String packageName,
            File file) throws Exception {

        String attributes = "";
        String functions = "";

        // String autoIncrementKey = null;
        // 生成属性和方法
        for (Map<String, Object> map : mapList) {
            String attr = (String) map.get("COLUMN_NAME");
            String type = "String";

            // if (p.arg2.indexOf("auto_increment") >= 0)
            // autoIncrementKey = attr;

            String dbType = (String) map.get("DATA_TYPE");
            if (dbType.equals("tinyint")) {
                type = "Byte";
                /*
                 * if (p.arg1.equals("tinyint(1)")) type = "Boolean";
                 */
            }

            else if (dbType.equals("smallint"))
                type = "Short";
            else if (dbType.equals("int"))
                type = "Integer";
            else if (dbType.equals("bigint"))
                type = "Long";
            else if (dbType.equals("datetime"))
                type = "Date";
            else if (dbType.equals("bit"))
                type = "Boolean";
            else if (dbType.equals("double"))
                type = "Double";
            else if (dbType.equals("float"))
                type = "Float";

            // attributes
            if (true) {
                attributes += String.format("\tprivate %s %s ; \r\n", type, attr);
            }

            // setters and getters
            if (true) {
                String func = setterMethodName(attr);
                String fmt = "\tpublic void %s(%s %s) {\r\n" + "\t\tthis.%s=%s;\r\n" + "\t}\r\n";
                String str = String.format(fmt, func, type, attr, attr, attr);
                functions += str;
            }
            if (true) {
                String func = getterMethodName(attr);
                String fmt = "\tpublic %s %s() {\r\n" + "\t\treturn this.%s;\r\n" + "\t}\r\n";
                String str = String.format(fmt, type, func, attr);
                functions += str;
            }
        }

        String text = "";

        text += String.format("package %s; \r\n\r\n", packageName);

        text += "import java.util.Date; \r\n";
        text += "\r\n";

        String className = firstCharUpperCase(tableName);
        text += String.format("public class %s { \r\n", className);
        // 生成方法
        text += "\r\n" + attributes + "\r\n";
        text += "\r\n" + functions + "\r\n";
        text += "}\r\n ";

        if (!file.exists()) {
            file.mkdirs();
        }

        FileOutputStream ostream = new FileOutputStream(new File(file, className + ".java"));
        ostream.write(text.getBytes("UTF-8"));
        ostream.close();
    }

    /**
     * 根据属性名称，构造出对应的get方法的名称
     * 
     * @param attr
     * @return
     */
    public static String getterMethodName(String attr) {
        // "name" -> "getName()"
        return "get" + firstCharUpperCase(attr);
    }

    /**
     * 根据属性名称，构造出对应的set方法的名称
     * 
     * @param attr
     * @return
     */
    public static String setterMethodName(String attr) {
        // "name" -> "setName()"
        return "set" + firstCharUpperCase(attr);
    }

    // 首字母大写
    public static String firstCharUpperCase(String str) {
        char firstChar = Character.toUpperCase(str.charAt(0));
        StringBuffer sb = new StringBuffer(str);
        sb.setCharAt(0, firstChar);
        return sb.toString();
    }
}
