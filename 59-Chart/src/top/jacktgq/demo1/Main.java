package top.jacktgq.demo1;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;

import java.util.ArrayList;
import java.util.Random;

public class Main extends Application {

    private XYChart.Series<Number, Number> series1;
    private XYChart.Series<Number, Number> series2;
    private NumberAxis xAxis;
    private volatile int count = 0;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button startBtn = new Button("开始");
        Button stopBtn = new Button("结束");
        FlowPane root = new FlowPane();
        root.getChildren().addAll(startBtn, stopBtn, getLineChart());

        primaryStage.setTitle("折线图");
        primaryStage.setScene(new Scene(root, 1800, 800));
        primaryStage.getScene().getStylesheets().add(getClass().getResource("chart.css").toExternalForm());
        primaryStage.show();

        DataTask task = new DataTask();
        task.setDelay(Duration.seconds(0));
        task.setPeriod(Duration.seconds(1));
        task.valueProperty().addListener(new ChangeListener<ArrayList<Integer>>() {

            @Override
            public void changed(ObservableValue<? extends ArrayList<Integer>> observable, ArrayList<Integer> oldValue, ArrayList<Integer> newValue) {
                if (newValue != null) {
                    int size = series1.getData().size();
                    // 只缓存50个数据
                    if (size > 50) {
                        series1.getData().remove(0, 10);
                        System.out.println("清除前10个数据");
                    }
                    if (count > 18) {
                        xAxis.setLowerBound(xAxis.getLowerBound() + 1);
                        xAxis.setUpperBound(xAxis.getUpperBound() + 1);
                    }
                    series1.getData().add(new XYChart.Data<Number, Number>(count, newValue.get(0)));
                    series2.getData().add(new XYChart.Data<Number, Number>(count, newValue.get(1)));
                    count ++;
                }
            }
        });

        startBtn.setOnAction(e -> {
            if (!task.isRunning()) {
                task.start();
            }
        });

        stopBtn.setOnAction(e -> {
            task.cancel();
            task.reset();
        });
    }

    private LineChart getLineChart() {
        series1 = new XYChart.Series<>();
        series1.setName("计划招生人数");
        series2 = new XYChart.Series<>();
        series2.setName("实际报道人数");
        // ObservableList<XYChart.Series<String, Number>> dataList = FXCollections.observableArrayList(series1, series2);

        series1.getData().forEach(data -> {
            data.setNode(new Label(data.getYValue() + ""));
            Tooltip tp = new Tooltip(data.getXValue() + "年计划招生人数：" + data.getYValue());
            Tooltip.install(data.getNode(), tp);
        });

        xAxis = new NumberAxis("x", 0, 20, 5);
        // x轴的相关参数
        xAxis.setLabel("专业");
        // 距离左（上）边的距离
        // xAxis.setStartMargin(50);
        // 距离右（下）边的距离
        // xAxis.setEndMargin(50);
        // 前后是否留空间，默认为true
        // xAxis.setGapStartAndEnd(true);
        // 轴位置
        // xAxis.setSide(Side.TOP);
        // 刻度标签颜色
        xAxis.setTickLabelFill(Color.web("#ffcc00"));
        // 刻度标签字体
        xAxis.setTickLabelFont(Font.font(16));
        // 类型名称与轴刻度的距离
        xAxis.setTickLabelGap(0);
        // 类型名称旋转
        xAxis.setTickLabelRotation(45);
        // 是否显示刻度标签
        xAxis.setTickLabelsVisible(true);
        // 刻度长度
        xAxis.setTickLength(20);
        // 是否显示刻度
        xAxis.setTickMarkVisible(true);

        NumberAxis yAxis = new NumberAxis("y", 0, 100, 5);
        yAxis.setLabel("人数");
        // 格式化y轴刻度标签显示
        yAxis.setTickLabelFormatter(new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                return object + "%";
            }

            @Override
            public Number fromString(String string) {
                return null;
            }
        });


        LineChart lineChart = new LineChart(xAxis, yAxis);
        lineChart.getData().addAll(series1, series2);
        lineChart.setId("candy-line-chart");

        // 从右往左开始绘制 RIGHT_TO_LEFT
        lineChart.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
        lineChart.setTitle("2016-2020空间专业招生情况2");
        // 设置标题显示的位置
        lineChart.setTitleSide(Side.TOP);
        // 是否显示图例
        lineChart.setLegendVisible(true);
        // 设置图例显示的位置
        lineChart.setLegendSide(Side.LEFT);
        // 当数据发生改变时是否显示动画
        lineChart.setAnimated(true);
        // 是否显示拐点
        lineChart.setCreateSymbols(true);
        // 是否显示网格线
        // 横网格线
        lineChart.setHorizontalGridLinesVisible(false);
        // 竖网格线
        lineChart.setVerticalGridLinesVisible(false);
        // 是否显示坐标轴
        lineChart.setHorizontalZeroLineVisible(true);
        lineChart.setHorizontalZeroLineVisible(true);

        lineChart.setPrefWidth(800);
        return lineChart;
    }

    private Random random = new Random();

    class DataTask extends ScheduledService<ArrayList<Integer>> {
        @Override
        protected Task<ArrayList<Integer>> createTask() {
            return new Task<ArrayList<Integer>>() {
                @Override
                protected ArrayList<Integer> call() throws Exception {
                    int value1 = random.nextInt(100);
                    int value2 = random.nextInt(100);
                    ArrayList<Integer> list = new ArrayList<>();
                    list.add(value1);
                    list.add(value2);

                    return list;
                }
            };
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}