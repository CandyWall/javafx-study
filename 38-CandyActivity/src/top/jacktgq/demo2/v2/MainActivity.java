package top.jacktgq.demo2.v2;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import top.jacktgq.custom_controls.activity.CandyActivity;

public class MainActivity extends CandyActivity {
    @Override
    public void onCreate(Object intent) {
        StackPane root = new StackPane();
        this.setContentView(root);

        Label label = new Label("恭喜您，" + intent + "，登录成功！");
        Button logoutBtn = new Button("退出登录");
        logoutBtn.setOnAction(e -> {
            startActivity(LoginActivity.class, null);
            finish();
        });
        root.getChildren().addAll(label, logoutBtn);
        StackPane.setAlignment(label, Pos.CENTER);
        StackPane.setAlignment(logoutBtn, Pos.BOTTOM_CENTER);
    }
}
