package top.jacktgq.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.Properties;

/**
 *
 * @Title: LoadConfigUtils.java
 * @Package top.jacktgq.utils
 * @Description: 读取配置文件中的常数
 * @author CandyWall
 * @date 2022年2月16日 下午5:10:52
 * @version V1.0
 */
public class LoadConfigUtils {
    private static Properties pro;

    static {
        try {
            initProperties();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getGitee_Url() {
        return pro.getProperty("gitee.url");
    }

    public static String getCSDN_Url() {
        return pro.getProperty("csdn.url");
    }

    private static synchronized void makeInstance() {
        if (pro == null) {
            pro = new Properties();
        }
    }

    private static void initProperties() throws Exception {
        File file = new File("config.properties");
        // 如果配置文件不存在，就创建一个
        if (!file.exists()) {
            file.createNewFile();
            // 将默认的空配置写入
            writeDefaultConfig(file);
        }
        InputStream in = new FileInputStream(file);

        if (pro == null) {
            makeInstance();
        }
        pro.load(in);
    }

    private static void writeDefaultConfig(File file) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "utf-8"));
            writer.write("# 改配置文件会覆盖，不配置就会使用默认配置\r\n");
            writer.write("gitee.url=\r\n");
            writer.write("csdn.url=");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
