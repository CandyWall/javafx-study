package top.jacktgq.custom_controls.layout;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class CandyVBox extends Pane {
    List<String> sizes = new ArrayList<>();
    double spacing = 0;

    public CandyVBox() {

        // 监听大小的改变
        this.layoutBoundsProperty().addListener(new ChangeListener<Bounds>() {

            @Override
            public void changed(ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) {

                relayout();
            }
        });
    }

    @Override
    protected void layoutChildren() {
        relayout();
    }

    public void relayout() {
        double w = this.getWidth();
        double h = this.getHeight();
        Insets insets = this.getInsets();
        if (w <= 0 || h <= 0)
            return;

        System.out.println("窗口: " + w + "," + h);

        int numOfSpace = sizes.size() - 1; // 间隔的数目
        double sizeOfSpace = numOfSpace * spacing;

        // 实用宽度和高度
        double rw = w - insets.getLeft() - insets.getRight();
        double rh = h - insets.getTop() - insets.getBottom() - sizeOfSpace;

        double[] rsizes = CandyLayoutUtils.layout(rh, sizes);

        double x = insets.getLeft();
        double y = insets.getRight();

        ObservableList<Node> children = this.getChildren();
        for (int i = 0; i < children.size(); i++) {
            Node node = children.get(i);
            node.resizeRelocate(x, y, rw, rsizes[i]);
            System.out.println("node: " + i + ", x=" + x + ", y=" + y + ", w=" + rsizes[i] + ",h=" + rh);

            y += (rsizes[i] + spacing);

        }
    }

    public void add(Node node, String size) {
        sizes.add(size);
        this.getChildren().add(node);
    }

    public void setLayout(Node node, String size) {
        ObservableList<Node> children = this.getChildren();
        int index = children.indexOf(node);
        if (index >= 0) {
            sizes.set(index, size);
        }
    }

    public void setLayout(String sss) {
        sizes.clear();

        String[] str = sss.split(" ");
        for (String s : str) {
            s = s.trim();
            if (s.length() == 0)
                continue;

            sizes.add(s);
        }
    }

    public void setSpacing(double spacing) {
        this.spacing = spacing;
        relayout();
    }

}
