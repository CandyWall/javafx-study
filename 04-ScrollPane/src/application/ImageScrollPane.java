package application;

import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ImageScrollPane extends ScrollPane {
    ImageView imageView = new ImageView();

    public ImageScrollPane() {
        setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
        setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

        this.setContent(imageView);
    }

    public void setImage(Image image) {
        imageView.setImage(image);
    }
}
