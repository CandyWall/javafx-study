package top.jacktgq.demo1;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import top.jacktgq.custom_controls.layout.CandyHBox;
import top.jacktgq.custom_controls.scrollPane.CandyListPane;

public class StudentPane extends BorderPane {
    CandyListPane listPane = new CandyListPane();

    public StudentPane() {
        this.getStyleClass().add("student-list");
        this.setCenter(listPane);

        addRow(new Student(20210001, "空条承太郎", true, "12900989989"));
        addRow(new Student(20210002, "花京院典明", true, "12354534989"));
        addRow(new Student(20210003, "乔瑟夫乔斯达", false, "12234345343"));
        addRow(new Student(20210004, "阿布德尔", false, "12343434344"));
    }

    private EventHandler<MouseEvent> itemClickHandler = e -> {
        ItemView itemView = (ItemView) e.getSource();
        Student stu = (Student) itemView.getUserData();
        System.out.println(stu);
    };

    public void addRow(Student stu) {
        ItemView itemView = new ItemView(stu);
        itemView.setUserData(stu);
        itemView.addEventHandler(MouseEvent.MOUSE_CLICKED, itemClickHandler);
        listPane.add(itemView);
    }

    // 自定义每一行的显示
    private class ItemView extends CandyHBox {
        public ItemView(Student stu) {
            Label idLabel = new Label();
            Label nameLabel = new Label();
            Label sexLabel = new Label();
            Label phoneLabel = new Label();
            this.getChildren().addAll(idLabel, nameLabel, sexLabel, phoneLabel);
            this.setLayout("80px 1 60px 1");
            this.getStyleClass().add("item");

            idLabel.setText(stu.id + "");
            nameLabel.setText(stu.name);
            sexLabel.setText(stu.sex ? "男" : "女");
            phoneLabel.setText(stu.phone);
        }
    }
}
