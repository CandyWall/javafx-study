package top.jacktgq.demo2;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import top.jacktgq.custom_controls.layout.CandyHBox;
import top.jacktgq.custom_controls.scrollPane.CandyListPane;

public class StudentPane extends BorderPane {
    private CandyListPane listPane = new CandyListPane();
    private static final Image CHECK_ICON = new Image("top/jacktgq/demo2/ic_check.png");
    private static final Image UNCHECK_ICON = new Image("top/jacktgq/demo2/ic_uncheck.png");

    public StudentPane() {
        this.getStyleClass().add("student-list");
        this.setCenter(listPane);

        addRow(new Student(20210001, "空条承太郎", true, "12900989989"));
        addRow(new Student(20210002, "花京院典明", true, "12354534989"));
        addRow(new Student(20210003, "乔瑟夫乔斯达", false, "12234345343"));
        addRow(new Student(20210004, "阿布德尔", false, "12343434344"));
    }

    private EventHandler<MouseEvent> itemClickHandler = e -> {
        ItemView itemView = (ItemView) e.getSource();
        if (!itemView.isSelected()) {
            selectAll(false);
            itemView.setSelected(true);
        }
    };

    // 全部选中 / 全部取消选中
    public void selectAll(boolean selected) {
        ObservableList<Node> items = listPane.getItems();
        for (Node node : items) {
            ItemView itemView = (ItemView) node;
            itemView.setSelected(selected);
        }
    }

    // 获取选中项
    public List<Student> getSelectedItems() {
        List<Student> result = new ArrayList<>();

        ObservableList<Node> items = listPane.getItems();
        for (Node node : items) {
            ItemView item = (ItemView) node;
            if (item.isSelected()) {
                result.add((Student) item.getUserData());
            }
        }

        return result;
    }

    public void addRow(Student stu) {
        ItemView itemView = new ItemView(stu);
        itemView.setUserData(stu);
        itemView.addEventHandler(MouseEvent.MOUSE_CLICKED, itemClickHandler);
        listPane.add(itemView);
    }

    // 自定义每一行的显示
    private class ItemView extends CandyHBox {
        private boolean selected = false;
        private ImageView imageView;

        public ItemView(Student stu) {
            imageView = new ImageView(UNCHECK_ICON);
            Label idLabel = new Label();
            Label nameLabel = new Label();
            Label sexLabel = new Label();
            Label phoneLabel = new Label();
            this.getChildren().addAll(imageView, idLabel, nameLabel, sexLabel, phoneLabel);
            this.setLayout("24px 80px 1 60px 1");
            this.getStyleClass().add("item");

            idLabel.setText(stu.id + "");
            nameLabel.setText(stu.name);
            sexLabel.setText(stu.sex ? "男" : "女");
            phoneLabel.setText(stu.phone);
        }

        // 当前行是否被选中
        public boolean isSelected() {
            return selected;
        }

        // 设置选中状态
        public void setSelected(boolean selected) {
            this.selected = selected;
            imageView.setImage(selected ? CHECK_ICON : UNCHECK_ICON);
        }
    }
}
