package top.jacktgq.demo1;

import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import top.jacktgq.custom_controls.tab.CandyTabPane;

public class TestPane extends BorderPane {
    private CandyTabPane tabPane = new CandyTabPane();
    private TabPage1 tab1 = new TabPage1();
    private TabPage2 tab2 = new TabPage2();

    public TestPane() {

        tabPane.setStyle("-fx-background-color: white");
        tabPane.addTab(tab1);
        tabPane.addTab(tab2);
        tabPane.setCurrentTab(tab1);
        this.setCenter(tabPane);

        initLabels();
    }

    private void initLabels() {
        HBox labelBar = new HBox();
        Button b1 = new Button("tab1");
        b1.setOnAction(e -> {
            tabPane.setCurrentTab(tab1);
        });
        Button b2 = new Button("tab2");
        b2.setOnAction(e -> {
            tabPane.setCurrentTab(tab2);
        });
        labelBar.getChildren().addAll(b1, b2);
        labelBar.setStyle("-fx-padding: 4; -fx-spacing: 4");

        this.setTop(labelBar);
    }
}
