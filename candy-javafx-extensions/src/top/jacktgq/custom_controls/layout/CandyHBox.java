package top.jacktgq.custom_controls.layout;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;

public class CandyHBox extends CandyLayoutPane {
    List<String> sizes = new ArrayList<>();
    double spacing = 0;

    public CandyHBox() {
    }

    public CandyHBox add(Node node, String size) {
        this.getChildren().add(node);
        this.sizes.add(size);
        return this;
    }

    public void setLayout(Node node, String size) {
        ObservableList<Node> children = this.getChildren();
        int index = children.indexOf(node);
        if (index >= 0) {
            sizes.set(index, size);
        }
    }

    public void setLayout(String sss) {
        sizes.clear();

        String[] str = sss.split(" ");
        for (String s : str) {
            s = s.trim();
            if (s.length() == 0)
                continue;

            sizes.add(s);
        }
    }

    public void setSpacing(double spacing) {
        this.spacing = spacing;
        this.requestLayout();
    }

    @Override
    protected void layoutChildren(double w, double h) {
        if (w <= 0 || h <= 0)
            return;
        Insets insets = this.getInsets();
        // System.out.println("窗口: " + w + "," + h);

        int numOfSpace = sizes.size() - 1; // 间隔的数目
        double sizeOfSpace = numOfSpace * spacing;

        // 实用宽度和高度
        double rw = w - insets.getLeft() - insets.getRight() - sizeOfSpace;
        double rh = h - insets.getTop() - insets.getBottom();
        // System.out.println("afhbox: insets=" + insets);

        double[] rsizes = CandyLayoutUtils.layout(rw, sizes);

        double x = insets.getLeft();
        double y = insets.getTop();

        ObservableList<Node> children = this.getChildren();
        for (int i = 0; i < children.size(); i++) {
            Node node = children.get(i);
            node.resizeRelocate(x, y, rsizes[i], rh);
            // System.out.println("node: " + i + ", x=" + x + ", y=" + y + ",
            // w=" + rsizes[i] + ",h=" + rh);

            x += (rsizes[i] + spacing);

        }
    }

}
