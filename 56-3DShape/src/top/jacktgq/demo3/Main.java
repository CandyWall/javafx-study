package top.jacktgq.demo3;

import javafx.application.Application;
import javafx.geometry.Point3D;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.CullFace;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.Sphere;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();
            // root.setStyle("-fx-background: transparent");

            HBox hbox1 = new HBox(100);
            Box[] boxes = new Box[5];
            for (int i = 0; i < 5; i++) {
                boxes[i] = new Box(100, 100, 200);
                hbox1.getChildren().add(boxes[i]);
                int a = i;
                boxes[i].setOnMouseClicked(e -> {
                    System.out.println("点击了" + a + "号长方体");
                });

                // 设置材质
                PhongMaterial pm = new PhongMaterial();
                // 设置漫反射的颜色
                // pm.setDiffuseColor(Color.ORANGE);
                // 设置高光
                // pm.setSpecularColor(Color.GREEN);
                // 高光强度，值越小，高光效果越强
                // pm.setSpecularPower(1);
                // 设置凹凸贴图
                pm.setDiffuseMap(new Image("images/bg.jpg"));
                pm.setBumpMap(new Image("images/fx.png"));

                boxes[i].setMaterial(pm);
            }
            boxes[0].setTranslateX(-100);
            // 设置旋转，默认沿着z轴旋转
            System.out.println(boxes[1].getRotationAxis());
            boxes[1].setRotate(30);
            // 可以设置旋转轴，设置沿着x轴旋转
            boxes[2].setRotationAxis(new Point3D(1, 0, 0));
            boxes[2].setRotate(30);
            // 设置沿着y轴旋转
            boxes[3].setRotationAxis(new Point3D(0, 1, 0));
            boxes[3].setRotate(30);

            // 设置绘制模式，只勾勒模型的线条
            boxes[3].setDrawMode(DrawMode.LINE);

            // 设置背面不渲染
            boxes[4].setCullFace(CullFace.BACK);
            // 设置前面不渲染
            boxes[2].setCullFace(CullFace.FRONT);

            root.getChildren().add(hbox1);
            AnchorPane.setTopAnchor(hbox1, 150.0);
            AnchorPane.setLeftAnchor(hbox1, 150.0);

            HBox hbox2 = new HBox(100);
            Cylinder[] cylinders = new Cylinder[5];
            for (int i = 0; i < 5; i++) {
                cylinders[i] = new Cylinder(50, 100, 10);
                hbox2.getChildren().add(cylinders[i]);
            }

            cylinders[1].setRotate(30);
            // 可以设置旋转轴，设置沿着x轴旋转
            cylinders[2].setRotationAxis(new Point3D(1, 0, 0));
            cylinders[2].setRotate(30);
            // 设置沿着y轴旋转
            cylinders[3].setRotationAxis(new Point3D(0, 1, 0));
            cylinders[3].setRotate(30);

            // 设置绘制模式，只勾勒模型的线条
            cylinders[3].setDrawMode(DrawMode.LINE);

            // 设置背面不渲染
            cylinders[4].setCullFace(CullFace.BACK);
            // 设置前面不渲染
            cylinders[2].setCullFace(CullFace.FRONT);

            root.getChildren().add(hbox2);
            AnchorPane.setTopAnchor(hbox2, 400.0);
            AnchorPane.setLeftAnchor(hbox2, 150.0);

            HBox hbox3 = new HBox(100);
            Sphere[] spheres = new Sphere[5];
            for (int i = 0; i < 5; i++) {
                spheres[i] = new Sphere(50, 50);
                // 设置材质
                PhongMaterial pm = new PhongMaterial();
                // 设置漫反射的颜色
                // pm.setDiffuseColor(Color.ORANGE);
                // 球面高光的颜色
                pm.setSpecularColor(Color.WHITE);
                // 高光强度，值越小，高光效果越强
                // pm.setSpecularPower(1);
                // 设置凹凸贴图
                pm.setDiffuseMap(new Image("images/bg.jpg"));
                pm.setBumpMap(new Image("images/fx.png"));
                // 自发光
                pm.setSelfIlluminationMap(new Image("images/bg.jpg"));

                spheres[i].setMaterial(pm);
                hbox3.getChildren().add(spheres[i]);
            }

            spheres[1].setRotate(30);
            // 可以设置旋转轴，设置沿着x轴旋转
            spheres[2].setRotationAxis(new Point3D(1, 0, 0));
            spheres[2].setRotate(30);
            // 设置沿着y轴旋转
            spheres[3].setRotationAxis(new Point3D(0, 1, 0));
            spheres[3].setRotate(30);

            // 设置绘制模式，只勾勒模型的线条
            spheres[3].setDrawMode(DrawMode.LINE);

            // 设置背面不渲染
            spheres[4].setCullFace(CullFace.BACK);
            // 设置前面不渲染
            spheres[2].setCullFace(CullFace.FRONT);

            root.getChildren().add(hbox3);
            AnchorPane.setTopAnchor(hbox3, 650.0);
            AnchorPane.setLeftAnchor(hbox3, 150.0);

            // 要想点击事件生效，需要把depthBuffer属性的值设置为false
            Scene scene = new Scene(root, 1600, 800, true);
            scene.setCamera(new PerspectiveCamera());

            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("3D图形绘制");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
