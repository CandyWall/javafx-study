package top.jacktgq.demo1;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();

            Student stu = new Student("20210501", "空条承太郎", 17);

            Button btn1 = new Button("修改姓名");
            btn1.setOnAction(e -> {
                stu.setName("花京院典明");
            });
            Button btn2 = new Button("修改年龄");
            btn2.setOnAction(e -> {
                stu.setAge(20);
            });
            root.getChildren().addAll(btn1, btn2);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            stu.psc.addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    System.out.println(evt.getSource());
                    System.out.println("oldValue:" + evt.getOldValue());
                    System.out.println("newValue:" + evt.getNewValue());
                }
            });

            stu.psc.addPropertyChangeListener("ageChanged", new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    System.out.println("age: oldValue:" + evt.getOldValue());
                    System.out.println("age: newValue:" + evt.getNewValue());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
