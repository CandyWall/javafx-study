package top.jacktgq.custom_controls.tab;

import java.util.ArrayList;
import java.util.List;

public class CandyTabContext {
    private List<CandyTab> tabs = new ArrayList<>();
    private CandyTabObserver observer;
    private CandyTab currentTab = null;

    public CandyTabContext() {

    }

    ////////////////////////

    public CandyTabObserver getObserver() {
        return observer;
    }

    public void setObserver(CandyTabObserver observer) {
        this.observer = observer;
    }

    /////////////////////////
    public void addTab(CandyTab tab) {
        tabs.add(tab);
        tab.context = this;
    }

    public CandyTab getTab(String tabName) {
        for (CandyTab tab : tabs) {
            if (tabName.equals(tab.getName())) {
                return tab;
            }
        }
        return null;
    }

    public void removeTab(CandyTab tab) {
        if (tab == currentTab) {
            tab.onTabStop();
            currentTab = null;
        }

        if (tab.created) {
            tab.onTabDestroy();
        }

        tabs.remove(tab);
    }

    public void removeTab(int index) {
        removeTab(tabs.get(index));
    }

    public void removeTab(String tabName) {
        for (int i = 0; i < tabs.size(); i++) {
            CandyTab tab = tabs.get(i);
            if (tabName.equals(tab.getName())) {
                removeTab(i);
                break;
            }
        }
    }

    public CandyTab getCurrentTab() {
        return currentTab;
    }

    public void setCurrentTab(CandyTab caller, CandyTab tab) {
        if (tab == caller)
            return;

        // 隐藏当前的 Tab
        if (caller != null) {
            caller.onTabStop();
            if (observer != null) {
                observer.onTabDismiss(caller);
            }
        }

        // 显示新的 Tab
        try {

            // onCreate
            if (!tab.created) {
                tab.onTabCreate();
                tab.created = true;
                if (tab.getContentView() == null) {
                    throw new Exception("Tab 没有设置 ContentView, " + tab.getName());
                }
            }

            // 回调到管理器
            if (observer != null) {
                observer.onTabShow(tab);
            }

            tab.onTabStart();
            currentTab = tab;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCurrentTab(CandyTab caller, String tabName) {
        CandyTab tab = getTab(tabName);
        if (tab == null) {
            System.out.println("Warning: No tab with name: " + tabName);
            return;
        }

        setCurrentTab(caller, tab);
    }

}
