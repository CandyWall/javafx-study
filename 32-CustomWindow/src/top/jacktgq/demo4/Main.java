package top.jacktgq.demo4;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {
    Stage primaryStage;
    MyWindow myWindow;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        try {
            BorderPane root = new BorderPane();
            Scene scene = new Scene(root, 400, 400);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            Button button = new Button("新窗口");
            root.setTop(button);

            // 点按钮时创建新窗口
            button.setOnAction((e) -> {

                openNewWindow();
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openNewWindow() {
        myWindow = new MyWindow(primaryStage);
        myWindow.resize(300, 150);
        myWindow.show();
        myWindow.centerInParent();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
