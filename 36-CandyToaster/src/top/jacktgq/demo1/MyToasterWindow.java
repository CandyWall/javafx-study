package top.jacktgq.demo1;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Popup;
import javafx.stage.Window;

/**
 * 
 * @Title: CandyToaster.java
 * @Package top.jacktgq.custom_controls.toaster
 * @Description: 短消息提示框
 * @author CandyWall
 * @date 2021年5月21日 下午10:11:59
 * @version V1.0
 */
public class MyToasterWindow extends Popup {

    private Label label;
    private StackPane root;

    public MyToasterWindow() {
        root = new StackPane();
        label = new Label();
        label.setAlignment(Pos.CENTER);
        root.getChildren().add(label);
        StackPane.setAlignment(label, Pos.CENTER);
        root.setPrefSize(200, 50);
        root.setStyle("-fx-background-color: #ADFF2F");

        getScene().setRoot(root);

        // setAutoHide(true); // 离焦不会自动消失
        setHideOnEscape(true); // 按ESC键可以关闭弹出框
        // 给Popup的左上角作为锚点，即设置Popup的x和y坐标的时候以左上角为基准
        setAnchorLocation(AnchorLocation.CONTENT_TOP_LEFT);
        sizeToScene();
    }

    public void setText(String text) {
        label.setText(text);
    }

    public void show(Window owner) {
        double x = owner.getX();
        double y = owner.getY();
        double w = owner.getWidth();
        double h = owner.getHeight();
        double width = root.getPrefWidth();
        double height = root.getPrefHeight();

        x += (w - width) / 2;
        y += (h - height) / 2;

        super.show(owner, x, y);
    }

}
