package top.jacktgq.custom_controls.waiting.canvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import top.jacktgq.animation.CandyAnimationTimer;
import top.jacktgq.custom_controls.image.CandyCanvasPane;

/**
 * 
 * @Title: CandyCanvasWaitingCircle.java
 * @Package top.jacktgq.waitingAnimation.canvas
 * @Description: 基于Canvas绘制的等待特效-圆圈状 等待对话框
 * @author CandyWall
 * @date 2021年5月19日 上午1:09:10
 * @version V1.0
 */
public class CandyCanvasWaitingCircle extends CandyCanvasPane {
    private Color bgColor = Color.rgb(0, 0, 0, 0.3);
    private Color lineColor = Color.rgb(102, 102, 102, 0.8);

    private int step = 5; // 每次转动的角度，可以控制转动速度
    private int startAngle = 0;

    public CandyCanvasWaitingCircle() {
        new CandyAnimationTimer(100) {
            @Override
            public void updateUI(long now) {
                // 通过改变角度，产生轮转效果
                startAngle += step;
                if (startAngle >= 360)
                    startAngle = 0;
                repaint();
                repaint();
            }
        }.start();
    }

    @Override
    public void paint(GraphicsContext gc, double x, double y, double w, double h) {
        // 取得一个最大的正方形
        // Rectangle rect = new Rectangle(0,0,200,200);
        double width = w;
        double height = w;
        if (w > h) {
            width = h;
            height = h;
        }
        Rectangle rect = new Rectangle(x + (w - width) / 2 + 2, y + (h - height) / 2 + 2, width - 2, height - 2);
        // 中心点及半径
        double centerX = rect.getX() + rect.getWidth() / 2;
        double centerY = rect.getY() + rect.getHeight() / 2;
        double radius = rect.getWidth() / 2;

        // 辐条的长度
        double size = radius * 0.9;
        if (size < 2)
            size = 2;
        if (size > 4)
            size = 4;

        // 绘制辐条
        gc.setStroke(lineColor);
        gc.setLineWidth(6);
        for (double angle = 0; angle < 360; angle += 30) {
            double a = angle + startAngle; // 转动因素在此
            drawRadialLine(gc, centerX, centerY, radius, radius - size, a);
        }

        // 绘制圆圈
        double r = radius - size - 4;
        gc.setFill(bgColor);
        gc.fillOval(centerX - r, centerY - r, 2 * r, 2 * r);
        gc.setLineWidth(4);
        gc.setStroke(lineColor);
        gc.strokeOval(centerX - r, centerY - r, 2 * r, 2 * r);
    }

    // 绘制从圆心发散出的辐条线条，中心centerX, centerY, 角度angle
    private void drawRadialLine(GraphicsContext gc, double centerX, double centerY, double r1, double r2,
            double angle) {
        double radian = angle / 180 * Math.PI; // 角度转成弧度

        double x1 = centerX + r1 * Math.cos(radian);
        double y1 = centerY + r1 * Math.sin(radian);
        double x2 = centerX + r2 * Math.cos(radian);
        double y2 = centerY + r2 * Math.sin(radian);
        gc.strokeLine(x1, y1, x2, y2);
    }
}
