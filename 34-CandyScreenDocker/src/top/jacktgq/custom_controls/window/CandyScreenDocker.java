package top.jacktgq.custom_controls.window;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Rectangle2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class CandyScreenDocker {
    Stage stage;
    Region root;

    private boolean docked = false;
    private double originalWidth = 0;
    private double originalHeight = 0;

    public CandyScreenDocker(Stage stage, Region root) {
        this.stage = stage;
        this.root = root;
        stage.setAlwaysOnTop(true);

        stage.xProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                testDocking();
            }
        });

        root.addEventFilter(MouseEvent.MOUSE_ENTERED, (e) -> {
            if (docked)
                expand();
        });

        root.addEventFilter(MouseEvent.MOUSE_EXITED, (e) -> {
            if (docked)
                fold();
        });

    }

    private void testDocking() {
        double x = stage.getX(), y = stage.getY();
        double w = stage.getWidth(), h = stage.getHeight();

        Rectangle2D screenVisualBounds = Screen.getPrimary().getVisualBounds();
        // Rectangle2D screenBounds = Screen.getPrimary().getBounds();

        if (x + w >= screenVisualBounds.getWidth() - 1) {
            if (!docked) {
                docked = true;
                originalWidth = root.getWidth();
                originalHeight = root.getHeight();
                System.out.println("靠近屏幕边缓, 开始吸附");
            }
        } else {
            if (docked) {
                docked = false;
                System.out.println("远离屏幕边缓, 结束吸附");
            }
        }
    }

    private void fold() {
        System.out.println("鼠标移出, 折叠显示");
        Screen screen = Screen.getPrimary();
        stage.setX(screen.getVisualBounds().getWidth() - 1);
    }

    private void expand() {
        System.out.println("鼠标移入, 展开显示");
        Screen screen = Screen.getPrimary();
        stage.setX(screen.getVisualBounds().getWidth() - originalWidth);
    }

    // 封装一个简便的方法给外部调用
    public static void initSupport(Stage stage, Region root) {
        new CandyScreenDocker(stage, root);
    }

}
