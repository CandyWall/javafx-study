package top.jacktgq;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.util.Callback;

public class RoleListView extends ListView<Role> {
    // 数据源
    private ObservableList<Role> listData = FXCollections.observableArrayList();

    public RoleListView() {
        // 添加数据源
        listData.add(new Role(1, "空条承太郎", true));
        listData.add(new Role(1, "花京院典明", true));
        listData.add(new Role(1, "乔瑟夫乔斯达", true));
        listData.add(new Role(1, "阿布德尔", true));
        listData.add(new Role(1, "波鲁那雷夫", true));
        listData.add(new Role(1, "伊奇", true));

        setItems(listData);

        setCellFactory(new Callback<ListView<Role>, ListCell<Role>>() {
            @Override
            public ListCell<Role> call(ListView<Role> param) {
                return new RoleListCell();
            }
        });
    }

    private class RoleListCell extends ListCell<Role> {
        @Override
        protected void updateItem(Role item, boolean empty) {
            super.updateItem(item, empty);

            if (item == null) {
                this.setText(null);
                this.setGraphic(null);
            } else {
                this.setText(item.name);

                CandyRoundIcon roundIcon = new CandyRoundIcon(new Image("images/" + item.name + ".jpg"));
                roundIcon.setPrefSize(60, 60);
                this.setGraphic(roundIcon);
                // this.setGraphic(new ImageView(new Image("images/" + item.name
                // + ".jpg")));
            }
        }
    }
}
