package top.jacktgq.demo3;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class StudentListView extends ListView<Student> {
    // 设置数据源
    ObservableList<Student> listData = FXCollections.observableArrayList();

    public StudentListView() {
        // 设置数据源
        setItems(listData);

        // 设置单元格生成器（工厂）
        setCellFactory(new Callback<ListView<Student>, ListCell<Student>>() {
            @Override
            public ListCell<Student> call(ListView<Student> param) {
                return new MyListCell();
            }
        });
    }

    public void add(Student stu) {
        listData.add(stu);
    }

    public ObservableList<Student> getListData() {
        return listData;
    }

    class MyListCell extends ListCell<Student> {

        @Override
        protected void updateItem(Student item, boolean empty) {
            // JavaFX 要求必须先调用super.updateItem();
            super.updateItem(item, empty);

            if (item == null) {
                this.setGraphic(null);
            } else {
                // 显示一个CheckBox
                CheckBox checkBox = new CheckBox();
                checkBox.setText(item.name);
                checkBox.setSelected(item.checked);
                this.setGraphic(checkBox);

                // 当CheckBox被点中取消时的处理

                checkBox.selectedProperty().addListener(new MyCheckBoxListener(item));
            }
        }
    }

    class MyCheckBoxListener implements ChangeListener<Boolean> {
        private Student item;

        public MyCheckBoxListener(Student item) {
            this.item = item;
        }

        @Override
        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
            item.checked = newValue;
        }
    }
}
