package top.jacktgq.demo4;

import java.util.Map;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import top.jacktgq.thread.CandyShortTask;
import top.jacktgq.utils.process.CandyProcess;

public class Main extends Application {
    private TextField websiteField;
    private TextArea contentArea;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            HBox toolbar = new HBox();
            websiteField = new TextField("www.baidu.com");
            Button openFilebtn = new Button("ping");
            toolbar.getChildren().addAll(websiteField, openFilebtn);
            HBox.setHgrow(websiteField, Priority.ALWAYS);
            toolbar.setPadding(new Insets(10));
            toolbar.setSpacing(10);

            openFilebtn.setOnAction(e -> {
                String website = websiteField.getText().trim();
                if (website.length() > 0) {
                    ping(website);
                }
            });
            root.setTop(toolbar);

            contentArea = new TextArea();
            root.setCenter(contentArea);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("网络连通性测试");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ping(String website) {
        String cmd = "ping " + website;
        new CandyShortTask() {
            private Map<String, Object> resultMap;

            @Override
            protected void doInBackground() throws Exception {
                resultMap = CandyProcess.execute(cmd, null, "gbk", output -> {
                    Platform.runLater(() -> {
                        contentArea.appendText(output + "\n");
                    });
                });
            }

            @Override
            protected void done() {
                int code = (int) resultMap.get("code");
                if (code == 0) {
                    System.out.println("可以ping通");
                }
            }
        }.execute();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
