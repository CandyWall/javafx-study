package top.jacktqg;


import javafx.scene.layout.Pane;

public class TopoPane extends Pane {
    public TopoPane() {
        this.setStyle("-fx-background-color: #0E1720");
    }

    public void add(TopoNode node) {
        this.getChildren().add(node);
        // node.relocate( 100, 100);

        // 随机位置
        double r = Math.random();
        if (r < 0.3)
            r = 0.3;
        if (r > 0.7)
            r = 0.7;

        double x = getWidth() * r;
        double y = getHeight() * r;
        node.relocate(x, y);

    }
}
