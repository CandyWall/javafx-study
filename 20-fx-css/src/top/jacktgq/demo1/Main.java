package top.jacktgq.demo1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();
            TextField textField = new TextField();
            textField.getStyleClass().add("candy-textField");

            Button submit = new Button("提交");
            submit.getStyleClass().addAll("candy-button", "candy-submit");
            Button reset = new Button("重置");
            reset.setId("candy-reset");
            reset.getStyleClass().add("candy-button");
            root.getChildren().addAll(textField, submit, reset);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("/top/jacktgq/demo1/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
