package top.jacktgq.timeline.demo1;

import javafx.animation.Animation.Status;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {
    private Timeline timeline;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            HBox controlbar = new HBox();
            ToggleButton tb1 = new ToggleButton("播放");
            ToggleButton tb2 = new ToggleButton("暂停");
            ToggleButton tb3 = new ToggleButton("停止");
            ToggleButton tb4 = new ToggleButton("动画跳跃");
            ToggleButton tb5 = new ToggleButton("从头开始播放");
            ToggleButton tb6 = new ToggleButton("开始反向播放动画");
            ToggleGroup group = new ToggleGroup();
            group.getToggles().addAll(tb1, tb2, tb3, tb4, tb5, tb6);
            controlbar.getChildren().addAll(tb1, tb2, tb3, tb4, tb5, tb6);

            root.setTop(controlbar);

            AnchorPane an = new AnchorPane();
            root.setCenter(an);

            Button btn = new Button("哈哈哈");
            an.getChildren().addAll(btn);

            timeline = new Timeline();

            KeyValue kv1 = new KeyValue(btn.translateXProperty(), 0);
            KeyValue kv1Y = new KeyValue(btn.translateYProperty(), 0);

            KeyFrame kf1 = new KeyFrame(Duration.seconds(0), "kf1", new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    System.out.println("kf1");
                }
            }, kv1, kv1Y);

            // 插值动画，
            // Interpolator.DISCRETE：中间不进行插值过渡
            // Interpolator.EASE_BOTH：缓慢进入，缓慢退出
            KeyValue kv2 = new KeyValue(btn.translateXProperty(), 300, Interpolator.EASE_BOTH);
            KeyValue kv2Y = new KeyValue(btn.translateYProperty(), 0);
            KeyFrame kf2 = new KeyFrame(Duration.seconds(2), "kf2", new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    System.out.println("kf2");
                }
            }, kv2, kv2Y);

            KeyValue kv3 = new KeyValue(btn.translateYProperty(), 300);
            KeyFrame kf3 = new KeyFrame(Duration.seconds(4), "kf3", new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    System.out.println("kf3");
                }
            }, kv3);
            timeline.getKeyFrames().addAll(kf1, kf2, kf3);
            timeline.setDelay(Duration.seconds(1));
            // 设置循环次数，
            // timeline.setCycleCount(2);
            // timeline.setCycleCount(Timeline.INDEFINITE);
            // 是否在逆运动
            timeline.setAutoReverse(true);
            // 动画播放速率为2倍速
            // timeline.setRate(5);
            // 默认为60帧
            System.out.println(timeline.getTargetFramerate());

            Scene scene = new Scene(root, 400, 400);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试时间线动画");
            primaryStage.show();

            tb1.setOnAction(e -> {
                timeline.play();
            });

            tb2.setOnAction(e -> {
                timeline.pause();
            });

            tb3.setOnAction(e -> {
                timeline.stop();
            });

            tb4.setOnAction(e -> {
                // timeline.jumpTo("kf3");
                timeline.jumpTo(Duration.seconds(3));
            });

            tb5.setOnAction(e -> {
                timeline.playFromStart();
            });

            tb6.setOnAction(e -> {
                timeline.setRate(-timeline.getRate());
            });

            timeline.statusProperty().addListener(new ChangeListener<Status>() {
                @Override
                public void changed(ObservableValue<? extends Status> observable, Status oldValue, Status newValue) {
                    System.out.println("状态=" + newValue.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
