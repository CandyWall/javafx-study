package top.jacktgq.custom_controls.waiting.canvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import top.jacktgq.animation.CandyAnimationTimer;
import top.jacktgq.custom_controls.image.CandyCanvasPane;

/**
 * 
 * @Title: CandyCanvasWaitingBar.java
 * @Package top.jacktgq.waitingAnimation.canvas
 * @Description: 基于Canvas绘制的等待特效-长条状
 * @author CandyWall
 * @date 2021年5月19日 上午1:08:20
 * @version V1.0
 */
public class CandyCanvasWaitingBar extends CandyCanvasPane {
    private Color fgColor = Color.web("#DCE7F5");
    private int count = 0;

    public CandyCanvasWaitingBar() {
        new CandyAnimationTimer(100) {
            @Override
            public void updateUI(long now) {
                if (count > 10)
                    count = 0;
                count++;
                repaint();
            }
        }.start();
    }

    @Override
    public void paint(GraphicsContext gc, double x, double y, double w, double h) {
        double step = 20;
        double halfstep = step / 2;

        double start = (count / 10.0) * step * 2; // 速度控制
        for (double i = 0 - start; i < w; i += step) {
            // 斜平行四边形
            gc.beginPath();
            gc.moveTo(i, 0);
            gc.lineTo(i + halfstep, 0);
            gc.lineTo(i, h);
            gc.lineTo(i - halfstep, h);
            gc.closePath();

            gc.setFill(fgColor);
            gc.fill();
        }
    }
}
