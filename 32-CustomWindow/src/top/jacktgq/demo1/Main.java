package top.jacktgq.demo1;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class Main extends Application {
    Button test1 = new Button("打开普通窗口");
    Button test2 = new Button("打开模式窗口");
    TextField textField = new TextField();

    @Override
    public void start(Stage primaryStage) {

        try {
            BorderPane root = new BorderPane();

            HBox hbox = new HBox();
            hbox.getChildren().addAll(test1, test2, textField);
            root.setTop(hbox);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            test1.setOnAction((e) -> {
                openNormalWindow(primaryStage);
            });
            test2.setOnAction((e) -> {
                openModalWindow(primaryStage);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 打开普通窗口
    private void openNormalWindow(Window owner) {
        // 根容器 root
        StackPane root = new StackPane();
        root.setPrefWidth(300);
        root.setPrefHeight(150);

        Label label = new Label("Normal Window");
        root.getChildren().add(label);
        StackPane.setAlignment(label, Pos.CENTER);

        // scene 场景 (客户区)
        Scene scene = new Scene(root);

        // stage 舞台 (窗口)
        Stage stage = new Stage();
        stage.initStyle(StageStyle.DECORATED); // 指定是否有边框
        stage.initOwner(owner); // 指定父窗口，会跟owner window共用一个任务栏图标，并且最小化按钮不可用
        stage.setScene(scene); // 指定scene

        stage.sizeToScene();
        stage.setTitle("普通窗口");

        // 显示窗口
        System.out.println("showing normal window ... ");
        stage.show();
        System.out.println("finish. ");
    }

    // 打开模式窗口，即对话框
    private void openModalWindow(Window owner) {
        // 根容器 root
        StackPane root = new StackPane();
        root.setPrefWidth(300);
        root.setPrefHeight(150);

        Label label = new Label("Modal Window");
        root.getChildren().add(label);
        StackPane.setAlignment(label, Pos.CENTER);

        // scene 场景 (客户区)
        Scene scene = new Scene(root);

        // stage 舞台 (窗口)
        Stage stage = new Stage();
        stage.initOwner(owner);
        stage.setScene(scene);
        stage.sizeToScene();
        stage.setTitle("模式窗口");

        // 显示窗口
        stage.initModality(Modality.WINDOW_MODAL);

        System.out.println("showing modal window ... ");
        stage.showAndWait(); // 卡住界面、直到对话框被关闭
        System.out.println("finish. ");

    }

    public static void main(String[] args) {
        launch(args);
    }
}
