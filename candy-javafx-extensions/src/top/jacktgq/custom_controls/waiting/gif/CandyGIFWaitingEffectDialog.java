package top.jacktgq.custom_controls.waiting.gif;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

/**
 * 
 * @Title: CandyGIFLoadingBarDialog.java
 * @Package top.jacktgq.loadingAnimation.gif.bar
 * @Description: 基于GIF图的等待特效-长条状 等待对话框
 * @author CandyWall
 * @date 2021年5月18日 下午11:34:25
 * @version V1.0
 */
public class CandyGIFWaitingEffectDialog extends Stage {
    private CandyGIFWaitingEffect candyGIFWaitingEffect;

    public CandyGIFWaitingEffectDialog(Window owner, Image gifImage) {
        if (gifImage == null) {
            candyGIFWaitingEffect = new CandyGIFWaitingEffect();
        } else {
            candyGIFWaitingEffect = new CandyGIFWaitingEffect(gifImage);
        }
        candyGIFWaitingEffect.setStyle("-fx-background: transparent");

        Scene scene = new Scene(candyGIFWaitingEffect);
        scene.setFill(null);
        setScene(scene);
        initOwner(owner);
        initModality(Modality.WINDOW_MODAL);
        initStyle(StageStyle.TRANSPARENT);
        centerInParent(owner);
    }

    public CandyGIFWaitingEffectDialog(Window owner) {
        this(owner, null);
    }

    // 相对于父窗口中心显示
    public void centerInParent(Window owner) {
        if (owner != null) {
            // 父窗口的位置px, py, 大小 pw, ph
            double px = owner.getX(), py = owner.getY();
            double pw = owner.getWidth(), ph = owner.getHeight();
            Image image = candyGIFWaitingEffect.getImage();
            double dx = (pw - image.getWidth()) / 2;
            double dy = (ph - image.getHeight()) / 2;

            this.setX(px + dx);
            this.setY(py + dy);
        }
    }
}
