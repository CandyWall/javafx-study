package top.jacktgq.demo1;

import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import top.jacktgq.custom_controls.activity.CandyActivity;

public class Page6 extends CandyActivity {
    @Override
    public void onCreate(Object intent) {
        BorderPane root = new BorderPane();
        this.setContentView(root);

        Label label = new Label("感谢您的参与");

        root.setCenter(label);
    }
}
