package top.jacktgq.defaultCss;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            HBox topPane = new HBox();
            TextField textField = new TextField();
            // textField.getStyleClass().add("candy-textField");
            // 查看textField的默认样式
            System.out.println(textField.getStyleClass());

            Button submit = new Button("提交");
            submit.getStyleClass().addAll("candy-button", "candy-submit");
            Button reset = new Button("重置");
            reset.setId("candy-reset");
            reset.getStyleClass().add("candy-button");

            topPane.getChildren().addAll(textField, submit, reset);
            root.setTop(topPane);

            TextArea textArea = new TextArea();
            root.setCenter(textArea);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("/top/jacktgq/skin/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
