package top.jacktgq.custom_controls.image;

import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;

/**
 * 
 * @Title: CandyCanvasPane.java
 * @Package top.jacktgq.layout
 * @Description: Canvas在实际使用的时候需要控制画布的大小和界面刷新的时机，比较麻烦
 *               只需要继承这个CanvasPane，然后实现这个paint方法，在paint方法体中写绘图逻辑即可。
 * @author CandyWall
 * @date 2021年5月15日 下午10:39:35
 * @version V1.0
 */
public abstract class CandyCanvasPane extends Pane {
    protected Canvas canvas = new Canvas();
    protected GraphicsContext gc;
    protected double w;
    protected double h;

    public CandyCanvasPane() {
        getChildren().add(canvas);
    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        // 宽高需要减去掉边框
        Insets insets = getInsets();
        w = getWidth() - insets.getLeft() - insets.getRight();
        h = getHeight() - insets.getTop() - insets.getBottom();

        // canvas内部缓冲区的大小
        canvas.setWidth(w);
        canvas.setHeight(h);
        // canvas节点的大小
        canvas.resizeRelocate(insets.getLeft(), insets.getTop(), w, h);
        // 重绘
        repaint();
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public void repaint() {
        gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, w, h);
        paint(gc, w, h);
    }

    /**
     * Canvas在实际使用的时候需要控制画布的大小和界面刷新的时机，比较麻烦
     * 只需要继承这个CanvasPane，然后实现这个paint方法，在paint方法体中写绘图逻辑即可。
     * 
     * @param gc
     *            画笔
     * @param w
     *            面板的宽度
     * @param h
     *            面板的高度
     */
    public abstract void paint(GraphicsContext gc, double w, double h);
}
