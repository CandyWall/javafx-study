package top.jacktgq.custom_controls.window;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import top.jacktgq.custom_controls.image.CandyCanvasImagePane;

public class CandyTransparentImageFloatWindow extends Stage {
    protected CandyCanvasImagePane imagePane;

    /**
     * 
     * @param stage
     *            需要实现透明浮动的窗口
     * @param floatImage
     *            背景图片，请使用png格式的图片
     * @param icon
     *            任务栏图标
     * @param width
     * @param height
     */
    public CandyTransparentImageFloatWindow(Window owner, Image floatImage, int width, int height) {
        if (owner != null) {
            initOwner(owner);
        }
        imagePane = new CandyCanvasImagePane(floatImage);
        imagePane.setStyle("-fx-background: transparent");
        Scene scene = new Scene(imagePane, width, height);
        scene.setFill(null);
        // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
        setScene(scene);

        initStyle(StageStyle.TRANSPARENT);
        setAlwaysOnTop(true);
        CandyWindowDragSupport.addDragSupport(this, imagePane, true);
    }

    public void setIcon(Image icon) {
        getIcons().add(icon);
    }

    public CandyCanvasImagePane getImagePane() {
        return imagePane;
    }
}
