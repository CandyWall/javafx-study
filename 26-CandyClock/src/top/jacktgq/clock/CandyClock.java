package top.jacktgq.clock;

import java.io.InputStream;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import top.jacktgq.custom_controls.image.CandyCanvasImageScaler;
import top.jacktgq.custom_controls.image.CandyCanvasPane;

public class CandyClock extends CandyCanvasPane {
    private Image image;
    private double diameter_outer;
    private Rectangle rect_outer = null;
    private Timer timer;
    private Font electronicWatchFont;

    public CandyClock() {
        this.image = new Image("images/bg-transparent.png");
        // 加载电子表字体
        InputStream in = Main.class.getClassLoader().getResourceAsStream("font/DS-DIGIT.TTF");
        electronicWatchFont = Font.loadFont(in, 60);
        setCanvasPadding(new Insets(10));
        canvas.setCursor(Cursor.HAND);
        timer = new Timer();
        // 启动定时器刷新时钟
        startRun();
    }

    public void startRun() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        repaint();
                    }
                });
            }
        }, 0, 1000);
    }

    public void stopRun() {
        timer.cancel();
    }

    @Override
    public void paint(GraphicsContext gc, double x, double y, double w, double h) {
        gc.save();
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(10);
        gc.beginPath();
        if (image == null) {
            return;
        }
        double imgW = image.getWidth();
        double imgH = image.getHeight();
        rect_outer = CandyCanvasImageScaler.fitCenter(imgW, imgH, w, h);
        rect_outer.setX(rect_outer.getX() + x);
        rect_outer.setY(rect_outer.getY() + y);
        gc.arc(rect_outer.getX() + rect_outer.getWidth() / 2, rect_outer.getY() + rect_outer.getHeight() / 2,
                rect_outer.getWidth() / 2, rect_outer.getHeight() / 2, 0, 360);
        gc.clip();
        gc.drawImage(image, 0, 0, imgW, imgH, rect_outer.getX(), rect_outer.getY(), rect_outer.getWidth(),
                rect_outer.getHeight());
        gc.strokeOval(rect_outer.getX(), rect_outer.getY(), rect_outer.getWidth(), rect_outer.getHeight());

        // 绘制水印
        gc.setFill(Color.color(Math.random(), Math.random(), Math.random()));
        gc.setFont(Font.font("华文行楷", 20));
        gc.setTextBaseline(VPos.TOP);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText("作者：糖果墙", x + w / 2, y + h / 4);

        // 绘制电子表
        gc.setFill(Color.valueOf("#ffeb7b"));
        gc.setFont(electronicWatchFont);
        gc.setTextBaseline(VPos.TOP);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText(DateTimeFormatter.ofPattern("HH:mm:ss").format(LocalTime.now()), x + w / 2, y + h * 2 / 3);

        diameter_outer = rect_outer.getWidth();

        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR); // 获取时
        int minute = c.get(Calendar.MINUTE); // 获取分
        int second = c.get(Calendar.SECOND); // 获取秒

        double multiple; // 角度倍数
        // 绘制秒针
        gc.setStroke(Color.RED); // 秒针颜色
        gc.setLineWidth(2); // 秒针宽度
        double secondHandSize = diameter_outer * 4 / 5;// 秒针长度
        if (second <= 15) {
            multiple = 15 - second;
        } else {
            multiple = 75 - second;
        }
        drawLine(gc, 0.0, 0.0, secondHandSize / 2 * Math.cos(Math.PI / 30 * multiple),
                secondHandSize / 2 * Math.sin(Math.PI / 30 * multiple));
        // 绘制分针
        gc.setStroke(Color.BLACK);// 分针颜色
        gc.setLineWidth(6); // 分针宽度
        double minuteHandSize = diameter_outer * 3 / 4;// 分针长度
        if (minute <= 15) {
            multiple = 15 - minute - second / 60.0;
        } else {
            multiple = 75 - minute - second / 60.0;
        }
        drawLine(gc, 0.0, 0.0, minuteHandSize / 2 * Math.cos(Math.PI / 30 * multiple),
                minuteHandSize / 2 * Math.sin(Math.PI / 30 * multiple));

        // 绘制时针
        gc.setLineWidth(10); // 时针宽度
        double hourHandSize = diameter_outer * 2 / 3; // 时针长度
        if (hour <= 3) {
            multiple = 3 - hour - minute / 60.0;
        } else {
            multiple = 15 - hour - minute / 60.0;
        }
        drawLine(gc, 0.0, 0.0, hourHandSize / 2 * Math.cos(Math.PI / 6 * multiple),
                hourHandSize / 2 * Math.sin(Math.PI / 6 * multiple));

        gc.restore();
    }

    // 绘制指针和刻度
    private void drawLine(GraphicsContext gc, Double x1, Double y1, Double x2, Double y2) {
        gc.strokeLine(transferX(x1), transferY(y1), transferX(x2), transferY(y2));
    }

    // 坐标转换
    private double transferX(double x) {
        return x + diameter_outer / 2.0 + rect_outer.getX();
    }

    private double transferY(double y) {
        return -y + diameter_outer / 2.0 + rect_outer.getY();
    }
}
