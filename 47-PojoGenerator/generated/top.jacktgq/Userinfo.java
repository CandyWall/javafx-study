package top.jacktgq; 

import java.util.Date; 

public class Userinfo { 

	private String cid ; 
	private String face_image ; 
	private String face_image_big ; 
	private String id ; 
	private String nickname ; 
	private String password ; 
	private String qrcode ; 
	private String username ; 
	private String class_id ; 
	private String head_portrait_url_big ; 
	private String head_portrait_url_small ; 
	private String name ; 
	private String password ; 
	private Byte role_type ; 
	private String user_id ; 
	private String name ; 
	private String password ; 
	private String userid ; 


	public void setCid(String cid) {
		this.cid=cid;
	}
	public String getCid() {
		return this.cid;
	}
	public void setFace_image(String face_image) {
		this.face_image=face_image;
	}
	public String getFace_image() {
		return this.face_image;
	}
	public void setFace_image_big(String face_image_big) {
		this.face_image_big=face_image_big;
	}
	public String getFace_image_big() {
		return this.face_image_big;
	}
	public void setId(String id) {
		this.id=id;
	}
	public String getId() {
		return this.id;
	}
	public void setNickname(String nickname) {
		this.nickname=nickname;
	}
	public String getNickname() {
		return this.nickname;
	}
	public void setPassword(String password) {
		this.password=password;
	}
	public String getPassword() {
		return this.password;
	}
	public void setQrcode(String qrcode) {
		this.qrcode=qrcode;
	}
	public String getQrcode() {
		return this.qrcode;
	}
	public void setUsername(String username) {
		this.username=username;
	}
	public String getUsername() {
		return this.username;
	}
	public void setClass_id(String class_id) {
		this.class_id=class_id;
	}
	public String getClass_id() {
		return this.class_id;
	}
	public void setHead_portrait_url_big(String head_portrait_url_big) {
		this.head_portrait_url_big=head_portrait_url_big;
	}
	public String getHead_portrait_url_big() {
		return this.head_portrait_url_big;
	}
	public void setHead_portrait_url_small(String head_portrait_url_small) {
		this.head_portrait_url_small=head_portrait_url_small;
	}
	public String getHead_portrait_url_small() {
		return this.head_portrait_url_small;
	}
	public void setName(String name) {
		this.name=name;
	}
	public String getName() {
		return this.name;
	}
	public void setPassword(String password) {
		this.password=password;
	}
	public String getPassword() {
		return this.password;
	}
	public void setRole_type(Byte role_type) {
		this.role_type=role_type;
	}
	public Byte getRole_type() {
		return this.role_type;
	}
	public void setUser_id(String user_id) {
		this.user_id=user_id;
	}
	public String getUser_id() {
		return this.user_id;
	}
	public void setName(String name) {
		this.name=name;
	}
	public String getName() {
		return this.name;
	}
	public void setPassword(String password) {
		this.password=password;
	}
	public String getPassword() {
		return this.password;
	}
	public void setUserid(String userid) {
		this.userid=userid;
	}
	public String getUserid() {
		return this.userid;
	}

}
 