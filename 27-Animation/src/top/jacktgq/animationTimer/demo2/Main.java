package top.jacktgq.animationTimer.demo2;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
    private AnimationTimer timer;
    private volatile boolean running = false;
    // 开始计时
    private long start = 0; // 如果start为0，表示定时器已经停止或者还没有启动过
    private Label timeLabel;

    @Override
    public void start(Stage primaryStage) {
        try {
            StackPane root = new StackPane();
            HBox buttonPane = new HBox();
            Button runBtn = new Button("开始计时");
            runBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if (start == 0) {
                        start = System.nanoTime();
                    }

                    if (running) {
                        runBtn.setText("开始计时");
                        timer.stop();
                    } else {
                        runBtn.setText("暂停计时");
                        timer.start();
                    }

                    running = !running;
                }
            });

            Button endBtn = new Button("停止计时");
            endBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    timer.stop();
                    runBtn.setText("开始计时");
                    running = false;
                    start = 0;
                    timeLabel.setText("00:00:00:000");
                }
            });

            buttonPane.getChildren().addAll(runBtn, endBtn);
            buttonPane.setMaxSize(200, 100);
            buttonPane.setAlignment(Pos.CENTER);

            timeLabel = new Label("00:00:00:000");
            timeLabel.setStyle(
                    "-fx-background-color: #0075ff; -fx-font-size: 40; -fx-text-fill: #d01d7a; -fx-padding: 20");

            root.getChildren().addAll(buttonPane, timeLabel);
            StackPane.setAlignment(buttonPane, Pos.TOP_CENTER);
            StackPane.setAlignment(timeLabel, Pos.CENTER);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("秒表");
            primaryStage.show();

            timer = new AnimationTimer() {
                @Override
                public void handle(long now) {
                    // 经过的毫秒
                    long pass = (now - start) / 1000000;
                    // 计算 HH:mm:ss.ms
                    // 毫秒
                    long nanoSecond = pass % 1000;
                    // 经过的秒数
                    pass /= 1000;
                    // 秒
                    long second = pass % 60;
                    // 经过的分钟数
                    pass /= 60;
                    // 分
                    long minute = pass % 60;
                    // 经过的小时数
                    // 时
                    long hour = pass / 60;

                    String timeStr = (hour < 10 ? "0" + hour : "" + hour) + ":"
                            + (minute < 10 ? "0" + minute : "" + minute) + ":"
                            + (second < 10 ? "0" + second : "" + second) + ":" + (nanoSecond < 10 ? "00" + nanoSecond
                                    : (nanoSecond < 100 ? "0" + nanoSecond : nanoSecond));
                    timeLabel.setText(timeStr);
                }
            };

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
