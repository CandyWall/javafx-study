package top.jacktgq.v1;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @Title: ModifyWindow.java
 * @Package application
 * @Description: 修改学生信息
 * @author CandyWall
 * @date 2021年5月14日 上午1:49:15
 * @version V1.0
 */
public class ModifyWindow extends Dialog {
    public TextField idField;
    public TextField nameField;
    public TextField phoneField;

    public ModifyWindow(Stage owner, ObservableList<Student> listData, int index) {
        VBox vbox = new VBox();
        if (true) {
            HBox row = new HBox();

            Label label = new Label("学号：");
            idField = new TextField(listData.get(index).id + "");
            idField.setPromptText("请输入学号");

            row.getChildren().addAll(label, idField);
            vbox.getChildren().add(row);
        }
        if (true) {
            HBox row = new HBox();

            Label label = new Label("姓名：");
            nameField = new TextField(listData.get(index).name);
            nameField.setPromptText("请输入姓名");

            row.getChildren().addAll(label, nameField);
            vbox.getChildren().add(row);
        }
        if (true) {
            HBox row = new HBox();

            Label label = new Label("手机号：");
            phoneField = new TextField(listData.get(index).phone);

            Button confirmModifyBtn = new Button("增加");
            confirmModifyBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    // 获取学号
                    String id = idField.getText();
                    // 获取姓名
                    String name = nameField.getText();
                    // 获取手机号
                    String phone = nameField.getText();
                    if (id.equals("") || name.equals("") || phone.equals("")) {
                        System.out.println("不能有空项！");
                    }
                    // 使用set方法修改数据会通知ListView更新显示
                    listData.set(index, new Student(Integer.parseInt(id), name, phone));
                    close();
                }
            });

            row.getChildren().addAll(label, phoneField, confirmModifyBtn);
            HBox.setMargin(confirmModifyBtn, new Insets(0, 10, 0, 50));

            vbox.getChildren().add(row);
        }

        vbox.setPadding(new Insets(10));
        vbox.setSpacing(10);

        setTitle("修改信息");
        setResizable(false);
    }
}