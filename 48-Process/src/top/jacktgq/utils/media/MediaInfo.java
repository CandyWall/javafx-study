package top.jacktgq.utils.media;

import java.io.File;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import top.jacktgq.utils.process.CandyProcess;

public class MediaInfo {
    public String type = "general"; // "general" "video" "image"
    public String format = "";
    public int width = 0;
    public int height = 0;
    public int bitrate = 0;
    public int duration = 0;

    public MediaInfo parse(File file) throws Exception {
        String program = "C:\\tools\\mediainfo\\MediaInfo.exe";
        if (!new File(program).exists())
            throw new Exception("MediaInfo.exe 路径不正确!");

        String shellCommand = String.format("%s --Output=XML \"%s\" ", program, file.getAbsolutePath());
        Map<String, Object> resultMap = CandyProcess.execute(shellCommand);
        int code = (int) resultMap.get("code");
        if (code != 0) {
            System.out.println("MediaInfo返回错误!" + resultMap.get("mediaInfo"));
            throw new Exception("MediaInfo解析错误!");
        }

        // 获取输出的XML
        String outputText = (String) resultMap.get("mediaInfo");

        // 解析XML
        try {
            SAXReader reader = new SAXReader();
            Document doc = reader.read(new StringReader(outputText));
            parse(doc);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return this;
    }

    private void parse(Document doc) throws Exception {

        // <root>/<media>/<track>/<Format>
        Element root = doc.getRootElement();
        Element media = root.element("media");
        List<Element> tracks = media.elements("track");
        for (Element track : tracks) {
            String type = track.attributeValue("type");
            if ("General".equals(type)) {
                this.type = "general";
                this.format = track.elementText("Format");
            } else if ("Video".equals(type)) {
                // 如果是视频文件
                this.type = "video";
                this.width = Integer.valueOf(track.elementText("Width"));
                this.height = Integer.valueOf(track.elementText("Height"));
                this.bitrate = Integer.valueOf(track.elementText("BitRate"));
                try {
                    this.duration = (int) (1000 * Double.valueOf(track.elementText("Duration")));
                } catch (Exception e) {
                }
            } else if ("Image".equals(type)) {
                // 如果是图片文件
                this.type = "image";
                this.width = Integer.valueOf(track.elementText("Width"));
                this.height = Integer.valueOf(track.elementText("Height"));
            }
        }
    }

}
