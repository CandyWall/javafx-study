package top.jacktgq.demo2;

import java.io.File;
import java.util.Map;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import top.jacktgq.utils.process.CandyProcess;

public class Main extends Application {
    private TextArea textArea;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            HBox toolbar = new HBox();
            Button openFilebtn = new Button("打开文件");
            toolbar.getChildren().add(openFilebtn);
            toolbar.setAlignment(Pos.CENTER);
            openFilebtn.setOnAction(e -> {
                FileChooser chooser = new FileChooser();
                chooser.setInitialDirectory(new File("testData/"));
                File file = chooser.showOpenDialog(primaryStage);
                if (file != null) {
                    showMediaInfo(file);
                }
            });
            root.setTop(toolbar);

            textArea = new TextArea();
            root.setCenter(textArea);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("打开系统文件夹");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showMediaInfo(File file) {
        File mediaInfoFile = new File("tools/mediainfo/MediaInfo.exe");
        String path = mediaInfoFile.getAbsolutePath();
        String cmd = path + " --Output=XML " + file.getAbsolutePath();
        try {
            Map<String, Object> resultMap = CandyProcess.execute(cmd);
            int code = (int) resultMap.get("code");
            if (code == 0) {
                textArea.setText((String) resultMap.get("consoleInfo"));
            }
            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
