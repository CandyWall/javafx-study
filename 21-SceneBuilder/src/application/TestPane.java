package application;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class TestPane extends BorderPane {
    @FXML
    private TextField nameField;

    @FXML
    private Button addBtn;

    @FXML
    private TextArea contentArea;

    public TestPane() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TestPane.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void doAdd() {
        String content = nameField.getText().trim();
        if (content.length() > 0) {
            contentArea.appendText(content + "\n");
        }
    }

}
