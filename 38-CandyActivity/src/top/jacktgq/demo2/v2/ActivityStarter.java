package top.jacktgq.demo2.v2;

import javafx.stage.Stage;
import top.jacktgq.custom_controls.activity.CandyActivity;
import top.jacktgq.custom_controls.activity.CandyActivityContext;
import top.jacktgq.custom_controls.activity.CandyActivityObserver;

public class ActivityStarter implements CandyActivityObserver {
    private CandyActivityContext context = new CandyActivityContext();
    private Stage primaryStage;

    public ActivityStarter(Stage primaryStage) {
        this.primaryStage = primaryStage;
        context.setObserver(this);
        context.startActivity(null, LoginActivity.class, null);
    }

    @Override
    public void onActivityShow(CandyActivity a) {
        primaryStage.getScene().setRoot(a.getContentView());
        if (a instanceof LoginActivity) {
            primaryStage.setTitle("用户登录");
            primaryStage.setMaximized(false);
        } else if (a instanceof MainActivity) {
            primaryStage.setMaximized(true);
            primaryStage.setTitle("用户主页");
        }
    }

    @Override
    public void onActivityDismiss(CandyActivity a) {

    }
}
