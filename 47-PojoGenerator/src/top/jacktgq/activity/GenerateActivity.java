package top.jacktgq.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;

import javafx.geometry.Pos;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.StackPane;
import top.jacktgq.custom_controls.activity.CandyActivity;
import top.jacktgq.thread.CandyShortTask;
import top.jacktgq.utils.JavaClassUtil;

public class GenerateActivity extends CandyActivity {
    @Override
    public void onCreate(Object intent) {
        StackPane root = new StackPane();
        this.setContentView(root);

        ProgressBar progressBar = new ProgressBar(ProgressBar.INDETERMINATE_PROGRESS);
        root.getChildren().add(progressBar);
        StackPane.setAlignment(progressBar, Pos.CENTER);

        HashMap<String, Object> paramsMap = (HashMap<String, Object>) intent;
        ArrayList<String> tableNameList = (ArrayList<String>) paramsMap.get("tableNameList");
        String packagePath = (String) paramsMap.get("packagePath");
        QueryRunner runner = (QueryRunner) context.getParam("queryRunner", null);

        String path = "";
        if (packagePath != null && packagePath.length() > 0) {
            path = packagePath.replace("\\.", "/") + "/";
        }
        File generateDir = new File("generated/" + path);

        new CandyShortTask() {
            private List<Map<String, Object>> mapList;
            private boolean hasError = false;

            @Override
            protected void doInBackground() throws Exception {
                try {
                    for (String tableName : tableNameList) {
                        String sql = "SELECT COLUMN_NAME, DATA_TYPE FROM `COLUMNS` WHERE TABLE_NAME = ?";
                        mapList = runner.query(sql, new MapListHandler(), tableName);
                        JavaClassUtil.generateJavaClass(mapList, tableName, packagePath, generateDir);
                    }
                } catch (Exception e) {
                    hasError = true;
                }
            }

            @Override
            protected void done() {
                HashMap<String, Object> paramsMap = new HashMap<>();
                paramsMap.put("generateDir", generateDir);
                paramsMap.put("hasError", hasError);
                startActivity(ResultActivity.class, paramsMap);
            }
        }.execute();
    }
}
