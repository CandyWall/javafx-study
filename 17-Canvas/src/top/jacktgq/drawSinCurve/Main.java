package top.jacktgq.drawSinCurve;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            double amplitude = 100;
            double period = 150;
            SinCurvePane sinCurvePane = new SinCurvePane(amplitude, period);

            root.setCenter(sinCurvePane);

            HBox sliderPane = new HBox();
            if (true) {
                VBox vbox = new VBox();
                vbox.setAlignment(Pos.CENTER);
                Slider slider1 = new Slider(0, 200, amplitude);
                slider1.setOrientation(Orientation.VERTICAL);
                slider1.valueProperty().addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue,
                            Number newValue) {
                        sinCurvePane.setAmplitude(newValue.doubleValue());
                    }
                });
                Label label = new Label("振幅");
                vbox.getChildren().addAll(slider1, label);
                sliderPane.getChildren().addAll(vbox);
            }
            if (true) {
                VBox vbox = new VBox();
                vbox.setAlignment(Pos.CENTER);
                Slider slider2 = new Slider(100, 300, period);
                slider2.setOrientation(Orientation.VERTICAL);
                slider2.valueProperty().addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue,
                            Number newValue) {
                        sinCurvePane.setPeriod(newValue.doubleValue());
                    }
                });
                Label label = new Label("周期");
                vbox.getChildren().addAll(slider2, label);
                sliderPane.getChildren().addAll(vbox);
            }

            sliderPane.setPadding(new Insets(20));
            sliderPane.setSpacing(20);

            root.setRight(sliderPane);

            Scene scene = new Scene(root, 1200, 480);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("绘制正弦曲线");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
