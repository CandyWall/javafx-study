package top.jacktgq.test;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.thread.CandyShortTask;
import top.jacktgq.waitingAnimation.gif.CandyGIFWaitingEffect;
import top.jacktgq.waitingAnimation.gif.CandyGIFWaitingEffectDialog;

public class Main1 extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            FlowPane root = new FlowPane();
            root.setAlignment(Pos.CENTER);
            Image[] gifImages = new Image[] { CandyGIFWaitingEffect.WAITINGBAR_RED_YELLOW,
                    CandyGIFWaitingEffect.WAITINGBAR_GREEN_WHITE, CandyGIFWaitingEffect.WAITINGBAR_LIKEWIN7,
                    CandyGIFWaitingEffect.WAITINGCIRCLE_BIG_GRAY_WHITE,
                    CandyGIFWaitingEffect.WAITINGCIRCLE_MEDIUM_BLACK_GRAY,
                    CandyGIFWaitingEffect.WAITINGCIRCLE_NOT_TRANSPARENT,
                    CandyGIFWaitingEffect.WAITINGCIRCLE_SMALL_BLACK_WHITE,
                    CandyGIFWaitingEffect.WAITINGCIRCLE_SMALL_ORANGE };

            CandyGIFWaitingEffectDialog[] dialogs = new CandyGIFWaitingEffectDialog[8];

            Button[] openGIFWaitingEffectBtns = new Button[8];
            for (int i = 0; i < 8; i++) {
                int _i = i;
                openGIFWaitingEffectBtns[i] = new Button("打开基于GIF的等待对话框" + (i + 1));
                openGIFWaitingEffectBtns[i].setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event) {
                        new CandyShortTask() {

                            @Override
                            protected void doInBackground() throws Exception {
                                // 在这里进行耗时任务
                                Thread.sleep(3000);
                            }

                            @Override
                            protected void done() {
                                if (dialogs[_i] != null) {
                                    // 任务执行完以后关闭等待对话框
                                    dialogs[_i].close();
                                }
                            }
                        }.execute();

                        // 显示等待对话框，等待的时候阻塞用户的其他操作
                        dialogs[_i] = new CandyGIFWaitingEffectDialog(primaryStage, gifImages[_i]);
                        dialogs[_i].showAndWait();
                    }
                });
            }

            root.getChildren().addAll(openGIFWaitingEffectBtns);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试基于GIF图的等待对话框");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
