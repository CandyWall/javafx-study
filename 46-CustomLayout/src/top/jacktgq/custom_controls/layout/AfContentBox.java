package top.jacktgq.custom_controls.layout;

import javafx.geometry.Insets;
import javafx.scene.Node;

/* 存放单一节点的容器
 * 
 */

public class AfContentBox extends CandyLayoutPane {
    public AfContentBox() {
    }

    public void setContent(Node node) {
        this.getChildren().clear();
        this.getChildren().add(node);
    }

    public Node getContent() {
        if (this.getChildren().size() > 0) {
            return this.getChildren().get(0);
        }
        return null;
    }

    @Override
    protected void layoutChildren(double w, double h) {
        Node node = getContent();
        if (node == null)
            return;

        Insets insets = this.getInsets();
        node.resizeRelocate(insets.getLeft(), insets.getTop(), w - insets.getLeft() - insets.getRight(),
                h - insets.getTop() - insets.getBottom());
    }

}
