package top.jacktgq.custom_controls.window;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

public class CandyPrettyWindowWrapper extends CandyWindowWrapper {
    private LayoutPane customPane;
    private boolean hasTitleBar;

    /**
     * 
     * @param stage
     *            需要进行美化的窗口
     * @param resizable
     *            是否可以缩放窗口
     * @param hasTitleBar
     *            是否有标题栏
     */
    public CandyPrettyWindowWrapper(Stage stage, boolean resizable, boolean hasTitleBar) {
        super(stage, resizable);
        this.hasTitleBar = hasTitleBar;
        customPane = new LayoutPane();
        customPane.closeBtn.setOnAction((e) -> {
            stage.close();
        });
    }

    public CandyPrettyWindowWrapper(Stage stage) {
        this(stage, true, true);
    }

    @Override
    public void setContentView(Region contentView) {
        setContentView(contentView, -1, -1);
    }

    /**
     * 传入宽高后，可以调用centerInParent定位到父窗口的中间
     * 
     * @param root
     * @param sceneWidth
     * @param sceneHeight
     */
    @Override
    public void setContentView(Region contentView, double width, double height) {
        customPane.setCenter(contentView);
        super.setContentView(customPane, width, height + 40);
        initDragSupport(customPane.titleLabel, resizable);
    }

    public void setTitle(String title) {
        customPane.titleLabel.setText(title);
    }

    private class LayoutPane extends BorderPane {
        Label titleLabel = new Label();
        Button closeBtn = new Button("", new ImageView("top/jacktgq/custom_controls/window/ic_close.png"));

        public LayoutPane() {
            HBox titlePane = new HBox();
            titlePane.getChildren().addAll(titleLabel, closeBtn);
            titlePane.setAlignment(Pos.TOP_LEFT);
            titleLabel.setMaxWidth(9999);

            HBox.setHgrow(titleLabel, Priority.ALWAYS);
            // titlePane.setPadding(new Insets(10));
            titlePane.setMaxHeight(40);
            setTop(titlePane);

            if (!hasTitleBar) {
                titlePane.setStyle("-fx-background-color: transparent;");
                titleLabel.setStyle("-fx-background-color: transparent;");
            } else {
                titlePane.setStyle("-fx-background-color: #ccc;");
                titleLabel.setStyle("-fx-background-color: #ccc;");
            }

            // 设置样式
            this.getStylesheets()
                    .add(CandyPrettyWindowWrapper.class.getResource("candy-pretty-window.css").toExternalForm());
            this.getStyleClass().add("candy-pretty-window");
            closeBtn.getStyleClass().add("candy-pretty-window-close");
            titleLabel.getStyleClass().add("candy-pretty-window-title");
            // getCenter().getStyleClass().add("candy-pretty-dialog-content");
        }
    }

    /**
     * 设置背景颜色
     * 
     * @param color
     */
    public void setBackground(Paint color) {
        customPane.setBackground(new Background(new BackgroundFill(color, null, null)));
    }

    /**
     * 设置背景图片
     * 
     * @param image
     */
    public void setBackground(Image image) {
        customPane.setBackground(
                new Background(new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                        BackgroundPosition.CENTER, new BackgroundSize(-1, -1, true, true, true, true))));
    }
}
