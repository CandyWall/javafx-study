package top.jacktgq.demo4;

import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import top.jacktgq.custom_controls.layout.CandyLinearLayoutPane;

public class MyLayoutPane extends CandyLinearLayoutPane {

    public MyLayoutPane() {
        this.orientation(Orientation.VERTICAL).spacing(10).padding(4);
        // 工具栏
        if (true) {
            CandyLinearLayoutPane toolbar = new CandyLinearLayoutPane().spacing(10).padding(4);
            toolbar.getStyleClass().add("toolbar");
            toolbar.getChildren().addAll(new Button("发送"), new Button("定时发送"), new Button("存草稿"), new Button("关闭"),
                    new Label(), new Label("预览"), new Label("新窗口写信"));
            toolbar.layout("40px 80px 60px 40px 1 30px 60px");

            this.add(toolbar, "30px");
        }

        // 第1行
        if (true) {
            CandyLinearLayoutPane row = new CandyLinearLayoutPane().spacing(10);
            row.getStyleClass().add("row");
            row.add(new Label("收件人"), "60px");
            row.add(new TextField(), "1");

            this.add(row, "30px");
        }

        // 第2行
        if (true) {
            CandyLinearLayoutPane row = new CandyLinearLayoutPane().spacing(10);
            row.getStyleClass().add("row");
            row.add(new Label("主题"), "60px");
            row.add(new TextField(), "1");

            this.add(row, "30px");
        }

        // 第3行
        if (true) {
            CandyLinearLayoutPane row = new CandyLinearLayoutPane().spacing(10);
            row.getStyleClass().add("row");
            Label body = new Label("正文");
            body.setId("body-label");
            row.add(body, "60px");
            row.add(new TextArea(), "1");

            this.add(row, "1");
        }

    }
}
