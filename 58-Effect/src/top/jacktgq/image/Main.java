package top.jacktgq.image;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.Effect;
import javafx.scene.effect.Shadow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();
            root.setStyle("-fx-background-color: transparent");
            root.setSpacing(30);
            root.setPadding(new Insets(20));
            ImageView iv = new ImageView("images/kenan.png");
            iv.setFitWidth(100);
            iv.setFitHeight(100);
            root.getChildren().addAll(iv);

            Scene scene = new Scene(root, 600, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试特效");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Effect shadowEffect() {
        Shadow shadow = new Shadow();
        shadow.setColor(Color.DARKORANGE);
        shadow.setWidth(100);
        shadow.setHeight(100);
        shadow.setBlurType(BlurType.GAUSSIAN);
        shadow.setRadius(2);

        return shadow;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
