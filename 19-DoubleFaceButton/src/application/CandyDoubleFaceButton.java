package application;

import java.io.File;

import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import top.jacktgq.custom_controls.image.CandyCanvasImagePane;

/**
 * 
 * @Title: CandyDoubleFaceButton.java
 * @Package application
 * @Description: 双面按钮
 * @author CandyWall
 * @date 2021年5月16日 下午8:29:05
 * @version V1.0
 */
public class CandyDoubleFaceButton extends CandyCanvasImagePane {
    // 正面显示图标，背面显示文字
    private String text;

    boolean hover = false; // 鼠标移上来时，hover为true

    public CandyDoubleFaceButton(File file, String text) {
        super(file, FIT_XY);
        init(text);
    }

    public CandyDoubleFaceButton(Image image, String text) {
        super(image, FIT_XY);
        init(text);
    }

    public void init(String text) {
        this.text = text;

        setPrefSize(100, 100);
        // 鼠标移入
        setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                hover = true;
                repaint();
            }
        });

        // 鼠标移出
        setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                hover = false;
                repaint();
            }
        });

        setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (handler != null) {
                    handler.handle(event);
                }
            }

        });
    }

    private EventHandler<MouseEvent> handler;

    public void setOnAction(EventHandler<MouseEvent> handler) {
        this.handler = handler;
    }

    public String getText() {
        return text;
    }

    @Override
    public void paint(GraphicsContext gc, double w, double h) {
        if (hover) {
            showText();
        } else {
            showImage();
        }
    }

    private void showText() {
        CandyFXPlotUtil.pathRoundRect(gc, new Rectangle2D(0, 0, w, h), 12);
        gc.setStroke(Color.web("#ccc"));
        gc.stroke();
        gc.setFill(Color.web("#eee"));
        gc.fill();

        // 设置文字的字体和大小
        gc.setFont(new Font("微软雅黑", 20));
        gc.setTextBaseline(VPos.CENTER);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setFill(Color.BLUEVIOLET);
        gc.fillText(text, w / 2, h / 2);
    }

    private void showImage() {
        gc.save(); // 使用clip之前先备份一下初始绘制参数
        // 开始绘制
        // gc.beginPath();
        // 绘制一个圆
        // gc.arc(w / 2, h / 2, w / 2 - 4, h / 2 - 4, 0, 360);
        // 绘制一个圆角矩形Path
        CandyFXPlotUtil.pathRoundRect(gc, new Rectangle2D(0, 0, w, h), 12);
        gc.clip();
        super.paint(gc, w, h);
        gc.restore(); // 恢复初始绘制参数
    }
}
