package top.jacktgq.path.demo1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcTo;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.HLineTo;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.QuadCurveTo;
import javafx.scene.shape.VLineTo;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();

            Path path = new Path();

            MoveTo mt = new MoveTo(100, 100);

            LineTo lt = new LineTo(200, 100);

            QuadCurveTo qct = new QuadCurveTo(50, 0, 100, 100);
            qct.setAbsolute(false);

            HLineTo hlt = new HLineTo(100);
            hlt.setAbsolute(false);

            CubicCurveTo cct = new CubicCurveTo(50, -150, 150, 150, 200, 0);
            cct.setAbsolute(false);

            VLineTo vlt = new VLineTo(100);
            vlt.setAbsolute(false);

            ClosePath cp = new ClosePath();

            ArcTo at = new ArcTo(100, 100, 0, 100, 100, true, true);
            at.setAbsolute(false);

            path.getElements().addAll(mt, lt, qct, hlt, cct, vlt, at, cp);
            path.setFill(Color.web("#FFCC00"));
            path.setStrokeWidth(10);
            path.setStroke(Color.RED);
            path.getStrokeDashArray().addAll(5.0, 5.0);
            // path.setStrokeLineCap(StrokeLineCap.ROUND);
            // path.setStrokeLineJoin(StrokeLineJoin.ROUND);

            root.getChildren().add(path);

            Scene scene = new Scene(root, 1000, 800);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
