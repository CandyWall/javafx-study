package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

/**
 * 
 * @Title: Main.java
 * @Package application
 * @Description: 数据录入
 * @author CandyWall
 * @date 2021年5月14日 上午12:02:50
 * @version V1.0
 */
public class Main extends Application {
    private TextArea textArea;

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            HBox topPane = new HBox();
            TextField textField = new TextField();
            textField.setPromptText("请在文本框中输入一行记录");
            Button button = new Button("添加");
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    String line = textField.getText();
                    textArea.appendText(line + "\n");
                    textField.setText("");
                }
            });
            topPane.getChildren().addAll(textField, button);
            HBox.setHgrow(textField, Priority.ALWAYS);
            HBox.setMargin(button, new Insets(0, 0, 0, 10));
            topPane.setPadding(new Insets(10));
            ;

            root.setTop(topPane);

            textArea = new TextArea();
            textArea.setPadding(new Insets(5));
            root.setCenter(textArea);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
