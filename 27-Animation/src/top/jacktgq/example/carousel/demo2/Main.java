package top.jacktgq.example.carousel.demo2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();
            Button btn1 = new Button("开始");
            Button btn2 = new Button("逆向");

            // 轮播图面板
            CarouselPane carouselPane = new CarouselPane();

            root.getChildren().addAll(btn1, btn2, carouselPane);
            AnchorPane.setTopAnchor(carouselPane, 200.0);
            AnchorPane.setLeftAnchor(carouselPane, 500.0);
            AnchorPane.setLeftAnchor(btn2, 100.0);

            Scene scene = new Scene(root, 1400, 800);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("跑马灯特效");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
