package top.jacktgq.custom_controls.tab.label;

import java.util.function.Predicate;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

public class CandyVLabelsBar extends VBox {

    public CandyVLabelsBar() {
        this.getStylesheets().add("/top/jacktgq/custom_controls/tab/label/candy-label-bar.css");
        this.getStyleClass().add("candy-vlabel-bar");
    }

    public void addLabel(String text) {
        addLabel(text, text);
    }

    public void addLabel(String text, Object data) {
        Label l = new Label(text);
        l.setUserData(data);
        l.setOnMouseClicked(labelClickHandler);
        this.getChildren().add(l);
    }

    // 根据data来查找一个label
    public Label getLabel(Object data) {
        for (int i = 0; i < getChildren().size(); i++) {
            Label l = (Label) getChildren().get(i);
            if (data.equals(l.getUserData())) {
                return l;
            }
        }
        return null;
    }

    public boolean isSelected(Label l) {
        return l.getStyleClass().indexOf("selected") >= 0;
    }

    public void select(Label l) {
        l.getStyleClass().add("selected");
    }

    public void select(Object data) {
        select(getLabel(data));
    }

    public void unselect(Label l) {
        l.getStyleClass().removeIf(new Predicate<String>() {

            @Override
            public boolean test(String t) {
                if (t.equals("selected"))
                    return true;
                return false;
            }

        });
    }

    public void unselect(Object data) {
        unselect(getLabel(data));
    }

    // 获取选中的
    public Label getSelected() {
        for (int i = 0; i < getChildren().size(); i++) {
            Label l = (Label) getChildren().get(i);
            if (isSelected(l)) {
                return l;
            }
        }
        return null;
    }

    // 事件回调
    public interface OnLabelClickedListener {
        public void onLabelClicked(Object data);
    }

    private OnLabelClickedListener onLabelClickedListener;

    public void setOnLabelClickedListener(OnLabelClickedListener onLabelClickedListener) {
        this.onLabelClickedListener = onLabelClickedListener;
    }

    // 点击事件处理
    EventHandler<MouseEvent> labelClickHandler = (e) -> {

        Label selected = getSelected();
        if (selected != null)
            unselect(selected);

        Label next = (Label) e.getSource();
        select(next);

        if (onLabelClickedListener != null)
            onLabelClickedListener.onLabelClicked(next.getUserData());
    };

}
