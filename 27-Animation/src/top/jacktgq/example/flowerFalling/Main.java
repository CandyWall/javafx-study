package top.jacktgq.example.flowerFalling;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import top.jacktgq.custom_controls.window.CandyWindowDragSupport;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            FlowerFallingPane root = new FlowerFallingPane(800, 1000, 40);

            Scene scene = new Scene(root, 800, 1000);
            scene.setFill(null);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("落花特效");
            primaryStage.initStyle(StageStyle.TRANSPARENT);
            primaryStage.show();
            CandyWindowDragSupport.addDragSupport(primaryStage, root, false);
            Media media = new Media(getClass().getResource("/mp3/1.mp3").toExternalForm());
            MediaPlayer player = new MediaPlayer(media);
            player.setCycleCount(MediaPlayer.INDEFINITE);
            player.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
