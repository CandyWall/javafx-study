package top.jacktgq.java_util_timer;

import java.io.InputStream;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

/**
 * 
 * @Title: Main.java
 * @Package top.jacktgq.font_import
 * @Description: 引入第三方字体 用法说明参考：
 *               https://docs.oracle.com/javase/8/javafx/api/javafx/scene/doc-files/cssref.html#typefont
 * @author CandyWall
 * @date 2021年5月18日 上午1:31:32
 * @version V1.0
 */
public class Main extends Application {
    private Timer timer;

    @Override
    public void start(Stage primaryStage) {
        try {
            InputStream in = Main.class.getClassLoader().getResourceAsStream("font/DS-DIGIT.TTF");
            Font font1 = Font.loadFont(in, 80);
            // System.out.println(font1);
            Label label = new Label();
            label.setFont(font1);
            // -fx-background-color: #101466
            label.setStyle("-fx-text-fill: #ffeb7b; -fx-background: transparent");

            Scene scene = new Scene(label, 400, 400);
            scene.setFill(null);
            scene.getStylesheets()
                    .add(getClass().getResource("/top/jacktgq/java_util_timer/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("JavaFX程序加载外部字体  制作电子表效果");
            primaryStage.initStyle(StageStyle.TRANSPARENT);
            primaryStage.setAlwaysOnTop(true);
            primaryStage.show();

            timer = new Timer();
            int interval = 1000; // 时间间隔1秒钟
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    String currentTimeStr = DateTimeFormatter.ofPattern("HH:mm:ss").format(LocalTime.now());
                    Platform.runLater(() -> {
                        label.setText(currentTimeStr);
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, 0, interval);

            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    // 窗口关闭的时候手动停止定时器
                    if (timer != null) {
                        timer.cancel();
                    }

                    Platform.exit();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
