package top.jacktgq.utils;

import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class AfCanvasUtils {
    public static void clearRect(GraphicsContext gc, Rectangle2D r) {
        gc.clearRect(r.getMinX(), r.getMinY(), r.getWidth(), r.getHeight());
    }

    public static void strokeRect(GraphicsContext gc, Rectangle2D r) {
        gc.strokeRect(r.getMinX(), r.getMinY(), r.getWidth(), r.getHeight());
    }

    public static void fillRect(GraphicsContext gc, Rectangle2D r) {
        gc.fillRect(r.getMinX(), r.getMinY(), r.getWidth(), r.getHeight());
    }

    public static void strokeRoundRect(GraphicsContext gc, Rectangle2D r, double rx, double ry) {
        gc.strokeRoundRect(r.getMinX(), r.getMinY(), r.getWidth(), r.getHeight(), rx, ry);
    }

    public static void fillRoundRect(GraphicsContext gc, Rectangle2D r, double rx, double ry) {
        gc.fillRoundRect(r.getMinX(), r.getMinY(), r.getWidth(), r.getHeight(), rx, ry);
    }

    public static void fillCircle(GraphicsContext gc, double cetnerX, double centerY, double radius) {
        gc.fillOval(cetnerX - radius, centerY - radius, radius * 2, radius * 2);
    }

    public static void strokeCircle(GraphicsContext gc, double cetnerX, double centerY, double radius) {
        gc.strokeOval(cetnerX - radius, centerY - radius, radius * 2, radius * 2);
    }

    public static void drawImage(GraphicsContext gc, Image image, Rectangle2D r) {
        gc.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), r.getMinX(), r.getMinY(), r.getWidth(),
                r.getHeight());
    }
}
