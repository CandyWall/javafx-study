package top.jacktgq.timeline.demo7;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Box;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();
            root.setPrefWidth(900);
            root.setPrefHeight(900);
            Box box = new Box(100, 100, 100);
            root.getChildren().addAll(box);
            AnchorPane.setTopAnchor(box, 200.0);
            AnchorPane.setLeftAnchor(box, 200.0);

            // 默认的旋转坐标在原点(0,0,0)，也是立方体的中心点
            // Rotate rotate = new Rotate(0, -50, -50, -50);
            // rotate.setAxis(new Point3D(0, 0, 1));
            // box.getTransforms().add(rotate);
            // KeyValue kv1 = new KeyValue(rotate.angleProperty(), 0);
            // KeyFrame kf1 = new KeyFrame(Duration.millis(0), kv1);
            // KeyValue kv2 = new KeyValue(rotate.angleProperty(), 360);
            // KeyFrame kf2 = new KeyFrame(Duration.millis(1000), kv2);
            Scale scale = new Scale(1, 1, 1, 0, 0, 0);
            box.getTransforms().add(scale);
            KeyValue kv1 = new KeyValue(scale.xProperty(), 1);
            KeyValue kv3 = new KeyValue(scale.zProperty(), 1);
            KeyFrame kf1 = new KeyFrame(Duration.millis(0), kv1, kv3);
            KeyValue kv2 = new KeyValue(scale.xProperty(), 2);
            KeyValue kv4 = new KeyValue(scale.zProperty(), 3);
            KeyFrame kf2 = new KeyFrame(Duration.millis(1000), kv2, kv4);
            Timeline timeline = new Timeline(kf1, kf2);
            timeline.setDelay(Duration.seconds(1));
            timeline.play();

            Scene scene = new Scene(root, 900, 900, true, SceneAntialiasing.BALANCED);
            PerspectiveCamera camera = new PerspectiveCamera();
            camera.setNearClip(0);
            camera.setFarClip(100);
            scene.setCamera(camera);

            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试三维动画");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
