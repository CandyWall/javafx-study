package top.jacktgq.custom_controls.dialog;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
    private Label label;

    @Override
    public void start(Stage primaryStage) {
        try {
            StackPane root = new StackPane();

            HBox buttonPane = new HBox();
            Button openCandyDialogBtn = new Button("打开输入对话框");
            openCandyDialogBtn.setOnAction(e -> {
                SimpleInputDialog inputDialog = new SimpleInputDialog(primaryStage);
                if (inputDialog.exec()) {
                    label.setText("昵称：" + inputDialog.getResult());
                }
            });

            Button openPrettyDialogBtn = new Button("打开美化后的输入对话框");
            openPrettyDialogBtn.setOnAction(e -> {
                SimplePrettyInputDialog inputDialog = new SimplePrettyInputDialog(primaryStage);
                if (inputDialog.exec()) {
                    label.setText("昵称：" + inputDialog.getResult());
                }
            });

            label = new Label("昵称：");

            buttonPane.getChildren().addAll(openCandyDialogBtn, openPrettyDialogBtn);
            buttonPane.setAlignment(Pos.TOP_CENTER);

            root.getChildren().addAll(buttonPane, label);
            StackPane.setAlignment(openPrettyDialogBtn, Pos.TOP_CENTER);
            StackPane.setAlignment(label, Pos.CENTER);

            Scene scene = new Scene(root, 400, 400);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("获取输入对话框中的返回值");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
