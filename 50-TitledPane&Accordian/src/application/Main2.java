package application;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.NodeOrientation;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main2 extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            VBox buttonPane1 = new VBox();
            buttonPane1.getStyleClass().add("button-pane");
            Button btn1 = new Button("查询用户信息");
            Button btn2 = new Button("修改密码");
            Button btn3 = new Button("修改用户权限");
            buttonPane1.getChildren().addAll(btn1, btn2, btn3);

            TitledPane titledPane1 = new TitledPane("用户管理", buttonPane1);

            VBox buttonPane2 = new VBox();
            buttonPane2.getStyleClass().add("button-pane");
            Button btn4 = new Button("查询商品信息");
            Button btn5 = new Button("下架商品管理");
            Button btn6 = new Button("商品回收站");
            buttonPane2.getChildren().addAll(btn4, btn5, btn6);

            TitledPane titledPane2 = new TitledPane();
            titledPane2.setText("商品管理");
            titledPane2.setContent(buttonPane2);

            titledPane2.setExpanded(false); // 设置默认不展开
            titledPane2.setAnimated(false); // 设置不显示展开和关闭的动画
            titledPane2.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);

            Accordion accordion = new Accordion();
            accordion.getPanes().addAll(titledPane1, titledPane2);
            root.setLeft(accordion);

            accordion.expandedPaneProperty().addListener(new ChangeListener<TitledPane>() {
                @Override
                public void changed(ObservableValue<? extends TitledPane> observable, TitledPane oldValue,
                        TitledPane newValue) {
                    if (newValue != null) {
                        System.out.println(newValue.getText() + "展开");
                    } else {
                        System.out.println(oldValue.getText() + "折叠");
                    }
                }
            });

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试手风琴面板");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
