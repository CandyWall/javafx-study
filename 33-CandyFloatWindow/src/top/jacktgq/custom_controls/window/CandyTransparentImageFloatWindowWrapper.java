package top.jacktgq.custom_controls.window;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import top.jacktgq.custom_controls.image.CandyCanvasImagePane;

public class CandyTransparentImageFloatWindowWrapper {
    private Stage stage;
    private CandyCanvasImagePane imagePane;

    /**
     * 
     * @param stage
     *            需要实现透明浮动的窗口
     * @param floatImage
     *            背景图片，请使用png格式的图片
     * @param icon
     *            任务栏图标
     * @param width
     * @param height
     */
    public CandyTransparentImageFloatWindowWrapper(Stage stage, Image floatImage, int width, int height) {
        this.stage = stage;
        imagePane = new CandyCanvasImagePane(floatImage);
        imagePane.setStyle("-fx-background: transparent");
        Scene scene = new Scene(imagePane, width, height);
        scene.setFill(null);
        // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
        stage.setScene(scene);

        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setAlwaysOnTop(true);
        CandyWindowDragSupport.addDragSupport(stage, imagePane, true);
    }

    public void setIcon(Image icon) {
        stage.getIcons().add(icon);
    }

    public CandyCanvasImagePane getImagePane() {
        return imagePane;
    }

    public void show() {
        stage.show();
    }
}
