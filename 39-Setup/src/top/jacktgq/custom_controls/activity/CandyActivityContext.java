package top.jacktgq.custom_controls.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javafx.stage.Stage;

public class CandyActivityContext {
    private List<CandyActivity> stack = new ArrayList<>();
    private Stage owner;
    private Map<String, Object> params = new HashMap<>(); // 上下文，全局参数
    private CandyActivityObserver observer;

    public Stage getOwner() {
        return owner;
    }

    public void setOwner(Stage owner) {
        this.owner = owner;
    }

    // 从Activity中调用 ( 可以在startActivity之后调用 finish)
    public void finish(CandyActivity caller) {
        if (stack.size() == 0)
            return;

        CandyActivity a = null;

        if (caller == null) {
            a = stack.get(0);
        } else {
            Iterator<CandyActivity> iter = stack.iterator();
            while (iter.hasNext()) {
                CandyActivity vv = iter.next();
                if (vv == caller) {
                    iter.remove();
                    a = caller;
                    break;
                }
            }

            // caller 在stack中不存在，是一个异常
            if (a == null)
                return;
        }

        // 停止这个Activity
        if (a.started) {
            a.onStop();
            a.started = false;
        }

        // 回调到管理器
        if (observer != null) {
            observer.onActivityDismiss(a);
        }

        if (a.created) {
            a.onDestroy();
            a.created = false;
        }
    }

    public void back(CandyActivity caller) {
        if (stack.size() <= 1)
            return; // 只有1个Activity，无法返回

        finish(caller);

        // 显示顶上的Activity
        CandyActivity a = stack.get(0);

        // 回调到管理器
        if (observer != null) {
            observer.onActivityShow(a);
        }

        if (!a.started) {
            a.onStart();
            a.started = true;
        }
    }

    public void startActivity(CandyActivity caller, Class<? extends CandyActivity> clazz, Object intent) {
        // 隐藏当前的Activity
        if (caller != null && caller.started) {
            caller.onStop();
            caller.started = false;

            if (observer != null) {
                observer.onActivityDismiss(caller);
            }
        }

        // 启动新的Activity
        try {
            CandyActivity a = clazz.newInstance();
            a.context = this;

            // onCreate
            if (!a.created) {
                a.intent = intent;
                a.onCreate(intent);
                a.created = true;
                if (a.getContentView() == null) {
                    throw new Exception("Activity 没有设置 ContentView, " + clazz.getName());
                }
            }

            // 加到stack , 放到最前面
            stack.add(0, a);

            // 回调到管理器
            if (observer != null) {
                observer.onActivityShow(a);
            }

            // onStart
            if (!a.started) {
                a.intent = intent;
                a.onStart();
                a.created = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /////////////////////////////////////

    public CandyActivityObserver getObserver() {
        return observer;
    }

    public void setObserver(CandyActivityObserver observer) {
        this.observer = observer;
    }

    ////////////// 获取参数 //////////////
    public Object getParam(String key, Object defValue) {
        Object v = params.get(key);
        if (v == null)
            return defValue;
        return v;
    }

    public void putParam(String key, Object value) {
        params.put(key, value);
    }

    public long getParamLong(String key, long defValue) {
        Long v = (Long) params.get(key);
        if (v == null)
            return defValue;
        return v;
    }

    public int getParamInt(String key, int defValue) {
        Integer v = (Integer) params.get(key);
        if (v == null)
            return defValue;
        return v;
    }

    public short getParamShort(String key, short defValue) {
        Short v = (Short) params.get(key);
        if (v == null)
            return defValue;
        return v;
    }

    public boolean getParamBoolean(String key, boolean defValue) {
        Boolean v = (Boolean) params.get(key);
        if (v == null)
            return defValue;
        return v;
    }

    public String getParamString(String key, String defValue) {
        String v = (String) params.get(key);
        if (v == null)
            return defValue;
        return v;
    }
}
