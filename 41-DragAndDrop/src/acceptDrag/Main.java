package acceptDrag;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.image.CandyImagePane;

public class Main extends Application {
    private BorderPane root;
    private CandyImagePane imagePane;

    @Override
    public void start(Stage primaryStage) {
        try {
            root = new BorderPane();

            imagePane = new CandyImagePane();
            root.setCenter(imagePane);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            initDropSupport();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 接受拖放
    private void initDropSupport() {
        // 有东西被拖进来
        root.setOnDragEntered(e -> {

        });

        // 有东西被拖进来，没有松手又出去了
        root.setOnDragExited(e -> {

        });

        // 有东西正在被拖动中
        root.setOnDragOver(e -> {
            Dragboard dragBorad = e.getDragboard();
            if (dragBorad.hasUrl()) {
                // 支持http、https
                String url = dragBorad.getUrl();
                System.out.println("url:" + url);
                if (url.startsWith("http://") || url.startsWith("https://")) {
                    // 支持 *.jpg *.png *.jpeg
                    if (url.endsWith("jpg") || url.endsWith("png") || url.endsWith("jpeg")) {
                        e.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                    }
                }
            }

            e.consume();
        });

        root.setOnDragDropped(e -> {
            Dragboard dragboard = e.getDragboard();
            boolean success = false;

            if (dragboard.hasUrl()) {
                String url = dragboard.getUrl();
                imagePane.setImage(new Image(url));
                success = true;
            }

            e.setDropCompleted(success);
            e.consume();
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
