package top.jacktgq; 

import java.util.Date; 

public class Chat_msg { 

	private String accept_user_id ; 
	private Date create_time ; 
	private String id ; 
	private String msg ; 
	private String send_user_id ; 
	private Integer sign_flag ; 


	public void setAccept_user_id(String accept_user_id) {
		this.accept_user_id=accept_user_id;
	}
	public String getAccept_user_id() {
		return this.accept_user_id;
	}
	public void setCreate_time(Date create_time) {
		this.create_time=create_time;
	}
	public Date getCreate_time() {
		return this.create_time;
	}
	public void setId(String id) {
		this.id=id;
	}
	public String getId() {
		return this.id;
	}
	public void setMsg(String msg) {
		this.msg=msg;
	}
	public String getMsg() {
		return this.msg;
	}
	public void setSend_user_id(String send_user_id) {
		this.send_user_id=send_user_id;
	}
	public String getSend_user_id() {
		return this.send_user_id;
	}
	public void setSign_flag(Integer sign_flag) {
		this.sign_flag=sign_flag;
	}
	public Integer getSign_flag() {
		return this.sign_flag;
	}

}
 