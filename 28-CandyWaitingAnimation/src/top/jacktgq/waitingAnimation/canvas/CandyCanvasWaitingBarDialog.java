package top.jacktgq.waitingAnimation.canvas;

import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

/**
 * 
 * @Title: CandyCanvasWaitingBarDialog.java
 * @Package top.jacktgq.waitingAnimation.canvas
 * @Description: 基于Canvas绘制的等待特效-圆圈状 等待对话框
 * @author CandyWall
 * @date 2021年5月19日 上午1:09:57
 * @version V1.0
 */
public class CandyCanvasWaitingBarDialog extends Stage {
    private CandyCanvasWaitingBar candyCanvasWaitingBar;
    private Scene scene;

    public CandyCanvasWaitingBarDialog(Window owner) {
        candyCanvasWaitingBar = new CandyCanvasWaitingBar();
        // candyCanvasWaitingEffect.setStyle("-fx-background: transparent");

        scene = new Scene(candyCanvasWaitingBar, 180, 25);
        // scene.setFill(null);
        setScene(scene);
        initOwner(owner);
        initModality(Modality.WINDOW_MODAL);
        initStyle(StageStyle.TRANSPARENT);
        centerInParent(owner);
    }

    // 相对于父窗口中心显示
    public void centerInParent(Window owner) {
        if (owner != null) {
            // 父窗口的位置px, py, 大小 pw, ph
            double px = owner.getX(), py = owner.getY();
            double pw = owner.getWidth(), ph = owner.getHeight();
            double dx = (pw - scene.getWidth()) / 2;
            double dy = (ph - scene.getHeight()) / 2;

            this.setX(px + dx);
            this.setY(py + dy);
        }
    }
}
