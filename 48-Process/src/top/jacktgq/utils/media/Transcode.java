package top.jacktgq.utils.media;

import java.io.File;
import java.util.Map;

import top.jacktgq.utils.process.CandyProcess;

public class Transcode {
    String ffmpgPath;

    private String srcPath;
    private String dstPath;

    public Transcode(String ffmpgPath, File srcFile, File dstFile) throws Exception {
        this.ffmpgPath = ffmpgPath;
        this.srcPath = srcFile.getCanonicalPath();
        this.dstPath = dstFile.getCanonicalPath();

        if (!new File(ffmpgPath).exists())
            throw new Exception("ffmpeg.exe 目录路径不正确!");

        // ffmpeg不支持中文路径名
        // checkChinese(srcPath);
        // checkChinese(dstPath);

        if (!srcFile.exists())
            throw new Exception("源文件不存在: " + srcFile);

        File f = new File(dstPath);
        if (!f.getParentFile().exists())
            throw new Exception("不能创建目标文件的目录!");
    }

    // 从视频中生成缩略图
    public void thumbOfVideo(int outputWidth) throws Exception {
        int startPoint = 1; // 1秒位置

        String cmdfmt = ffmpgPath + " -y -ss %d -v 0 -i \"%s\"  -f image2  -vframes 1  -vf scale=%d:-1  \"%s\"";

        String cmdline = String.format(cmdfmt, startPoint, srcPath, outputWidth, dstPath);

        System.out.println("为视频生成缩略图: " + cmdline);

        Map<String, Object> resultMap = CandyProcess.execute(cmdline);
        int code = (int) resultMap.get("code");
        if (code != 0)
            throw new Exception("转码出错");
    }

    // 为大图片生成缩略图
    public void thumbOfImage(int outputWidth) throws Exception {
        String cmdfmt = ffmpgPath + " -i \"%s\" -y -v 0 -vf scale=%d:-1 \"%s\"";

        String cmdline = String.format(cmdfmt, srcPath, outputWidth, dstPath);

        System.out.println("为图片生成缩略图: " + cmdline);

        Map<String, Object> resultMap = CandyProcess.execute(cmdline);
        int code = (int) resultMap.get("code");
        if (code != 0)
            throw new Exception("转码出错");
    }

    // 视频转码
    public void video2video(int outputWidth, int outputBitrate) throws Exception {
        int w = outputWidth;
        int h = 0;
        int bitrate = outputBitrate;

        // 按比例确定输出宽度
        MediaInfo info = new MediaInfo();
        info.parse(new File(srcPath));
        h = w * info.height / info.width;

        // 把宽和高做成4对齐
        if (w % 4 != 0)
            w -= (w % 4);
        if (h % 4 != 0)
            h -= (h % 4);

        // 设置最小宽度和最小高度
        if (h < 160)
            h = 160;
        if (w < 160)
            w = 160;
        if (bitrate < info.bitrate)
            bitrate = info.bitrate;
        if (bitrate < 10000)
            bitrate = 10000;

        // 注意: 必须使用 -v 0 参数，避免ffmpeg输出进度信息
        // 如果ffmpeg输出进度信息，则无法终止进程
        String cmdfmt = ffmpgPath
                + " -i \"%s\" -y -v 0 -vcodec libx264 -b:v %d -s %dx%d -acodec libvo_aacenc -ac 2 -ar 48000 -ab 128000  \"%s\"";

        String cmdline = String.format(cmdfmt, srcPath, bitrate, w, h, dstPath);

        System.out.println("视频转码 : " + cmdline);

        Map<String, Object> resultMap = CandyProcess.execute(cmdline);
        int code = (int) resultMap.get("code");
        throw new Exception("转码出错");

    }

    // 视频抓帧
    // ffmpeg.exe -y -ss 353 -v 0 -i "d:\media\main.mp4" -f image2 -vframes 1
    // "d:\media\snapshot.jpg"
    // -y 覆盖
    // -ss 61 超始时间 (秒) -ss必须放在前面
    // -f image2 格式
    // -vframes 1 只取1帧
    public void snapshotOfVideo(int startPoint) throws Exception {

        String cmdfmt = ffmpgPath + " -y -ss %d -v 0 -i \"%s\" -f image2  -vframes 1 \"%s\"";
        String cmdline = String.format(cmdfmt, startPoint / 1000, srcPath, dstPath);

        System.out.println("视频抓帧: " + cmdline);

        Map<String, Object> resultMap = CandyProcess.execute(cmdline);
        int code = (int) resultMap.get("code");
        if (code != 0)
            throw new Exception("转码出错");
    }

    // 视频片段剪辑
    // ffmpeg.exe -i c:\big.mp4 -y -ss 10 -t 20 -vcodec copy -acodec copy
    // c:\big-clip.mp4
    // 不要使用 -vcodec copy -acodec copy 时，开头会有黑场
    // 必须加上 -v 0 ，不然控制台输出会卡住
    public void clipOfVideo(int startPoint, int endPoint) throws Exception {
        int start = startPoint / 1000;
        int len = (endPoint - startPoint) / 1000 + 1;

        String cmdfmt = ffmpgPath + " -i \"%s\" -y -v 0 -ss %d -t %d  \"%s\"";
        String cmdline = String.format(cmdfmt, srcPath, start, len, dstPath);

        System.out.println("视频截取片段: " + cmdline);

        Map<String, Object> resultMap = CandyProcess.execute(cmdline);
        int code = (int) resultMap.get("code");
        if (code != 0)
            throw new Exception("转码出错");
    }

    public static void checkChinese(String path) throws Exception {
        byte[] b = path.getBytes("UTF-8");
        if (b.length > path.length())
            throw new Exception("ffmpeg要求路径中不能有中文!" + path);
    }

}
