package top.jacktgq.setup;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import top.jacktgq.custom_controls.activity.CandyActivity;

public class SetupResultActivity extends CandyActivity {
    @Override
    public void onCreate(Object intent) {
        HBox box = new HBox();
        setContentView(box);

        boolean hasError = (boolean) intent;
        box.getChildren().addAll(new ImageView("top/jacktgq/setup/ic_" + (hasError ? "cry" : "smile") + ".png"),
                new Label(hasError ? "安装失败" : "安装成功！"));
        box.setAlignment(Pos.CENTER);
        box.setSpacing(10);
    }
}
