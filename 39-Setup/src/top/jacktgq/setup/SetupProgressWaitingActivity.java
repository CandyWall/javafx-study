package top.jacktgq.setup;

import java.io.File;
import java.util.zip.ZipEntry;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import top.jacktgq.custom_controls.activity.CandyActivity;
import top.jacktgq.custom_controls.progress.CandyProgressBar;
import top.jacktgq.util.CandyUnzipUtil;
import top.jacktgq.util.CandyUnzipUtil.ProgressChangeListener;

/**
 * 
 * @Title: SetupProgressWaitingActivity.java
 * @Package top.jacktgq.setup
 * @Description: 安装并显示进度
 * @author CandyWall
 * @date 2021年5月23日 下午2:36:02
 * @version V1.0
 */
public class SetupProgressWaitingActivity extends CandyActivity {
    private boolean hasError;

    @Override
    public void onCreate(Object intent) {
        VBox box = new VBox();
        setContentView(box);
        Label label = new Label("正在安装...");
        label.setMaxWidth(400);
        CandyProgressBar progressBar = new CandyProgressBar();
        progressBar.setPrefSize(400, 30);
        progressBar.setMaxWidth(400);
        box.getChildren().addAll(label, progressBar);
        box.setSpacing(10);
        box.setAlignment(Pos.CENTER);
        File srcFile = new File("testData/1.zip");
        File targetDir = new File((String) intent);
        new Thread(() -> {
            hasError = false;
            CandyUnzipUtil.extract(srcFile, targetDir, new ProgressChangeListener() {
                private long lastTime = 0;
                private String content;

                @Override
                public void handle(ZipEntry entry, int done, int error, int total) {
                    // 为了增强演示效果， 故意让解压缩慢一点
                    try {
                        Thread.sleep(40);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    double percent = done * 1.0 / total;
                    content = done + " / " + total;
                    if (error > 0) {
                        hasError = false;
                    }
                    if (done + error == total) {
                        // content = "解压成功：" + done + " 个" + "，解压错误：" + error +
                        // " 个" + "，总共：" + total + " 个";
                        Platform.runLater(() -> {
                            progressBar.updateProgress(percent, content);
                        });
                        return;
                    }
                    // 不需要太频繁更新进度条，这里选择每隔0.2秒更新一次
                    long now = System.currentTimeMillis();
                    long pass = now - lastTime;
                    if (pass > 100) {
                        lastTime = now;

                        Platform.runLater(() -> {
                            progressBar.updateProgress(percent, content);
                        });
                    }

                }
            });

            Platform.runLater(() -> {
                // 解压完成后，跳转到解压成功页面
                startActivity(SetupResultActivity.class, hasError);
                finish();
            });
        }).start();
    }
}
