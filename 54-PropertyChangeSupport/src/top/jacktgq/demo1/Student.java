package top.jacktgq.demo1;

import java.beans.PropertyChangeSupport;

public class Student {
    private String id;
    private String name;
    private Integer age;

    public PropertyChangeSupport psc = new PropertyChangeSupport(this);

    public Student(String id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldValue = this.name;
        this.name = name;
        psc.firePropertyChange("nameChanged", oldValue, this.name);
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        Integer oldValue = this.age;
        this.age = age;
        psc.firePropertyChange("ageChanged", oldValue, this.age);
    }

    @Override
    public String toString() {
        return "Student [id=" + id + ", name=" + name + ", age=" + age + "]";
    }
}
