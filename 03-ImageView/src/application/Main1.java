package application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * @Title: Main1.java
 * @Package application
 * @Description: 显示一张网络图片
 * @author CandyWall
 * @date 2021年5月12日 下午5:51:37
 * @version V1.0
 */
public class Main1 extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            // 写法1：
            // ImageView imageView = new
            // ImageView("https://img-blog.csdnimg.cn/img_convert/616df7027ae6d8215f9dd89437a40c56.gif");
            // 写法2：
            Image image = new Image("https://img-blog.csdnimg.cn/img_convert/616df7027ae6d8215f9dd89437a40c56.gif");
            ImageView imageView = new ImageView();
            imageView.setImage(image);
            imageView.setPreserveRatio(true);

            // 获取图片的宽高
            double width = image.getWidth();
            double height = image.getHeight();
            root.setCenter(imageView);
            Scene scene = new Scene(root, width, height);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            scene.getAccelerators().put(KeyCombination.valueOf("Ctrl+m"), new Runnable() {
                @Override
                public void run() {
                    // 按下ctrl+m 全屏显示
                    primaryStage.setFullScreen(true);
                }
            });
            primaryStage.setScene(scene);
            primaryStage.setTitle("显示一张网络图片");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
