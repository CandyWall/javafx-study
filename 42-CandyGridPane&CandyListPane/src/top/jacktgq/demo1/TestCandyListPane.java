package top.jacktgq.demo1;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.scrollPane.CandyListPane;

public class TestCandyListPane extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            CandyListPane root = new CandyListPane();
            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("自制列表");
            primaryStage.show();

            EventHandler<MouseEvent> onClickListener = new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    // 取出事件源对象
                    Node node = (Node) event.getSource();
                    Integer data = (Integer) node.getUserData();
                    System.out.println("第" + data + "项被点击");
                }
            };

            for (int i = 0; i < 20; i++) {
                Label label = new Label();
                label.setText("测试" + i);
                label.getStyleClass().add("item");
                label.setUserData(i + 1);
                label.setOnMouseClicked(onClickListener);
                root.add(label);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
