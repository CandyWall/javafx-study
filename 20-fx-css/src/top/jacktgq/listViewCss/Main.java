package top.jacktgq.listViewCss;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.util.Callback;

public class Main extends Application {
    // 创建ListView，指定数据项类型
    private ListView<Student> listView = new ListView<>();

    // 数据源
    ObservableList<Student> listData = FXCollections.observableArrayList();

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            // 添加操作按钮和文本框
            HBox topPane = new HBox();
            TextField textField = new TextField();
            textField.setPromptText("请输入要增加、删除或者修改的记录");
            HBox.setHgrow(textField, Priority.ALWAYS);
            Button addBtn = new Button("增加");
            Button deleteBtn = new Button("删除");
            Button modifyBtn = new Button("修改");
            topPane.getChildren().addAll(textField, addBtn, deleteBtn, modifyBtn);
            topPane.setPadding(new Insets(10));
            topPane.setSpacing(10);
            root.setTop(topPane);

            addBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    new AddWindow(primaryStage, listView, listData);
                }
            });
            deleteBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    int selectedIndex = listView.getSelectionModel().getSelectedIndex();
                    if (selectedIndex >= 0) {
                        listData.remove(selectedIndex); // 删除数据会通知
                    }
                }
            });

            modifyBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    int selectedIndex = listView.getSelectionModel().getSelectedIndex();
                    if (selectedIndex >= 0) {
                        new ModifyWindow(primaryStage, listView, listData, selectedIndex);
                    }
                }
            });

            // 准备数据
            listData.add(new Student(111, "空条承太郎", true));
            listData.add(new Student(222, "花京院典明", true));
            listData.add(new Student(333, "空条徐伦", false));
            listData.add(new Student(444, "波鲁那雷夫", true));

            // 样式设置
            listView.getStyleClass().add("student-list-view");

            // 设置列表项
            listView.setItems(listData);

            // 设置单元格生成器（工厂）
            listView.setCellFactory(new Callback<ListView<Student>, ListCell<Student>>() {
                @Override
                public ListCell<Student> call(ListView<Student> param) {
                    return new MyListCell();
                }
            });

            root.setCenter(listView);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets()
                    .add(getClass().getResource("/top/jacktgq/listViewCss/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    // 负责listView的ListCell
    class MyListCell extends ListCell<Student> {
        @Override
        protected void updateItem(Student item, boolean empty) {
            // fx框架奥球必须先调用super.updateItem()
            super.updateItem(item, empty);

            // 自己的代码
            if (item != null) {
                this.setText(item.id + "\t" + item.name + "\t" + (item.sex ? "男" : "女"));
            } else {
                this.setText("");
            }
        }
    }

}
