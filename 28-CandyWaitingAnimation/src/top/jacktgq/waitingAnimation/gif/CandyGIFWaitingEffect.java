package top.jacktgq.waitingAnimation.gif;

import javafx.scene.image.Image;
import top.jacktgq.custom_controls.image.CandyImagePane;

/**
 * 
 * @Title: CandyCanvasLoadingBar.java
 * @Package animationTimer.waitingbar
 * @Description: 基于GIF图的等待特效 -长条状 -圆圈状
 * @author CandyWall
 * @date 2021年5月18日 下午10:11:50
 * @version V1.0
 */
public class CandyGIFWaitingEffect extends CandyImagePane {
    public static final Image WAITINGBAR_RED_YELLOW = new Image(
            "top/jacktgq/waitingAnimation/gif/bar/waitingbar-red-yellow.gif");
    public static final Image WAITINGBAR_GREEN_WHITE = new Image(
            "top/jacktgq/waitingAnimation/gif/bar/waitingbar-green-white.gif");
    public static final Image WAITINGBAR_LIKEWIN7 = new Image(
            "top/jacktgq/waitingAnimation/gif/bar/waitingbar-likewin7.gif");
    public static final Image WAITINGCIRCLE_SMALL_BLACK_WHITE = new Image(
            "top/jacktgq/waitingAnimation/gif/circle/waitingcircle-small-black-white.gif");
    public static final Image WAITINGCIRCLE_BIG_GRAY_WHITE = new Image(
            "top/jacktgq/waitingAnimation/gif/circle/waitingcircle-big-gray-white.gif");
    public static final Image WAITINGCIRCLE_MEDIUM_BLACK_GRAY = new Image(
            "top/jacktgq/waitingAnimation/gif/circle/waitingcircle-medium-black-gray.gif");
    public static final Image WAITINGCIRCLE_NOT_TRANSPARENT = new Image(
            "top/jacktgq/waitingAnimation/gif/circle/waitingcircle-not-transparent.gif");
    public static final Image WAITINGCIRCLE_SMALL_ORANGE = new Image(
            "top/jacktgq/waitingAnimation/gif/circle/waitingcircle-small-orange.gif");

    public CandyGIFWaitingEffect() {
        this(WAITINGBAR_RED_YELLOW);
    }

    public CandyGIFWaitingEffect(Image image, int scaleType) {
        super(image, scaleType);
    }

    public CandyGIFWaitingEffect(Image image) {
        super(image);
    }
}
