package top.jacktgq.translate;

import javafx.application.Application;
import javafx.geometry.Bounds;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = new AnchorPane();

            AnchorPane an = new AnchorPane();
            an.setPrefSize(200, 200);
            an.setStyle("-fx-background-color: pink");

            Button btn1 = new Button("提交");
            btn1.setPrefSize(80, 40);
            btn1.setLayoutX(50);
            btn1.setLayoutY(50);
            // 如果用setLayoutX()和setLayoutY()设置位置超过父结点的区域，父结点的区域会扩大
            btn1.setLayoutX(300);
            btn1.setLayoutY(300);
            an.getChildren().add(btn1);

            Button btn2 = new Button("重置");
            btn2.setPrefSize(80, 40);
            btn2.setLayoutX(50);
            btn2.setLayoutY(50);

            // 同一个translate可以多次添加，并且效果会叠加，而setLayoutX()和setLayoutY()多次设置只会覆盖
            // translate可以让结点移出父节点的区域
            Translate translate1 = new Translate(100, 100);
            // Translate translate2 = new Translate(100, 100);
            btn2.getTransforms().addAll(translate1, translate1);

            an.getChildren().add(btn2);
            root.getChildren().add(an);
            AnchorPane.setLeftAnchor(an, 100.0);
            AnchorPane.setTopAnchor(an, 100.0);

            Scene scene = new Scene(root, 600, 600);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            // 获取当前结点在父节点中的坐标位置
            System.out.println("(" + btn2.getLayoutX() + ", " + btn2.getLayoutY() + ")");
            // 获取当前结点在父节点中的经过变换后的真实位置
            System.out.println("(" + btn2.getLocalToParentTransform().getTx() + ", "
                    + btn2.getLocalToParentTransform().getTy() + ")");
            // 获取当前结点在父节点中的经过变换后的真实位置
            System.out.println("(" + btn2.getLocalToSceneTransform().getTx() + ", "
                    + btn2.getLocalToSceneTransform().getTy() + ")");

            Bounds bd = btn2.getLayoutBounds();
            System.out.println(bd);
            System.out.println(btn2.localToParent(bd));
            System.out.println(btn2.localToScreen(bd));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
