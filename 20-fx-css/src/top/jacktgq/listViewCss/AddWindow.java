package top.jacktgq.listViewCss;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class AddWindow extends Stage {
    public TextField idField;
    public TextField nameField;
    private RadioButton maleRadio;

    public AddWindow(Stage owner, ListView<Student> listView, ObservableList<Student> listData) {
        VBox vbox = new VBox();
        if (true) {
            HBox row = new HBox();

            Label label = new Label("学号：");
            idField = new TextField();
            idField.setPromptText("请输入学号");

            row.getChildren().addAll(label, idField);
            vbox.getChildren().add(row);
        }
        if (true) {
            HBox row = new HBox();

            Label label = new Label("姓名：");
            nameField = new TextField();
            nameField.setPromptText("请输入姓名");

            row.getChildren().addAll(label, nameField);
            vbox.getChildren().add(row);
        }
        if (true) {
            HBox row = new HBox();

            Label label = new Label("性别：");
            ToggleGroup tg = new ToggleGroup();

            maleRadio = new RadioButton("男");
            RadioButton femaleRadio = new RadioButton("女");
            maleRadio.setSelected(true);

            tg.getToggles().addAll(maleRadio, femaleRadio);

            Button confirmAddBtn = new Button("增加");
            confirmAddBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    // 获取学号
                    String id = idField.getText();
                    // 获取姓名
                    String name = nameField.getText();
                    if (id.equals("") || name.equals("")) {
                        System.out.println("学号和姓名不能为空！");
                    }
                    // 获取性别
                    boolean sex = false;
                    if (maleRadio.isSelected()) {
                        sex = true;
                    }

                    // 使用add方法添加记录会通知ListView更新显示
                    listData.add(new Student(Integer.parseInt(id), name, sex));
                    close();
                }
            });

            row.getChildren().addAll(label, maleRadio, femaleRadio, confirmAddBtn);
            HBox.setMargin(femaleRadio, new Insets(0, 0, 0, 10));
            HBox.setMargin(confirmAddBtn, new Insets(0, 10, 0, 50));

            vbox.getChildren().add(row);
        }

        vbox.setPadding(new Insets(10));
        vbox.setSpacing(10);
        Scene scene = new Scene(vbox);
        setScene(scene);
        setTitle("新增学生");
        setResizable(false);
        // 设置成对话框样式
        initStyle(StageStyle.UTILITY);
        initOwner(owner);
        // 设置对话框属性，该对话框弹出后不能操作其他界面
        initModality(Modality.WINDOW_MODAL);

        show();
    }
}