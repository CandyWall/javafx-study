package top.jacktgq.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Dbutil {
    private static String baseJdbcUrl = "jdbc:mysql://";
    private static final String JDBCURLPARAMS = "information_schema?useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai&useSSL=false&allowPublicKeyRetrieval=true";
    private static final String JDBCDRIVERCLASS = "com.mysql.cj.jdbc.Driver";
    private static String USERNAME;
    private static String PASSWORD;
    static {
        try {
            Class.forName(JDBCDRIVERCLASS);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void init(String host, String port, String username, String password) {
        baseJdbcUrl += host + ":" + port + "/";
        USERNAME = username;
        PASSWORD = password;
    }

    // 根据数据库名称获取数据库连接
    public static Connection getConn(String databaseName) {
        String jdbcUrl = baseJdbcUrl + databaseName + JDBCURLPARAMS;
        try {
            return DriverManager.getConnection(jdbcUrl, USERNAME, PASSWORD);
        } catch (SQLException e) {
            throw new RuntimeException("获取数据库连接失败！", e);
        }
    }
}
