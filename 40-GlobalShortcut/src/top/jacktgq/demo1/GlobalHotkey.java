package top.jacktgq.demo1;

import com.melloware.jintellitype.HotkeyListener;
import com.melloware.jintellitype.JIntellitype;
import com.melloware.jintellitype.JIntellitypeConstants;
import com.sun.glass.events.KeyEvent;

public class GlobalHotkey {
    public static final GlobalHotkey i = new GlobalHotkey();

    public void init() throws Exception {
        JIntellitype.getInstance();

        // OPTIONAL: check to see if an instance of this application is already
        // running, use the name of the window title of this JFrame for checking
        if (JIntellitype.checkInstanceAlreadyRunning("MyApp")) {
            throw new Exception("An instance of this application is already running");
        }

        // 1号热键 CTRL + S
        JIntellitype.getInstance().registerHotKey(1, JIntellitypeConstants.MOD_CONTROL, 'S');

        // 2号热键 ALT + SHIFT + B
        JIntellitype.getInstance().registerHotKey(2, JIntellitypeConstants.MOD_ALT + JIntellitypeConstants.MOD_SHIFT,
                'B');

        // 3号热键 F8
        JIntellitype.getInstance().registerSwingHotKey(3, 0, KeyEvent.VK_F8);

        // assign this class to be a HotKeyListener
        // JIntellitype.getInstance().addHotKeyListener( new HotkeyListener()
        // {
        // @Override
        // public void onHotKey(int id)
        // {
        // System.out.println("hotkey pressed, ID:" + id );
        //
        // }
        // });
    }

    public void addHotKeyListener(HotkeyListener listener) {
        JIntellitype.getInstance().addHotKeyListener(listener);
    }

    public void cleanUp() {
        // JIntellitype.getInstance().unregisterHotKey(1);
        // JIntellitype.getInstance().unregisterHotKey(2);
        JIntellitype.getInstance().cleanUp();
    }

}
