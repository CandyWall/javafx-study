package top.jacktgq;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            TextFlow flow1 = new TextFlow();
            Text text1 = new Text("日照香炉生紫烟");
            text1.setStyle("-fx-fill: white");
            Text text2 = new Text("要看瀑布挂前川\n");
            Text text3 = new Text("飞流直下三千尺");

            flow1.setStyle("-fx-background-color: #ffcc00");
            flow1.getChildren().addAll(text1, text2, text3);
            flow1.setPadding(new Insets(10));
            flow1.setTextAlignment(TextAlignment.CENTER);
            flow1.setLineSpacing(10);

            TextFlow flow2 = new TextFlow();
            Text text4 = new Text("久闻公之大名，今日有幸相会！公既只天命，识时务，为何要兴无名之师犯我疆界？");
            flow2.getChildren().add(text4);
            AnchorPane.setTopAnchor(flow2, 100.0);
            AnchorPane.setLeftAnchor(flow2, 100.0);

            AnchorPane root = new AnchorPane(flow1, flow2);

            Scene scene = new Scene(root, 600, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            primaryStage.widthProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    System.out.println("AnchorPane宽度：" + root.getWidth());
                    System.out.println("flow1宽度：" + flow1.getWidth());
                    System.out.println("flow2宽度：" + flow2.getWidth());
                }
            });

            root.widthProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    flow1.setPrefWidth(newValue.doubleValue());
                    flow2.setPrefWidth(newValue.doubleValue() - 100);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
