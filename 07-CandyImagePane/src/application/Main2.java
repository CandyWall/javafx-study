package application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

public class Main2 extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox thumbPane = new HBox();
            BorderStroke borderStroke = new BorderStroke(Paint.valueOf("red"), BorderStrokeStyle.SOLID,
                    new CornerRadii(20), new BorderWidths(10));
            BorderPane borderPane = new BorderPane();

            borderPane.setPrefWidth(200);
            borderPane.setPrefHeight(100);
            ImageView imageView = new ImageView("images/game.png");
            borderPane.setCenter(imageView);
            borderPane.setBorder(new Border(borderStroke));

            CandyImagePane pane = new CandyImagePane(new Image("images/game.png"), CandyImagePane.FIT_XY);
            pane.setPrefWidth(498);
            pane.setPrefHeight(100);
            pane.setBorder(new Border(borderStroke));
            thumbPane.getChildren().addAll(borderPane, pane);

            Scene scene = new Scene(thumbPane, 1200, 1000);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            System.out.println("BorderPane:" + borderPane.getWidth() + "===" + borderPane.getHeight());
            System.out.println("CandyImagePane:" + pane.getWidth() + "===" + pane.getHeight());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
