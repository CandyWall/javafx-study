package top.jacktgq.utils;

import java.io.*;
import java.nio.channels.FileChannel;
import java.time.LocalDate;

/**
 * @Author CandyWall
 * @Date 2021/5/8--23:01
 * @Description 文件工具类
 */
public class CandyFileUtil {
    // 文件夹不存在就重新创建
    public static void createDirIfNotExist(File dir) {
        if (!dir.exists()) {
            dir.mkdir();
        }
    }

    // 向文件中追加内容
    public static void appendContentToFile(File file, String content) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
            writer.write(content + "\r\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 文件复制
    public static void transferTo(File srcFile, File targetFile) {
        try (
                FileChannel from = new FileInputStream(srcFile).getChannel();
                FileChannel to = new FileOutputStream(targetFile).getChannel();
        ) {
            // 复制文件效率高，底层会利用操作系统的零拷贝进行优化，不过每次传输会限制2G文件大小
            long size = from.size();
            // left变量代表还有多少字节没有传输
            for (long left = size; left > 0; ) {
                left -= from.transferTo(size - left, left, to);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
