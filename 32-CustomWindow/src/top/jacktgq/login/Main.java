package top.jacktgq.login;

import javafx.application.Application;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.window.CandyPrettyWindowWrapper;

public class Main extends Application {

    CandyPrettyWindowWrapper cpww;

    @Override
    public void start(Stage primaryStage) {

        try {
            LoginPane root = new LoginPane();

            cpww = new CandyPrettyWindowWrapper(primaryStage, false, true);
            cpww.setContentView(root, 800, 600);
            cpww.setTitle("用户登录");
            root.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            root.setId("candy-root");
            cpww.show();

            initLayout();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 登录界面布局
    private void initLayout() {

    }

    public static void main(String[] args) {
        launch(args);
    }
}
