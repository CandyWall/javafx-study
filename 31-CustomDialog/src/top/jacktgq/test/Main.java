package top.jacktgq.test;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            HBox root = new HBox();

            Button openNormalStageBtn = new Button("打开普通窗口");
            openNormalStageBtn.setOnAction(e -> {
                openNormalStage(primaryStage);
            });
            Button openModalStageBtn = new Button("打开模态窗口");
            openModalStageBtn.setOnAction(e -> {
                openModalStage(primaryStage);
            });

            TextField textField = new TextField();

            root.getChildren().addAll(openNormalStageBtn, openModalStageBtn, textField);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("普通窗口和模态窗口的对比");
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 打开普通窗口
    private void openNormalStage(Stage owner) {
        Label label = new Label("普通窗口");
        label.setAlignment(Pos.CENTER);
        Scene scene = new Scene(label, 200, 200);
        Stage stage = new Stage();
        stage.setScene(scene);

        stage.setTitle("普通窗口");
        stage.show();
        System.out.println("空条承太郎");
    }

    // 打开模态窗口
    private void openModalStage(Stage owner) {
        Label label = new Label("模态窗口");
        label.setAlignment(Pos.CENTER);
        Scene scene = new Scene(label, 200, 200);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.initOwner(owner);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setTitle("普通窗口");
        stage.showAndWait();
        System.out.println("花京院典明");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
