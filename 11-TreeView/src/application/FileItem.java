package application;

import java.io.File;

/**
 * 
 * @Title: ItemData.java
 * @Package application
 * @Description: TreeView的数据项
 * @author CandyWall
 * @date 2021年5月14日 下午3:20:31
 * @version V1.0
 */
public class FileItem {
    public File file; // 文件名或目录名
    boolean isDir = true; // 是否是目录

    public FileItem() {
    }

    public FileItem(File file, boolean isDir) {
        this.file = file;
        this.isDir = isDir;
    }
}
