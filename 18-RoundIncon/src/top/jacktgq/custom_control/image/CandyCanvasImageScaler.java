package top.jacktgq.custom_control.image;

import javafx.scene.shape.Rectangle;

/**
 * 
 * @Title: CandyImageScaler.java
 * @Package top.jacktgq.drawImage
 * @Description: 图片缩放工具
 * @author CandyWall
 * @date 2021年5月15日 下午10:57:58
 * @version V1.0
 */
public class CandyCanvasImageScaler {
    /**
     * x、y方向拉伸占满
     * 
     * @return rect:图片绘制的矩形区域
     */
    public static Rectangle fitXY(double rectW, double rectH) {

        return new Rectangle(0, 0, rectW, rectH);
    }

    /**
     * 居中显示，并保持长宽比
     * 
     * @return rect:图片绘制的矩形区域
     */
    public static Rectangle fitCenter(double imgW, double imgH, double rectW, double rectH) {
        // 将图片的宽度缩放至矩形区域的宽度
        double fitW = rectW;
        // 由fitH / fitW = imgH / imgW 得,fitH = fitW * imgH / imgW
        // 故此时高度等比缩放后的结果为
        double fitH = fitW * imgH / imgW;
        // 图片缩放后水平方向和竖直方向的间距
        double horizontalGap = 0;
        double verticalGap = (rectH - fitH) / 2;
        // 如果此时缩放导致图片高度比矩形高度大，那么该缩放不能满足要求
        // 则需要将图片的高度缩放至矩形区域的高度
        if (fitH > rectH) {
            fitH = rectH;
            fitW = fitH * imgW / imgH;
            horizontalGap = (rectW - fitW) / 2;
            verticalGap = 0;
        }
        return new Rectangle(horizontalGap, verticalGap, fitW, fitH);
    }

    /**
     * 如果图片较小，则居中显示，如果图片越出范围，则缩放显示，并保持长宽比
     * 
     * @return rect:图片绘制的矩形区域
     */
    public static Rectangle fitCenterInside(double imgW, double imgH, double rectW, double rectH) {
        if (imgW > rectW || imgH > rectH) {
            return fitCenter(imgW, imgH, rectW, rectH);
        }
        double fitW = imgW;
        double fitH = imgH;
        double horizontalGap = (rectW - fitW) / 2;
        double verticalGap = (rectH - fitH) / 2;

        return new Rectangle(horizontalGap, verticalGap, fitW, fitH);
    }
}
