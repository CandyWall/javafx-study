package top.jacktgq.demo5;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.window.CandyPrettyWindow;

public class TestCandyPrettyWindow extends Application {
    Stage primaryStage;
    CandyPrettyWindow myWindow;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        try {
            BorderPane root = new BorderPane();
            Scene scene = new Scene(root, 400, 400);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            Button button = new Button("打开带标题栏的自定义窗口");
            root.setTop(button);

            // 点按钮时创建新窗口
            button.setOnAction((e) -> {
                openNewWindow();
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openNewWindow() {
        HBox root = new HBox();
        TextField textField = new TextField();
        Button button = new Button("确定");
        root.setAlignment(Pos.CENTER);
        root.getChildren().addAll(textField, button);
        myWindow = new CandyPrettyWindow(primaryStage, false, true);
        myWindow.setContentView(root, 300, 150);
        myWindow.setWindowTitle("自定义窗口");
        myWindow.setBackground(Color.RED);
        myWindow.show();
        myWindow.centerInParent();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
