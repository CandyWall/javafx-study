package top.jacktgq.activity;

import org.apache.commons.dbutils.QueryRunner;

import com.zaxxer.hikari.HikariDataSource;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import top.jacktgq.custom_controls.activity.CandyActivity;
import top.jacktgq.custom_controls.toaster.CandyToaster;
import top.jacktgq.utils.Dbutil;
import top.jacktgq.utils.HikariUtils;

public class ConnectActivity extends CandyActivity {
    private TextField hostField;
    private TextField portField;
    private TextField usernameField;
    private PasswordField passwordField;

    @Override
    public void onCreate(Object arg0) {
        VBox root = new VBox();
        this.setContentView(root);

        if (true) {
            HBox row = new HBox();
            row.setAlignment(Pos.CENTER);
            row.setSpacing(10);
            Label label = new Label("主机");
            label.setAlignment(Pos.CENTER_RIGHT);
            label.setMaxWidth(60);
            label.setPrefWidth(60);
            hostField = new TextField("localhost");
            row.getChildren().addAll(label, hostField);
            HBox.setHgrow(hostField, Priority.ALWAYS);
            root.getChildren().add(row);
        }
        if (true) {
            HBox row = new HBox();
            row.setAlignment(Pos.CENTER);
            row.setSpacing(10);
            Label label = new Label("端口");
            label.setAlignment(Pos.CENTER_RIGHT);
            label.setMaxWidth(60);
            label.setPrefWidth(60);
            portField = new TextField(3306 + "");
            row.getChildren().addAll(label, portField);
            HBox.setHgrow(portField, Priority.ALWAYS);
            root.getChildren().add(row);
        }
        if (true) {
            HBox row = new HBox();
            row.setAlignment(Pos.CENTER);
            row.setSpacing(10);
            Label label = new Label("用户名");
            label.setAlignment(Pos.CENTER_RIGHT);
            label.setMaxWidth(60);
            label.setPrefWidth(60);
            usernameField = new TextField("root");
            row.getChildren().addAll(label, usernameField);
            HBox.setHgrow(usernameField, Priority.ALWAYS);
            root.getChildren().add(row);
        }
        if (true) {
            HBox row = new HBox();
            row.setAlignment(Pos.CENTER);
            row.setSpacing(10);
            Label label = new Label("密码");
            label.setAlignment(Pos.CENTER_RIGHT);
            label.setMaxWidth(60);
            label.setPrefWidth(60);
            passwordField = new PasswordField();
            passwordField.setText("TGQ@candywall123");
            row.getChildren().addAll(label, passwordField);
            HBox.setHgrow(passwordField, Priority.ALWAYS);
            root.getChildren().add(row);
        }
        if (true) {
            HBox row = new HBox();
            row.setAlignment(Pos.CENTER);
            row.setSpacing(10);
            Button connectBtn = new Button("连接");
            connectBtn.setOnAction(e -> {
                String host = hostField.getText().trim();
                String port = portField.getText().trim();
                String username = usernameField.getText().trim();
                String password = passwordField.getText().trim();
                if (host.length() > 0 && port.length() > 0 && username.length() > 0 && password.length() > 0) {
                    Dbutil.init(host, port, username, password);
                    try {
                        HikariDataSource datasource = HikariUtils.init(host, port, username, password);
                        if (datasource != null) {
                            context.putParam("datasource", datasource);
                            context.putParam("queryRunner", new QueryRunner(datasource));
                            startActivity(DataBaseActivity.class, null);
                        } else {
                            CandyToaster.show(context.getOwner(), "连接数据失败！请检查填写的信息后重试。", CandyToaster.ERROR);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        CandyToaster.show(context.getOwner(), "连接数据失败！请检查填写的信息后重试。", CandyToaster.ERROR);
                    }

                }
            });
            Button resetBtn = new Button("重置");
            resetBtn.setOnAction(e -> {
                hostField.setText("");
                portField.setText("");
                usernameField.setText("");
                passwordField.setText("");
            });
            row.getChildren().addAll(connectBtn, resetBtn);
            HBox.setHgrow(passwordField, Priority.ALWAYS);
            root.getChildren().add(row);
        }
        root.setAlignment(Pos.CENTER);
        root.setSpacing(20);
        root.setPadding(new Insets(20, 150, 20, 150));
    }
}
