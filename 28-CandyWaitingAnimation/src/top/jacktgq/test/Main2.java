package top.jacktgq.test;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import top.jacktgq.custom_controls.thread.CandyShortTask;
import top.jacktgq.waitingAnimation.canvas.CandyCanvasWaitingBarDialog;
import top.jacktgq.waitingAnimation.canvas.CandyCanvasWaitingCircleDialog;

public class Main2 extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            FlowPane root = new FlowPane();
            root.setAlignment(Pos.CENTER);

            Button openCanvasWaitingBarBtn = new Button("打开基于Canvas绘制的长条状等待对话框");
            openCanvasWaitingBarBtn.setOnAction(new EventHandler<ActionEvent>() {
                private CandyCanvasWaitingBarDialog waitingCircleDialog;

                @Override
                public void handle(ActionEvent event) {
                    new CandyShortTask() {
                        @Override
                        protected void doInBackground() throws Exception {
                            // 在这里进行耗时任务
                            Thread.sleep(3000);
                        }

                        @Override
                        protected void done() {
                            if (waitingCircleDialog != null) {
                                // 任务执行完以后关闭等待对话框
                                waitingCircleDialog.close();
                            }
                        }
                    }.execute();

                    waitingCircleDialog = new CandyCanvasWaitingBarDialog(primaryStage);

                    waitingCircleDialog.showAndWait();
                }
            });

            Button openCanvasWaitingCircleBtn = new Button("打开基于Canvas绘制的圆圈状等待对话框");
            openCanvasWaitingCircleBtn.setOnAction(new EventHandler<ActionEvent>() {
                private CandyCanvasWaitingCircleDialog waitingCircleDialog;

                @Override
                public void handle(ActionEvent event) {
                    new CandyShortTask() {
                        @Override
                        protected void doInBackground() throws Exception {
                            // 在这里进行耗时任务
                            Thread.sleep(3000);
                        }

                        @Override
                        protected void done() {
                            if (waitingCircleDialog != null) { // 任务执行完以后关闭等待对话框
                                waitingCircleDialog.close();
                            }
                        }
                    }.execute();

                    waitingCircleDialog = new CandyCanvasWaitingCircleDialog(primaryStage);

                    waitingCircleDialog.showAndWait();
                }
            });

            root.getChildren().addAll(openCanvasWaitingBarBtn, openCanvasWaitingCircleBtn);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("测试Canvas绘制的等待对话框");
            primaryStage.show();
        } catch (

        Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
