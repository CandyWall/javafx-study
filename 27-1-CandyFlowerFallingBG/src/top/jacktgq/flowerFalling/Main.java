package top.jacktgq.flowerFalling;

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import top.jacktgq.custom_controls.window.CandyWindowDragSupport;
import top.jacktgq.utils.LoadConfigUtils;

public class Main extends Application {

    private boolean run = true;
    private MediaPlayer player;
    private static final ImageView play_img = new ImageView("icons/play.png");
    private static final ImageView pause_img = new ImageView("icons/pause.png");
    private String gitee_url = "https://gitee.com/CandyWall/javafx-study/tree/master/27-1-CandyFlowerFallingBG";
    private String csdn_url = "https://blog.csdn.net/qq_38505969?type=blog";

    private ContextMenu contextMenu;
    private MenuItem menuPlayOrPause;
    HostServices host = getHostServices();

    @Override
    public void start(Stage primaryStage) {
        // 加载配置
        String gitee_Url2 = LoadConfigUtils.getGitee_Url();
        String csdn_Url2 = LoadConfigUtils.getCSDN_Url();
        if (gitee_Url2 != null && gitee_Url2.length() > 0)
            gitee_url = gitee_Url2;
        if (csdn_Url2 != null && csdn_Url2.length() > 0)
            csdn_url = csdn_Url2;

        try {
            FlowerFallingPane root = new FlowerFallingPane(800, 1000, 40);
            Scene scene = new Scene(root, 800, 1000);
            scene.setFill(null);
            // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("落花特效");
            primaryStage.getIcons().add(new Image("images/flower.png"));
            primaryStage.initStyle(StageStyle.TRANSPARENT);
            primaryStage.show();

            // 初始化右键菜单
            initContextMenu();

            // 给root面板焦点，否则键盘事件无效
            root.requestFocus();
            root.setOnKeyPressed(keyEvent -> {
                // 按下空格键
                if (keyEvent.getCode() == KeyCode.SPACE) {
                    // 暂停或者播放背景音乐
                    pauseOrPlayBGMusic();
                }
            });

            root.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    root.requestFocus();
                }
            });

            root.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (event.getButton() == MouseButton.SECONDARY) {
                        if (run) {
                            menuPlayOrPause.setGraphic(pause_img);
                            menuPlayOrPause.setText("音乐暂停（按空格）");
                        } else {
                            menuPlayOrPause.setGraphic(play_img);
                            menuPlayOrPause.setText("音乐播放（按空格）");
                        }
                        contextMenu.show(primaryStage, event.getScreenX(), event.getScreenY());
                    }
                }
            });

            CandyWindowDragSupport.addDragSupport(primaryStage, root, false);
            Media media = new Media(getClass().getResource("/mp3/1.mp3").toExternalForm());
            player = new MediaPlayer(media);
            player.setCycleCount(MediaPlayer.INDEFINITE);
            player.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 初始化右键菜单
    private void initContextMenu() {
        menuPlayOrPause = new MenuItem("音乐暂停（按空格）", pause_img);
        menuPlayOrPause.setOnAction(e -> {
            // 暂停或者播放背景音乐
            Main.this.pauseOrPlayBGMusic();
        });
        MenuItem menuOpenGitee = new MenuItem("Gitee地址", new ImageView("icons/gitee.png"));
        menuOpenGitee.setOnAction(e -> {
            host.showDocument(gitee_url);// 使用默认浏览器打开一个url
        });
        MenuItem menuOpenCSDN = new MenuItem("CSDN博客地址", new ImageView("icons/csdn.png"));
        menuOpenCSDN.setOnAction(e -> {
            host.showDocument(csdn_url);// 使用默认浏览器打开一个url
        });
        MenuItem menuExit = new MenuItem("退出", new ImageView("icons/exit.png"));
        menuExit.setOnAction(e -> {
            System.exit(0);
        });
        contextMenu = new ContextMenu(menuPlayOrPause, menuOpenGitee, menuOpenCSDN, menuExit);
    }

    // 暂停或者播放背景音乐
    private void pauseOrPlayBGMusic() {
        if (run)
            player.pause();
        else
            player.play();
        run = !run;
    }

    public static void main(String[] args) {
        launch(args);
    }

}
