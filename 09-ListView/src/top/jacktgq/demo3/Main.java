package top.jacktgq.demo3;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {
    // 创建StudentListView，指定数据项类型
    private StudentListView listView = new StudentListView();

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();
            HBox topPane = new HBox();
            Button btn = new Button("获取选中项");
            btn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    for (Student stu : listView.getListData()) {
                        if (stu.checked) {
                            System.out.println(stu);
                        }
                    }
                }
            });
            topPane.getChildren().add(btn);
            root.setTop(topPane);

            // 准备数据
            listView.add(new Student(111, "空条承太郎", true));
            listView.add(new Student(222, "花京院典明", true));
            listView.add(new Student(333, "空条徐伦", false));
            listView.add(new Student(444, "波鲁那雷夫", true));

            root.setCenter(listView);

            Scene scene = new Scene(root, 400, 400);
            scene.getStylesheets().add(getClass().getResource("/top/jacktgq/demo1/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    // 获取点击的是哪一项
                    int selectedIndex = listView.getSelectionModel().getSelectedIndex();
                    Student student = listView.getListData().get(selectedIndex);
                    if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 1) {
                        System.out.println("左键单击 " + student);
                    } else if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                        System.out.println("左键双击 " + student);
                    } else if (event.getButton() == MouseButton.MIDDLE && event.getClickCount() == 1) {
                        System.out.println("鼠标中键单击 " + student);
                    } else if (event.getButton() == MouseButton.MIDDLE && event.getClickCount() == 2) {
                        System.out.println("鼠标中键双击 " + student);
                    } else if (event.getButton() == MouseButton.SECONDARY && event.getClickCount() == 1) {
                        System.out.println("右键单击 " + student);
                    } else if (event.getButton() == MouseButton.SECONDARY && event.getClickCount() == 2) {
                        System.out.println("右键双击");
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    // 负责listView的ListCell
    class MyListCell extends ListCell<Student> {
        @Override
        protected void updateItem(Student item, boolean empty) {
            // fx框架奥球必须先调用super.updateItem()
            super.updateItem(item, empty);

            // 自己的代码
            if (item != null) {
                this.setText(item.id + "\t" + item.name + "\t" + (item.sex ? "男" : "女"));
            } else {
                this.setText("");
            }
        }
    }

}
